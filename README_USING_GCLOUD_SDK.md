Google Cloud Platform:

OS: Linux (Debian)

Package Manager: apt

Check for packages: apt-get update

AppEngine: PHP 7.1.0

The PHP 7.1 runtime is available on the App Engine flexible environment, and is currently in beta.

https://cloudplatform.googleblog.com/2017/03/digging-deep-on-php-71-for-google-app.html

# Create a new project on Google Cloud AppEngine, e.g. 'verkeerslicht'

Recommended reading: https://devopscloud.net/2014/11/30/a-real-quick-quick-start-with-google-cloud-platform-command-line-tools/

In Visual Studio Code, inside a folder where you want to keep the project's code, type in the Integrated Terminal:

gcloud components update

Next, authenticate yourself:

gcloud auth login

NOTE: you can switch between projects where [PROJECT_ID] is the id of the project you want to switch to, using: gcloud config set project [PROJECT_ID], e.g. gcloud config set project verkeerslicht-165607

Follow by typing:

gcloud init

If the default configuration is not suitable, you will be prompted for a new configuration:

Pick configuration to use:

[1] Re-initialize this configuration [default]
[2] Create a new configuration

Please enter your numeric choice:

For now, type: 2 (followed by ENTER)

Enter configuration name: Names start with a lower case letter and contain only lower case letters a-z, digits 0-9, and hyphens '-':

Type, e.g.: sumedia-config-verkeerslicht-165607

Your current configuration has been set to: [sumedia-config-verkeerslicht-165607]

You can skip diagnostics next time by using the following flag:

gcloud init --skip-diagnostics

Network diagnostic detecs and fixes local network connection issues.
Checking network connection...done.
Reachability Check passed.
Network diagnostic (1/1 checks) passed.

Choose the account you would like to use to perform operations for this configuration:
[1] willem.vanheemstrasystems@gmail.com
[2] Log in with a new account
Please enter your numeric choice: 

Type: 2 (followed by ENTER)

Your browser has been opened to visit:

https://accounts.google.com/o/oauth2/auth?redirect_uri=..........

In the browser, log in with the account chosen for this project (e.g. willem@sumedia.nl).

When prompted, enter your email and password.

Allow Google Cloud SDK to view and manage your applications deployed on Google App Engine, view and manage your Google Compute Engine resources, and view and manage your data across Google Cloud Platform services, by clicking the button 'Allow'.

You will be prompted as follows:

You are logged in as: [willem@sumedia.nl].

Pick cloud project to use:
[1] hello-world-with-nodejs-159123
...
[5] verkeerslicht-165607
...
Please enter numeric choice or text value (must exactly match list item):

Type: verkeerslicht-165607 (followed by ENTER)

You will be prompted as follows:

Your current project has been set to: [verkeerslicht-165607].

Not setting default zone/region (this feature makes it easier to use
[gcloud compute] by setting an appropriate default value for the
--zone and --region flag).

See https://cloud.google.com/compute/docs/gcloud-compute section on how to set
default compute region and zone manually. 
If you would like [gcloud init] to be 
able to do this for you the next time you run it, make sure the
Compute Engine API is enabled for your project on the 
https://console.developers.google.com/apis page.

Your Google Cloud SDK is configured and ready to use!

* Commands that require authentication will use willem@sumedia.nl by default

* Commands will reference project `verkeerslicht-165607` by default

Run `gcloud help config` to learn how to change individual settings

This gcloud configuration is called [sumedia-config-verkeerslicht-165607]. 

You can create additional configurations if you work with multiple acc
ounts and/or projects.

Run `gcloud topic configurations` to learn more.

Some things to try next:

* Run `gcloud --help` to see the Cloud Platform services you can interact with. 
  And run `gcloud help COMMAND` to get help on any
 gcloud command.

* Run `gcloud topic -h` to learn about advanced features of the SDK like arg files and output formatting

Now inside the project 'verkeerslicht-googlecloudplatform' directory create the following files and folders in Visual Studio Code, if not already exist:

app.yaml

The content of 'app.yaml' should contain:

runtime: php
env: flex

runtime_config:
  document_root: webroot

beta_settings:
  cloud_sql_instances: "verkeerslicht-165607:europe-west1:verkeerslicht"

where verkeerslicht-165607:europe-west1:verkeerslicht is the database instance connection name. You can find this database instance connection name in the SQL section of Google Cloud Platform, with the instance named 'verkeerslicht'.  

NOTE: Connecting to a Cloud SQL instance requires the use of a Unix socket and the appropriate extension available to the runtime.  mysql and mysqli are enabled by default in the PHP runtime for App Engine.  An example of connecting using mysqli_connect can be found here (http://stackoverflow.com/questions/24554143/connection-issue-connecting-from-php-app-engine-instance-to-a-cloud-sql-instance).  The key element here is the use of the socket argument in the mysqli_connect() call.

Also make sure that in the root directory of the project is a file called 'php.ini', which contains:

extension = "mongodb.so"
extension = "imagick.so"
extension = "intl.so"

NOTE: Use "mongodb.so", not the deprecated "mongo.so".

Next, run the following command in Visual Studio Code Intergrated Terminal:

php -v

It should tell you which version of PHP it is using, e.g. PHP 5.6.28.

If the version is not PHP 7.1.0, download and install the latest version of xammp, if that is your local server, from https://www.apachefriends.org/download.html for PHP 7.1.* support

(See also https://github.com/Microsoft/vscode/issues/13356)

If you require PHP 7.1.0, on Windows search for "environment"

- click on edit environment variables (for your acount)
- click on the "path" variable and press "Edit..."
- click on "new"
- write down the path to your php 7 files, for example: D:\php7

(in your case it should be: C:\OpenServer\modules\php\PHP-7-x64 )

When starting Visual Studio Code the error should be gone.

Check by typing again (perhaps in a new terminal window): php -v

NOTE: Make sure you have 'Billing Enabled' for this project (i.e. verkeerslicht-165607) before deploying.
You can check this at the 'Billing' sidebar menu option at the Google Cloud Platform Web Console.

Now deploy the app by typing:

gcloud app deploy

You will be prompted as follows:

You are creating an app for project [php-flex-166211].
WARNING: Creating an App Engine application for a project is irreversible and the region cannot be changed. 
More information about regions is at https://cloud.google.com/appengine/docs/locations.

Please choose the region where you want your App Engine application located:

[1] us-east1    (supports standard and flexible)
[2] europe-west (supports standard and flexible)
[3] us-central  (supports standard and flexible)
[4] asia-nordeast1 (supports standard and flexible)
[5] cancel
Please enter your numeric choice:

Type for europe-west: 2 (followed by ENTER)

You will be prompted with the following:

Creating App Engine application in project [verkeerslicht-165607] and region [europe-west]....done.
You are about to deploy the following services:
 - verkeerslicht-165607/default/20170430t150924 (from [C:\Users\user\git\sumedia\verkeerslicht\app.yaml])
     Deploying to URL: [https://verkeerslicht-165607.appspot.com]

Do you want to continue (Y/n)?

Confirm by typing: Y (followed by ENTER)

You will be prompted by:

If this is your first deployment, this may take a while... 
...
DONE
-----------------------------------------------------------------------------------
Updating service [default]...done.

Deployed service [default] to [https://verkeerslicht-165607.appspot.com]

You can stream logs from the command line by running:
  
gcloud app logs tail -s default

To view your application in the web browser run:
  
gcloud app browse

Type: gcloud app browse

You will be prompted with:

Opening [https://verkeerslicht-165607.appspot.com] in a new tab in your default browser.

Open the browser (if not done so automatically) and browse for above URL.

You should see the front page of the application.

# Using MongoDB with PHP

See https://cloud.google.com/php/getting-started/deploy-mongodb

If you plan to use an unmanaged MongoDB instance running on Compute Engine, check out Bitnami's preconfigured click-to-deploy solution. We are not choosing 'mLab' managed MongoDB enviroenment as they charge more than Bitnami does.

At Bitnami (https://console.cloud.google.com/launcher/config/bitnami-launchpad/mongodb) create a new MongoDB deployment:

Operating System: Debian (8)

Software: 

- MongoDB (3.4.3)
- OpenSSL (1.0.2k)

Deployment name: mongodb-1

Zone: europe-west1-b

Machine type: micro (0.6 GB memory)

Boot Disk Type: Standard Persistent Disk

Disk size in GB: 10

Networking name: default

Subnetwork name: default

Firewall: Checked Allow TCP 27017 traffic

External IP: Ephemeral

Check to Agree to Terms and Conditions

Click the button 'Implement'.

## Bitname MongoDB Deployment Manager

Browse to https://console.cloud.google.com/deployments/details/mongodb-1

Make a note of the configuration details, such as user accounts and passwords for next steps.

On the Bitnami MongoDB Deployment Manager page, click the button 'SSH':

A terminal window will open and you will be prompted as follows:

Connected, host fingerprint: ssh-rsa 2048 CD:72:FE:5F:26:E9:68:61:C1:17:A0:D3:A5:2C:39:27:98:17:32:5E
The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.
Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
       ___ _ _                   _
      | _ |_) |_ _ _  __ _ _ __ (_)
      | _ \ |  _| ' \/ _` | '  \| |
      |___/_|\__|_|_|\__,_|_|_|_|_|
  *** Welcome to the Bitnami MongoDB 3.4.3-0 ***
  *** Service accessible using hostname 35.189.219.104 port 27017 ***
  *** Documentation:  https://docs.bitnami.com/google/infrastructure/mongodb/ ***
  ***                 https://docs.bitnami.com/google/ ***
  *** Bitnami Forums: https://community.bitnami.com/ ***
willem@mongodb-1-vm:~$

Make a note of the hostname (here: 35.189.219.104) as you need to type that in the file 'config/settings.yml' in the project directory of this project 'verkeerslicht-165607' as follows:

Here is the instance of mongodb-1 when started:

https://console.cloud.google.com/compute/instancesDetail/zones/europe-west1-b/instances/mongodb-1-vm?project=verkeerslicht-165607&organizationId=71636021515&graph=GCE_CPU&duration=PT1H

...
# configure Mongo backend
mongo_url: "mongodb://root:XW422xRu@35.189.219.104:27017"
mongo_database: "devices"
mongo_collection: "reports"
...

NOTE: port 27017 is the default port for MongoDB and mongodb-1-vm is an instance of mongodb-1 which contains a database called 'devices', with a collection called 'reports'.

MAKE SURE TO SET THE ABOVE VALUES ALSO IN THE MySQL DATABASE, IN THE TABLE settings_configurations
IF NO CONNECTION WITH THE DATABASE CAN BE MADE, IT WILL USE THE VALUES FROM THE DATABASE TABLE INSTEAD OF THESE

## Install and enable MongoDB for PHP locally

See http://php.net/manual/en/mongodb.installation.php

Get the MongoDB driver for PHP from http://pecl.php.net/package/mongodb

NOTE: On Windows you can download the DLL directly from above URL. 
Make sure to get the MongoDB driver for PHP 7.1.0, e.g. 7.1 Thread Safe (TS) x86

On Windows, if using XAMMP, place the file 'php_mongodb.dll' into the folder 'C:/xammp/php/ext/'.

Add the following line to your php.ini file:

extension=php_mongodb.dll

Now that the extension is installed, you need to add the MongoDB library to composer as well:

composer require "mongodb/mongodb":"^1.1.0"

Alternatively, add the following line to composer.json:

  "require": {
    ...
    "mongodb/mongodb": "^1.1.0",
    ...
  }


## php.ini

See also https://cloud.google.com/appengine/docs/standard/php/config/php_ini

You can include a php.ini file with your App Engine application. This file lets you customize the behavior of the PHP interpreter directives.

Make sure the file in the root directory of this project 'verkeerslicht-165607', called 'php.ini', has at least the following entries:

extension = "mongodb.so"
extension = "intl.so"

NOTE: Use "mongodb.so", not the deprecated "mongo.so".

## Configuring settings

Make sure the following is contained in the 'composer.lock' file:

NOTE: Don't let yourself be fooled that the version of mongodb/mongodb states '1.0.0'. 
It will still rightfully install the required '1.1.0' version of the ext-mongodb.

...
"packages": [
        ...
        {
            "name": "mongodb/mongodb",
            "version": "1.0.0",
            "source": {
                "type": "git",
                "url": "https://github.com/mongodb/mongo-php-library.git",
                "reference": "851a560864281cbf91fe182aefed85bfc3395031"
            },
            "dist": {
                "type": "zip",
                "url": "https://api.github.com/repos/mongodb/mongo-php-library/zipball/851a560864281cbf91fe182aefed85bfc3395031",
                "reference": "851a560864281cbf91fe182aefed85bfc3395031",
                "shasum": ""
            },
            "require": {
                "ext-mongodb": "^1.1.0",
                "php": ">=5.4"
            },
            "type": "library",
            "extra": {
                "branch-alias": {
                    "dev-master": "1.0.x-dev"
                }
            },
            "autoload": {
                "psr-4": {
                    "MongoDB\\": "src/"
                },
                "files": [
                    "src/functions.php"
                ]
            },
            "notification-url": "https://packagist.org/downloads/",
            "license": [
                "Apache-2.0"
            ],
            "authors": [
                {
                    "name": "Jeremy Mikola",
                    "email": "jmikola@gmail.com"
                },
                {
                    "name": "Hannes Magnusson",
                    "email": "bjori@mongodb.com"
                },
                {
                    "name": "Derick Rethans",
                    "email": "github@derickrethans.nl"
                }
            ],
            "description": "MongoDB driver library",
            "homepage": "https://jira.mongodb.org/browse/PHPLIB",
            "keywords": [
                "database",
                "driver",
                "mongodb",
                "persistence"
            ],
            "time": "2016-01-21T19:43:25+00:00"
        }
        ...
}
...        

Go to the verkeerslicht-165607 directory, and copy the settings.yml.dist file:

cp config/settings.yml.dist config/settings.yml

Open config/settings.yml for editing.

This link explains how the google_client_id and google_client-secret can be set.

https://auth0.com/docs/connections/social/google

For example:

# Google credentials and configuration
google_client_id:      1087266958918-e6ps33q6ermd5a1t1d8gfg3l2eph4arm.apps.googleusercontent.com
google_client_secret:  01oNyIZIVySbP43FfBtuzUjZ
google_project_id:     verkeerslicht-165607

Replace YOUR_PROJECT_ID with your project ID.

Set the value of verkeerslicht_backend to mongodb.

verkeerslicht_backend: mongodb

Set the values of mongo_url, mongo_database, and mongo_collection to the appropriate values for your MongoDB instance. For example (taking the IP address from the steps before):

mongo_url: "mongodb://root:XW422xRu@35.189.219.104:27017"
mongo_database: "devices"
mongo_collection: "reports"

Save and close settings.yml.

NOTE: port 27017 is the default port for MongoDB and mongodb-1-vm is an instance of mongodb-1 which contains a database called 'devices', with a collection called 'reports'.

Enter the same information in config/bootstrap.php in the root of the project repository of Verkeerslicht, like so

// Global Vars
// MAKE SURE TO SET THESE VALUES ALSO IN THE MySQL DATABASE, IN THE TABLE settings_configurations
// IF NO CONNECTION WITH THE DATABASE CAN BE MADE, IT WILL USE THE VALUES FROM THE DATABASE TABLE INSTEAD OF THESE
Setting::register( 'App.Mongo.DB.Server', 'mongodb://root:XW422xRu@35.189.219.104:27017' );
Setting::register( 'App.Mongo.Database', 'devices' );
Setting::register( 'App.Mongo.Collection', 'reports' );

These global vars will be used in the code e.g. at src/Controller/ReportsController.php at the 'add' function.

## Testing MongoDB with Robomongo

Download and install the free client software Robomongo.

With the credentials detailed before (user: root, password: - see saved credentials - , database: devices), create a new connection.

Connection:

- Name: GC_Verkeerslicht (can be anything you like)

- Address: 35.189.219.104

- Port: 27017

Authentication:

Check Perform Authentication

- Database: admin (as this is the database we authenticate against)

- User Name: root

- Password: XW422xRu

- Auth Mechanism: SCRAM-SHA-1

SSH:

- Optional, for now leave empty

SSL:

- Optional, for now leave empty

Advanced:

- Optional, for now leave empty

It should give you access to the MongoDB on Google Cloud Platform.

## Installing dependencies

In the verkeerslicht-165607 directory, enter this command.

composer install

You will be prompted with a long list of installation results, 
and finally the line 
"Generating autoload files"

All required files will be in the 'vendor' directory (including mongodb) as well as the file 'autoload.php'.

## Running the app on your local machine

Start a local web server:

php -S localhost:8001 -t webroot

In your web browser, enter this address.

http://localhost:8001

Now you can browse the app's web page.

## Deploying the app to the App Engine flexible environment

NOTE: 

1) From within the root '/' of the repository, first run 'npm install' before deploying.

2) From within the 'webroot/' of the repository, first run 'npm install' before deploying.

3) From within the 'webroot/' directory, first run 'gulp' before deploying.

4) From within the 'src/app/' directory, first run 'ng build --prod' before deploying. If the 'ng' command is not recognized, install angular-cli first (see https://cli.angular.io for instructions).

Deploy the app:

gcloud app deploy

In your web browser, enter this address. Replace [YOUR_PROJECT_ID] with your project ID (i.e. verkeerslicht-165607):

https://[YOUR_PROJECT_ID].appspot.com

If you update your app, you can deploy the updated version by entering the same command you used to deploy the app the first time. The new deployment creates a new version of your app and promotes it to the default version. The older versions of your app remain, as do their associated VM instances. Be aware that all of these app versions and VM instances are billable resources.

You can reduce costs by deleting the non-default versions of your app.

## Installing and using MySQL on Google Cloud Platform with Cloud SQL

See for an example,  https://www.benwixen.com/articles/how-to-deploy-a-ghost-blog-to-google-app-engine/

Or http://aymanrb.blogspot.nl/2013/05/cakephp-deployment-on-google-app-engine.html

As well as https://cloud.google.com/sql/docs/mysql/quickstart

NOTE: We need to grant access to the App Engine Flexible environment. Click on the top-left hamburger menu in the Console of Google Cloud Platform and select API Manager. Click Enable API and search for "Google Cloud SQL API". Click on it, and then on the Enable button. You will see a warning that you need credentials to use it, but that's actually not the case with App Engine. 

NOTE: If you get below error message on the default cakephp page:

"SQLSTATE[HY000] [2002] No such file or directory" 

you have to add the following to app.yaml:

beta_settings:
  cloud_sql_instances: "verkeerslicht-165607:europe-west1:verkeerslicht"

where verkeerslicht-165607:europe-west1:verkeerslicht is the database instance connection name. You can find this database instance connection name in the SQL section of Google Cloud Platform, with the instance named 'verkeerslicht'.

NOTE: Connecting to a Cloud SQL instance requires the use of a Unix socket and the appropriate extension available to the runtime.  mysql and mysqli are enabled by default in the PHP runtime for App Engine.  An example of connecting using mysqli_connect can be found here (http://stackoverflow.com/questions/24554143/connection-issue-connecting-from-php-app-engine-instance-to-a-cloud-sql-instance).  The key element here is the use of the socket argument in the mysqli_connect() call.

## Allow log files 

Create a bucket for the log files of the project Verkeerslicht, with the following name:

verkeerslicht-logs

Then in the webroot/index.php add the following code:

// Change the default TMP Folder path to a Cloud Storage Bucket
define('TMP', "gs://verkeerslicht-logs/");

Make sure the application 'verkeerslicht-165607' in the AppEngine has access to the bucket 'verkeerslicht-logs'. See an example of how to do so here, https://www.benwixen.com/articles/how-to-deploy-a-ghost-blog-to-google-app-engine/

## Deploying Angular 2 app on Google Cloud AppEngine

Note: Verkeerslicht uses an Angular 2 app for its 'meldingen' page. It needs to be installed following the guidelines found in below references.

See also
https://jaykhimani.blogspot.nl/2016/10/deploying-angular-2-app-with-angular.html

UPDATE: As long as you have 'build' the Angular 2 application in the Verkeerslicht project BEFORE deploying the project to Google Cloud Platform, you do NOT have to do anything for it to work. It is just a set of files saved in the webroot sub directories and will execute in the browser as javascript. To 'build' the Angular 2 application, go inside of the /src/app directory and run the follwoing command (uses Angular CLI, see https://cli.angular.io):

ng build --prod

## Creating and running a 'cronjob' on Google Cloud Platform AppEngine

In order for the Verkeerlicht application to 'move' the documents in the MongoDB database to the MySQL Cloud SQL database on regular intervals, read this page:

https://cloud.google.com/appengine/docs/standard/php/config/cron

More to follow ...

To deploy a cron.yaml file use the following command:

gcloud app deploy cron.yaml

NOTE: cron.yaml is NOT automatically deployed when running: gcloud app deploy

## Connecting to MongoDB instance from gcloud

Learn about MongoDB at http://www.w3resource.com/mongodb/introduction-mongodb.php

The following gcloud command line can be used to SSH into this instance.

```javascript
gcloud compute --project "verkeerslicht-165607" ssh --zone "europe-west1-b" "mongodb-1-vm"
```

NOTE: The above instruction can be found on 'Get started with MongoDB' dropdown menu on the deployments page on Google Cloud Platform (i.e. https://console.cloud.google.com/deployments/details/mongodb-1?project=verkeerslicht-165607&organizationId=71636021515)

Run the above line in your Google Cloud Shell.

You will be prompted as follows:

```javascript
WARNING: The public SSH key file for gcloud does not exist.
WARNING: The private SSH key file for gcloud does not exist.
WARNING: You do not have an SSH key for gcloud.
WARNING: SSH keygen will be executed to generate a key.
This tool needs to create the directory [/home/willem/.ssh] before 
being able to generate SSH keys.

Do you want to continue (Y/n)?
```

Type: Y (followed by ENTER)

You will be prompted as follows:

```javascript
Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase):
```

Leave the passphrase empty, to avoid being asked for it (even when in an automated process thus blocking the process). 

Hit: ENTER (so, no passphrase)

You will be prompted as follows:

```javascript
Enter same passphrase again:
```

Again hit: ENTER (so, no passphrase)

You will be prompted as follows:

```javascript
Your identification has been saved in /home/willem/.ssh/google_compute_engine.
Your public key has been saved in /home/willem/.ssh/google_compute_engine.pub.
The key fingerprint is:
51:b3:33:90:2e:4e:cb:f9:25:4d:18:6a:ee:c3:20:be willem@cs-6889-devshell-vm-53ddb07b-e9e2-4781-a6ac-2eeb0e6fd200-153
The key's randomart image is:
+---[RSA 2048]----+
|        ..o      |
|        oo o     |
|       o.o+      |
|      = o..o     |
|     * +So       |
|  . . * . o      |
| . . + . o       |
|  .   + .        |
|  E.   .         |
+-----------------+
Updating project ssh metadata.../Updated [https://www.googleapis.com/compute/v1/projects/verkeerslicht-165607].                                           
Updating project ssh metadata...done.                                                                                                                     
Waiting for SSH key to propagate.
Warning: Permanently added 'compute.580693809923121505' (ECDSA) to the list of known hosts.

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
       ___ _ _                   _
      | _ |_) |_ _ _  __ _ _ __ (_)
      | _ \ |  _| ' \/ _` | '  \| |
      |___/_|\__|_|_|\__,_|_|_|_|_|

  *** Welcome to the Bitnami MongoDB 3.4.3-0 ***
  *** Service accessible using hostname 35.189.219.104 port 27017 ***
  *** Documentation:  https://docs.bitnami.com/google/infrastructure/mongodb/ ***
  ***                 https://docs.bitnami.com/google/ ***
  *** Bitnami Forums: https://community.bitnami.com/ ***
```

From the command line (here: willem@mongodb-1-vm:~$) you can conclude that you are pointing at the MongoDB instance named mongodb-1-vm.

Now type: mongo

You will be prompted as follows:

```javascript
MongoDB shell version v3.4.3
connecting to: mongodb:///opt/bitnami/mongodb/tmp/mongodb-27017.sock/
MongoDB server version: 3.4.3
Welcome to the MongoDB shell.
For interactive help, type "help".
For more comprehensive documentation, see
        http://docs.mongodb.org/
Questions? Try the support group
        http://groups.google.com/group/mongodb-user
> 
```

Read the documentation at https://docs.bitnami.com/google/infrastructure/mongodb/

### How To Connect To The MongoDB Database?

NOTE: An excellent client for MongoDB on Mac is MongoHub, which can be downloaded at https://github.com/jeromelebel/MongoHub-Mac

You can connect to the MongoDB database from the same computer where it is installed. In our case, as we connect through Google Cloud Shell Console, we should be able to connect. Run the mongo client authenticating as the 'root' user against the 'admin' database:

mongo admin --username root -p

You will be prompted to enter the root user password. This is the same as the application password.

Enter the password for the user 'root' (i.e. XW422xRu)

You will be prompted as follows:

```javascript
connecting to: mongodb:///opt/bitnami/mongodb/tmp/mongodb-27017.sock/admin
MongoDB server version: 3.4.3
Server has startup warnings: 
2017-05-09T15:27:22.216+0000 I STORAGE  [initandlisten] 
2017-05-09T15:27:22.216+0000 I STORAGE  [initandlisten] ** WARNING: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine
2017-05-09T15:27:22.216+0000 I STORAGE  [initandlisten] **          See http://dochub.mongodb.org/core/prodnotes-filesystem
> 
```

To see which database you are currently using, inside mongod-1-vm instance, type:

db

You will be prompted with:

admin

As this is the database you have authenticated against.

Ask to be shown all available databases, by typing:

show dbs

You will be prompted with:

admin  0.000GB
local  0.000GB

### How To Create A Database (here: devices) For A Custom Application (here: Verkeerslicht)?

If you want to install an application manually, it may require the database to be set up first. Use the commands below to create a database. Replace the DATABASE_NAME placeholder with the name of the database you wish to use (here: devices) and the PASSWORD placeholder with your MongoDB password (here: XW422xRu).

mongo admin --username root --password PASSWORD
MongoDB shell version: 2.4.8
connecting to: 35.189.219.104:27017/admin

See also https://www.tutorialspoint.com/mongodb/mongodb_create_database.htm

MongoDB use DATABASE_NAME is used to create database. The command will create a new database if it doesn't exist, otherwise it will return the existing database.

We want to create a database with name 'devices', thus use the DATABASE statement as follows:

```javascript
use devices
```

You will be prompted as follows:

```javascript
switched to db devices
```

To check your currently selected database, use the command db

>db
devices

If you want to check your databases list, use the command show dbs.

>show dbs
admin     0.000GB
local      0.000GB

NOTE: Your created database (devices) is NOT present in list. To display database, you need to insert at least one document into it.

Insert a document in the database 'devices' now, as follows:

>use devices
>db.reports.insert({"name":"test document"})

You will be prompted with:

```javascript
WriteResult({ "nInserted" : 1 })
```

>show dbs
admin      0.000GB
devices    0.000GB
local      0.000GB


More to follow ...

## Connecting to MongoDB from a terminal locally

See also 'How To Connect To MongoDB From A Different Machine?' at https://docs.bitnami.com/google/infrastructure/mongodb/#how-to-connect-to-the-mongodb-database

Type the following in your terminal locally on your computer (provided you have mongo installed):

```javascript
mongo admin --username root -p --host 35.189.219.104 --port 27017
```

You will be prompted for the password of the user 'root'.

Type: XW422xRu (followed by ENTER)

You will be prompted as follows:

```javascript
connecting to: mongodb://35.189.219.104:27017/admin
MongoDB server version: 3.4.3
Server has startup warnings:
2017-05-09T15:27:22.216+0000 I STORAGE  [initandlisten]
2017-05-09T15:27:22.216+0000 I STORAGE  [initandlisten] ** WARNING: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine
2017-05-09T15:27:22.216+0000 I STORAGE  [initandlisten] **          See http://dochub.mongodb.org/core/prodnotes-filesystem
>
```

You can now execute mongo commands.

To be continued...


## Google Maps API Key

Look here for the current Google Maps API Key, https://console.developers.google.com/apis/credentials/key/217?project=verkeerslicht-165607

This key is used in several places in the application, so best search for '?key=' to find all occurrences.

The browser will show a message if the Google Maps API Key is not valid or not authorized. That indicates that a new key needs to be enabled through the Google Cloud Platform Console. 

## Using Google POSTMAN to GET all reports

You can download the free software called 'Postman' from Google with which you can test the API of Verkeerslicht.

Execute the following GET command in Postman:

GET https://verkeerslicht-165607.appspot.com/api/reports/

You should receive a list of all reports (i.e. 'meldingen').

## Managing Users who can access Google Cloud Platform Console

See https://cloud.google.com/compute/docs/access/add-remove-change-permissions-for-team-members