<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::prefix( 'admin', function ( $routes ) {
	$routes->extensions( [ 'json' ] );
	$routes->connect( '/', [ 'controller' => 'Users', 'action' => 'dashboard' ] );

	$routes->connect( '/reports/getReportsFromDay/*', [ 'controller' => 'Reports', 'action' => 'getReportsFromDay' ] );
	$routes->connect( '/reportresponses/massreport/*', [ 'controller' => 'Reports', 'action' => 'getReportsFromDay' ] );

	$routes->connect( '/report-response-presets/get/*', [ 'controller' => 'ReportResponsePresets', 'action' => 'get' ] );

	$routes->connect( '/chat-messages/edit/*', [ 'controller' => 'ChatMessages', 'action' => 'edit' ] );
	$routes->connect( '/chat-messages/add', [ 'controller' => 'ChatMessages', 'action' => 'add' ] );

	$routes->connect( '/:controller', [ 'action' => 'index' ], [ 'routeClass' => 'InflectedRoute' ] );
	$routes->connect( '/:controller/:action/*', [], [ 'routeClass' => 'InflectedRoute' ] );
	$routes->fallbacks( 'InflectedRoute' );
} );

Router::prefix( 'api', function ( $routes ) {
    $routes->extensions( [ 'json' ] );
    $routes->connect( '/', [ 'controller' => 'Reports', 'action' => 'index' ] );
    $routes->connect( '/:controller', [ 'action' => 'index' ], [ 'routeClass' => 'InflectedRoute' ] );
    $routes->connect( '/:controller/:action/*', [], [ 'routeClass' => 'InflectedRoute' ] );
    $routes->fallbacks( 'InflectedRoute' );
} );

Router::scope('/', function (RouteBuilder $routes) {

    // REMOVE BELOW COMMENTED CODE, IT IS NOT ORIGINAL
    // /**
    //  * Here, we are connecting '/' (base path) to a controller called 'Pages',
    //  * its action called 'display', and we pass a param to select the view file
    //  * to use (in this case, src/Template/Pages/home.ctp)...
    //  */
    // $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']); // FOR TESTING ONLY!!

    // /**
    //  * ...and connect the rest of 'Pages' controller's URLs.
    //  */
    // $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    $routes->connect( '/', [ 'controller' => 'Pages', 'action' => 'display', 'home', 'plugin' => 'Cms' ] );

    // auth
    $routes->connect( '/login', [ 'controller' => 'Reporters', 'action' => 'login' ] );
    $routes->connect( '/logout', [ 'controller' => 'Reporters', 'action' => 'logout' ] );
    $routes->connect( '/forgot_password', [ 'controller' => 'Reporters', 'action' => 'forgotPassword' ] );
    $routes->connect( '/forgot_password/*', [ 'controller' => 'Reporters', 'action' => 'forgotPassword' ] );
    $routes->connect( '/reporters/confirm-registration/*', [ 'controller' => 'Reporters', 'action' => 'confirm_registration' ] );
    $routes->connect( '/registreren', [ 'controller' => 'Reporters', 'action' => 'add' ] );
    $routes->connect( '/account', [ 'controller' => 'Reporters', 'action' => 'account' ] );
    $routes->connect( '/account/change_password', [ 'controller' => 'Reporters', 'action' => 'changePassword' ] );

    // reports
    $routes->connect( '/meldingen', [ 'controller' => 'Reports', 'action' => 'index' ] );
    $routes->connect( '/reports/complete/*', [ 'controller' => 'Reports', 'action' => 'complete' ] );
    $routes->connect( '/reports/thank_you', [ 'controller' => 'Reports', 'action' => 'thankYou' ] );

    // blog
    $routes->connect( '/blog', [ 'controller' => 'BlogArticles', 'action' => 'index' ] );
    $routes->connect( '/blog-articles/view/*', [ 'controller' => 'BlogArticles', 'action' => 'view' ] );

    // faq
    $routes->connect( '/kennisbank', [ 'controller' => 'FaqQuestions', 'action' => 'index' ] );
    $routes->connect( '/faq', [ 'controller' => 'FaqQuestions', 'action' => 'index' ] );
    $routes->connect( '/faq-user-questions/add', [ 'controller' => 'FaqUserQuestions', 'action' => 'add' ] );
    $routes->connect( '/faq-user-questions/', [ 'controller' => 'FaqUserQuestions', 'action' => 'add' ] );

    // contact
	$routes->connect( '/contact', [ 'controller' => 'Contact', 'action' => 'index' ] );
	$routes->connect( '/contact/add', [ 'controller' => 'Contact', 'action' => 'add' ] );

	// cms fallback
    $routes->connect( '/:slug', [ 'controller' => 'Pages', 'action' => 'display', 'plugin' => 'Cms' ], [
        'pass' => [ 'slug' ],
        'slug' => '.*?'
    ] );

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
