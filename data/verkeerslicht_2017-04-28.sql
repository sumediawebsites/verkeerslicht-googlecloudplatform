# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.1.102 (MySQL 5.5.53-MariaDB)
# Database: verkeerslicht
# Generation Time: 2017-04-28 09:03:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table background_tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `background_tasks`;

CREATE TABLE `background_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `scheduled_at` datetime DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `result` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT 'pending',
  `arguments` text,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table blog_articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_articles`;

CREATE TABLE `blog_articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_category_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `body` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `image_url` text,
  `views` int(255) DEFAULT NULL,
  `read_time` varchar(255) DEFAULT NULL,
  `highlighted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  KEY `blog_category_id` (`blog_category_id`),
  CONSTRAINT `blog_articles_ibfk_6` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blog_articles_ibfk_7` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blog_articles_ibfk_8` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

LOCK TABLES `blog_articles` WRITE;
/*!40000 ALTER TABLE `blog_articles` DISABLE KEYS */;

INSERT INTO `blog_articles` (`id`, `blog_category_id`, `title`, `slug`, `body`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`, `image_url`, `views`, `read_time`, `highlighted`)
VALUES
	(1,NULL,'Test entry','TestSlug','Test Body',65,65,'2017-01-17 10:15:07','2017-01-17 10:15:07','2017-01-17 10:15:20',NULL,NULL,NULL,NULL),
	(2,2,'Primeur: A2 wordt de breedste snelweg ter wereld verlicht met led.','De A2 wordt de breedste snelweg ter wereld die wordt verlicht door led. Maar liefst 2 x 6 rijstroken worden vanaf de middenberm verlicht.','<p>De A2 wordt de breedste snelweg ter wereld die wordt verlicht door led. Maar liefst 2 x 6 rijstroken worden vanaf de middenberm verlicht. Rijkswaterstaat is 10 oktober gestart met het vervangen van de reguliere verlichting op de A2 tussen Holendrecht en Maarssen door led.</p>\r\n',65,69,'2017-01-17 11:23:40','2017-04-20 12:31:53',NULL,'/uploads/blogpost_003.png',58,'',1),
	(3,3,'Officiele opening nieuwe aansluiting A44 Royal FloraHolland.','De Nieuwe Botlekbrug kampte tot augustus 2016 met 73 storingen. Dit aantal vinden we te hoog ...','<p>De Nieuwe Botlekbrug kampte tot augustus 2016 met 73 storingen. Dit aantal vinden we te hoog ...</p>\r\n\r\n<p>&nbsp;</p>\r\n',65,NULL,'2017-01-17 11:24:00','2017-04-20 09:23:07',NULL,'/uploads/blogpost_002.png',26,'',0),
	(4,3,'ANWB Alarmcentrale sluit drukke periode af.','Vind jij dat het ergens te lang rood, te kort groen of een onveilige situatie is? Maak dan een melding.','<p>Vind jij dat het ergens te lang rood, te kort groen of een onveilige situatie is? Maak dan een melding.</p>\r\n',69,69,'2017-01-25 09:44:12','2017-04-20 12:26:10',NULL,'uploads/blog_articles/4/4.png',29,'2',0),
	(5,3,'Werkzaamheden uit aan de hoofdrijbaan van de N207','De provincie Zuid-Holland voert van vrijdagavond 3 juni tot en met vrijdag 1 juli werkzaamheden uit aan de hoofdrijbaan van de N207 tussen Boskoop en Alphen aan den Rijn.','<p>De provincie Zuid-Holland voert van vrijdagavond 3 juni tot en met vrijdag 1 juli werkzaamheden uit aan de hoofdrijbaan van de N207 tussen Boskoop en Alphen aan den Rijn. Voor de werkzaamheden is een afsluiting van 4 weken nodig. Gemotoriseerd verkeer wordt omgeleid.</p>\r\n',69,69,'2017-01-25 10:54:12','2017-04-20 12:29:40',NULL,'uploads/blog_articles/5/5.png',8,'',0),
	(6,3,'N207 tussen Alphen aan den Rijn en Leimuiden is aankomend weekend afgesloten','De N207 tussen Alphen aan den Rijn en Leimuiden is aankomend weekend afgesloten vanwege de verbreding van de provinciale weg.','<p>De N207 tussen Alphen aan den Rijn en Leimuiden is aankomend weekend afgesloten vanwege de verbreding van de provinciale weg. De weekendafsluiting is de eerste van drie weekenden in april en mei.</p>\r\n',69,69,'2017-01-25 10:54:58','2017-04-20 12:28:50',NULL,'uploads/blog_articles/6/6.png',6,'',0),
	(7,3,'De Oostkanaalweg is dinsdag even na 11.00 uur weer vrijgegeven. ','De Oostkanaalweg is dinsdag even na 11.00 uur weer vrijgegeven. Tussen Alphen aan den Rijn en de rotonde bij Aarlanderveen was de weg afgesloten.','<p>De Oostkanaalweg is dinsdag even na 11.00 uur weer vrijgegeven. Tussen Alphen aan den Rijn en de rotonde bij Aarlanderveen was de weg afgesloten in verband met een ernstig ongeval op de N207.</p>\r\n',69,69,'2017-01-25 10:55:53','2017-04-20 12:27:15',NULL,'/uploads/blogpost_004.png',4,'',0),
	(8,3,'Fietspaden langs de Prins Bernhardlaan in Alphen aan den Rijn afgesloten.','De fietspaden langs de Prins Bernhardlaan in Alphen aan den Rijn zijn vanaf vandaag afgesloten.','<p>De fietspaden langs de Prins Bernhardlaan in Alphen aan den Rijn zijn vanaf vandaag afgesloten. In verband met de reconstructie van de weg, zijn de paden tot medio september afgesloten. Fietsers worden omgeleid via de Ambonstraat en het Burgemeester Visserpark.</p>\r\n',69,69,'2017-01-25 10:56:17','2017-04-20 12:27:33',NULL,'/uploads/blogpost_005.png',3,'00:02',0),
	(9,3,'Gemeente Alphen aan den Rijn en aannemer KWS Infra Rotterdam organiseren een inloopavond','Op woensdag 12 april organiseren gemeente Alphen aan den Rijn en aannemer KWS Infra Rotterdam een inloopavond.','<p>Op woensdag 12 april organiseren gemeente Alphen aan den Rijn en aannemer KWS Infra Rotterdam een inloopavond over de komende werkzaamheden aan de Prins Bernhardlaan in Alphen aan den Rijn.</p>\r\n',69,69,'2017-01-25 10:56:43','2017-04-20 12:27:57',NULL,'/uploads/blogpost_006.png',3,'2:00',0),
	(10,3,'Groene licht van het gloednieuwe fietsers stoplicht op zijn kop gedraaid.','Volledig in lijn met de naam van de nabijgelegen Draaistraat is het groene licht van het gloednieuwe fietsers stoplicht op zijn kop gedraaid.','<p>In het Haagse Laak worden fietsers op een wel heel toepasselijke manier groen licht gegeven. Volledig in lijn met de naam van de nabijgelegen Draaistraat is het groene licht van het gloednieuwe fietsers stoplicht op zijn kop gedraaid.</p>\r\n',69,69,'2017-01-25 10:57:13','2017-04-20 12:28:15',NULL,'/uploads/blogpost_007.png',3,'',0),
	(11,3,'Nieuw tankstation blijkt onvindbaar: geen klant te zien.','Er staat sinds een paar weken een nieuw tankstation langs de A4 van Amsterdam naar Rotterdam bij de afslag Wateringen.','<p>Nieuw tankstation blijkt onvindbaar: geen klant te zien.&nbsp;Er staat sinds een paar weken een nieuw tankstation langs de A4 van Amsterdam naar Rotterdam bij de afslag Wateringen, maar voor veel automobilisten is die vulplek nog volstrekt onvindbaar. Hoewel de gloednieuwe pomp er fris, fruitig en blinkend bij staat, lijkt het er bepaald nog geen storm te lopen.</p>\r\n',69,69,'2017-01-25 10:57:35','2017-04-20 12:29:08',NULL,'/uploads/blogpost_008.png',6,'',0),
	(12,3,'Weer flinke file op A4 richting Rotterdam.','Weer flinke file op A4 richting Rotterdam. Op de A4 was het vrijdagmiddag opnieuw raak.','<p>Weer flinke file op A4 richting Rotterdam.&nbsp;Op de A4 was het vrijdagmiddag opnieuw raak. Richting Rotterdam waren drie rijstroken dicht door een ongeluk tussen Plaspoelpolder en Rijswijk. Bij de aanrijding waren twee vrachtwagens en een auto betrokken. Even na 15.30 uur was de weg weer vrij.</p>\r\n',69,69,'2017-01-25 11:33:57','2017-04-20 12:29:51',NULL,'/uploads/blogpost_009.png',7,'',0),
	(13,3,'A4 richting Den Haag tijdelijk dicht door ongeluk bij Leidschendam.','A4 richting Den Haag tijdelijk dicht door ongeluk bij Leidschendam. De A4 vanuit Amsterdam richting Den Haag was vrijdagmiddag tijdelijk afgesloten.','<p>A4 richting Den Haag tijdelijk dicht door ongeluk bij Leidschendam.&nbsp;De A4 vanuit Amsterdam richting Den Haag was vrijdagmiddag tijdelijk afgesloten. Ter hoogte van Leidschendam gebeurde rond 13.00 uur een ongeluk. Hierbij raakte &eacute;&eacute;n persoon gewond.</p>\r\n',69,69,'2017-01-25 11:34:16','2017-04-20 12:26:35',NULL,'/uploads/blogpost_001.png',12,'',0),
	(14,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit.','Dat enim intervalla et relaxat. Duo Reges: constructio interrete. Quorum altera prosunt, nocent altera.... ','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dat enim intervalla et relaxat. Duo Reges: constructio interrete. Quorum altera prosunt, nocent altera. Quacumque enim ingredimur, in aliqua historia vestigium ponimus. Certe, nisi voluptatem tanti aestimaretis. Hoc sic expositum dissimile est superiori. Sed tamen enitar et, si minus multa mihi occurrent, non fugiam ista popularia. Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium.',69,69,'2017-01-25 11:34:48','2017-01-25 11:34:48','2017-01-25 11:35:31',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `blog_articles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table blog_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_categories`;

CREATE TABLE `blog_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `blog_categories_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `blog_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blog_categories_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blog_categories_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `blog_categories` WRITE;
/*!40000 ALTER TABLE `blog_categories` DISABLE KEYS */;

INSERT INTO `blog_categories` (`id`, `parent_id`, `name`, `description`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,NULL,'Test Category','This category is for testing purposes only.',65,65,'2017-01-17 11:22:37','2017-01-17 11:22:37','2017-01-17 13:21:54'),
	(2,3,'Nieuwsbericht','Subcategorie 1 beschrijving',65,65,'2017-01-17 11:23:06','2017-02-15 09:45:53',NULL),
	(3,NULL,'Algemeen','Hoofdcategorie 1 beschrijving',65,65,'2017-01-17 13:42:40','2017-02-15 09:45:30',NULL),
	(4,3,'Informatie','Subcategorie 2 beschrijving',65,65,'2017-01-17 16:24:39','2017-02-15 09:45:21',NULL);

/*!40000 ALTER TABLE `blog_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table chat_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chat_messages`;

CREATE TABLE `chat_messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `message` text,
  `read` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `receiver_id` (`receiver_id`),
  CONSTRAINT `chat_messages_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `chat_messages_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

LOCK TABLES `chat_messages` WRITE;
/*!40000 ALTER TABLE `chat_messages` DISABLE KEYS */;

INSERT INTO `chat_messages` (`id`, `parent_id`, `sender_id`, `receiver_id`, `message`, `read`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,NULL,65,72,'Hoi',NULL,NULL,NULL,NULL,NULL,'2017-04-13 12:46:33'),
	(2,NULL,65,74,'Hoi',NULL,NULL,NULL,NULL,NULL,'2017-04-13 12:46:46'),
	(3,NULL,65,76,'Hoi',NULL,NULL,NULL,NULL,NULL,'2017-04-13 12:46:18'),
	(4,NULL,65,78,'Hoi',NULL,NULL,NULL,NULL,NULL,NULL),
	(5,NULL,65,79,'Hoi',NULL,NULL,NULL,NULL,NULL,'2017-04-13 12:46:40'),
	(6,NULL,79,65,'Hoi Aart, ik ben wegbeheerder 2',NULL,NULL,NULL,NULL,NULL,NULL),
	(7,6,79,79,'Correctie, wegbeheerder3',NULL,NULL,NULL,NULL,NULL,NULL),
	(8,6,65,79,'OK, leuk.',NULL,NULL,NULL,NULL,NULL,NULL),
	(9,6,65,79,'Voor',NULL,NULL,NULL,NULL,NULL,NULL),
	(10,6,65,79,'Jou',NULL,NULL,NULL,NULL,NULL,NULL),
	(11,6,65,79,'Bericht voor wegbeheerder3',NULL,NULL,NULL,NULL,NULL,NULL),
	(12,6,79,79,'Bericht voor aart',NULL,NULL,NULL,NULL,NULL,NULL),
	(13,6,65,79,'Bericht voor wegbeheerder3',NULL,NULL,NULL,NULL,NULL,NULL),
	(14,6,79,79,'Bericht voor aart',NULL,NULL,NULL,NULL,NULL,NULL),
	(15,6,65,79,'die',NULL,NULL,NULL,NULL,NULL,NULL),
	(16,6,79,79,'die',NULL,NULL,NULL,NULL,NULL,NULL),
	(17,6,65,79,'yo',NULL,NULL,NULL,NULL,NULL,NULL),
	(18,6,65,79,'yo',NULL,NULL,NULL,NULL,NULL,NULL),
	(19,6,79,79,'yo',NULL,NULL,NULL,NULL,NULL,NULL),
	(20,6,65,79,'Hoi',NULL,NULL,NULL,NULL,NULL,NULL),
	(21,6,65,79,'Hoi',NULL,NULL,NULL,NULL,NULL,NULL),
	(22,6,65,79,'Hoi',NULL,NULL,NULL,NULL,NULL,NULL),
	(23,6,79,79,'Hoi',NULL,NULL,NULL,NULL,NULL,NULL),
	(24,6,65,65,'Hoi Wegb3',NULL,NULL,NULL,NULL,NULL,NULL),
	(25,6,65,65,'Hoi Wegbeh3',NULL,NULL,NULL,NULL,NULL,NULL),
	(26,6,79,65,'Hoi a',NULL,NULL,NULL,NULL,NULL,NULL),
	(27,6,79,65,'Hoi B',NULL,NULL,NULL,NULL,NULL,NULL),
	(28,6,65,65,'Yo w',NULL,NULL,NULL,NULL,NULL,NULL),
	(29,6,79,65,'Yo a',NULL,NULL,NULL,NULL,NULL,NULL),
	(30,6,65,79,'v A v W',NULL,65,65,'2017-04-14 12:29:58','2017-04-14 12:29:58',NULL),
	(31,6,79,65,'Van W v A',NULL,79,79,'2017-04-14 12:30:01','2017-04-14 12:30:01',NULL),
	(32,6,65,79,'A2W',NULL,65,65,'2017-04-14 12:31:43','2017-04-14 12:31:43',NULL),
	(33,6,79,65,'W2A',NULL,79,79,'2017-04-14 12:31:50','2017-04-14 12:31:50',NULL),
	(34,6,79,65,'Die',NULL,79,79,'2017-04-14 12:33:11','2017-04-14 12:33:11',NULL),
	(35,6,65,79,'Die',NULL,65,65,'2017-04-14 12:33:13','2017-04-14 12:33:13',NULL),
	(36,6,79,65,'Die2',NULL,79,79,'2017-04-14 12:39:44','2017-04-14 12:39:44',NULL),
	(37,6,79,65,'Die2',NULL,79,79,'2017-04-14 12:44:21','2017-04-14 12:44:21',NULL),
	(38,6,79,65,'Die2',NULL,79,79,'2017-04-14 12:44:41','2017-04-14 12:44:41',NULL),
	(39,6,65,79,'yo W',NULL,65,65,'2017-04-14 12:46:44','2017-04-14 12:46:44',NULL),
	(40,6,79,65,'Yo A',NULL,79,79,'2017-04-14 12:46:48','2017-04-14 12:46:48',NULL),
	(41,6,65,79,'Krijg de notificaties!',NULL,65,65,'2017-04-14 12:48:52','2017-04-14 12:48:52',NULL),
	(42,6,79,65,'Krijg zelf de notificaties!',NULL,79,79,'2017-04-14 12:49:17','2017-04-14 12:49:17',NULL),
	(43,6,65,79,'Yo w',NULL,65,65,'2017-04-14 12:51:43','2017-04-14 12:51:43',NULL),
	(44,6,79,65,'Yo a',NULL,79,79,'2017-04-14 12:51:53','2017-04-14 12:51:53',NULL),
	(45,6,65,79,'yo',NULL,65,65,'2017-04-14 12:57:03','2017-04-14 12:57:03',NULL),
	(46,6,65,79,'help',NULL,65,65,'2017-04-14 13:02:06','2017-04-14 13:02:06',NULL),
	(47,6,79,65,'help',NULL,79,79,'2017-04-14 13:02:28','2017-04-14 13:02:28',NULL),
	(48,6,65,79,'2W',NULL,65,65,'2017-04-14 13:10:36','2017-04-14 13:10:36',NULL),
	(49,6,65,79,'Yo',NULL,65,65,'2017-04-14 13:13:07','2017-04-14 13:13:07',NULL),
	(50,6,79,65,'y',NULL,79,79,'2017-04-14 13:18:52','2017-04-14 13:18:52',NULL),
	(51,6,79,65,'a',NULL,79,79,'2017-04-14 13:19:14','2017-04-14 13:19:14',NULL),
	(52,6,79,65,'b',NULL,79,79,'2017-04-14 13:19:38','2017-04-14 13:19:38',NULL),
	(53,6,65,79,'TEST',NULL,65,65,'2017-04-14 13:38:21','2017-04-14 13:38:21',NULL),
	(54,6,65,79,'TEST',NULL,65,65,'2017-04-14 13:39:10','2017-04-14 13:39:10',NULL),
	(55,6,79,65,'Hoi Wegbeheerder3',NULL,79,79,'2017-04-14 14:20:52','2017-04-14 14:20:52',NULL),
	(56,6,79,65,'Yo, doe het.',NULL,79,79,'2017-04-14 14:22:23','2017-04-14 14:22:23',NULL),
	(57,6,65,79,'YO, doe het zelf.',NULL,65,65,'2017-04-14 14:24:28','2017-04-14 14:24:28',NULL);

/*!40000 ALTER TABLE `chat_messages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT '',
  `message` text,
  `parent_id` int(11) DEFAULT NULL,
  `is_answered` tinyint(1) DEFAULT '0',
  `is_admin` tinyint(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;

INSERT INTO `contact` (`id`, `firstname`, `lastname`, `email`, `subject`, `message`, `parent_id`, `is_answered`, `is_admin`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(6,'Test','Gebruiker','aart@sumedia.nl','Mijn onderwerp','Is het beste onderwerp!',NULL,1,0,NULL,65,'2017-02-20 14:16:23','2017-02-20 14:54:58',NULL),
	(7,'Test','Gebruiker','aart@sumedia.nl','Mijn onderwerp','<p>Is het beste onderwerp!</p>\r\n',NULL,1,0,NULL,65,'2017-02-20 14:16:41','2017-02-24 11:36:15',NULL),
	(8,'Test','Gebruiker','aart@sumedia.nl','Mijn onderwerp','Is het beste onderwerp!',7,0,1,65,65,'2017-02-20 14:53:16','2017-02-20 14:53:16',NULL),
	(9,'Test2','Gebruiker','aart@sumedia.nl','Mijn onderwerp','Is het beste onderwerp!',6,0,1,65,65,'2017-02-20 14:54:58','2017-02-20 14:54:58',NULL),
	(10,'Aartie','Paartie','aart+sender@sumedia.nl','Subject','Dit is mijn bericht\r\nbla\r\nbla \r\nbla',NULL,0,0,NULL,NULL,'2017-02-20 15:01:02','2017-02-20 15:01:02',NULL),
	(11,'Test','a','aart@sumedia.nl','aa','bbb',NULL,0,0,NULL,NULL,'2017-02-20 15:02:04','2017-02-20 15:02:04',NULL),
	(12,'Test','Gebruiker','aart@sumedia.nl','Mijn onderwerp','<p>Dit is cool.</p>\r\n\r\n<p>hahahahahah&nbsp;&reg;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<p>Origineel bericht:<br />\r\nIs het beste onderwerp!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n',7,0,1,65,65,'2017-02-20 15:09:14','2017-02-20 15:09:14',NULL),
	(13,'Test','Gebruiker','aart@sumedia.nl','Mijn onderwerp','<p>Dit is cool.</p>\r\n\r\n<p>hahahahahah&nbsp;&reg;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<p>Origineel bericht:<br />\r\nIs het beste onderwerp!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n',7,0,1,65,65,'2017-02-20 15:11:09','2017-02-20 15:11:09',NULL),
	(14,'Test','Gebruiker','aart@sumedia.nl','Mijn onderwerp','<p>Dit is cool.</p>\r\n\r\n<p>hahahahahah&nbsp;&reg;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<p>Origineel bericht:<br />\r\nIs het beste onderwerp!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n',7,0,1,65,65,'2017-02-20 15:12:04','2017-02-20 15:12:04',NULL),
	(15,'Aart','Contact','aart+contact@sumedia.nl','Onderwerpie','bericht jwz\r\nmzl',NULL,1,0,NULL,65,'2017-02-20 15:36:17','2017-02-20 15:36:57',NULL),
	(16,'Aart','Contact','aart+contact@sumedia.nl','Onderwerpie','<p>YOYO</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<p>Origineel bericht:<br />\r\nbericht jwz mzl</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n',15,1,1,65,65,'2017-02-20 15:36:57','2017-02-20 16:08:14',NULL),
	(17,'Aart','albers','aart@sumedia.nl','Onderwerp','Bericht\r\nook\r\nja\r\nvolgende\r\n\r\n\r\n\r\nregel',NULL,1,0,NULL,65,'2017-02-23 13:34:19','2017-02-23 13:38:14',NULL),
	(18,'Aart','albers','aart@sumedia.nl','Onderwerp','<p>Thanks ;)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<p>Origineel bericht:<br />\r\nBericht ook ja volgende regel</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n',17,0,1,65,65,'2017-02-23 13:38:14','2017-02-23 13:38:14',NULL),
	(19,'Aart','Albers','aart@sumedia.nl','Lipsum.com','What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp',NULL,0,0,NULL,NULL,'2017-02-23 14:47:01','2017-02-23 14:47:01',NULL),
	(20,'Test','aart','aart@sumedia.nl','Te kort!','<p>What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type spa</p>\r\n',NULL,0,0,NULL,65,'2017-02-23 14:48:53','2017-02-24 11:33:28',NULL),
	(21,'Very','Long','aart@sumedia.nl','Message','<p>Հայերեն Shqip ‫العربية‫العربية Български Catal&agrave; 中文简体 Hrvatski Česky Dansk Nederlands English Eesti Filipino Suomi Fran&ccedil;ais ქართული Deutsch &Epsilon;&lambda;&lambda;&eta;&nu;&iota;&kappa;ά ‫עברית‫עברית Magyar Indonesia Italiano Latviski Lietuvi&scaron;kai македонски Melayu Norsk Posalski Portugu&ecirc;s Rom&acirc;na Pyccкий Српски Slovenčina Sloven&scaron;čina Espa&ntilde;ol Svenska ไทย T&uuml;rk&ccedil;e Українська Tiếng Việt Lorem Ipsum &quot;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...&quot; &quot;There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...&quot; What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the pdrinting and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with daesktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). Where does it come from? Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. Where can I get some? There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. 5 paragraphs words bytes lists Start with &#39;Lorem ipsum dolor sit amet...&#39; Generate Lorem Ipsum Time tracking for freelancers Translations: Can you help translate this site into a foreign language ? Please email us with details if you can help. There are now a set of mock banners available here in three colours and in a range of standard banner sizes: BannersBannersBanners Donate: If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation, any sum is appreciated - click here to donate using PayPal. Thank you for your support. Chrome Firefox Add-on NodeJS TeX Package Python Interface GTK Lipsum Rails .NET Groovy Adobe Plugin The standard Lorem Ipsum passage, used since the 1500s &quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot; Section 1.10.32 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC &quot;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&quot; 1914 translation by H. Rackham &quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot; Section 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC &quot;At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.&quot; 1914 translation by H. Rackham &quot;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.&quot; help@lipsum.com</p>\r\n',NULL,0,0,NULL,65,'2017-02-23 14:58:19','2017-02-24 12:00:14',NULL);

/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contentblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contentblocks`;

CREATE TABLE `contentblocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table councils
# ------------------------------------------------------------

DROP TABLE IF EXISTS `councils`;

CREATE TABLE `councils` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `province_id` (`province_id`),
  CONSTRAINT `councils_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

LOCK TABLES `councils` WRITE;
/*!40000 ALTER TABLE `councils` DISABLE KEYS */;

INSERT INTO `councils` (`id`, `province_id`, `name`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,2,'Arnhem',65,65,'2017-01-17 10:21:55','2017-01-17 10:21:55','2017-01-17 10:22:44'),
	(2,3,'Arnhem',65,65,'2017-01-17 11:21:08','2017-01-17 11:21:08',NULL),
	(3,3,'Velp',69,69,'2017-02-07 14:25:43','2017-02-07 14:25:43',NULL),
	(4,4,'Zwolle',69,69,'2017-02-07 14:26:26','2017-02-07 14:26:26',NULL),
	(5,3,'Nijmegen',69,69,'2017-02-07 14:26:39','2017-02-07 14:26:39',NULL),
	(6,5,'Eindhoven',69,69,'2017-03-02 14:45:17','2017-03-02 14:45:17',NULL),
	(7,5,'\'s Hertogenbosch',75,75,'2017-03-24 09:17:16','2017-03-24 09:17:16',NULL);

/*!40000 ALTER TABLE `councils` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table device_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `device_types`;

CREATE TABLE `device_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `device_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `device_types_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `device_types` WRITE;
/*!40000 ALTER TABLE `device_types` DISABLE KEYS */;

INSERT INTO `device_types` (`id`, `name`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,'Test Type',65,65,'2017-01-17 10:33:57','2017-01-17 10:33:57',NULL),
	(2,'VRI in Eindhoven',69,69,'2017-03-02 14:20:13','2017-03-02 14:20:13','2017-03-02 14:21:49'),
	(3,'VRI in Eindhoven',69,69,'2017-03-02 14:21:38','2017-03-02 14:21:38','2017-03-02 14:25:03'),
	(4,'VRI in Eindhoven',69,69,'2017-03-02 14:24:56','2017-03-02 14:24:56','2017-03-02 14:25:10');

/*!40000 ALTER TABLE `device_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table devices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `devices`;

CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `disclaimer` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  KEY `location_id` (`location_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `devices_ibfk_3` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `devices_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `devices_ibfk_6` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `devices_ibfk_7` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `devices_ibfk_8` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;

INSERT INTO `devices` (`id`, `location_id`, `user_id`, `supplier_id`, `name`, `disclaimer`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(3,1,NULL,1,'DEMO Test Device',NULL,65,65,'2017-01-17 10:34:18','2017-01-17 11:27:40','2017-02-07 15:08:33'),
	(4,NULL,NULL,1,'DEMO Second test device',NULL,65,65,'2017-01-17 10:41:28','2017-01-17 10:41:28','2017-01-17 10:46:17'),
	(5,1,72,1,'DEMO Arnhem',NULL,65,65,'2017-02-06 13:58:40','2017-02-06 13:58:40','2017-02-07 15:08:43'),
	(6,1,73,1,'DEMO Device for Wegbeheerder2',NULL,73,73,'2017-02-06 14:38:32','2017-02-06 14:38:32','2017-02-07 15:08:46'),
	(7,1,NULL,1,'DEMO ',NULL,69,69,'2017-02-07 14:35:57','2017-02-07 14:35:57','2017-02-07 15:08:39'),
	(8,1,72,1,'DEMO VRI001','<p>Disclaimer tekst ...</p>\r\n',69,69,'2017-02-07 14:58:28','2017-02-24 15:35:22',NULL),
	(9,7,74,1,'DEMO VRI002','<p>Disclaimer tekst ...</p>\r\n',69,69,'2017-02-07 15:12:58','2017-02-24 15:35:42',NULL),
	(10,1,74,1,'DEMO VRI003','<p>Disclaimer tekst ...</p>\r\n',69,69,'2017-02-07 15:57:33','2017-02-24 15:34:32',NULL),
	(11,4,72,1,'DEMO VRI004','<p>Disclaimer tekst ...</p>\r\n',69,69,'2017-02-07 16:06:30','2017-02-24 15:36:14',NULL),
	(12,5,72,1,'DEMO VRI005','<p>Disclaimer tekst ...</p>\r\n',69,69,'2017-02-07 16:07:22','2017-02-24 15:36:02',NULL),
	(13,9,72,1,'DEMO VRI006','<p>Disclaimer tekst ...</p>\r\n',69,69,'2017-02-07 16:08:44','2017-02-24 15:36:26',NULL),
	(14,7,72,1,'DEMO VRI007','<p>Disclaimer tekst ...</p>\r\n',69,69,'2017-02-07 16:09:07','2017-02-24 15:36:45',NULL),
	(15,3,72,1,'DEMO VRI 100','<p>Disclaimer tekst ...</p>\r\n',65,69,'2017-02-15 14:47:12','2017-02-24 15:30:21',NULL),
	(16,1,72,1,'DEMO Test vri','<p>Disclaimer tekst ...</p>\r\n',65,69,'2017-02-24 09:37:56','2017-02-24 15:35:05',NULL),
	(17,10,74,1,'DEMO VRI in Groningen','<p>Disclaimer tekst ...</p>\r\n',65,65,'2017-02-24 09:40:43','2017-03-01 15:39:34',NULL),
	(18,8,79,1,'DEMO refererer test','<ul>\r\n	<li><s>On</s>veilig</li>\r\n	<li><em>Ga</em> <strong>terug</strong></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<blockquote>\r\n<p>quote</p>\r\n</blockquote>\r\n',65,65,'2017-02-24 10:03:49','2017-04-13 09:46:25',NULL),
	(19,11,72,1,'DEMO VRI in Eindhoven','<p>Geen opmerkingen over deze VRI.</p>\r\n',69,69,'2017-03-02 14:48:13','2017-03-02 14:48:13',NULL),
	(20,12,76,1,'DEMO VRI 102 Molenstraat','<p>Dit stoplicht staat vaak te lang op rood. Omdat ie stuk is.</p>\r\n',75,76,'2017-03-24 09:20:05','2017-03-24 09:41:40',NULL);

/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table faq_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq_categories`;

CREATE TABLE `faq_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `faq_categories_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `faq_categories_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `faq_categories` WRITE;
/*!40000 ALTER TABLE `faq_categories` DISABLE KEYS */;

INSERT INTO `faq_categories` (`id`, `parent_id`, `name`, `description`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,NULL,'test','',NULL,65,NULL,'2017-01-17 11:25:26',NULL),
	(2,NULL,'Faq Cat 1','This is the first category',65,65,'2017-01-17 11:24:52','2017-01-17 11:24:52',NULL);

/*!40000 ALTER TABLE `faq_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table faq_questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq_questions`;

CREATE TABLE `faq_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category_id` int(11) DEFAULT NULL,
  `title` varchar(500) DEFAULT NULL,
  `body` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `highlighted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `faq_category_id` (`faq_category_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `faq_questions_ibfk_1` FOREIGN KEY (`faq_category_id`) REFERENCES `faq_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `faq_questions_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `faq_questions_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

LOCK TABLES `faq_questions` WRITE;
/*!40000 ALTER TABLE `faq_questions` DISABLE KEYS */;

INSERT INTO `faq_questions` (`id`, `faq_category_id`, `title`, `body`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`, `highlighted`)
VALUES
	(1,2,'Ben ik verplicht te stoppen voor een rood verkeerslicht als er niemand anders op de weg is?','Ja, als het verkeerslicht voor de rijstrook waarop u rijdt op rood staat, bent u verplicht te stoppen.',NULL,69,NULL,'2017-04-19 15:13:46',NULL,1),
	(2,2,'Hoe moet ik een kind veilig vervoeren in de auto?','Kinderen tot 18 jaar en kleiner dan 1,35 meter moeten vervoerd worden in een goedgekeurd kinderzitje. Een kind mag dan ook op de voorstoel, maar dat heeft niet de voorkeur.',65,69,'2017-01-17 11:26:28','2017-04-20 07:07:51',NULL,0),
	(3,2,'Kan je ook een stoplicht huren?','Nee, helaas. Gezien de bijzonder scherpe verkoopprijzen t.o.v. de nieuwprijs zijn we erachter gekomen dat huren voor ons te onrendabel is. Maar ter geruststelling en om een indruk te krijgen: de huurprijs van een stoplicht bij een bekend rekwisieten verhuurbedrijf ligt een stuk hoger dan de aanschafprijs van de meeste van onze verkoopmodellen.',65,69,'2017-01-17 15:21:11','2017-04-19 15:15:27',NULL,0),
	(4,2,'Schakelen de lampen vanzelf van rood naar groen?','Nee, als je alle draden aansluit op een stekker dan branden alle lampen tegelijk. Voor schakelaars, dimmers, timers of andere sensoren dient u zelf zorg te dragen. Omdat de toepassingen van de verkeerslichten voor onze klanten zo uiteen loopt is het voor ons ondoenlijk om schakelaars op voorraad te houden.',65,69,'2017-01-17 15:24:46','2017-04-19 15:16:37',NULL,0),
	(5,2,'Ik snap een bepaalde verkeerssituatie niet. Waar kan ik terecht?','U kunt dan terecht bij de wegbeheerder. In principe is Rijkswaterstaat de wegbeheerder van de snelwegen, de provincies voor de provinciale wegen, de gemeenten voor de lokale wegen en de waterschappen voor de waterschapswegen.',69,69,'2017-01-26 13:15:55','2017-04-20 07:12:12',NULL,0),
	(6,2,' Ik snap een bepaalde verkeerssituatie niet. Waar kan ik terecht?','U kunt dan terecht bij de wegbeheerder. In principe is Rijkswaterstaat de wegbeheerder van de snelwegen, de provincies voor de provinciale wegen, de gemeenten voor de lokale wegen en de waterschappen voor de waterschapswegen.',69,69,'2017-01-26 13:16:18','2017-04-19 15:20:55',NULL,1),
	(7,2,'Waar vind ik de verkeersregels en de betekenis van verkeersborden?','Die vindt u in de publicatie Verkeersborden en Verkeersregels in Nederland, een uitgave van het ministerie van Infrastructuur en Milieu. Dit ministerie maakt de verkeerswetten.',69,69,'2017-01-26 13:16:48','2017-04-20 07:10:14',NULL,0),
	(8,2,'Zijn er speciale verkeerslichten voor blinden en slechtzienden?','De gemeente Helmond heeft twaalf bijzondere verkeerslichten op de drukste punten van de stad geïnstalleerd. Deze verkeerslichten maken het voor blinden en slechtzienden veiliger om over te steken. Het verkeerslicht maakt altijd geluid – niet pas nadat een knop is ingedrukt – en bevat bovendien een pratende drukknop.',69,69,'2017-01-26 13:17:13','2017-04-19 15:18:15',NULL,0),
	(9,2,'Ik heb een vraag over het rijbewijs. Waar kan ik terecht?','U kunt daarvoor terecht op de website www.rijbewijs.nl.',69,69,'2017-01-26 13:17:46','2017-04-20 07:11:21',NULL,0),
	(10,2,'Mijn verkeersvraag staat er niet tussen. Hoe kan ik contact opnemen?','Hebt u het antwoord op uw vraag nog niet gevonden? Neem dan contact met ons op.',69,69,'2017-01-26 13:18:13','2017-04-20 07:08:57',NULL,0),
	(11,1,'Waar kan ik verkeersonveiligheid melden?','U kunt de verkeersonveiligheid, of het nu gaat om een verkeerssituatie of om gedrag, melden op ons online participatiepunt. U vindt daar een heldere instructie over hoe u op een eenvoudige wijze de verkeersonveiligheid aan Veilig Verkeer Nederland kunt doorsturen en de status van uw melding kunt volgen.',65,69,'2017-02-14 11:26:27','2017-04-19 15:19:32',NULL,1);

/*!40000 ALTER TABLE `faq_questions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table faq_user_questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq_user_questions`;

CREATE TABLE `faq_user_questions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `faq_category_id` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `response` text,
  `email` varchar(255) DEFAULT NULL,
  `answered` tinyint(1) DEFAULT '0',
  `is_response` tinyint(1) DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `faq_category_id` (`faq_category_id`),
  CONSTRAINT `faq_user_questions_ibfk_1` FOREIGN KEY (`faq_category_id`) REFERENCES `blog_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

LOCK TABLES `faq_user_questions` WRITE;
/*!40000 ALTER TABLE `faq_user_questions` DISABLE KEYS */;

INSERT INTO `faq_user_questions` (`id`, `faq_category_id`, `subject`, `message`, `response`, `email`, `answered`, `is_response`, `parent_id`, `lft`, `rght`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(14,2,'Ik heb en vraag','Dit is mijn vraag.',NULL,'aart@sumedia.nl',0,0,NULL,1,2,NULL,NULL,'2017-02-20 11:29:22','2017-02-20 11:29:22',NULL),
	(15,2,'Nog een vraag','hier komt ie',NULL,'aart@sumedia.nl',1,0,NULL,3,6,NULL,65,'2017-02-20 11:29:37','2017-02-20 11:30:33',NULL),
	(16,1,'Nog een vraag',NULL,'<p>Dit is het antwoord,, jwz</p>\r\n','aart@sumedia.nl',0,1,15,4,5,65,65,'2017-02-20 11:30:33','2017-02-20 11:30:33',NULL),
	(17,1,'subject','Dit is mijn vraag',NULL,'aart+faq@sumedia.nl',1,0,NULL,7,10,NULL,65,'2017-02-20 11:59:34','2017-02-20 12:00:26',NULL),
	(18,1,'subject',NULL,'<p>Dit is de reactie</p>\r\n','aart+faq@sumedia.nl',0,1,17,8,9,65,65,'2017-02-20 12:00:26','2017-02-20 12:00:26',NULL),
	(19,1,'Super onderwerp','Ik heb een vraag',NULL,'aart+contact@sumedia.nl',1,0,NULL,11,14,NULL,65,'2017-02-20 15:57:09','2017-02-20 15:58:03',NULL),
	(22,1,'Onderwerp1','Bericht\r\n',NULL,'aart@sumedia.nl',1,0,NULL,17,20,NULL,65,'2017-02-21 14:07:52','2017-02-21 14:09:34',NULL);

/*!40000 ALTER TABLE `faq_user_questions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table filter_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filter_groups`;

CREATE TABLE `filter_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `public` tinyint(1) DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table filters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filters`;

CREATE TABLE `filters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filter_group_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `public` tinyint(1) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `grid` varchar(100) DEFAULT NULL,
  `content` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `i18n`;

CREATE TABLE `i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  KEY `I18N_FIELD` (`model`,`foreign_key`,`field`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

LOCK TABLES `i18n` WRITE;
/*!40000 ALTER TABLE `i18n` DISABLE KEYS */;

INSERT INTO `i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`)
VALUES
	(1,'nl-NL','Pages',8,'name','sitemap'),
	(2,'nl-NL','Pages',8,'slug','sitemap'),
	(3,'nl-NL','PageBlocks',55,'content','a'),
	(4,'nl-NL','PageBlocks',56,'content','<p>b</p>\r\n'),
	(5,'nl-NL','PageBlocks',10,'content','<p>Verkeerslicht.nl ondersteunt weggebruikers hun buurt leuker te maken omdat een goede doorstroom van het wegverkeer bijdraagt aan een gezonde economie.</p>\r\n\r\n<p>Jij kunt bijdragen aan de verkeersveiligheid door een melding te plaatsen wanneer de situatie bij een verkeerslichtenregeling onveilig is.</p>\r\n'),
	(6,'nl-NL','PageBlocks',26,'content','<p>Benieuwd naar actuele meldingen, en of deze al in behandeling zijn genomen? Bekijk onze actuele radar en blijf volledig op de hoogte van alle updates over de melding bij jou in de buurt.</p>\r\n'),
	(7,'nl-NL','PageBlocks',29,'content','<p>Download de app en maak een melding als je een situatie tegenkomt waarbij de verkeersveiligheid wordt belemmerd.</p>\r\n');

/*!40000 ALTER TABLE `i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table locations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `council_id` int(11) DEFAULT NULL,
  `place_id` int(11) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `latitude` float(10,6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `council_id` (`council_id`),
  KEY `place_id` (`place_id`),
  CONSTRAINT `locations_ibfk_1` FOREIGN KEY (`council_id`) REFERENCES `councils` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `locations_ibfk_2` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;

INSERT INTO `locations` (`id`, `address`, `council_id`, `place_id`, `longitude`, `latitude`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,'DEMO De la Reijstraat 9, 6814 AD Arnhem',2,2,5.907258,51.986046,65,69,'2017-01-17 11:21:56','2017-02-07 14:23:51',NULL),
	(2,'DEMO Jacob Cremerstraat 66-70, 6821 DG Velp',3,3,5.910444,51.989067,69,69,'2017-02-07 14:24:38','2017-02-07 14:28:16',NULL),
	(3,'DEMO Willemsplein 23, 6811 KB Arnhem',2,2,5.903129,51.983250,69,69,'2017-02-07 14:29:47','2017-02-07 14:29:47',NULL),
	(4,'DEMO Johan de Wittlaan 22, 8022 BB Zwolle',4,4,6.114252,52.523045,69,69,'2017-02-07 14:30:40','2017-02-07 14:30:40',NULL),
	(5,'DEMO Pythagorasstraat 28, 6836 GC Arnhem',2,2,5.890838,51.946537,69,69,'2017-02-07 14:31:36','2017-02-07 14:31:36',NULL),
	(6,'DEMO Den Heuvel 101, 6881 VD Velp',3,3,5.974777,51.994976,69,69,'2017-02-07 14:32:24','2017-02-07 14:32:24',NULL),
	(7,'DEMO Oude Groenestraat 50-56, 6515 ED Nijmegen',5,5,5.927273,51.990730,69,65,'2017-02-07 14:33:07','2017-02-24 09:04:16',NULL),
	(8,'DEMO Johan van Arnhemstraat',2,2,5.936023,51.987106,65,69,'2017-02-15 14:49:20','2017-04-05 12:18:26',NULL),
	(9,'DEMO Oudenaarde, Belgie',2,2,3.513365,50.781475,65,65,'2017-02-24 08:54:55','2017-02-24 08:56:50',NULL),
	(10,'DEMO Gaykingastraat / Hamplaats, Groningen',2,2,6.694914,53.277531,65,65,'2017-03-01 15:38:17','2017-03-01 15:38:56',NULL),
	(11,'DEMO PSV-laan, Eindhoven',6,6,5.466100,51.443394,69,69,'2017-03-02 14:46:41','2017-03-02 14:46:41',NULL),
	(12,'DEMO Molenstraat',7,7,5.361776,51.703758,75,75,'2017-03-24 09:18:54','2017-03-24 09:18:54',NULL);

/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_items`;

CREATE TABLE `menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  KEY `parent_id` (`parent_id`),
  KEY `name` (`name`(255)),
  CONSTRAINT `menu_items_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `menu_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menu_items_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;

INSERT INTO `menu_items` (`id`, `menu_id`, `parent_id`, `name`, `url`, `lft`, `rght`, `created_at`, `created_by`, `modified_at`, `modified_by`, `deleted`)
VALUES
	(1,1,NULL,'Home','/',1,2,'2017-02-10 15:20:22',65,NULL,65,NULL),
	(2,1,NULL,'FAQ','/faq',3,4,'2017-02-10 15:20:44',65,NULL,65,'2017-02-13 08:30:09'),
	(3,1,NULL,'Meldingen','/meldingen',3,4,'2017-02-13 08:28:49',65,NULL,65,NULL),
	(4,1,NULL,'Kennisbank','/kennisbank',5,6,'2017-02-13 08:30:29',65,NULL,65,'2017-02-13 08:31:09'),
	(5,1,NULL,'Apps','/apps',5,6,'2017-02-13 08:31:20',65,NULL,65,NULL),
	(6,1,NULL,'Over verkeerslicht','/wat-doet-verkeerslicht',7,8,'2017-02-13 08:33:32',65,NULL,65,NULL),
	(7,1,NULL,'Wat doet verkeerslicht','/wat-doet-verkeerslicht',15,16,'2017-02-13 08:34:00',65,NULL,65,'2017-03-01 13:27:05'),
	(8,1,6,'Wie wij zijn','/wat-doet-verkeerslicht',8,9,'2017-02-13 08:34:22',65,NULL,65,'2017-03-01 13:24:03'),
	(9,1,6,'Hoe het werkt','/wat-doet-verkeerslicht',8,9,'2017-02-13 08:35:57',65,NULL,65,'2017-03-01 13:24:14'),
	(10,1,NULL,'Blog','/blog',9,10,'2017-02-13 08:36:26',65,NULL,65,NULL),
	(11,1,NULL,'Kennisbank','/kennisbank',11,12,'2017-02-13 08:36:52',65,NULL,65,NULL),
	(12,1,NULL,'Contact','/contact',13,14,'2017-02-13 08:37:02',65,NULL,65,NULL);

/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;

INSERT INTO `menus` (`id`, `name`, `locale`, `created_at`, `created_by`, `modified_at`, `modified_by`, `deleted`)
VALUES
	(1,'Hoofmenu','nl_NL','2017-02-10 15:20:02',65,NULL,65,NULL);

/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notification_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification_templates`;

CREATE TABLE `notification_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `template` varchar(50) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `body` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `notification_templates` WRITE;
/*!40000 ALTER TABLE `notification_templates` DISABLE KEYS */;

INSERT INTO `notification_templates` (`id`, `name`, `template`, `title`, `body`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(2,'Taak afgerond','completedTask','Taak afgerond',':taskName is afgerond',NULL,NULL,'2016-11-22 12:45:30','2017-01-15 09:36:45',NULL),
	(4,'Chat bericht','chatMessage','Chat bericht van :user_name',':body',NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `notification_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template` varchar(150) DEFAULT NULL,
  `vars` text,
  `user_id` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `tracking_id` text,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

# Dump of table page_blocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `page_blocks`;

CREATE TABLE `page_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL DEFAULT '',
  `content` text,
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`),
  KEY `code` (`code`),
  CONSTRAINT `page_blocks_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `page_blocks_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

LOCK TABLES `page_blocks` WRITE;
/*!40000 ALTER TABLE `page_blocks` DISABLE KEYS */;

INSERT INTO `page_blocks` (`id`, `page_id`, `code`, `content`)
VALUES
	(7,4,'intro_tekst','<p>uitleg</p>\r\n'),
	(8,5,'test','<p>ja</p>\r\n'),
	(9,1,'Title','Wat doet verkeerslicht.nl'),
	(10,1,'Subtitle','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),
	(11,1,'BTN_CTA_report','Plaats jouw melding'),
	(12,1,'Persoon_naam','George Rijsink'),
	(13,1,'Persoon_titel','Wegbeheerde'),
	(14,1,'Testimonials_titel','Gebruikers aan het woord'),
	(15,1,'Testimonial_1_text','<p>Het kruispunt bij Kronenburg in Arnhem was zeer onveilig, dankzij een melding bij Verkeerslicht.nl is het aanzienlijke veiliger geworden voor fietsers.</p>\r\n'),
	(16,1,'Testimonial_1_person','Martijn van de Laar, Arnhem'),
	(17,1,'Testimonial_2_text','<p>Ik stond altijd minuten lang in onze wijk te wachten op een rood stoplicht terwijl en 9/10 geen verkeer was, blij dat het nu verholpen is.</p>\r\n'),
	(18,1,'Testimonial_2_person','Marly Flint, Varsseveld'),
	(19,1,'Testimonial_3_text','<p>Binnen 2 weken kreeg ik een mailtje dat mijn melding in behandeling was en ik merkte direct verschil. Raad het iedereen aan!</p>\r\n'),
	(20,1,'Testimonial_3_person','Mark Vlieringa, Arnhem'),
	(21,1,'BTN_reviews_more','<p>Meer reviews</p>\r\n'),
	(25,3,'Apps_1_title','Verkeerslicht App.'),
	(26,3,'Apps_1_text','<p>Benieuwd naar actuele meldingen, en of deze al in behandeling zijn genomen? bekijk onze actuele radar en blijf volledig op de hoogte van alle updates over de melding bij jou in de buurt.</p>\r\n'),
	(27,3,'Apps_banner_title','Overal snel een melding plaatsen.'),
	(28,3,'Apps_2_title','Veilig een  melding plaatsen.'),
	(29,3,'Apps_2_text','<p>Benieuwd naar actuele meldingen, en of deze al in behandeling zijn genomen? bekijk onze actuele radar en blijf volledig op de hoogte van alle updates over de melding bij jou in de buurt.</p>\r\n'),
	(30,2,'Home_1_title','Altijd veilig op weg.'),
	(31,2,'Home_1_subtitle','<p>Te lang wachten bij een verkeerslicht?<br />\r\nMaak direct een melding!</p>\r\n'),
	(32,2,'Home_1_btn','Direct melden'),
	(33,2,'Home_nav_about','Hoe werkt het?'),
	(34,2,'Home_nav_meldingen','Bekijk actuele meldingen'),
	(35,2,'Home_nav_apps','Download onze app'),
	(36,2,'Home_btn_scroll','Ontdek meer over verkeerslicht'),
	(37,2,'Home_2_title','Hoe werkt het?'),
	(38,2,'Home_2_mobile_subtitle_1','Plaats een melding'),
	(39,2,'Home_2_mobile_text_1','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>\r\n'),
	(40,2,'Home_2_mobile_subtitle_2','Wij onderzoeken het kruispunt'),
	(41,2,'Home_2_mobile_text_2','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>\r\n'),
	(42,2,'Home_2_mobile_text_3','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>\r\n'),
	(43,2,'Home_2_desktop_introtext','<p>Vind jij dat het ergens te lang rood, te kort groen of een onveilige situatie is? Maak dan een melding via onze app of website.</p>\r\n'),
	(44,2,'Home_3_title','Blijf op de hoogte'),
	(45,2,'Home_2_mobile_subtitle_3','Het resultaat, minder onnodig wachten'),
	(46,6,'Title','<p>Contact</p>\r\n'),
	(47,6,'Address','<p>Verkeerslicht.nl</p>\r\n\r\n<p>De la Reijstraat 9</p>\r\n\r\n<p>6814AD Arnhem</p>\r\n'),
	(48,6,'Phone','<p>026 - 381 02 01</p>\r\n'),
	(49,6,'Email','<p>info@verkeerslicht.nl</p>\r\n'),
	(50,6,'KvK','<p>11067137</p>\r\n'),
	(52,7,'Title','<p>Disclaimer</p>\r\n'),
	(53,7,'Subtitle',''),
	(54,7,'Content','<p><strong>Disclaimer voor www.verkeerslicht.nl</strong></p>\r\n\r\n<p>Verkeerslicht.nl (Kamer van Koophandel: 11067137), hierna te noemen Verkeerslicht.nl, verleent u hierbij toegang tot www.verkeerslicht.nl en nodigt u uit de hier aangeboden diensten af te nemen.</p>\r\n\r\n<p>Verkeerslicht.nl behoudt zich daarbij het recht voor op elk moment de inhoud aan te passen of onderdelen te verwijderen zonder daarover aan u mededeling te hoeven doen.</p>\r\n\r\n<p><strong>Beperkte aansprakelijkheid</strong></p>\r\n\r\n<p>Verkeerslicht.nl spant zich in om de inhoud van www.verkeerslicht.nl zo vaak mogelijk te actualiseren en/of aan te vullen. Ondanks deze zorg en aandacht is het mogelijk dat inhoud onvolledig en/of onjuist is.&nbsp;</p>\r\n\r\n<p>De op www.verkeerslicht.nl aangeboden materialen worden aangeboden zonder enige vorm van garantie of aanspraak op juistheid. Deze materialen kunnen op elk moment wijzigen zonder voorafgaande mededeling van Verkeerslicht.nl.</p>\r\n\r\n<p>In het bijzonder zijn alle <strong>prijzen </strong>op www.verkeerslicht.nl onder voorbehoud van type- en programmeerfouten. Voor de gevolgen van dergelijke fouten wordt geen aansprakelijkheid aanvaard. Geen overeenkomst komt tot stand op basis van dergelijke fouten.</p>\r\n\r\n<p>Voor op www.verkeerslicht.nl opgenomen hyperlinks naar websites of diensten van derden kan Verkeerslicht.nl nimmer aansprakelijkheid aanvaarden.</p>\r\n\r\n<p><strong>Auteursrechten</strong></p>\r\n\r\n<p>Alle rechten van intellectuele eigendom betreffende deze materialen liggen bij Verkeerslicht.nl.</p>\r\n\r\n<p>Kopi&euml;ren, verspreiden en elk ander gebruik van deze materialen is niet toegestaan zonder schriftelijke toestemming van Verkeerslicht.nl, behoudens en slechts voor zover anders bepaald in regelingen van dwingend recht (zoals citaatrecht), tenzij bij specifieke materialen anders aangegeven is.&nbsp;</p>\r\n\r\n<p><strong>Overig</strong></p>\r\n\r\n<p>Deze disclaimer kan van tijd tot tijd wijzigen.&nbsp;</p>\r\n'),
	(55,8,'Subtitle',NULL),
	(56,8,'Content',NULL);

/*!40000 ALTER TABLE `page_blocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `controller` varchar(250) DEFAULT NULL,
  `action` varchar(250) DEFAULT NULL,
  `slug` varchar(500) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `sort_order` int(5) DEFAULT NULL,
  `meta_keywords` varchar(500) DEFAULT NULL,
  `meta_description` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `template_id` (`template_id`),
  KEY `controller` (`controller`,`action`),
  KEY `slug` (`slug`(255))
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `parent_id`, `template_id`, `controller`, `action`, `slug`, `name`, `sort_order`, `meta_keywords`, `meta_description`, `created_at`, `created_by`, `modified_at`, `modified_by`, `deleted`)
VALUES
	(1,NULL,2,NULL,NULL,'wat-doet-verkeerslicht','Over Verkeerslicht',4,NULL,NULL,'2017-01-24 08:52:43',68,NULL,69,NULL),
	(2,NULL,1,NULL,NULL,'home','Home',1,NULL,NULL,'2017-01-24 09:07:22',64,NULL,65,NULL),
	(3,NULL,4,NULL,NULL,'apps','Apps',0,NULL,NULL,'2017-01-24 11:45:32',68,NULL,69,NULL),
	(4,NULL,NULL,'Reporters','account','uw-account','Uw account',NULL,NULL,NULL,'2017-02-09 12:49:51',65,NULL,65,NULL),
	(5,NULL,NULL,'Reporters','login','test','Test',NULL,NULL,NULL,'2017-02-09 12:52:25',65,NULL,65,'2017-02-09 12:57:38'),
	(6,NULL,NULL,'Contact','index','contact','Contact',10,NULL,NULL,'2017-02-13 10:48:03',65,NULL,65,NULL),
	(7,NULL,6,NULL,NULL,'disclaimer','disclaimer',NULL,NULL,NULL,'2017-02-24 10:43:40',65,NULL,65,NULL),
	(8,NULL,9,NULL,NULL,NULL,NULL,10,NULL,NULL,'2017-04-19 14:00:38',69,NULL,65,NULL);

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table phinxlog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `phinxlog`;

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `phinxlog` WRITE;
/*!40000 ALTER TABLE `phinxlog` DISABLE KEYS */;

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`)
VALUES
	(20170126140259,'Initial','2017-01-26 14:02:59','2017-01-26 14:02:59',0),
	(20170126140817,'BlogArticles','2017-01-26 14:17:04','2017-01-26 14:17:04',0),
	(20170201154330,'OneFebruary','2017-02-01 15:43:30','2017-02-01 15:43:30',0),
	(20170201154812,'InitialOneFebruary','2017-02-01 15:48:12','2017-02-01 15:48:12',0),
	(20170206152753,'AddPasswordtokenToReporters','2017-02-06 15:34:28','2017-02-06 15:34:29',0),
	(20170208114305,'AddFieldsToReports','2017-02-08 11:57:08','2017-02-08 11:57:09',0),
	(20170209094814,'AddStreetsToReports','2017-02-09 10:07:44','2017-02-09 10:07:44',0),
	(20170209115811,'AddClassToReportStatuses','2017-02-09 11:59:06','2017-02-09 11:59:06',0);

/*!40000 ALTER TABLE `phinxlog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table places
# ------------------------------------------------------------

DROP TABLE IF EXISTS `places`;

CREATE TABLE `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `council_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `council_id` (`council_id`),
  CONSTRAINT `places_ibfk_1` FOREIGN KEY (`council_id`) REFERENCES `councils` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;

INSERT INTO `places` (`id`, `council_id`, `name`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,1,'DEMO Arnhem City',65,65,'2017-01-17 10:22:31','2017-01-17 10:22:31','2017-01-17 10:22:37'),
	(2,2,'DEMO Arnhem',65,69,'2017-01-17 11:21:18','2017-02-07 14:27:43',NULL),
	(3,3,'DEMO Velp',69,69,'2017-02-07 14:26:05','2017-02-07 14:26:05',NULL),
	(4,4,'DEMO Zwolle',69,69,'2017-02-07 14:27:30','2017-02-07 14:27:30',NULL),
	(5,5,'DEMO Nijmegen',69,69,'2017-02-07 14:27:55','2017-02-07 14:27:55',NULL),
	(6,6,'DEMO Eindhoven',69,69,'2017-03-02 14:45:39','2017-03-02 14:45:39',NULL),
	(7,7,'DEMO Rosmalen',75,75,'2017-03-24 09:17:35','2017-03-24 09:17:35',NULL);

/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table provinces
# ------------------------------------------------------------

DROP TABLE IF EXISTS `provinces`;

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;

INSERT INTO `provinces` (`id`, `name`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,'1',65,65,'2017-01-17 10:15:32','2017-01-17 10:15:32','2017-01-17 10:15:37'),
	(2,'Gelderland',65,65,'2017-01-17 10:20:02','2017-01-17 10:20:02','2017-01-17 10:22:48'),
	(3,'Gelderland',65,65,'2017-01-17 11:21:01','2017-01-17 11:21:01',NULL),
	(4,'Overijssel',69,69,'2017-02-07 14:25:18','2017-02-07 14:25:18',NULL),
	(5,'Noord-Brabant',69,69,'2017-03-02 14:41:19','2017-03-02 14:41:19',NULL),
	(6,'Groningen',69,69,'2017-03-02 14:41:48','2017-03-02 14:41:48',NULL),
	(7,'Friesland',69,69,'2017-03-02 14:42:04','2017-03-02 14:42:04',NULL),
	(8,'Drenthe',69,69,'2017-03-02 14:42:18','2017-03-02 14:42:18',NULL),
	(9,'Flevoland',69,69,'2017-03-02 14:42:33','2017-03-02 14:42:33',NULL),
	(10,'Utrecht',69,69,'2017-03-02 14:42:58','2017-03-02 14:42:58',NULL),
	(11,'Noord-Holland',69,69,'2017-03-02 14:43:13','2017-03-02 14:43:13',NULL),
	(12,'Zuid-Holland',69,69,'2017-03-02 14:43:34','2017-03-02 14:43:34',NULL),
	(13,'Zeeland',69,69,'2017-03-02 14:43:48','2017-03-02 14:43:48',NULL),
	(14,'Limburg',69,69,'2017-03-02 14:44:09','2017-03-02 14:44:09',NULL);

/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table report_response_presets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `report_response_presets`;

CREATE TABLE `report_response_presets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) DEFAULT '',
  `message` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `report_response_presets` WRITE;
/*!40000 ALTER TABLE `report_response_presets` DISABLE KEYS */;

INSERT INTO `report_response_presets` (`id`, `name`, `subject`, `message`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,'Bericht voor status: \"te lang op rood\"','Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',65,65,'2017-04-13 12:57:57','2017-04-13 13:00:55',NULL),
	(2,'Bericht voor status: \"te kort op groen\"','Reactie op uw melding.','<p>B<s>e</s>ste,</p>\r\n\r\n<p>Bij deze wil ik reageren op uw melding.</p>\r\n\r\n<ul>\r\n	<li>U heeft ons gemeld dat het stoplicht te <strong>kort</strong> op <em>groen</em> staat.</li>\r\n	<li>Dit is een <em>semi</em>-automatisch bericht.</li>\r\n</ul>\r\n\r\n<p>Bedankt voor het lezen van deze mail.&nbsp;&reg;</p>\r\n',65,65,'2017-04-13 14:37:30','2017-04-13 14:37:30',NULL);

/*!40000 ALTER TABLE `report_response_presets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table report_responses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `report_responses`;

CREATE TABLE `report_responses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `reporter_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT '',
  `message` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reporter_id` (`reporter_id`),
  KEY `user_id` (`user_id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `report_responses_ibfk_1` FOREIGN KEY (`reporter_id`) REFERENCES `reporters` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `report_responses_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `report_responses_ibfk_3` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

LOCK TABLES `report_responses` WRITE;
/*!40000 ALTER TABLE `report_responses` DISABLE KEYS */;

INSERT INTO `report_responses` (`id`, `reporter_id`, `user_id`, `device_id`, `subject`, `message`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,NULL,NULL,NULL,'Hoi Aart.','<p>Hoi Aart,</p>\r\n\r\n<p>Dit is een biericht speciaal voor jou.</p>\r\n\r\n<p>:)</p>\r\n',74,74,'2017-02-21 10:16:38','2017-02-21 10:16:38',NULL),
	(2,NULL,NULL,NULL,'Hoi Aart.','<p>Hoi Aart,</p>\r\n\r\n<p>Dit is een biericht speciaal voor jou.</p>\r\n\r\n<p>:)</p>\r\n',74,74,'2017-02-21 10:16:55','2017-02-21 10:16:55',NULL),
	(3,NULL,NULL,NULL,'','',74,74,'2017-02-21 10:19:32','2017-02-21 10:19:32',NULL),
	(4,NULL,NULL,NULL,'Onderwerp','<p>Dit is mijn bericht</p>\r\n\r\n<p>hoi.</p>\r\n',74,74,'2017-02-21 10:19:49','2017-02-21 10:19:49',NULL),
	(5,NULL,NULL,NULL,'Onderwerp ja','<p>Bericht</p>\r\n\r\n<p>dit</p>\r\n\r\n<p>is.</p>\r\n',74,74,'2017-02-21 10:26:37','2017-02-21 10:26:37',NULL),
	(6,NULL,NULL,NULL,'ONderwerp','<p>jaja</p>\r\n\r\n<p>jaja</p>\r\n\r\n<p>neenee</p>\r\n',74,74,'2017-02-21 10:28:02','2017-02-21 10:28:02',NULL),
	(7,NULL,NULL,NULL,'Onderwerp','<p>Dit</p>\r\n\r\n<p>is&nbsp;</p>\r\n\r\n<p>mijn</p>\r\n\r\n<p>bericht.</p>\r\n',74,74,'2017-02-21 10:29:25','2017-02-21 10:29:25',NULL),
	(8,NULL,NULL,NULL,'Onderpewer','<p>Super</p>\r\n\r\n<ol>\r\n	<li>man</li>\r\n	<li>yo</li>\r\n</ol>\r\n',74,74,'2017-02-21 10:30:45','2017-02-21 10:30:45',NULL),
	(9,NULL,NULL,NULL,'Onderwerp','<blockquote>\r\n<p>Met echte naaam.</p>\r\n</blockquote>\r\n',74,74,'2017-02-21 10:37:27','2017-02-21 10:37:27',NULL),
	(10,NULL,NULL,NULL,'jaja','<p>nenene</p>\r\n',74,74,'2017-02-21 10:38:36','2017-02-21 10:38:36',NULL),
	(11,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:32:01','2017-02-22 11:32:01',NULL),
	(12,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:32:23','2017-02-22 11:32:23',NULL),
	(13,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:32:43','2017-02-22 11:32:43',NULL),
	(14,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:33:39','2017-02-22 11:33:39',NULL),
	(15,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:35:24','2017-02-22 11:35:24',NULL),
	(16,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:35:27','2017-02-22 11:35:27',NULL),
	(17,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:36:37','2017-02-22 11:36:37',NULL),
	(18,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:37:19','2017-02-22 11:37:19',NULL),
	(19,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:38:18','2017-02-22 11:38:18',NULL),
	(20,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:38:43','2017-02-22 11:38:43',NULL),
	(21,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:38:56','2017-02-22 11:38:56',NULL),
	(22,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:41:23','2017-02-22 11:41:23',NULL),
	(23,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:41:55','2017-02-22 11:41:55',NULL),
	(24,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:43:08','2017-02-22 11:43:08',NULL),
	(25,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:43:52','2017-02-22 11:43:52',NULL),
	(26,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:43:59','2017-02-22 11:43:59',NULL),
	(27,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:44:41','2017-02-22 11:44:41',NULL),
	(28,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:44:43','2017-02-22 11:44:43',NULL),
	(29,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:44:55','2017-02-22 11:44:55',NULL),
	(30,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:45:04','2017-02-22 11:45:04',NULL),
	(31,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:45:24','2017-02-22 11:45:24',NULL),
	(32,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:46:01','2017-02-22 11:46:01',NULL),
	(33,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:46:04','2017-02-22 11:46:04',NULL),
	(34,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:46:31','2017-02-22 11:46:31',NULL),
	(35,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:46:54','2017-02-22 11:46:54',NULL),
	(36,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:47:09','2017-02-22 11:47:09',NULL),
	(37,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:47:24','2017-02-22 11:47:24',NULL),
	(38,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:47:54','2017-02-22 11:47:54',NULL),
	(39,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:48:03','2017-02-22 11:48:03',NULL),
	(40,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:48:26','2017-02-22 11:48:26',NULL),
	(41,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:48:35','2017-02-22 11:48:35',NULL),
	(42,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:48:50','2017-02-22 11:48:50',NULL),
	(43,NULL,NULL,NULL,'onderwerp','<p>blalbala</p>\r\n',65,65,'2017-02-22 11:49:21','2017-02-22 11:49:21',NULL),
	(44,NULL,NULL,NULL,'Massa mail','<p>Dit is een massa bericht&nbsp;&reg;</p>\r\n',65,65,'2017-02-22 11:51:15','2017-02-22 11:51:15',NULL),
	(45,NULL,NULL,NULL,'Bedankt','<p><strong>Voor uw melding</strong></p>\r\n',65,65,'2017-02-23 12:00:24','2017-02-23 12:00:24',NULL),
	(46,NULL,NULL,NULL,'Hoi aart','<p>t werkt nog :)</p>\r\n\r\n<p>ja</p>\r\n',65,65,'2017-02-23 13:30:16','2017-02-23 13:30:16',NULL),
	(47,NULL,NULL,NULL,'Test','<p>Mag ik dit?</p>\r\n',65,65,'2017-04-13 09:38:30','2017-04-13 09:38:30',NULL),
	(48,NULL,NULL,NULL,'Hoi','<p>Hoi <strong>Aart</strong></p>\r\n',79,79,'2017-04-13 09:53:39','2017-04-13 09:53:39',NULL),
	(49,NULL,NULL,NULL,'Hoi','<p>Hoi test.</p>\r\n',65,65,'2017-04-13 13:38:21','2017-04-13 13:38:21',NULL),
	(50,NULL,NULL,NULL,'Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',65,65,'2017-04-13 14:34:12','2017-04-13 14:34:12',NULL),
	(51,NULL,NULL,NULL,'Reactie op uw melding.','<p>B<s>e</s>ste,</p>\r\n\r\n<p>Bij deze wil ik reageren op uw melding.</p>\r\n\r\n<ul>\r\n	<li>U heeft ons gemeld dat het stoplicht te <strong>kort</strong> op <em>groen</em> staat.</li>\r\n	<li>Dit is een <em>semi</em>-automatisch bericht.</li>\r\n</ul>\r\n\r\n<p>Bedankt voor het lezen van deze mail.&nbsp;&reg;</p>\r\n',65,65,'2017-04-13 14:38:16','2017-04-13 14:38:16',NULL),
	(52,NULL,NULL,NULL,'Reactie op uw melding.','<p>B<s>e</s>ste,</p>\r\n\r\n<p>Bij deze wil ik reageren op uw melding.</p>\r\n\r\n<ul>\r\n	<li>U heeft ons gemeld dat het stoplicht te <strong>kort</strong> op <em>groen</em> staat.</li>\r\n	<li>Dit is een <em>semi</em>-automatisch bericht.</li>\r\n</ul>\r\n\r\n<p>Bedankt voor het lezen van deze mail.&nbsp;&reg;</p>\r\n',65,65,'2017-04-13 14:52:15','2017-04-13 14:52:15',NULL),
	(53,NULL,NULL,NULL,'Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',65,65,'2017-04-14 06:50:49','2017-04-14 06:50:49',NULL),
	(54,NULL,NULL,NULL,'Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',65,65,'2017-04-14 07:39:06','2017-04-14 07:39:06',NULL),
	(55,NULL,NULL,NULL,'Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',65,65,'2017-04-14 07:41:49','2017-04-14 07:41:49',NULL),
	(56,NULL,NULL,NULL,'Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',65,65,'2017-04-14 07:44:29','2017-04-14 07:44:29',NULL),
	(57,NULL,NULL,NULL,'Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',65,65,'2017-04-14 07:46:45','2017-04-14 07:46:45',NULL),
	(58,NULL,NULL,NULL,'Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',65,65,'2017-04-14 07:48:02','2017-04-14 07:48:02',NULL),
	(59,NULL,NULL,NULL,'Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',65,65,'2017-04-14 07:50:50','2017-04-14 07:50:50',NULL),
	(60,NULL,NULL,NULL,'Reactie op uw melding.','<p>Beste,</p>\r\n\r\n<p>Met plezier deel ik uw mede dat uw melding over het stoplicht dat te lang op rood heeft gestaan in behandeling is genomen.</p>\r\n\r\n<p>Bedankt voor uw medewerking.</p>\r\n',79,79,'2017-04-14 08:29:41','2017-04-14 08:29:41',NULL);

/*!40000 ALTER TABLE `report_responses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table report_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `report_statuses`;

CREATE TABLE `report_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

LOCK TABLES `report_statuses` WRITE;
/*!40000 ALTER TABLE `report_statuses` DISABLE KEYS */;

INSERT INTO `report_statuses` (`id`, `name`, `description`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`, `class`)
VALUES
	(1,'Queued','Report has been submitted to MongoDB que.',65,69,'2017-01-17 11:05:43','2017-02-07 13:52:01','2017-02-09 08:35:00',NULL),
	(2,'Accepted','Report has been stored in MySQL database.',65,65,'2017-01-19 09:30:42','2017-01-19 09:30:52','2017-02-09 08:34:55',NULL),
	(3,'Nieuwe melding','Nieuwe melding',69,65,'2017-02-07 14:14:15','2017-02-09 12:36:31',NULL,'is-new'),
	(4,'In de wacht','In de wacht',69,65,'2017-02-07 14:14:37','2017-02-09 12:38:23',NULL,'is-on-hold'),
	(5,'Behandeld en positief','Behandeld en positief',69,65,'2017-02-07 14:14:58','2017-02-09 12:35:54',NULL,'is-processed-positive'),
	(6,'Behandeld maar neutraal','Behandeld maar neutraal',69,65,'2017-02-07 14:15:19','2017-02-09 12:36:03',NULL,'is-processed-neutral'),
	(7,'Openstaand','Openstaand',69,65,'2017-02-07 14:15:30','2017-02-09 12:36:39',NULL,'is-pending'),
	(8,'Afgewezen','De melding is afgewezen.',65,65,'2017-02-09 08:26:16','2017-02-09 12:35:34',NULL,'is-rejected'),
	(9,'Geaccepteerd','De melding is goedgekeurd.',65,65,'2017-02-09 08:28:54','2017-02-09 12:36:13',NULL,'is-accepted');

/*!40000 ALTER TABLE `report_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table report_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `report_types`;

CREATE TABLE `report_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `report_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `report_types_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `report_types` WRITE;
/*!40000 ALTER TABLE `report_types` DISABLE KEYS */;

INSERT INTO `report_types` (`id`, `name`, `description`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,'Test Report','For testing purposes only.',65,65,'2017-01-17 11:05:23','2017-01-17 11:05:23','2017-02-07 15:16:57'),
	(2,'Stoplicht te lang rood','Stoplicht te lang rood',69,69,'2017-02-07 15:16:51','2017-02-07 15:16:51',NULL),
	(3,'Stoplicht te kort groen','Stoplicht te kort groen',69,69,'2017-02-07 15:17:36','2017-02-07 15:17:36',NULL),
	(4,'Onveilige situatie','Onveilige situatie',69,69,'2017-02-07 15:18:15','2017-02-07 15:18:15',NULL),
	(5,'Overige','Overige',69,69,'2017-02-07 15:18:30','2017-02-07 15:19:19',NULL);

/*!40000 ALTER TABLE `report_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reporters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reporters`;

CREATE TABLE `reporters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initials` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `settings` text,
  `picture` varchar(500) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `password_token` varchar(255) DEFAULT NULL,
  `remember_me` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `reporters_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reporters_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

LOCK TABLES `reporters` WRITE;
/*!40000 ALTER TABLE `reporters` DISABLE KEYS */;

INSERT INTO `reporters` (`id`, `initials`, `firstname`, `middlename`, `lastname`, `username`, `phone`, `email`, `password`, `settings`, `picture`, `is_active`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`, `password_token`, `remember_me`)
VALUES
	(1,'A','Aart','Awesomest','Albers','AAA','','aart@sumedia.nl','password','','',1,65,65,'2017-01-17 11:04:09','2017-01-17 11:04:09','2017-02-01 12:43:35',NULL,NULL),
	(2,'T','Test','Test','Test','Test','Test','aart@sumedia.nl','pass','','',1,65,65,'2017-02-01 12:44:07','2017-02-03 10:04:36',NULL,NULL,NULL),
	(3,'Test','Test',NULL,'','','','aart234@sumedia.nl','$2y$10$GerD5isJNp9PXwysZQ6KdOdVPf69rUL.mqPjTIwr5GaNFX3jtY82S',NULL,NULL,1,NULL,NULL,'2017-02-03 12:01:11','2017-02-03 12:01:11',NULL,NULL,NULL),
	(4,'A','Aart',NULL,'Albers','aart','06 123456','aart+2@sumedia.nl','$2y$10$RqAfK0IOv2BEr80Ae9SgJusJbvdWMzjC4pRla/5TDNH77faBkmxbK',NULL,NULL,1,NULL,NULL,'2017-02-03 12:45:20','2017-02-06 16:48:22',NULL,'w4zONuh2SdQ7',NULL),
	(5,'b','c',NULL,'d','e','f','aart+500@sumedia.nl','$2y$10$M8EegKEmuRGRuMU.eVLJ3ezPDD01jsgZ3w0Xhw1d.z1x/9D9D2Sn6',NULL,NULL,1,NULL,NULL,'2017-02-03 12:48:38','2017-02-03 12:48:38',NULL,NULL,NULL),
	(6,'V','Voornaam',NULL,'Achternaam','voor','','aart+voornaam@sumedia.nl','$2y$10$HMJUUwHNMggfIPyDPcB/N..ELWb.jWhWK9fugZpdmGhsH7OK9HJO.',NULL,NULL,1,NULL,NULL,'2017-02-03 15:11:48','2017-02-03 15:11:48',NULL,NULL,NULL),
	(7,'Test','Test',NULL,'A','user6','','aart+6@sumedia.nl','$2y$10$AQ1bevJmLTj9b70Utlb/LetoyD6Ovom3aT9php/ghrp9abnTVM3Zy',NULL,NULL,1,NULL,NULL,'2017-02-03 15:15:34','2017-02-03 15:15:34',NULL,NULL,NULL),
	(8,'a','b',NULL,'c','testuser','','aart+testuser@sumedia.nl','$2y$10$m/xlhYPaoA9Kh3MU6pOBwOidLx40b7ly3GDVw6vyxqJ6MVNFBi.5e',NULL,NULL,1,NULL,NULL,'2017-02-03 15:16:48','2017-02-03 15:16:48',NULL,NULL,NULL),
	(9,'a','b',NULL,'c','user','','aart+user@sumedia.nl','$2y$10$fRWPL4sL3DKNvCP.iXaZy.4MbqlI8tPib030ErsL2B4Il7Dx5dB8W',NULL,NULL,1,NULL,NULL,'2017-02-03 15:17:50','2017-02-03 15:17:50',NULL,NULL,NULL),
	(10,'SU','Aart','Awesomest','Albers','Aartsome','','aart+awesome@sumedia.nl','$2y$10$vVaJmtFLfqEsFg5hb0Pfqe1vJESZAWaWg80Hhos.L6jMStaJC2UJq',NULL,NULL,1,NULL,NULL,'2017-02-03 15:22:25','2017-02-03 15:22:25',NULL,NULL,NULL),
	(11,'a','aa','bb','cc','Aa','','aart+html@sumedia.nl','$2y$10$9XN2hl5ChlvHJzYh28dOfOXUqgpD/O55h5GqbLdbK06Hvwfp2f3qm',NULL,NULL,1,NULL,NULL,'2017-02-03 15:30:06','2017-02-03 15:30:06',NULL,NULL,NULL),
	(12,'a','Aart','Awesomest','Albers','AAA','XXX','aart+attempt@sumedia.nl','$2y$10$XFpbWxJu7GmkPGasqqurBeX84Z4BndHvXs8ypTtrVqxA.0JjRjri6',NULL,NULL,1,NULL,NULL,'2017-02-03 15:36:58','2017-02-03 15:36:58',NULL,NULL,NULL),
	(13,'A','B','C','D','adjkhfa','F','aart+x@sumedia.nl','$2y$10$4y45cDhZ1JEp7NtbRXb67e2xlisNE9ml/MKbRJDJ9yfQtoYiypxqe',NULL,NULL,1,NULL,NULL,'2017-02-03 15:41:10','2017-02-03 15:41:10',NULL,NULL,NULL),
	(14,'Test','Test','a','b','c','dd','aart+100@sumedia.nl','$2y$10$rVPWNo9LYiWfl5L7rcMB.Ovr5lWu3ytFITOHTTTjektYl6A2JCJWi',NULL,NULL,1,NULL,NULL,'2017-02-03 15:46:43','2017-02-06 16:49:06',NULL,'coJVzQbYTh1A',NULL),
	(15,'TEST','Testperson','testtussenvoegsel','TEstlastname','TESTUSER234','dkafjlsdjfa','aart+testperson@sumedia.nl','$2y$10$0d8EHUJShJ4incfDC8um8u/EZYLJfHxlDL7REupWSJyqDkWljK/sm',NULL,NULL,1,NULL,NULL,'2017-02-06 08:23:17','2017-02-06 08:23:17',NULL,NULL,NULL),
	(16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'$2y$10$gO0f1mkPGPKtKHi2CfswUeLZWxJ3gCPM4/YQxKYbdK/szzod/TmyW',NULL,NULL,1,NULL,NULL,'2017-02-06 08:35:59','2017-02-06 08:35:59',NULL,NULL,NULL),
	(17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'$2y$10$FVYHrvt4Fovhm/wocz78eu.JAbdnNXvARYwVqAyyiz.jXoG8iggHe',NULL,NULL,1,NULL,NULL,'2017-02-06 08:38:03','2017-02-06 08:38:03',NULL,NULL,NULL),
	(18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'$2y$10$HHbWD014JtMLRQffjX3LOu8M56lE9SybNEbYMTfZKpupInoPOXcyW',NULL,NULL,1,NULL,NULL,'2017-02-06 08:40:16','2017-02-06 08:40:16',NULL,NULL,NULL),
	(19,'A','Test','All The','Things','TestAll','','aart+things@sumedia.nl','$2y$10$THy13sv.2QrSi.1mpvWm4e7kvaFG1rqTOZ7Ury3pvx8bDamilTzwK',NULL,NULL,1,NULL,NULL,'2017-02-06 15:47:25','2017-02-07 11:13:00',NULL,'yn94eEEbFBcO',NULL),
	(20,NULL,NULL,NULL,NULL,NULL,NULL,'aart+newuser@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-07 11:51:51','2017-02-07 11:53:48',NULL,'TQOMJdvh83hb',NULL),
	(21,NULL,NULL,NULL,NULL,NULL,NULL,'aart+heelergnieuw@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-07 12:03:53','2017-02-07 12:03:53',NULL,'pTeCTW9K5xGM',NULL),
	(22,NULL,NULL,NULL,NULL,NULL,NULL,'aart+superman@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-07 13:15:48','2017-02-07 13:15:48',NULL,'RI7aGg56iElf',NULL),
	(23,NULL,NULL,NULL,NULL,NULL,NULL,'aart+superuser@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-07 13:26:17','2017-02-07 13:26:17',NULL,'L7rvAgyvrUQ2',NULL),
	(24,NULL,NULL,NULL,NULL,NULL,NULL,'aart+superswa@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-07 13:32:59','2017-02-07 13:32:59',NULL,'9onNYsPqbmqw',NULL),
	(25,NULL,'Aart',NULL,'Albers',NULL,NULL,'aart+whatever@sumedia.nl','$2y$10$4LNhdX1p0Bd55bE0d1GYlO5AVeTqvk9l4ofz/k6pmYAyAUukAKHia',NULL,NULL,1,NULL,NULL,'2017-02-07 13:50:05','2017-02-07 13:53:37',NULL,'sFWtFh4vmRu3',NULL),
	(26,NULL,NULL,NULL,NULL,NULL,NULL,'aart+nieuwste@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-08 08:12:55','2017-02-08 08:12:55',NULL,'KyUAUIFOhIxR',NULL),
	(27,NULL,NULL,NULL,NULL,NULL,NULL,'aart+newmobile@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-08 08:34:07','2017-02-08 08:34:07',NULL,'YGPYQuy8yF3p',NULL),
	(28,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,1,NULL,NULL,'2017-02-08 10:32:17','2017-02-20 10:47:30',NULL,'M55vuLnjTuZr',NULL),
	(29,NULL,NULL,NULL,NULL,NULL,NULL,'aart+whatever@sumedia.nl2',NULL,NULL,NULL,1,NULL,NULL,'2017-02-08 12:53:11','2017-02-08 12:53:11',NULL,'4257FnSL1y5Z',NULL),
	(30,NULL,NULL,NULL,NULL,NULL,NULL,'aart+whatever2@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-08 12:53:11','2017-02-08 12:53:11',NULL,'l870AvsdbBMm',NULL),
	(31,'','','','','whatever2','','aart+whatever3@sumedia.nl','$2y$10$emmdRW2xGtlDVQEo9VhPQ.yYIic0BhICqDQBLD8KJeA4DCwhsjp0i',NULL,NULL,1,NULL,NULL,'2017-02-08 13:30:43','2017-02-08 13:33:04',NULL,'FGuf5QSS9SWr',NULL),
	(32,'','','','','whatever4','','aart+whatever4@sumedia.nl','password',NULL,NULL,1,NULL,NULL,'2017-02-08 13:34:57','2017-02-14 11:01:07',NULL,'rfEGGLiVyUDd',0),
	(33,NULL,NULL,NULL,NULL,NULL,NULL,'aart+10000@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-09 14:23:32','2017-02-09 14:23:32',NULL,'3T34vrn6XaOL',NULL),
	(34,NULL,NULL,NULL,NULL,NULL,NULL,'aart+klasflkjalf@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-09 14:23:33','2017-02-09 14:23:33',NULL,'VcqG2xBdt4U8',NULL),
	(35,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2017-02-09 14:23:36','2017-02-09 14:23:36',NULL,'kA1CfUhXI7yn',NULL),
	(36,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2017-02-09 14:36:29','2017-02-09 14:36:29',NULL,'hpzpZDJIgAbb',NULL),
	(37,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2017-02-09 14:40:06','2017-02-09 14:40:06',NULL,'TLLOdTNO0NbO',NULL),
	(38,NULL,'',NULL,NULL,NULL,NULL,'aart+123@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-09 15:05:28','2017-02-09 15:05:28',NULL,'lcV2BmeLf4d2',NULL),
	(39,NULL,'',NULL,NULL,NULL,NULL,'aart+password@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-10 15:47:10','2017-02-10 15:47:10',NULL,'wZ1SOm1y2ICb',NULL),
	(40,NULL,'',NULL,NULL,NULL,NULL,'aart+albers@sumedia.nl','$2y$10$pa/kQdz6CPXYqbcY5k7Tpu2VH7RMDtblTeaRjlToXGxbcdrVDkFpW',NULL,NULL,1,NULL,NULL,'2017-02-10 15:49:03','2017-02-23 08:28:29',NULL,'7CbZbC2yhspx',1),
	(41,NULL,'',NULL,NULL,'willem',NULL,'willem@sumedia.nl','$2y$10$TPupos6/sKMS9w/a3shs5.UZKJb1vGQoo1IBIgkI2FxZF.NVjm/QO',NULL,NULL,1,NULL,NULL,'2017-02-10 16:10:27','2017-02-15 12:34:53',NULL,'9r2it24M8MD8',1),
	(42,NULL,'Weg',NULL,NULL,NULL,NULL,'aart+600@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-13 12:47:57','2017-02-13 12:47:57',NULL,'1tjNwGP3BWxX',NULL),
	(43,NULL,'Weg',NULL,NULL,NULL,NULL,'aart+mobile@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-13 12:47:57','2017-02-13 12:47:57',NULL,'6hPwXkFUEflN',NULL),
	(44,NULL,'',NULL,NULL,NULL,NULL,'daan@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-13 13:32:39','2017-02-13 13:32:39',NULL,'hv8LPOewkZEh',NULL),
	(45,NULL,'aart',NULL,'Albers',NULL,NULL,'aart+completetest@sumedia.nl','$2y$10$1ON6DJ1AO2YZMXhH.fB7X.DTBBcjXsyWEuSKriQZ8a5fENcTRcXj6',NULL,NULL,1,NULL,NULL,'2017-02-14 11:55:48','2017-02-14 11:59:28',NULL,'bCTDsgDbDuno',NULL),
	(46,NULL,'Aart2',NULL,'Latest2',NULL,NULL,'aart+latest@sumedia.nl','password',NULL,NULL,1,NULL,NULL,'2017-02-14 12:43:08','2017-02-15 09:24:40',NULL,'DLZrgj1f3aB2',0),
	(47,NULL,'Aart3',NULL,'Aart3',NULL,NULL,'aart+latest2@sumedia.nl','password',NULL,NULL,1,NULL,NULL,'2017-02-14 12:58:03','2017-02-14 13:33:08',NULL,NULL,0),
	(48,NULL,'Aart',NULL,'Albers',NULL,NULL,'aart+email@sumedia.nl','$2y$10$sI4wv6KWrP2c9f3vCMLMEuFHOaGxMe/iubaJeYii9IoP7uGMxQwe.',NULL,NULL,1,NULL,NULL,'2017-02-14 13:45:00','2017-02-14 13:45:00',NULL,NULL,NULL),
	(49,NULL,'test',NULL,'aart',NULL,NULL,'aart+testaart@sumedia.nl','$2y$10$uik6ViqZBQbHpiuRSVy8wubvlvp19.aFhfJ9LHRkvPjtGPoBH9cDq',NULL,NULL,1,NULL,NULL,'2017-02-14 13:50:07','2017-02-14 13:50:07',NULL,NULL,NULL),
	(50,NULL,'Test',NULL,'Gebruiker',NULL,NULL,'aart+testgebruiker@sumedia.nl','$2y$10$BG9yyjBzQ2kb99TQXPgfH.MuJTZMAp0D4qa8B19JV4b.b4xWS2YXW',NULL,NULL,1,NULL,NULL,'2017-02-14 16:51:32','2017-02-14 16:51:32',NULL,NULL,1),
	(51,NULL,'Aart',NULL,'Albers',NULL,NULL,'aart+present@sumedia.nl','Password',NULL,NULL,1,NULL,NULL,'2017-02-15 08:24:59','2017-02-15 09:22:02',NULL,'tAb49uapf6f1',0),
	(52,NULL,'Test',NULL,'Gebruiker',NULL,NULL,'aart+gebruiker@sumedia.nl','$2y$10$wxdh0JM6thcc6pWqIV/OqelcAa3M965ThPdYYOA3FGeom0vBhPaTe',NULL,NULL,1,NULL,NULL,'2017-02-15 09:31:17','2017-02-15 09:35:49',NULL,NULL,1),
	(53,NULL,'Aart',NULL,'Sumedia',NULL,NULL,'aart+nieuw@sumedia.nl','$2y$10$oNzGEZ2G/hUgljkFTZU7PugVMcCsmN2lvVN2Uo58nWJ4Ypit3XhYW',NULL,NULL,1,NULL,NULL,'2017-02-15 14:34:16','2017-02-15 14:35:38',NULL,'UEZPssNnoHR0',1),
	(54,NULL,'Test',NULL,'Test',NULL,NULL,'willem+2@sumedia.nl','$2y$10$dt2r3ABxd5mTImtUvRNxkuKvmMCc.TwBPB3qEOQ6ucf.4ZSZ684t.',NULL,NULL,1,NULL,NULL,'2017-02-15 16:48:46','2017-02-22 13:14:33',NULL,'Lv5iSwvPWBP9',1),
	(55,NULL,'Tobias',NULL,'Boekwijt',NULL,NULL,'tobias@sumedia.nl','$2y$10$szdCspUOTvn8y/tU7tSUc.nAUM.xz6l10szKp6Y9meKGeQL1Pzq0C',NULL,NULL,1,NULL,NULL,'2017-02-20 10:27:38','2017-02-21 09:10:30',NULL,NULL,1),
	(56,NULL,'Aart',NULL,'Albers',NULL,NULL,'aart+faq@sumedia.nl','$2y$10$sQymAvUrerFDOV46s72Mne.h8fJMQ2pyqlU.D01KUVDp7Y1TxcUjC',NULL,NULL,1,NULL,NULL,'2017-02-20 11:58:59','2017-02-20 12:01:40',NULL,NULL,1),
	(57,NULL,'Aart',NULL,'Albers',NULL,NULL,'aart+contact@sumedia.nl','$2y$10$sf1emvK4y493WPRuv5UvZul2vCV0QJN7CTz4QpuhS2rdwxDWDX51a',NULL,NULL,1,NULL,NULL,'2017-02-20 15:35:39','2017-02-20 15:35:39',NULL,NULL,0),
	(58,NULL,'Aart',NULL,NULL,NULL,NULL,'aart+wegb@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-02-22 07:56:51','2017-02-22 07:56:51',NULL,'x038UOyGcHoi',NULL),
	(59,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(60,NULL,'Guus',NULL,'Van der Burgt',NULL,NULL,'guusvabderburgt@home.nl','$2y$10$CekeDbZVPhULaakueAw/1.Kh/Av5YyCSE5v2xmvuK3qI/tthvPNE6',NULL,NULL,1,NULL,NULL,'2017-03-03 17:59:29','2017-03-03 17:59:29',NULL,NULL,NULL),
	(61,NULL,'Guus',NULL,NULL,NULL,NULL,'guus@fvl-holding.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-03-08 08:59:01','2017-03-08 08:59:01',NULL,'LBDNjdUbBHHV',NULL),
	(62,NULL,'Guus',NULL,'Van der Burgt',NULL,NULL,'guusvanderburgt@home.nl','$2y$10$UHYxH0LN5fzCgvqUhrc6Qu9Yx1KJB88ZvsWw/B8N5q8EpRpPA4Z36',NULL,NULL,1,NULL,NULL,'2017-03-08 10:42:44','2017-03-08 10:42:44',NULL,NULL,NULL),
	(63,NULL,'',NULL,NULL,NULL,NULL,'leeroy@sumedia.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-03-10 08:43:00','2017-03-10 08:43:00',NULL,'IRiY4eWCXhgU',NULL),
	(64,NULL,'Willem',NULL,'van Heemstra',NULL,NULL,'willem+3@sumedia.nl','$2y$10$8F99cht3rpefN0U3/SBCdu07eiiFSXyD403ASByqdCYCMd7OOrm1K',NULL,NULL,1,NULL,NULL,'2017-04-05 08:36:06','2017-04-05 08:37:24',NULL,'jjnLGRoBvNCk',NULL),
	(65,NULL,'henk',NULL,NULL,NULL,NULL,'henk@henk.nl',NULL,NULL,NULL,1,NULL,NULL,'2017-04-11 09:17:07','2017-04-11 09:17:07',NULL,'Pno8E1y7rwU1',NULL),
	(66,NULL,'Patrick',NULL,'van der Werf',NULL,NULL,'patrick@awfulmen.nl','$2y$10$6pQXtppIcQy/PyQlG1Jp9e6B2tSZzY8deE1M8zB6iQG7NFGG9m6eO',NULL,NULL,1,NULL,NULL,'2017-04-11 14:05:52','2017-04-11 14:05:52',NULL,NULL,NULL);

/*!40000 ALTER TABLE `reporters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reports
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `report_status_id` int(11) DEFAULT NULL,
  `reporter_id` int(11) DEFAULT NULL,
  `report_type_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `body` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `latitude` float(10,6) DEFAULT NULL,
  `vri_place` varchar(255) DEFAULT NULL,
  `vri_street_1` varchar(255) DEFAULT NULL,
  `vri_street_2` varchar(255) DEFAULT NULL,
  `vehicle` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `fromstreet` varchar(255) DEFAULT NULL,
  `tostreet` varchar(255) DEFAULT NULL,
  `keep_me_informed` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `report_status_id` (`report_status_id`),
  KEY `report_type_id` (`report_type_id`),
  KEY `reporter_id` (`reporter_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `reports_ibfk_1` FOREIGN KEY (`report_status_id`) REFERENCES `report_statuses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reports_ibfk_3` FOREIGN KEY (`report_type_id`) REFERENCES `report_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reports_ibfk_4` FOREIGN KEY (`reporter_id`) REFERENCES `reporters` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reports_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reports_ibfk_6` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reports_ibfk_7` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;

INSERT INTO `reports` (`id`, `device_id`, `report_status_id`, `reporter_id`, `report_type_id`, `title`, `body`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`, `longitude`, `latitude`, `vri_place`, `vri_street_1`, `vri_street_2`, `vehicle`, `date`, `time`, `fromstreet`, `tostreet`, `keep_me_informed`)
VALUES
	(62,10,5,2,2,'De la Reijstraat 9, Arnhem','TestReport1',69,74,'2017-02-20 08:57:53','2017-02-22 15:49:45',NULL,5.907258,51.986046,'De la Reijstraat 9, Arnhem','Amsterdamseweg','naar',NULL,'2018-02-09','00:00:02',NULL,NULL,1),
	(63,9,5,2,3,'Jacob Cremerstraat 66-70, Velp','Body tekst',69,74,'2017-02-20 09:17:06','2017-02-22 15:54:23',NULL,5.910444,51.989067,'Jacob Cremerstraat 66-70, Velp','Amsterdamseweg','s2',NULL,'2017-02-02','00:00:02',NULL,NULL,1),
	(64,10,5,2,4,'Willemsplein 23, Arnhem','Body tekst',69,74,'2017-02-20 09:18:24','2017-02-22 15:50:06',NULL,5.903129,51.983250,'Willemsplein 23, Arnhem','Amsterdamseweg','4',NULL,'2017-08-02','00:00:02',NULL,NULL,1),
	(65,11,6,2,5,'Johan de Wittlaan 22, Zwolle','Body tekst',69,69,'2017-02-20 09:19:35','2017-02-20 09:24:44',NULL,6.114252,52.523045,'Johan de Wittlaan 22, Zwolle','Amsterdamseweg','',NULL,'2017-02-21',NULL,NULL,NULL,1),
	(66,12,3,2,2,'Pythagorasstraat 28, Arnhem','Body tekst',69,69,'2017-02-20 09:20:54','2017-02-20 09:24:24','2017-02-20 09:59:01',5.890838,51.946537,'Pythagorasstraat 28, Arnhem','Amsterdamseweg','',NULL,'2017-02-21',NULL,NULL,NULL,1),
	(67,13,8,2,3,'Den Heuvel 101, Velp','Body tekst',69,69,'2017-02-20 09:21:59','2017-02-20 09:23:56','2017-02-20 09:58:41',5.974777,51.994976,'Den Heuvel 101, Velp','Amsterdamseweg','',NULL,'2017-02-21',NULL,NULL,NULL,1),
	(68,9,9,2,4,'Oude Groenestraat 50-56, Nijmegen','Body tekst',69,69,'2017-02-20 09:23:01','2017-02-20 09:23:38','2017-02-20 09:58:23',5.836849,51.878960,'Oude Groenestraat 50-56, Nijmegen','Amsterdamseweg','',NULL,'2017-02-21',NULL,NULL,NULL,1),
	(69,NULL,3,2,2,NULL,'safasf',NULL,NULL,'2017-02-21 07:58:36','2017-02-21 07:59:00',NULL,NULL,NULL,'asdf','adf','asf','car','2017-02-21','01:02:00',NULL,NULL,NULL),
	(70,NULL,3,55,3,NULL,'',NULL,NULL,'2017-02-21 08:10:32','2017-02-21 08:10:42',NULL,5.845900,51.983200,'Oosterbeek, Nederland','Amsterdamseweg','Naarlaan','car','2017-02-21','00:00:00',NULL,NULL,NULL),
	(71,NULL,3,2,2,NULL,'',NULL,65,'2017-02-21 14:20:55','2017-02-22 15:43:00',NULL,NULL,NULL,'Lombok, Arnhem, Nederland','Amsterdamseweg','Van Toulon van der Koogweg<?php die();?>','moped','2018-09-02','02:22:00',NULL,NULL,0),
	(72,NULL,3,40,3,NULL,'',NULL,65,'2017-02-21 14:34:02','2017-02-24 09:21:41',NULL,5.906979,51.986004,'P.C. Hoofstraat 76, 1012 Amsterdam, Nederland','asdfdas','asdfsdaf','car','2013-02-01','00:00:02',NULL,NULL,0),
	(73,NULL,3,58,2,NULL,'',NULL,NULL,'2017-02-21 16:14:56','2017-02-22 07:56:51',NULL,NULL,NULL,'Amsterdam Airport Schiphol, Evert van de Beekstraat 202, 1118 CP Schiphol, Nederland','badhoevdorp','rijsenhout','car','2017-02-21','00:00:00',NULL,NULL,NULL),
	(74,NULL,6,2,2,NULL,'opzich..',NULL,65,'2017-02-22 07:56:14','2017-04-14 07:41:49',NULL,NULL,NULL,'Elsrijk, 1181 Amstelveen, Nederland','Stadhart','Ouderkerk','moped','2017-02-22','12:12:00',NULL,NULL,NULL),
	(75,NULL,3,2,2,NULL,'opzich..',NULL,65,'2017-02-22 08:14:09','2017-02-22 15:42:41',NULL,NULL,NULL,'Elsrijk, 1181 Amstelveen, Nederland','Stadhart','Ouderkerk','moped','2017-09-06','03:00:00',NULL,NULL,0),
	(76,NULL,3,2,2,NULL,'ja',NULL,NULL,'2017-02-22 08:19:30','2017-02-22 08:21:01','2017-02-22 11:51:47',NULL,NULL,'Elsrijk, 1181 Amstelveen, Nederland','van','naar','car','2017-02-22','12:12:00',NULL,NULL,1),
	(77,10,6,2,2,NULL,'',NULL,74,'2017-02-22 08:20:22','2017-02-22 15:55:27',NULL,4.870087,52.311420,'Elsrijk, 1181 Amstelveen, Nederland','van','naar','scooter','2018-03-02','00:00:02',NULL,NULL,1),
	(78,NULL,6,2,2,NULL,'',NULL,65,'2017-02-22 08:27:22','2017-04-14 07:41:49',NULL,5.845900,51.983200,'Oosterbeek, Nederland','a2','a3','car','2017-02-22','00:00:00',NULL,NULL,1),
	(79,10,4,46,2,NULL,'',NULL,65,'2017-02-23 11:57:00','2017-02-24 09:34:54',NULL,3.508480,50.782295,'Molenbeke, 6824 Arnhem, Nederland','Hoornestraat','Vosdijk','car','2018-02-11','00:00:02',NULL,NULL,1),
	(80,10,5,43,3,NULL,'',NULL,65,'2017-02-23 12:06:06','2017-02-23 12:08:05',NULL,5.907189,51.985958,'Bothaplein 9, 6814 AJ Arnhem, Nederland','van','naar','car','2018-11-02','00:00:02',NULL,NULL,1),
	(81,NULL,3,2,2,NULL,'); die();\r\n\'); die();\r\n\"); die();',NULL,NULL,'2017-02-23 13:36:32','2017-02-23 13:36:51',NULL,5.907170,51.985954,'Bothaplein 9, 6814 AJ Arnhem, Nederland','); die(); \'); die(); \"); die();','); die(); \'); die(); \"); die();','car','2017-02-23','12:12:00',NULL,NULL,NULL),
	(82,NULL,3,2,3,NULL,'',NULL,NULL,'2017-02-23 16:03:28','2017-02-23 16:03:42',NULL,NULL,NULL,'','Test','9','car','2017-02-23','23:01:00',NULL,NULL,NULL),
	(83,NULL,3,54,2,NULL,'',NULL,NULL,'2017-02-24 08:05:10','2017-02-24 08:05:27',NULL,5.907186,51.985958,'Bothaplein 9, 6814 AJ Arnhem, Nederland','Van','Naar','car','2017-02-24','09:04:00',NULL,NULL,1),
	(84,NULL,3,54,2,NULL,'',NULL,NULL,'2017-02-24 08:09:17','2017-02-24 08:09:27',NULL,5.907216,51.985996,'Bothaplein 9, 6814 AJ Arnhem, Nederland','Van','Naar','car','2017-02-24','09:08:00',NULL,NULL,NULL),
	(85,NULL,3,54,2,NULL,'',NULL,NULL,'2017-02-24 08:11:25','2017-02-24 08:12:12',NULL,5.907167,51.985939,'','Van','Naar','car','2017-02-24','09:08:00',NULL,NULL,NULL),
	(86,NULL,3,54,5,NULL,'Ik heb het formulier afgemaakt.',NULL,65,'2017-02-24 08:11:58','2017-02-24 08:53:07',NULL,3.586224,50.808758,'4121 Everdingen, Nederland','Van','Naar','car','2018-12-02','00:00:02',NULL,NULL,1),
	(87,NULL,3,55,2,NULL,'aaaaaaaaa',NULL,NULL,'2017-02-27 16:36:44','2017-02-27 16:37:03',NULL,5.907194,51.985947,'Bothaplein 9, 6814 AJ Arnhem, Nederland','van','naar','car','2017-02-27','17:36:00',NULL,NULL,NULL),
	(88,NULL,3,2,2,NULL,'',NULL,NULL,'2017-02-27 16:37:38','2017-02-27 16:37:42',NULL,5.907236,51.985966,'Bothaplein 9, 6814 AJ Arnhem, Nederland','VAan','Naar','car','2017-02-27','17:37:00',NULL,NULL,NULL),
	(89,NULL,3,2,2,NULL,'',NULL,NULL,'2017-02-27 16:38:32','2017-02-27 16:38:35',NULL,5.907202,51.985928,'Bothaplein 9, 6814 AJ Arnhem, Nederland','a','b','car','2017-02-27','17:38:00',NULL,NULL,NULL),
	(90,NULL,3,2,2,NULL,'',NULL,NULL,'2017-02-27 16:46:58','2017-02-27 16:47:05',NULL,5.907184,51.985924,'Bothaplein 9, 6814 AJ Arnhem, Nederland','VAN','Naar',NULL,'2017-02-27','17:46:00',NULL,NULL,NULL),
	(91,NULL,3,2,2,NULL,'',NULL,NULL,'2017-02-27 16:49:38','2017-02-27 16:49:41',NULL,5.907181,51.985931,'','Van','Naar',NULL,'2017-02-27','17:49:00',NULL,NULL,NULL),
	(92,NULL,3,2,4,NULL,'',NULL,NULL,'2017-02-27 16:51:28','2017-02-27 16:51:31',NULL,NULL,NULL,'','a','b',NULL,'2017-02-27','17:51:00',NULL,NULL,NULL),
	(93,NULL,3,2,2,NULL,'',NULL,65,'2017-02-27 16:53:07','2017-03-01 15:41:56',NULL,6.694914,53.277531,'','a2','b',NULL,'2019-03-02','00:00:03',NULL,NULL,0),
	(94,NULL,3,2,2,NULL,'',NULL,NULL,'2017-02-27 16:53:33','2017-02-27 16:53:35',NULL,NULL,NULL,'','a','b',NULL,'2017-02-27','17:53:00',NULL,NULL,NULL),
	(95,NULL,3,2,2,NULL,'',NULL,NULL,'2017-02-27 16:56:49','2017-02-27 16:56:55',NULL,5.907180,51.985935,'Bothaplein 9, 6814 AJ Arnhem, Nederland','a','b',NULL,'2017-02-27','17:56:00',NULL,NULL,NULL),
	(96,NULL,3,55,3,NULL,'',NULL,65,'2017-02-27 16:57:41','2017-03-01 15:42:39',NULL,5.907169,50.985970,'Arnhem, Bothaplein, 6814 Arnhem, Nederland','Werk','Huis',NULL,'2019-03-02','00:00:03',NULL,NULL,0),
	(97,NULL,3,2,2,NULL,'',NULL,NULL,'2017-03-01 12:05:48','2017-03-01 13:51:49',NULL,5.907259,51.985970,'Arnhem, Bothaplein, 6814 Arnhem, Nederland','van','naar','motorcycle','2017-03-01','13:05:00',NULL,NULL,NULL),
	(98,NULL,3,2,2,NULL,'',NULL,NULL,'2017-03-01 13:19:21','2017-03-01 13:51:49',NULL,5.907214,51.985950,'Bothaplein 9, 6814 AJ Arnhem, Nederland','Van','Naar',NULL,'2017-03-01','14:01:00',NULL,NULL,NULL),
	(99,NULL,3,2,2,NULL,'Hoi',NULL,NULL,'2017-03-01 13:20:08','2017-03-01 13:51:49',NULL,5.907174,51.985947,'De la Reijstraat 9, 6814 AD Arnhem, Nederland','Van','Naar',NULL,'2017-03-01','14:01:00',NULL,NULL,NULL),
	(100,NULL,3,2,2,NULL,'',NULL,NULL,'2017-03-01 13:52:16','2017-03-01 15:10:03',NULL,5.907201,51.985947,'Bothaplein 9, 6814 AJ Arnhem, Nederland','Van','Naar',NULL,'2017-03-01','14:42:00',NULL,NULL,NULL),
	(101,NULL,3,2,5,NULL,'',NULL,NULL,'2017-03-01 15:27:27','2017-03-01 15:28:01',NULL,5.907213,51.985954,'Bothaplein 9, 6814 AJ Arnhem, Nederland','Februari','Maart',NULL,'2017-03-01','16:26:00',NULL,NULL,NULL),
	(102,17,3,54,3,NULL,'In Groningen, Ten Boer',NULL,65,'2017-03-01 15:27:49','2017-03-01 16:10:19',NULL,6.693126,53.278252,'Ten Boer, Stadsweg, 9791 LA Ten Boer, Nederland','Stadsweg','Blinkerdlaan','motorcycle','2017-04-01','00:00:03',NULL,NULL,1),
	(103,NULL,3,2,5,NULL,'Nee',NULL,65,'2017-03-01 15:31:49','2017-03-01 16:47:21',NULL,5.907233,51.985947,'Bothaplein 9, 6814 AJ Arnhem, Nederland','Februari','Maart','car','2017-12-07','00:15:00',NULL,NULL,0),
	(104,19,4,2,4,NULL,'Geen opmerkingen',NULL,65,'2017-03-02 13:16:55','2017-04-14 07:50:50',NULL,5.469336,51.441528,'Philipsdorp, 5616 Eindhoven, Nederland','Vonderweg','PSV-laan','bicycle','2017-03-02','13:00:00',NULL,NULL,1),
	(105,NULL,3,54,2,NULL,'',NULL,NULL,'2017-03-03 10:16:49','2017-03-03 10:17:00',NULL,5.906985,51.986095,'Arnhem, Bothaplein, 6814 AM Arnhem, Nederland','Van straat','Naar straat',NULL,'2017-03-03','11:14:00',NULL,NULL,1),
	(106,NULL,3,54,2,NULL,'',NULL,NULL,'2017-03-03 11:08:55','2017-03-03 11:09:01',NULL,5.906978,51.986080,'Arnhem, Bothaplein, 6814 AM Arnhem, Nederland','Van straat','Naar straat',NULL,'2017-03-03','12:08:00',NULL,NULL,1),
	(107,NULL,3,54,2,NULL,'',NULL,NULL,'2017-03-03 12:00:21','2017-03-03 12:01:02',NULL,5.906999,51.986057,'Arnhem, Bothaplein, 6814 AM Arnhem, Nederland','Van straat','Naar straat',NULL,'2017-03-03','12:58:00',NULL,NULL,NULL),
	(108,NULL,3,54,5,NULL,'',NULL,NULL,'2017-03-03 12:45:45','2017-03-03 16:29:05',NULL,5.906966,51.986012,'Arnhem, Bothaplein, 6814 AM Arnhem, Nederland','Van straat','Naar straat','motorcycle','2017-03-03','13:43:00',NULL,NULL,0),
	(109,NULL,3,54,2,NULL,'Teen opmerkingen.',NULL,NULL,'2017-03-03 14:01:39','2017-03-03 14:02:01',NULL,5.469722,51.441643,'Eindhoven, Philips Stadion, 5616 LA Eindhoven, Nederland','Mathildelaan','Vonderweg','bicycle','2017-03-03','14:58:00',NULL,NULL,1),
	(110,NULL,3,54,3,NULL,'',NULL,NULL,'2017-03-03 14:06:28','2017-03-03 15:23:48',NULL,5.906980,51.986050,'Arnhem, Bothaplein, 6814 AM Arnhem, Nederland','Van straat','Naar straat','car','2017-03-03','15:03:00',NULL,NULL,1),
	(111,NULL,3,54,3,NULL,'Geen opmerkingen',NULL,NULL,'2017-03-03 14:12:00','2017-03-03 14:12:01',NULL,5.669122,51.984711,'Wageningen, Nederland','Van Straat','Naar Straat','scooter','2017-03-03','15:06:00',NULL,NULL,1),
	(112,NULL,3,54,5,NULL,'',NULL,NULL,'2017-03-03 14:22:45','2017-03-03 14:23:01',NULL,5.127761,52.089142,'Nieuwegracht-Oost, 3512 Utrecht, Nederland','Van straat','Naar straat','motorcycle','2017-03-03','15:01:00',NULL,NULL,NULL),
	(113,NULL,3,54,2,NULL,'Nee',NULL,NULL,'2017-03-03 15:28:45','2017-03-03 15:29:00',NULL,5.907221,51.985916,'Bothaplein 9, 6814 AJ Arnhem, Nederland','A','B','motorcycle','2017-03-03','16:26:00',NULL,NULL,1),
	(114,NULL,3,54,2,NULL,'',NULL,NULL,'2017-03-03 15:50:42','2017-03-03 15:51:01',NULL,5.907237,51.985920,'Bothaplein 9, 6814 AJ Arnhem, Nederland','A','B','car','2017-03-03','16:50:00',NULL,NULL,NULL),
	(115,NULL,3,54,2,NULL,'',NULL,NULL,'2017-03-03 15:51:27','2017-03-03 15:52:00',NULL,5.907206,51.985928,'Bothaplein 9, 6814 AJ Arnhem, Nederland','A','B',NULL,'2017-03-03','16:51:00',NULL,NULL,NULL),
	(116,NULL,3,54,2,NULL,'',NULL,NULL,'2017-03-03 15:52:46','2017-03-03 15:53:00',NULL,5.907252,51.985935,'Bothaplein 9, 6814 AJ Arnhem, Nederland','A','B','motorcycle','2017-03-03','16:51:00',NULL,NULL,NULL),
	(117,NULL,3,54,2,NULL,'',NULL,NULL,'2017-03-03 15:56:50','2017-03-03 15:57:00',NULL,5.906980,51.986023,'Arnhem, Bothaplein, 6814 AM Arnhem, Nederland','A','B',NULL,'2017-03-03','16:56:00',NULL,NULL,NULL),
	(118,NULL,3,54,4,NULL,'',NULL,NULL,'2017-03-03 15:58:08','2017-03-03 15:59:01',NULL,5.633415,52.032127,'Ede, Nederland','A','B','motorcycle','2017-03-03','16:56:00',NULL,NULL,NULL),
	(119,NULL,3,54,3,NULL,'',NULL,NULL,'2017-03-03 16:01:15','2017-03-03 16:13:29',NULL,5.622024,52.176174,'3781 Voorthuizen, Nederland','A','B','car','2017-03-03','17:00:00',NULL,NULL,0),
	(120,NULL,3,54,2,NULL,'',NULL,NULL,'2017-03-03 16:30:08','2017-03-03 16:33:26',NULL,5.672880,51.981834,'Wageningen, Hoevestein, 6708 PE Wageningen, Nederland','A','B','car','2017-03-03','17:29:00',NULL,NULL,0),
	(121,NULL,3,61,3,NULL,'Normaal gaat het wel goed',NULL,NULL,'2017-03-08 08:58:43','2017-03-08 08:59:01',NULL,5.361956,51.703697,'Rosmalen, Nederland','Molenstraat','Graafsebaan','motorcycle','2017-03-08','09:56:00',NULL,NULL,1),
	(122,NULL,3,54,4,NULL,'',NULL,NULL,'2017-03-08 10:20:50','2017-03-08 10:21:06',NULL,5.635959,52.112888,'6741 Lunteren, Nederland','Ruitenbeekweg','Oude Bisschopsweg',NULL,'2017-03-08','11:15:00',NULL,NULL,1),
	(123,NULL,3,62,2,NULL,'Test 4',NULL,NULL,'2017-03-08 15:46:16','2017-03-08 15:47:00',NULL,5.360607,51.705627,'Rosmalen, Nederland','Molenstraat','Graafsebaan','motorcycle','2017-03-08','16:44:00',NULL,NULL,NULL),
	(124,NULL,3,63,2,NULL,'',NULL,NULL,'2017-03-10 08:42:45','2017-03-10 08:43:01',NULL,5.857108,52.002728,'Westelijk van Schaarsbergen, Arnhem, Nederland','amsterdamseweg','lawick','car','2017-03-10','09:41:00',NULL,NULL,NULL),
	(125,20,5,2,2,NULL,'',NULL,76,'2017-03-24 09:26:08','2017-03-24 09:58:09',NULL,5.361834,51.703861,'Rosmalen, Nederland','Graafsebaan','Molenstraat','car','2017-03-24','10:20:00',NULL,NULL,0),
	(126,NULL,3,62,2,NULL,'',NULL,NULL,'2017-04-05 08:06:03','2017-04-05 08:07:00',NULL,5.359371,51.703053,'Rosmalen, Graafsebaan, 5248 JT \'s-Hertogenbosch, Nederland','Graafsebaan','Molenstraat',NULL,'2017-04-05','10:00:00',NULL,NULL,NULL),
	(127,NULL,3,61,3,NULL,'Test 3',NULL,NULL,'2017-04-05 08:18:01','2017-04-05 08:18:05',NULL,5.364202,51.701141,'Rosmalen, Nederland','Molenstraat','Graafsebaan','car','2017-04-05','10:15:00',NULL,NULL,NULL),
	(128,NULL,3,64,5,NULL,'Dit was een test melding, je kunt \'m negeren.',NULL,NULL,'2017-04-05 08:35:55','2017-04-05 08:50:38',NULL,4.760797,52.956280,'Den Helder, Station, 1782 HB Den Helder, Nederland','Parallelweg','Middenweg','car','2017-04-05','10:33:00',NULL,NULL,0),
	(129,NULL,3,41,3,NULL,'',NULL,NULL,'2017-04-06 08:25:49','2017-04-06 08:26:05',NULL,5.799913,53.201233,'Leeuwarden, Nederland','Oude Oosterstraat','Tweebaksmarkt',NULL,'2017-04-06','10:18:00',NULL,NULL,NULL),
	(130,NULL,3,64,4,NULL,'',NULL,NULL,'2017-04-11 07:18:01','2017-04-11 07:18:06',NULL,5.907289,51.985973,'Arnhem, Bothaplein, 6814 AM Arnhem, Nederland','','',NULL,'2017-04-11','09:16:00',NULL,NULL,NULL),
	(131,NULL,3,64,4,NULL,'',NULL,NULL,'2017-04-11 07:25:52','2017-04-11 07:26:06',NULL,5.907322,51.985970,'Arnhem, Bothaplein, 6814 AM Arnhem, Nederland','Van straat','Naar straat','car','2017-04-11','09:23:00',NULL,NULL,NULL),
	(132,NULL,3,65,2,NULL,'',NULL,NULL,'2017-04-11 09:16:12','2017-04-11 09:17:07',NULL,5.900428,51.983448,'Stationsplein, Arnhem, Nederland','de la reijestraat','bakkerstraat','car','2017-04-11','11:13:00',NULL,NULL,NULL),
	(133,18,9,2,2,NULL,'TEST of de melding aan een VRI gekoppeld wordt.',65,65,'2017-04-13 09:12:48','2017-04-14 07:48:02',NULL,5.907258,51.986046,'a','b','c',NULL,'2017-04-14','11:30:00',NULL,NULL,0),
	(134,NULL,3,62,3,NULL,'',NULL,NULL,'2017-04-18 08:12:17','2017-04-18 08:13:05',NULL,5.275151,51.690105,'Schutskamp, \'s-Hertogenbosch, Nederland','Rondweg','Schutskamp','car','2017-04-18','10:04:00',NULL,NULL,NULL);

/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rights`;

CREATE TABLE `rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `code` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `code_2` (`code`),
  KEY `fk_rights_users1_idx1` (`created_by`),
  KEY `fk_rights_users2_idx1` (`modified_by`)
) ENGINE=InnoDB AUTO_INCREMENT=767 DEFAULT CHARSET=utf8;

LOCK TABLES `rights` WRITE;
/*!40000 ALTER TABLE `rights` DISABLE KEYS */;

INSERT INTO `rights` (`id`, `name`, `code`, `created_at`, `modified_at`, `created_by`, `modified_by`, `deleted`)
VALUES
	(618,'reports_dashboard','reports_dashboard',NULL,NULL,NULL,NULL,NULL),
	(619,'roles_index','roles_index',NULL,NULL,NULL,NULL,NULL),
	(620,'users_index','users_index',NULL,NULL,NULL,NULL,NULL),
	(621,'faqquestions_add','faqquestions_add',NULL,NULL,NULL,NULL,NULL),
	(622,'faqquestions_index','faqquestions_index',NULL,NULL,NULL,NULL,NULL),
	(623,'rights_index','rights_index',NULL,NULL,NULL,NULL,NULL),
	(624,'faqquestions_edit','faqquestions_edit',NULL,NULL,NULL,NULL,NULL),
	(625,'roles_edit','roles_edit',NULL,NULL,NULL,NULL,NULL),
	(626,'blogarticles_index','blogarticles_index',NULL,NULL,NULL,NULL,NULL),
	(627,'blogarticles_add','blogarticles_add',NULL,NULL,NULL,NULL,NULL),
	(628,'places_index','places_index',NULL,NULL,NULL,NULL,NULL),
	(629,'places_edit','places_edit',NULL,NULL,NULL,NULL,NULL),
	(630,'places_add','places_add',NULL,NULL,NULL,NULL,NULL),
	(631,'councils_index','councils_index',NULL,NULL,NULL,NULL,NULL),
	(632,'councils_add','councils_add',NULL,NULL,NULL,NULL,NULL),
	(633,'provinces_index','provinces_index',NULL,NULL,NULL,NULL,NULL),
	(634,'provinces_add','provinces_add',NULL,NULL,NULL,NULL,NULL),
	(635,'blogcategories_index','blogcategories_index',NULL,NULL,NULL,NULL,NULL),
	(636,'devices_index','devices_index',NULL,NULL,NULL,NULL,NULL),
	(637,'devicetypes_index','devicetypes_index',NULL,NULL,NULL,NULL,NULL),
	(638,'devicetypes_add','devicetypes_add',NULL,NULL,NULL,NULL,NULL),
	(639,'devicetypes_edit','devicetypes_edit',NULL,NULL,NULL,NULL,NULL),
	(640,'devices_edit','devices_edit',NULL,NULL,NULL,NULL,NULL),
	(641,'suppliers_index','suppliers_index',NULL,NULL,NULL,NULL,NULL),
	(642,'suppliers_add','suppliers_add',NULL,NULL,NULL,NULL,NULL),
	(643,'faqcategories_index','faqcategories_index',NULL,NULL,NULL,NULL,NULL),
	(644,'faqcategories_add','faqcategories_add',NULL,NULL,NULL,NULL,NULL),
	(645,'reports_add','reports_add',NULL,NULL,NULL,NULL,NULL),
	(646,'reportstatuses_index','reportstatuses_index',NULL,NULL,NULL,NULL,NULL),
	(647,'reports_index','reports_index',NULL,NULL,NULL,NULL,NULL),
	(648,'reporttypes_add','reporttypes_add',NULL,NULL,NULL,NULL,NULL),
	(649,'reporttypes_index','reporttypes_index',NULL,NULL,NULL,NULL,NULL),
	(650,'reporters_index','reporters_index',NULL,NULL,NULL,NULL,NULL),
	(651,'locations_index','locations_index',NULL,NULL,NULL,NULL,NULL),
	(652,'managers_index','managers_index',NULL,NULL,NULL,NULL,NULL),
	(653,'blogarticles_delete','blogarticles_delete',NULL,NULL,NULL,NULL,NULL),
	(654,'provinces_delete','provinces_delete',NULL,NULL,NULL,NULL,NULL),
	(655,'places_delete','places_delete',NULL,NULL,NULL,NULL,NULL),
	(656,'councils_delete','councils_delete',NULL,NULL,NULL,NULL,NULL),
	(657,'managers_add','managers_add',NULL,NULL,NULL,NULL,NULL),
	(658,'devices_add','devices_add',NULL,NULL,NULL,NULL,NULL),
	(659,'devices_delete','devices_delete',NULL,NULL,NULL,NULL,NULL),
	(660,'managers_edit','managers_edit',NULL,NULL,NULL,NULL,NULL),
	(661,'reporters_add','reporters_add',NULL,NULL,NULL,NULL,NULL),
	(662,'reportstatuses_add','reportstatuses_add',NULL,NULL,NULL,NULL,NULL),
	(663,'reports_edit','reports_edit',NULL,NULL,NULL,NULL,NULL),
	(664,'locations_add','locations_add',NULL,NULL,NULL,NULL,NULL),
	(665,'blogcategories_add','blogcategories_add',NULL,NULL,NULL,NULL,NULL),
	(666,'faqcategories_edit','faqcategories_edit',NULL,NULL,NULL,NULL,NULL),
	(667,'blogarticles_edit','blogarticles_edit',NULL,NULL,NULL,NULL,NULL),
	(668,'users_edit','users_edit',NULL,NULL,NULL,NULL,NULL),
	(669,'users_view','users_view',NULL,NULL,NULL,NULL,NULL),
	(670,'roles_add','roles_add',NULL,NULL,NULL,NULL,NULL),
	(671,'roles_delete','roles_delete',NULL,NULL,NULL,NULL,NULL),
	(672,'blogcategories_delete','blogcategories_delete',NULL,NULL,NULL,NULL,NULL),
	(673,'blogcategories_edit','blogcategories_edit',NULL,NULL,NULL,NULL,NULL),
	(674,'users_add','users_add',NULL,NULL,NULL,NULL,NULL),
	(675,'reportstatuses_edit','reportstatuses_edit',NULL,NULL,NULL,NULL,NULL),
	(676,'pages_index','pages_index',NULL,NULL,NULL,NULL,NULL),
	(677,'pages_add','pages_add',NULL,NULL,NULL,NULL,NULL),
	(678,'pages_edit','pages_edit',NULL,NULL,NULL,NULL,NULL),
	(679,'templates_index','templates_index',NULL,NULL,NULL,NULL,NULL),
	(680,'templates_add','templates_add',NULL,NULL,NULL,NULL,NULL),
	(681,'pages_delete','pages_delete',NULL,NULL,NULL,NULL,NULL),
	(682,'templates_edit','templates_edit',NULL,NULL,NULL,NULL,NULL),
	(683,'pages_page_blocks','pages_page_blocks',NULL,NULL,NULL,NULL,NULL),
	(684,'users_delete','users_delete',NULL,NULL,NULL,NULL,NULL),
	(685,'reporters_edit','reporters_edit',NULL,NULL,NULL,NULL,NULL),
	(686,'provinces_edit','provinces_edit',NULL,NULL,NULL,NULL,NULL),
	(687,'suppliers_edit','suppliers_edit',NULL,NULL,NULL,NULL,NULL),
	(688,'councils_edit','councils_edit',NULL,NULL,NULL,NULL,NULL),
	(689,'reporttypes_edit','reporttypes_edit',NULL,NULL,NULL,NULL,NULL),
	(690,'users_change_password','users_change_password',NULL,NULL,NULL,NULL,NULL),
	(691,'reports_addjson','reports_addjson',NULL,NULL,NULL,NULL,NULL),
	(692,'widgets_index','widgets_index',NULL,NULL,NULL,NULL,NULL),
	(693,'users_managerdashboard','users_managerdashboard',NULL,NULL,NULL,NULL,NULL),
	(694,'users_managersdashboard','users_managersdashboard',NULL,NULL,NULL,NULL,NULL),
	(695,'locations_edit','locations_edit',NULL,NULL,NULL,NULL,NULL),
	(696,'reports_delete','reports_delete',NULL,NULL,NULL,NULL,NULL),
	(697,'reporttypes_delete','reporttypes_delete',NULL,NULL,NULL,NULL,NULL),
	(698,'reportstatuses_delete','reportstatuses_delete',NULL,NULL,NULL,NULL,NULL),
	(699,'pages_dynamic_blocks','pages_dynamic_blocks',NULL,NULL,NULL,NULL,NULL),
	(700,'users_account','users_account',NULL,NULL,NULL,NULL,NULL),
	(701,'users_dashboard','users_dashboard',NULL,NULL,NULL,NULL,NULL),
	(702,'users_admindashboard','users_admindashboard',NULL,NULL,NULL,NULL,NULL),
	(703,'menus_index','menus_index',NULL,NULL,NULL,NULL,NULL),
	(704,'menus_add','menus_add',NULL,NULL,NULL,NULL,NULL),
	(705,'menuitems_index','menuitems_index',NULL,NULL,NULL,NULL,NULL),
	(706,'menuitems_add','menuitems_add',NULL,NULL,NULL,NULL,NULL),
	(707,'menuitems_edit','menuitems_edit',NULL,NULL,NULL,NULL,NULL),
	(708,'menuitems_delete','menuitems_delete',NULL,NULL,NULL,NULL,NULL),
	(709,'templates_delete','templates_delete',NULL,NULL,NULL,NULL,NULL),
	(711,'my_account','my_account',NULL,NULL,NULL,NULL,NULL),
	(712,'change_password','change_password',NULL,NULL,NULL,NULL,NULL),
	(713,'logout','logout',NULL,NULL,NULL,NULL,NULL),
	(714,'dashboard','dashboard',NULL,NULL,NULL,NULL,NULL),
	(715,'reports_getreportsfromday','reports_getreportsfromday',NULL,NULL,NULL,NULL,NULL),
	(716,'faquserquestions_index','faquserquestions_index',NULL,NULL,NULL,NULL,NULL),
	(717,'faquserquestions_add','faquserquestions_add',NULL,NULL,NULL,NULL,NULL),
	(718,'faquserquestions_edit','faquserquestions_edit',NULL,NULL,NULL,NULL,NULL),
	(719,'faquserquestions_respond','faquserquestions_respond',NULL,NULL,NULL,NULL,NULL),
	(720,'contact_edit','contact_edit',NULL,NULL,NULL,NULL,NULL),
	(721,'contact_index','contact_index',NULL,NULL,NULL,NULL,NULL),
	(722,'contact_respond','contact_respond',NULL,NULL,NULL,NULL,NULL),
	(724,'messages_index','messages_index',NULL,NULL,NULL,NULL,NULL),
	(725,'reportresponses_respond','report_responses_respond',NULL,NULL,NULL,NULL,NULL),
	(726,'reportresponses_index','report_responses_index',NULL,NULL,NULL,NULL,NULL),
	(727,'managers_dashboard','managers_dashboard',NULL,NULL,NULL,NULL,NULL),
	(728,'reportresponses_massrespond','report_responses_massrespond',NULL,NULL,NULL,NULL,NULL),
	(729,'admin_dashboard','admin_dashboard',NULL,NULL,NULL,NULL,NULL),
	(743,'reportresponses_massrespond','reportresponses_massrespond',NULL,NULL,NULL,NULL,NULL),
	(744,'reportresponses_respond','reportresponses_respond',NULL,NULL,NULL,NULL,NULL),
	(745,'users_savetablesettings','users_savetablesettings',NULL,NULL,NULL,NULL,NULL),
	(746,'pages_template_blocks','pages_template_blocks',NULL,NULL,NULL,NULL,NULL),
	(747,'notifications_index','notifications_index',NULL,NULL,NULL,NULL,NULL),
	(748,'notificationtemplates_index','notificationtemplates_index',NULL,NULL,NULL,NULL,NULL),
	(749,'notificationtemplates_add','notificationtemplates_add',NULL,NULL,NULL,NULL,NULL),
	(750,'chats_index','chats_index',NULL,NULL,NULL,NULL,NULL),
	(751,'chats_edit','chats_edit',NULL,NULL,NULL,NULL,NULL),
	(752,'notifications_delete_all','notifications_delete_all',NULL,NULL,NULL,NULL,NULL),
	(753,'notificationtemplates_edit','notificationtemplates_edit',NULL,NULL,NULL,NULL,NULL),
	(754,'chatmessages_index','chatmessages_index',NULL,NULL,NULL,NULL,NULL),
	(755,'chatmessages_view','chatmessages_view',NULL,NULL,NULL,NULL,NULL),
	(756,'chatmessages_add','chatmessages_add',NULL,NULL,NULL,NULL,NULL),
	(757,'chatmessages_edit','chatmessages_edit',NULL,NULL,NULL,NULL,NULL),
	(758,'chatmessages_delete','chatmessages_delete',NULL,NULL,NULL,NULL,NULL),
	(759,'notifications_delete','notifications_delete',NULL,NULL,NULL,NULL,NULL),
	(760,'menus_edit','menus_edit',NULL,NULL,NULL,NULL,NULL),
	(761,'devicetypes_delete','devicetypes_delete',NULL,NULL,NULL,NULL,NULL),
	(762,'reportresponses_index','reportresponses_index',NULL,NULL,NULL,NULL,NULL),
	(763,'reportresponsepresets_index','reportresponsepresets_index',NULL,NULL,NULL,NULL,NULL),
	(764,'reportresponsepresets_add','reportresponsepresets_add',NULL,NULL,NULL,NULL,NULL),
	(765,'reportresponsepresets_edit','reportresponsepresets_edit',NULL,NULL,NULL,NULL,NULL),
	(766,'reportresponsepresets_get','reportresponsepresets_get',NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `rights` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rights_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rights_roles`;

CREATE TABLE `rights_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `right_id` int(11) NOT NULL,
  `allow` tinyint(1) DEFAULT NULL,
  `deny` tinyint(1) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_right_roles_roles1_idx` (`role_id`),
  KEY `fk_right_roles_rights1_idx` (`right_id`),
  CONSTRAINT `fk_right_roles_rights1` FOREIGN KEY (`right_id`) REFERENCES `rights` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_right_roles_roles1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2387 DEFAULT CHARSET=utf8;

LOCK TABLES `rights_roles` WRITE;
/*!40000 ALTER TABLE `rights_roles` DISABLE KEYS */;

INSERT INTO `rights_roles` (`id`, `role_id`, `right_id`, `allow`, `deny`, `deleted`)
VALUES
	(2050,1,618,1,NULL,NULL),
	(2051,1,619,1,NULL,NULL),
	(2052,1,620,1,NULL,NULL),
	(2053,1,621,1,NULL,NULL),
	(2054,1,622,1,NULL,NULL),
	(2055,1,623,1,NULL,NULL),
	(2056,1,624,1,NULL,NULL),
	(2057,1,625,1,NULL,NULL),
	(2058,1,626,1,NULL,NULL),
	(2059,1,627,1,NULL,NULL),
	(2060,1,628,1,NULL,NULL),
	(2061,1,629,1,NULL,NULL),
	(2062,1,630,1,NULL,NULL),
	(2063,1,631,1,NULL,NULL),
	(2064,1,632,1,NULL,NULL),
	(2065,1,633,1,NULL,NULL),
	(2066,1,634,1,NULL,NULL),
	(2067,1,635,1,NULL,NULL),
	(2068,1,636,1,NULL,NULL),
	(2069,1,637,1,NULL,NULL),
	(2070,1,638,1,NULL,NULL),
	(2071,1,639,1,NULL,NULL),
	(2072,1,640,1,NULL,NULL),
	(2073,1,641,1,NULL,NULL),
	(2074,1,642,1,NULL,NULL),
	(2075,1,643,1,NULL,NULL),
	(2076,1,644,1,NULL,NULL),
	(2077,1,645,1,NULL,NULL),
	(2078,1,646,1,NULL,NULL),
	(2079,1,647,1,NULL,NULL),
	(2080,1,648,1,NULL,NULL),
	(2081,1,649,1,NULL,NULL),
	(2082,1,650,1,NULL,NULL),
	(2083,1,651,1,NULL,NULL),
	(2084,1,652,1,NULL,NULL),
	(2085,1,653,1,NULL,NULL),
	(2086,1,654,1,NULL,NULL),
	(2087,1,655,1,NULL,NULL),
	(2088,1,656,1,NULL,NULL),
	(2089,1,657,1,NULL,NULL),
	(2090,1,658,1,NULL,NULL),
	(2091,1,659,1,NULL,NULL),
	(2092,1,660,1,NULL,NULL),
	(2093,1,661,1,NULL,NULL),
	(2094,1,662,1,NULL,NULL),
	(2095,1,663,1,NULL,NULL),
	(2096,1,664,1,NULL,NULL),
	(2097,1,665,1,NULL,NULL),
	(2098,1,666,1,NULL,NULL),
	(2099,1,667,1,NULL,NULL),
	(2100,1,668,1,NULL,NULL),
	(2101,1,669,1,NULL,NULL),
	(2102,1,670,1,NULL,NULL),
	(2156,1,671,1,NULL,NULL),
	(2157,1,672,1,NULL,NULL),
	(2158,1,673,1,NULL,NULL),
	(2159,1,674,1,NULL,NULL),
	(2160,1,675,1,NULL,NULL),
	(2161,1,676,1,NULL,NULL),
	(2162,1,677,1,NULL,NULL),
	(2163,1,678,1,NULL,NULL),
	(2164,1,679,1,NULL,NULL),
	(2165,1,680,1,NULL,NULL),
	(2166,1,681,1,NULL,NULL),
	(2167,1,682,1,NULL,NULL),
	(2168,1,683,1,NULL,NULL),
	(2169,1,684,1,NULL,NULL),
	(2170,1,685,1,NULL,NULL),
	(2171,1,686,1,NULL,NULL),
	(2172,1,687,1,NULL,NULL),
	(2173,1,688,1,NULL,NULL),
	(2174,1,689,1,NULL,NULL),
	(2175,2,627,NULL,1,NULL),
	(2176,2,653,NULL,1,NULL),
	(2177,2,667,NULL,1,NULL),
	(2178,2,626,NULL,1,NULL),
	(2179,2,665,NULL,1,NULL),
	(2180,2,672,NULL,1,NULL),
	(2181,2,673,NULL,1,NULL),
	(2182,2,635,NULL,1,NULL),
	(2183,2,632,NULL,1,NULL),
	(2184,2,656,NULL,1,NULL),
	(2185,2,688,NULL,1,NULL),
	(2186,2,631,NULL,1,NULL),
	(2187,2,658,1,NULL,NULL),
	(2188,2,659,1,NULL,NULL),
	(2189,2,640,1,NULL,NULL),
	(2190,2,636,1,NULL,NULL),
	(2191,2,638,NULL,1,NULL),
	(2192,2,639,NULL,1,NULL),
	(2193,2,637,NULL,1,NULL),
	(2194,2,644,NULL,1,NULL),
	(2195,2,666,NULL,1,NULL),
	(2196,2,643,NULL,1,NULL),
	(2197,2,621,NULL,1,NULL),
	(2198,2,624,NULL,1,NULL),
	(2199,2,622,NULL,1,NULL),
	(2200,2,664,NULL,1,NULL),
	(2201,2,651,NULL,1,NULL),
	(2202,2,657,NULL,1,NULL),
	(2203,2,660,NULL,1,NULL),
	(2204,2,652,NULL,1,NULL),
	(2205,2,677,NULL,1,NULL),
	(2206,2,681,NULL,1,NULL),
	(2207,2,678,NULL,1,NULL),
	(2208,2,676,NULL,1,NULL),
	(2209,2,683,NULL,1,NULL),
	(2210,2,630,NULL,1,NULL),
	(2211,2,655,NULL,1,NULL),
	(2212,2,629,NULL,1,NULL),
	(2213,2,628,NULL,1,NULL),
	(2214,2,634,NULL,1,NULL),
	(2215,2,654,NULL,1,NULL),
	(2216,2,686,NULL,1,NULL),
	(2217,2,633,NULL,1,NULL),
	(2218,2,661,NULL,1,NULL),
	(2219,2,685,NULL,1,NULL),
	(2220,2,650,NULL,1,NULL),
	(2221,2,662,NULL,1,NULL),
	(2222,2,675,NULL,1,NULL),
	(2223,2,646,NULL,1,NULL),
	(2224,2,645,1,NULL,NULL),
	(2225,2,618,1,NULL,NULL),
	(2226,2,663,1,NULL,NULL),
	(2227,2,647,1,NULL,NULL),
	(2228,2,648,NULL,1,NULL),
	(2229,2,689,NULL,1,NULL),
	(2230,2,649,NULL,1,NULL),
	(2231,2,623,NULL,1,NULL),
	(2232,2,670,NULL,1,NULL),
	(2233,2,671,NULL,1,NULL),
	(2234,2,625,NULL,1,NULL),
	(2235,2,619,NULL,1,NULL),
	(2236,2,642,NULL,1,NULL),
	(2237,2,687,NULL,1,NULL),
	(2238,2,641,NULL,1,NULL),
	(2239,2,680,NULL,1,NULL),
	(2240,2,682,NULL,1,NULL),
	(2241,2,679,NULL,1,NULL),
	(2242,2,674,NULL,1,NULL),
	(2243,2,684,NULL,1,NULL),
	(2244,2,668,1,NULL,NULL),
	(2245,2,620,NULL,1,NULL),
	(2246,2,669,1,NULL,NULL),
	(2247,1,690,1,NULL,NULL),
	(2248,1,691,1,NULL,NULL),
	(2249,1,692,1,NULL,NULL),
	(2250,1,693,1,NULL,NULL),
	(2251,1,694,1,NULL,NULL),
	(2252,2,691,1,NULL,NULL),
	(2253,2,690,1,NULL,NULL),
	(2254,2,693,1,NULL,NULL),
	(2255,2,694,1,NULL,NULL),
	(2256,2,692,1,NULL,NULL),
	(2257,1,695,1,NULL,NULL),
	(2258,1,696,1,NULL,NULL),
	(2259,1,697,1,NULL,NULL),
	(2260,1,698,1,NULL,NULL),
	(2261,1,699,1,NULL,NULL),
	(2262,1,700,1,NULL,NULL),
	(2263,2,695,NULL,1,NULL),
	(2264,2,699,NULL,1,NULL),
	(2265,2,698,NULL,1,NULL),
	(2266,2,696,1,NULL,NULL),
	(2267,2,697,NULL,NULL,NULL),
	(2268,2,700,1,NULL,NULL),
	(2269,2,701,1,NULL,NULL),
	(2270,1,701,1,NULL,NULL),
	(2271,1,702,1,NULL,NULL),
	(2272,1,703,1,NULL,NULL),
	(2273,1,704,1,NULL,NULL),
	(2274,1,705,1,NULL,NULL),
	(2275,1,706,1,NULL,NULL),
	(2276,2,706,NULL,1,NULL),
	(2277,2,705,NULL,1,NULL),
	(2278,2,704,NULL,1,NULL),
	(2279,2,703,NULL,1,NULL),
	(2280,2,702,NULL,1,NULL),
	(2281,1,707,1,NULL,NULL),
	(2282,1,708,1,NULL,NULL),
	(2283,1,709,1,NULL,NULL),
	(2287,1,711,1,NULL,NULL),
	(2291,1,712,1,NULL,NULL),
	(2292,1,713,1,NULL,NULL),
	(2293,1,714,1,NULL,NULL),
	(2294,1,715,1,NULL,NULL),
	(2295,1,716,1,NULL,NULL),
	(2296,1,717,1,NULL,NULL),
	(2297,1,718,1,NULL,NULL),
	(2298,1,719,1,NULL,NULL),
	(2299,1,720,1,NULL,NULL),
	(2300,1,721,1,NULL,NULL),
	(2301,1,722,1,NULL,NULL),
	(2302,1,724,1,NULL,NULL),
	(2303,2,712,1,NULL,NULL),
	(2304,2,720,NULL,1,NULL),
	(2305,2,721,NULL,1,NULL),
	(2306,2,722,NULL,1,NULL),
	(2307,2,714,1,NULL,NULL),
	(2308,2,717,NULL,1,NULL),
	(2309,2,718,NULL,1,NULL),
	(2310,2,716,NULL,1,NULL),
	(2311,2,719,NULL,1,NULL),
	(2312,2,713,1,NULL,NULL),
	(2313,2,708,NULL,1,NULL),
	(2314,2,707,NULL,1,NULL),
	(2315,2,724,NULL,1,NULL),
	(2316,2,711,1,NULL,NULL),
	(2317,2,725,1,NULL,NULL),
	(2318,2,715,1,NULL,NULL),
	(2319,2,709,NULL,1,NULL),
	(2322,2,727,1,NULL,NULL),
	(2323,1,728,1,NULL,NULL),
	(2325,1,729,1,NULL,NULL),
	(2326,2,729,NULL,1,NULL),
	(2327,1,725,1,NULL,NULL),
	(2328,2,725,1,NULL,NULL),
	(2329,2,728,1,NULL,NULL),
	(2331,1,743,1,NULL,NULL),
	(2332,1,727,NULL,NULL,NULL),
	(2333,1,726,1,NULL,NULL),
	(2334,2,743,1,NULL,NULL),
	(2335,2,726,NULL,1,NULL),
	(2336,2,725,1,NULL,NULL),
	(2337,2,725,1,NULL,NULL),
	(2338,2,725,1,NULL,NULL),
	(2339,1,744,1,0,NULL),
	(2340,1,745,1,0,NULL),
	(2341,1,746,1,0,NULL),
	(2342,1,747,1,0,NULL),
	(2343,1,748,1,0,NULL),
	(2344,1,749,1,0,NULL),
	(2345,1,750,1,0,NULL),
	(2346,1,751,1,0,NULL),
	(2347,1,752,1,0,NULL),
	(2348,1,753,1,0,NULL),
	(2349,1,754,1,0,NULL),
	(2350,1,755,1,0,NULL),
	(2351,1,756,1,0,NULL),
	(2352,1,757,1,0,NULL),
	(2353,2,756,1,NULL,NULL),
	(2354,2,757,1,NULL,NULL),
	(2355,2,754,1,NULL,NULL),
	(2356,2,755,NULL,NULL,NULL),
	(2357,2,751,NULL,NULL,NULL),
	(2358,2,750,NULL,NULL,NULL),
	(2359,2,752,1,NULL,NULL),
	(2360,2,747,1,NULL,NULL),
	(2361,2,749,NULL,NULL,NULL),
	(2362,2,753,NULL,NULL,NULL),
	(2363,2,748,NULL,NULL,NULL),
	(2364,2,746,NULL,NULL,NULL),
	(2365,2,744,1,NULL,NULL),
	(2366,2,725,1,NULL,NULL),
	(2367,2,745,NULL,NULL,NULL),
	(2368,2,725,1,NULL,NULL),
	(2369,1,758,1,0,NULL),
	(2370,1,759,1,0,NULL),
	(2371,2,758,NULL,NULL,NULL),
	(2372,2,759,1,NULL,NULL),
	(2373,2,725,1,NULL,NULL),
	(2374,1,760,1,0,NULL),
	(2375,1,761,1,0,NULL),
	(2376,1,762,1,0,NULL),
	(2377,2,761,NULL,NULL,NULL),
	(2378,2,760,NULL,NULL,NULL),
	(2379,2,762,NULL,NULL,NULL),
	(2380,2,725,1,NULL,NULL),
	(2381,2,725,1,NULL,NULL),
	(2382,1,763,1,0,NULL),
	(2383,1,764,1,0,NULL),
	(2384,1,765,1,0,NULL),
	(2385,1,766,1,0,NULL),
	(2386,2,724,1,0,NULL);

/*!40000 ALTER TABLE `rights_roles` ENABLE KEYS */;
UNLOCK TABLES;

# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_roles_users1_idx1` (`created_by`),
  KEY `fk_roles_users2_idx1` (`modified_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `created_at`, `modified_at`, `created_by`, `modified_by`, `deleted`)
VALUES
	(1,'Beheerder',NULL,NULL,NULL,NULL,NULL),
	(2,'Wegbeheerder',NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings_configurations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings_configurations`;

CREATE TABLE `settings_configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `value` text,
  `description` text,
  `type` varchar(50) NOT NULL DEFAULT '',
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `weight` int(11) NOT NULL DEFAULT '0',
  `autoload` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `settings_configurations` WRITE;
/*!40000 ALTER TABLE `settings_configurations` DISABLE KEYS */;

INSERT INTO `settings_configurations` (`id`, `name`, `value`, `description`, `type`, `editable`, `weight`, `autoload`, `created`, `modified`)
VALUES
	(1,'App.Mongo.DB.Server','mongodb://localhost:27017',NULL,'',1,0,1,'2017-01-18 08:07:29','2017-01-19 10:57:05'),
	(2,'App.Mongo.DB.Username','test',NULL,'',1,0,1,'2017-01-18 08:07:29','2017-01-18 08:07:29'),
	(3,'App.Mongo.DB.QueueStatus','Default Value',NULL,'',1,0,1,'2017-01-19 10:54:03','2017-01-19 10:54:03'),
	(4,'App.Email.DefaultSenderName','Verkeerslicht',NULL,'',1,0,1,'2017-02-03 15:14:06','2017-02-03 15:14:06'),
	(5,'App.Email.DefaultSenderEmail','aart@sumedia.nl',NULL,'',1,0,1,'2017-02-03 15:14:06','2017-02-03 15:14:06');

/*!40000 ALTER TABLE `settings_configurations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings_phinxlog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings_phinxlog`;

CREATE TABLE `settings_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `settings_phinxlog` WRITE;
/*!40000 ALTER TABLE `settings_phinxlog` DISABLE KEYS */;

INSERT INTO `settings_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`)
VALUES
	(20150126111319,'SettingsInitial','2017-01-18 08:05:28','2017-01-18 08:05:28',0);

/*!40000 ALTER TABLE `settings_phinxlog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table suppliers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `suppliers_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `suppliers_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;

INSERT INTO `suppliers` (`id`, `name`, `description`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,'Test Supplier','',65,65,'2017-01-17 10:33:45','2017-01-17 10:33:45',NULL);

/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table template_blocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `template_blocks`;

CREATE TABLE `template_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL DEFAULT '',
  `type` varchar(50) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `template_id` (`template_id`),
  KEY `code` (`code`),
  CONSTRAINT `template_blocks_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

LOCK TABLES `template_blocks` WRITE;
/*!40000 ALTER TABLE `template_blocks` DISABLE KEYS */;

INSERT INTO `template_blocks` (`id`, `template_id`, `code`, `type`, `name`, `deleted`)
VALUES
	(4,3,'Hoofdtekst','wysiwyg','Hoofdtekst',NULL),
	(7,2,'Title','text','Titel van pagina',NULL),
	(8,2,'Subtitle','wysiwyg','Inleiding van pagina',NULL),
	(9,2,'BTN_CTA_report','text','Knop: plaats melding',NULL),
	(10,2,'Persoon_naam','text','Naam van uitgelicht persoon',NULL),
	(11,2,'Persoon_titel','text','Ondertitel van uitgelicht persoon',NULL),
	(12,2,'Testimonials_titel','text','Titel van testimonials',NULL),
	(13,2,'Testimonial_1_text','wysiwyg','Text van testimonial 1',NULL),
	(14,2,'Testimonial_1_person','text','Persoon van testimonial 1',NULL),
	(15,2,'Testimonial_2_text','wysiwyg','Text van testimonial 2',NULL),
	(16,2,'Testimonial_2_person','text','Persoon van testimonial 2',NULL),
	(17,2,'Testimonial_3_text','wysiwyg','Text van testimonial 3',NULL),
	(18,2,'Testimonial_3_person','text','Persoon van testimonial 3',NULL),
	(23,4,'Apps_1_title','text','Apps_1_title',NULL),
	(24,4,'Apps_1_text','wysiwyg','Apps_1_text',NULL),
	(25,4,'Apps_banner_title','text','Apps_banner_title',NULL),
	(26,4,'Apps_2_title','text','Apps_2_title',NULL),
	(27,4,'Apps_2_text','wysiwyg','Apps_2_text',NULL),
	(28,1,'Home_1_title','text','Hoofd titel',NULL),
	(29,1,'Home_1_subtitle','wysiwyg','Hoofd omschrijving',NULL),
	(30,1,'Home_1_btn','text','Hoofd knop',NULL),
	(31,1,'Home_nav_about','text','Knop naar \"over\" (mobiel)',NULL),
	(32,1,'Home_nav_meldingen','text','Knop naar \"meldingen\" (mobiel)',NULL),
	(33,1,'Home_nav_apps','text','Knop naar \"apps\" (mobiel)',NULL),
	(34,1,'Home_btn_scroll','text','Tekst op scroll knop',NULL),
	(35,1,'Home_2_title','text','Blok 2: titel',NULL),
	(36,1,'Home_2_mobile_subtitle_1','text','Stap 1: titel (mobiel)',NULL),
	(37,1,'Home_2_mobile_text_1','wysiwyg','Stap 1: omschrijving (mobiel)',NULL),
	(38,1,'Home_2_mobile_subtitle_2','text','Stap 2: titel (mobiel)',NULL),
	(39,1,'Home_2_mobile_text_2','wysiwyg','Stap 2: omschrijving (mobiel)',NULL),
	(41,1,'Home_2_mobile_text_3','wysiwyg','Stap 3: omschrijving (mobiel)',NULL),
	(42,1,'Home_2_desktop_introtext','wysiwyg','Intro tekst (desktop)',NULL),
	(43,1,'Home_3_title','text','Blok 3: titel',NULL),
	(44,1,'Home_2_mobile_subtitle_3','text','Stap 3: titel (mobiel)',NULL),
	(46,6,'Title','text','Titel',NULL),
	(47,6,'Subtitle','text','Ondertitel',NULL),
	(48,6,'Content','wysiwyg','Inhoud',NULL),
	(49,9,'Subtitle','text','Subtitle',NULL),
	(50,9,'Content','wysiwyg','Content',NULL);

/*!40000 ALTER TABLE `template_blocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templates`;

CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `location` varchar(250) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;

INSERT INTO `templates` (`id`, `name`, `location`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(1,'Home','home.ctp',64,65,'2017-01-24 09:01:02',NULL,NULL),
	(2,'Over','about.ctp',68,65,'2017-01-24 11:38:06',NULL,NULL),
	(3,'Standaard','default.ctp',68,68,'2017-01-24 11:46:07',NULL,NULL),
	(4,'Apps','apps.ctp',68,65,'2017-01-27 13:36:08',NULL,NULL),
	(5,'Contact',NULL,65,65,'2017-02-13 10:44:41',NULL,'2017-02-13 10:46:07'),
	(6,'Disclaimer','disclaimer.ctp',65,65,'2017-02-24 10:38:02',NULL,NULL),
	(7,'Sitemap',NULL,69,69,'2017-04-19 13:34:58',NULL,'2017-04-19 13:57:32'),
	(8,'Sitemap',NULL,69,69,'2017-04-19 13:35:25',NULL,'2017-04-19 13:35:50'),
	(9,'Sitemap','sitemap.ctp',69,65,'2017-04-19 13:57:49',NULL,NULL);

/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `initials` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `settings` text,
  `picture` varchar(500) DEFAULT 'default_user_img.png',
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_roles_idx` (`role_id`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `users_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `users_ibfk_5` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `role_id`, `initials`, `firstname`, `middlename`, `lastname`, `username`, `phone`, `email`, `password`, `settings`, `picture`, `is_active`, `created_by`, `modified_by`, `created_at`, `modified_at`, `deleted`)
VALUES
	(64,1,NULL,'fabian',NULL,NULL,'fabian',NULL,'fabian@sumedia.nl','$2y$10$aHvZHNXagtpSs3QQyAaJIuUwH5oo36E6R6LmHBjAzCPUaJvKqpxze',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(65,1,'A','Aart',NULL,'Albers','aart','','aart@sumedia.nl','$2y$10$IMjn1hL29awCX4CwtGg4i.TUwMFHR/RgOkNj1AtsDRsKiL2QP9PKS','{\"contact\":{\"id\":{\"visible\":false},\"Contact__firstname\":{\"visible\":true},\"Contact__lastname\":{\"visible\":true},\"Contact__email\":{\"visible\":true},\"Contact__subject\":{\"visible\":true},\"Contact__is_answered\":{\"visible\":true},\"Contact__created_at\":{\"visible\":false},\"Contact__modified_at\":{\"visible\":false}}}','uploads/users/65/65.png',1,NULL,NULL,NULL,NULL,NULL),
	(68,1,NULL,'roel',NULL,NULL,'roel',NULL,'roel@sumedia.nl','$2y$10$A2FzRHk9y7mbWs4OjTAfo.xVrsOrCeJ5GWZ/aq5/KnehzUFgV2H1S',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(69,1,NULL,'willem',NULL,NULL,'willem',NULL,'willem@sumedia.nl','$2y$10$DIpefnnySu4wc4ccuIRTeus4Y40MdE8iC8TLoUswbwMtDoQN5S6BO',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(70,1,NULL,'simon',NULL,NULL,'simon',NULL,'simon@sumedia.nl','$2y$10$A8zHGjCs/G1mlbfpzb6oIuc7TgjqC0ExR6X79kjt.1r64GSTSjpXy',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
	(71,1,'A','Aart2',NULL,'Albers2','Aart2','0','aart+200@sumedia.nl','$2y$10$KO3NmrBIPhQOEoP8pxfAcOBsenVndx/dGMC8hnXl8rBE3jJm3NRFC','','uploads/users/71/71.png',1,NULL,NULL,NULL,NULL,NULL),
	(72,2,'WEG','Weg',NULL,'Heerder','wegbeheerder1','','aart+wegbeheerder@sumedia.nl','$2y$10$zc4VmpFR4at1f4L7i7r0C.OZVqe/Sm1ZnznrDgIm9v72EasRve4W2','','',1,NULL,NULL,NULL,NULL,NULL),
	(73,2,'W','Weg','B','Heerder','wegbeheerder2','','aart+wegbeheerder2@sumedia.nl','$2y$10$b0RxZzZ4WyvBRut4QeeJeOF57rell6Ui0uR5GbGwxstSjMqVUqtB2','','',1,NULL,NULL,'2017-02-16 09:34:00','2017-02-16 09:34:00','2017-02-16 09:34:00'),
	(74,2,'A','Aart',NULL,'Albers','aart_wegbeheerder','','aart+wegb@sumedia.nl','$2y$10$1dcfp/ZbOgaO3hYUbJhsqOZIQYGXztHOKM8fIzEqKKtrMJ1gOuECm','','uploads/users/74/blogpost_00900.png',1,NULL,NULL,NULL,NULL,NULL),
	(75,1,'','Beheerder',NULL,'Verkeerslicht.nl','Beheerder','','','$2y$10$W/t6H7wjObjmtrD4epot3.POe8owyjC6hRNOct071vq/gF4gKMxNu','','uploads/users/75/android-chrome-512x512.png',1,NULL,NULL,NULL,NULL,NULL),
	(76,2,'','Eric',NULL,'Gereweldinger','Eric','','guusvanderburgt@home.nl','$2y$10$SGk0zZtRQeT38Eg2MczD0uCfzradA6E3Xhz0OtYfJsSaSDWhaHFLi','','default_user_img.png',1,NULL,NULL,NULL,NULL,NULL),
	(77,1,'','Willem',NULL,'','Willem','','willem+3@sumedia.nl','$2y$10$w6vYZvgIZwk0SVtkwIPwkujPEW0QfinSs3hNauMx093zvuIOXlKJe','','default_user_img.png',1,NULL,NULL,NULL,NULL,NULL),
	(78,2,'T.J.','Tobias',NULL,'Boekwijt','t0byman','','tobias@sumedia.nl','$2y$10$c.sqs3o6zqTF7DWSQ49ZZu7ceHtoKypNGTo3uxiVcvrJhhv9kmOkC','','default_user_img.png',1,NULL,NULL,NULL,NULL,NULL),
	(79,2,'W.B.','Weg',NULL,'Heerder','Wegbeheerder3','','aart+wegbeheerder3@sumedia.nl','$2y$10$Am//ul4yZ115qtfvpL1GYOou1hE3D7kF3Yz7Bgo1dGjxvYVIbToHC',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_notification_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_notification_templates`;

CREATE TABLE `users_notification_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `notification_template_id` int(11) DEFAULT NULL,
  `show_notification` tinyint(1) DEFAULT '1',
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `notification_template_id` (`notification_template_id`),
  CONSTRAINT `users_notification_templates_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `users_notification_templates_ibfk_2` FOREIGN KEY (`notification_template_id`) REFERENCES `notification_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `users_notification_templates` WRITE;
/*!40000 ALTER TABLE `users_notification_templates` DISABLE KEYS */;

INSERT INTO `users_notification_templates` (`id`, `user_id`, `notification_template_id`, `show_notification`, `deleted`)
VALUES
	(1,65,2,1,NULL),
	(2,64,4,1,NULL),
	(3,65,4,1,NULL),
	(4,68,4,1,NULL),
	(5,69,4,1,NULL),
	(6,70,4,1,NULL),
	(7,71,4,1,NULL),
	(8,72,4,1,NULL),
	(9,74,4,1,NULL),
	(10,79,2,1,NULL),
	(11,79,4,1,NULL);

/*!40000 ALTER TABLE `users_notification_templates` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
