# README #

== IT IS RECOMMENDED TO READ README_USING_GCLOUD_SDK.md AS IT COVERS MORE CONFIGURATION AND IS MORE RECENT ==

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## GOOGLE CLOUD PLATFORM

See also http://rricketts.com/google-cloud-platform-cakephp-3-x-3-4/

### Using Google Cloud Shell

Open the Google Cloud Shell on the Web console (https://console.cloud.google.com) using the [>_] button.

You will be prompted as follows:

```javascript
Welcome to Cloud Shell! Type "help" to get started.
willem@verkeerslicht-165607:~$
```

where 'willem' is the user with which you are logged in, and 'verkeerslicht-165607' is the project name.

Start in your user's src directory on the google cloud console

```javascript
cd ~/src
```

### Install Composer

```javascript
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
```

Update your composer requirements:

```javascript
composer require "cakephp/cakephp:3.4.*" --ignore-platform-reqs
composer require "php:7.1.*" --ignore-platform-reqs
```

2. Clone the sample code

Use Cloud Shell to clone and navigate to the code. The application code is cloned from your project repository to the Cloud Shell.

In Cloud Shell enter:

```javascript
CLONEDTODIR=~/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform
```

Clone a sample repository:

```javascript
git clone https://willem_sumedia@bitbucket.org/willem_sumedia/verkeerslicht-googlecloudplatform.git $CLONEDTODIR
```

You will be prompted with:

```javascript
Cloning into '/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform'...
remote: Counting objects: 53, done.
remote: Compressing objects: 100% (43/43), done.
remote: Total 53 (delta 15), reused 0 (delta 0)
Unpacking objects: 100% (53/53), done.
```

Switch to the tutorial directory:

```javascript
cd $CLONEDTODIR
```

Or, if you have cloned into the directory before, pull the sample repository instead:

```javascript
git pull
```

You will be prompted with:

```javascript
willem@verkeerslicht-165607:~/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform$
```

### Dynamically loadable extensions

The following extensions are dynamically loadable by configuring php.ini. On Google Cloud Platform the php.ini file is located at '/etc/php5/cli/php.ini'.

***cURL*** - This extension uses the socket service to make requests, and is subject to that services' quota and restrictions, and is only available for applications with billing enabled. See Outbound Requests for more information.

***MongoDB*** - This extension allows a developer to connect to an existing MongoDB instance. It uses the socket service to make requests, and is subject to that services' quota and restrictions, and is only available for applications with billing enabled.

***ImageMagick***

***intl***

***fileinfo***

To enable these extensions, add directives for them in your php.ini file under extension like so:

```javascript
extension = "curl.so"
extension = "mongo.so"
extension = "imagick.so"
extension = "intl.so"
extension = "fileinfo.so"
```

### Install required packages with composer

Before using composer, make sure all required libraries are enabled in php.ini.

On Google Cloud Platform the php.ini file is located at '/etc/php5/cli/php.ini'.

It should have the following libraries enabled in php.ini:

#### intl

Should intl not be on the server it can be installed. Installing on Linux (Ubuntu), run the following commands:

```javascript
sudo apt-get install php5-intl
```

You can also check what extensions are compiled in by running:

```javascript
php -m
```

Optionally, enable these settings in php.ini; ```sudo nano php.ini```

```javascript
[intl]
intl.default_locale = en_utf8
; This directive allows you to produce PHP errors when some error
; happens within intl functions. The value is the level of the error produced.
; Default is 0, which does not produce any errors.
intl.error_level = E_WARNING
intl.use_exceptions = 0
```

#### Run composer

Run the following command from within the cloned repository directory:

```javascript
composer install
```

This will install all required 'vendor' packages in the 'vendor' directory.

You will be prompted:

```javascript
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
Package operations: 35 installs, 0 updates, 0 removals
  - Installing cakephp/plugin-installer (1.0.0): Downloading (100%)         
  - Installing aura/intl (3.0.0): Downloading (100%)         
  - Installing cakephp/chronos (1.1.1): Downloading (100%)         
  - Installing symfony/yaml (v3.2.7): Downloading (100%)         
  - Installing symfony/polyfill-mbstring (v1.3.0): Downloading (100%)         
  - Installing psr/log (1.0.2): Downloading (100%)         
  - Installing symfony/debug (v3.2.7): Downloading (100%)         
  - Installing symfony/console (v3.2.7): Downloading (100%)         
  - Installing symfony/filesystem (v3.2.7): Downloading (100%)         
  - Installing symfony/config (v3.2.7): Downloading (100%)         
  - Installing robmorgan/phinx (v0.6.5): Downloading (100%)         
  - Installing psr/http-message (1.0.1): Downloading (100%)         
  - Installing zendframework/zend-diactoros (1.4.0): Downloading (100%)         
  - Installing cakephp/cakephp (3.4.5): Downloading (100%)         
  - Installing cakephp/migrations (1.6.7): Downloading (100%)         
  - Installing mobiledetect/mobiledetectlib (2.8.25): Downloading (100%)         
  - Installing cakephp/bake (1.3.3): Downloading (100%)         
  - Installing jdorn/sql-formatter (v1.2.17): Downloading (100%)         
  - Installing symfony/process (v3.2.7): Downloading (100%)         
  - Installing symfony/finder (v3.2.7): Downloading (100%)         
  - Installing seld/phar-utils (1.0.1): Downloading (100%)         
  - Installing seld/jsonlint (1.6.0): Downloading (100%)         
  - Installing seld/cli-prompt (1.0.3): Downloading (100%)         
  - Installing justinrainbow/json-schema (5.2.0): Downloading (100%)         
  - Installing composer/spdx-licenses (1.1.6): Downloading (100%)         
  - Installing composer/semver (1.4.2): Downloading (100%)         
  - Installing composer/ca-bundle (1.0.7): Downloading (100%)         
  - Installing composer/composer (1.4.1): Downloading (100%)         
  - Installing cakephp/debug_kit (3.9.3): Downloading (100%)         
  - Installing jakub-onderka/php-console-color (0.1): Downloading (100%)         
  - Installing symfony/var-dumper (v3.2.7): Downloading (100%)         
  - Installing nikic/php-parser (v3.0.5): Downloading (100%)         
  - Installing jakub-onderka/php-console-highlighter (v0.3.2): Downloading (100%)         
  - Installing dnoegel/php-xdg-base-dir (0.1): Downloading (100%)         
  - Installing psy/psysh (v0.8.3): Downloading (100%)         
symfony/console suggests installing symfony/event-dispatcher ()
cakephp/cakephp suggests installing lib-ICU (The intl PHP library, to use Text::transliterate() or Text::slug())
cakephp/debug_kit suggests installing ext-sqlite (DebugKit needs to store panel data in a database. SQLite is simple and easy to use
.)
symfony/var-dumper suggests installing ext-symfony_debug ()
psy/psysh suggests installing ext-pdo-sqlite (The doc command requires SQLite to work.)
psy/psysh suggests installing ext-readline (Enables support for arrow-key history navigation, and showing and manipulating command h
istory.)
psy/psysh suggests installing hoa/console (A pure PHP readline implementation. You'll want this if your PHP install doesn't already 
support readline or libedit.)
Generating autoload files
> Cake\Composer\Installer\PluginInstaller::postAutoloadDump
> App\Console\Installer::postInstall
Created `/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/logs` directory
Created `/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp` directory
Created `/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/cache` directory
Created `/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/cache/models` directory
Created `/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/cache/persistent` directory
Created `/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/cache/views` directory
Created `/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/sessions` directory
Created `/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/tests` directory
Set Folder Permissions ? (Default to Y) [Y,n]? 
```

Confirm by typing: Y (followed by ENTER)

```javascript
Permissions set on /home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/cache
Permissions set on /home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/cache/models
Permissions set on /home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/cache/persistent
Permissions set on /home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/cache/views
Permissions set on /home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/sessions
Permissions set on /home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp/tests
Permissions set on /home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/tmp
Permissions set on /home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/logs
No Security.salt placeholder to replace.
```

### Deploying the app to the App Engine flexible environment

NOTE: Make sure you have completed the steps of the previous sections; "Using Google Cloud Shell" and "Install required packages with composer".

1. Deploy the app, by typing in the online Cloud Shell console :

```javascript
gcloud app deploy --project verkeerslicht-165607
```

You will be prompted with:

```javascript
You are about to deploy the following services:
 - verkeerslicht-165607/default/20170425t104700 (from [/home/willem/src/verkeerslicht-165607/verkeerslicht-googlecloudplatform/app.yaml])
     Deploying to URL: [https://verkeerslicht-165607.appspot.com]

Do you want to continue (Y/n)?
```

Type: Y (followed by ENTER)

You will be prompted:

```javascript
...
DONE
----------------------------------------------------------------------------------------------------------------------------------

Updating service [default]...\Stopping version [verkeerslicht-165607/default/20170424t153456].                                    
Updating service [default]...|Sent request to stop version [verkeerslicht-165607/default/20170424t153456]. This operation may take some time to complete. If you would like to verify that it succeeded, run:
  $ gcloud app versions describe -s default 20170424t153456
until it shows that the version has stopped.
Updating service [default]...done.                                                                                                
Deployed service [default] to [https://verkeerslicht-165607.appspot.com]

You can stream logs from the command line by running:
  $ gcloud app logs tail -s default

To view your application in the web browser run:
  $ gcloud app browse
```

Type:

```javascript
gcloud app browse
```
You will be prompted:

```javascript
Opening [https://verkeerslicht-165607.appspot.com] in a new tab in your default browser.
https://verkeerslicht-165607.appspot.com
```

Open a web browser and browse to https://verkeerslicht-165607.appspot.com

NOTE: The CakePHP application requires connection to a MySQL database. Follow below instructions.

## Using Cloud SQL with PHP

See also https://cloud.google.com/php/getting-started/using-cloud-sql

Creating a Cloud SQL instance and database

### Install the SQL proxy

Download and install the Cloud SQL Proxy. The Cloud SQL Proxy is used to connect to your Cloud SQL instance when running locally.

***OS X 32-Bit***:

Download the proxy:

```javascript
curl -o cloud_sql_proxy https://dl.google.com/cloudsql/cloud_sql_proxy.darwin.386
```

Make the proxy executable:

```javascript
chmod +x cloud_sql_proxy
```

## Create a Cloud SQL instance

From the [Google Cloud Platform SQL web console](https://console.cloud.google.com/sql/), create a Cloud SQL Second Generation instance. Choose ***MySQL*** as the database engine.

Name the instance ***verkeerslicht***. It can take a few minutes for the instance to be ready. After the instance is ready, it should be visible in the instances list.

Note: Be sure to create a Second Generation instance.

If you haven't already, set the password for the MySQL root user on your Cloud SQL instance. You can do this from the command-line with the Cloud SDK (where YOUR_INSTANCE_NAME is 'verkeerslicht' and YOUR_INSTANCE_ROOT_PASSWORD is 'secret'):

```javascript
gcloud sql instances set-root-password [YOUR_INSTANCE_NAME] --password [YOUR_INSTANCE_ROOT_PASSWORD]
```

Now use the Cloud SDK from command-line to run the following command. Copy the connectionName value for the next step (where YOUR_INSTANCE_NAME is 'verkeerslicht').

```javascript
gcloud beta sql instances describe [YOUR_INSTANCE_NAME]
```

Here connectionName: verkeerslicht-165607:europe-west1:verkeerslicht

### Initialize your Cloud SQL instance

1. Start the Cloud SQL Proxy using the connectionName from the previous step.

[Mac OS X, type this locally from where you installed the Cloud SQL Proxy] 

```javascript
./cloud_sql_proxy -instances="[YOUR_INSTANCE_CONNECTION_NAME]"=tcp:3306
```

where [YOUR_INSTANCE_CONNECTION_NAME] = verkeerslicht-165607:europe-west1:verkeerslicht

This step establishes a connection from your local computer to your Cloud SQL instance for local testing purposes. Keep the Cloud SQL Proxy running the entire time you test your application locally.

NOTE: You may be prompted with the following:

```javascript
2017/04/24 17:07:59 google: could not find default credentials. See https://developers.google.com/accounts/docs/application-default-credentials for more information.
```

Then look here, https://developers.google.com/identity/protocols/application-default-credentials

In short; running the command ```gcloud auth application-default login``` in the Google Cloud web console terminal will make your identity available to the Application Default Credentials as proxy when testing application code locally. An application_default_credentials.json file will be created. Make a copy of the content of this file and create a file with the same name locally on your computer, e.g. ~/application_default_credentials.json

It will look somewhat like this:

```javascript
{
  "client_id": "664086051850-6qr4p6gpi6hn506pt8ejuq83di341hur.apps.googleusercontent.com",
  "client_secret": "d-FL95Q19q7MQmFpd7hHD0Ty",
  "refresh_token": "1/svcrmBthvp5bbWk-KsFKI-emywSqLv8LxGFMkKXYLFs",
  "type": "authorized_user"
}
```

Before logging on to the remote SQL Proxy, export the following:

```javascript
export GOOGLE_APPLICATION_CREDENTIALS=~/application_default_credentials.json
```

Now when running: ```./cloud_sql_proxy -instances="verkeerslicht-165607:europe-west1:verkeerslicht"=tcp:3306```

you will be prompted with:

```javascript
2017/04/24 17:32:57 Listening on 127.0.0.1:3306 for verkeerslicht-165607:europe-west1:verkeerslicht
2017/04/24 17:32:57 Ready for new connections
```

2. Next you will need to create a new Cloud SQL user and database.

a. Create a [new database using the Cloud Platform Console](https://cloud.google.com/sql/docs/mysql/create-manage-databases#creating_a_database) for your Cloud SQL instance library. For example you can use the name ***verkeerslicht***.

b. Create a [new user using the Cloud Platform Console](https://cloud.google.com/sql/docs/mysql/create-manage-users#creating) for your Cloud SQL instance library. For example you can use the name ***user*** with password ***secret***.

### Configuring settings

1. Open app.yaml for editing.

2. Set the values for your Cloud SQL instance:

```javascript
env_variables:
  # Uncomment the following to enable debug mode.
  CAKEPHP_DEBUG: '1'

  # If connecting via TCP/IP to Google Cloud SQL
  PROD_DB_HOSTNAME: "35.187.124.173"
  PROD_DB_PORT: "3306"

  # If connecting via App Engine to Google Cloud SQL
  PROD_DB_UNIXSOCKET: "verkeerslicht-165607:europe-west1:verkeerslicht"
  PROD_DB_USERNAME: "user"
  PROD_DB_PASSWORD: "secret"
  PROD_DB_DATABASE: "verkeerslicht"

  # Change below to match your settings for local development.

  # If connecting to MySQL using TCP/IP
  DEV_DB_HOSTNAME: "localhost"
  DEV_DB_PORT: "3306"

  # If connecting to MySQL using sockets.
  DEV_DB_UNIXSOCKET: "verkeerslicht-165607:europe-west1:verkeerslicht"
  DEV_DB_USERNAME: "user"
  DEV_DB_PASSWORD: "secret"
  DEV_DB_DATABASE: "verkeerslicht"
```

5. Save and close app.yaml.

## Using phpMyAdmin

See https://cloud.google.com/sql/docs/mysql/phpmyadmin-on-app-engine