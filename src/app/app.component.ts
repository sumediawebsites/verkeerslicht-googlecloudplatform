import { Component, OnInit, Renderer } from '@angular/core';
import { Router } from '@angular/router';

import { App } from './models/app.model';
import { GoogleMapComponent } from './google-map/google-map.component';
import { PageHeaderComponent } from './shared/page-header/page-header.component';
import { PageFooterComponent } from './shared/page-footer/page-footer.component';
import { ReportsViewerComponent } from './reports/reports-viewer/reports-viewer.component';

import { HttpModule } from "@angular/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  app: App;

  constructor(private router: Router) {

  }

  ngOnInit() {
    this.app = new App('Reports Manager', 'Our Company, Inc.', (new Date()).getFullYear());
  }

}
