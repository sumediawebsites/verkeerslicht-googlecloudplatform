import { Component, Input, Output, EventEmitter, ViewContainerRef, NgZone } from '@angular/core';

import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/bootstrap';

import { Report } from '../../../models/report.model';
import { CapitalizePipe } from '../../../pipes/capitalize.pipe';
import { FilterPipe } from '../../../pipes/filter.pipe';

@Component({
	selector: 'reports-custom-list-group',
	templateUrl: './reports-custom-list-group.component.html',
	styles: ['./reports-custom-list-group.component.scss'],
})
export class ReportsCustomListGroupComponent {

	currentReport: Report;

	filterReports: string = '';

	paginationLimit: number = 9999; // Max pagination limit when searching for reports

	searchInputValue: string = '';

	@Input()
	reports: Report[];

	@Input()
	defaultPaginationlimit: number;

	@Output()
	viewReport: EventEmitter<number> = new EventEmitter<number>();

	@Output()
	editReport: EventEmitter<number> = new EventEmitter<number>();

	@Output()
	showOnMap: EventEmitter<number> = new EventEmitter<number>();

    @Output()
	setSearchInput: EventEmitter<string> = new EventEmitter<string>();

	@Output()
	setPaginationLimit: EventEmitter<number> = new EventEmitter<number>();

	constructor(
		overlay: Overlay,
		vcRef: ViewContainerRef,
		public modal: Modal,
		private _ngZone: NgZone) {
		overlay.defaultViewContainer = vcRef;
	}

	viewReportButton(reportId: number) {
		this.viewReport.emit(reportId)
	}

	editReportButton(reportId: number) {
		this.editReport.emit(reportId)
	}

	setPaginationLimitOnInput() {
		this.setSearchInput.emit(this.searchInputValue);
        // If a single character or more has been typed in the input field
		// we should reset the limit.
		if(this.searchInputValue.length >= 1) {
          this.setPaginationLimit.emit(this.paginationLimit);
		}
		// With no text in the input field
		// we should reset the limit back to its default value
		else if(this.searchInputValue.length == 0) {
		  this.setPaginationLimit.emit(this.defaultPaginationlimit); // default value of 25
		}
	}

	modalReportButton(reportId: number) {
		this.currentReport = this.reports.find(x => x.id === reportId);
		let locationId = this.currentReport.device['location_id'];

		let sameLocationReports = [];
		// Find all reports that contain the same location id
		for(var i=0; i<this.reports.length; i++) {
			// Check if the current report has the same location id
			if(this.reports[i].device['location_id'] == locationId) {
				sameLocationReports.push(this.reports[i]);
			}
		}

		// TEMP center and zoom on Map for the current report
		let event:Event = new Event('showOnMap');
		this._ngZone.run( () =>
				this.showOnMap.emit(reportId)); // Emits the event raised showing the report's marker on the map to the output port called 'showOnMap'.
		let bodyHTML = this.createHTMLList(sameLocationReports);

		this.modal.alert()
		.size('lg')
		.isBlocking(false)
		.showClose(true)
		.title('[' + this.currentReport['id'] + '] ' + this.currentReport['vri_place'])
		.body(bodyHTML)
		.headerClass('modal-ng__header')
		.bodyClass('modal-ng__body')
		.footerClass('modal-ng__footer')
        .okBtn('')
		.open();

		// Capture click event on modal button 'Ook een melding plaatsen?'
        let reportsModalDialogClass = "modal-dialog";
        document.getElementsByClassName(reportsModalDialogClass)[0].addEventListener("click", clickReportDirectButtonHandler, false);
        function clickReportDirectButtonHandler(event) {
            if(typeof event.srcElement.className === 'undefined') {
				return;
			} else if(event.srcElement.className.indexOf('report-direct-button') !== -1) {
				goToReportDirect();
			} else if(event.srcElement.className.indexOf('report-direct-button-icon') !== -1) {
				goToReportDirect();
			} else if(typeof event.srcElement.firstChild.className === 'undefined') {
                return;
			} else if(event.srcElement.firstChild.className.indexOf('report-direct-button') !== -1){
				goToReportDirect();
			} else if(event.srcElement.firstChild.className.indexOf('report-direct-button-icon') !== -1){
				goToReportDirect();
			}
		}

		function goToReportDirect() {
           // Close the current reports modal
		   var evt = new MouseEvent("click", {
		     view: window,
		     bubbles: true,
			 cancelable: true,
			 clientX: 20
             /* whatever properties you want to give it */
		   });
		   let reportsModalNGHeaderClass = "modal-ng__header";
		   document.getElementsByClassName(reportsModalNGHeaderClass)[0].firstElementChild.dispatchEvent(evt);
		   // Allow time for the modal to Close
           setTimeout(function() {
			 // Open the 'Report Direct' modal
			 let remodalReportDirectButton = document.querySelectorAll('[data-remodal-target="modal-report"]');
			 remodalReportDirectButton[0].dispatchEvent(evt);
           }, 1000); // 1 second, adjust to fit
		};
	}

	createHTMLList(reportsList:Report[]) {
		let outerDiv = document.createElement('div');
        let p = document.createElement('p');
        p.innerHTML=reportsList[0]['device']['disclaimer'];
		outerDiv.appendChild(p);
		let span = document.createElement('span');
		let countLabel = reportsList.length == 1 ? "melding" : "meldingen";
		span.innerHTML = reportsList.length.toString() + ' ' + countLabel;
		outerDiv.appendChild(span);
		let ul = document.createElement('ul');
		for(var i=0; i<reportsList.length; i++) {
			// create a list here of all list items
			let li = document.createElement('li');
			let innerSpanCreatedAt = document.createElement('span');
			let date = new Date(reportsList[i]['date']);
			let dayNumber = date.getDate();
			let dayString = ("0" + dayNumber).slice (-2);
			let monthNumber = date.getMonth() + 1;
            let monthString = ("0" + monthNumber).slice (-2);
			let yearNumber = date.getFullYear();
			let yearString = ("0" + yearNumber).slice (-2);
            let time = new Date(reportsList[i]['time']);
			let hoursNumber = time.getHours();
			let hoursString = ("0" + hoursNumber).slice (-2);
			let minutesNumber = time.getMinutes();
			let minutesString = ("0" + minutesNumber).slice (-2);
			innerSpanCreatedAt.innerHTML = '['+ reportsList[i]['id'] +'] ' + reportsList[i]['vri_place'] + '<span>' + dayString + '-' + monthString + '-' + yearString + ' | ' + hoursString + ":" + minutesString + '</span>';
			li.appendChild(innerSpanCreatedAt);
			let innerSpanReportType = document.createElement('span');
			innerSpanReportType.innerHTML=reportsList[i]['report_type']['description'];
			li.appendChild(innerSpanReportType);
			let innerSpanFromStreet = document.createElement('span');
			innerSpanFromStreet.innerHTML='van: '+reportsList[i]['vri_street_1'];
			li.appendChild(innerSpanFromStreet);
			let innerSpanToStreet = document.createElement('span');
			innerSpanToStreet.innerHTML='naar: '+reportsList[i]['vri_street_2'];
			li.appendChild(innerSpanToStreet);
			ul.appendChild(li);
		}
		outerDiv.appendChild(ul);
		// Create a button to report directly
		let button = document.createElement('div');
		button.innerHTML = '<a class="report-direct-button">Ook een melding plaatsen? <i class="icon-arrow-right report-direct-button-icon"></i></a>';
		outerDiv.appendChild(button);
		// Return HTML Elements as a string
		return outerDiv.outerHTML;
	}
}
