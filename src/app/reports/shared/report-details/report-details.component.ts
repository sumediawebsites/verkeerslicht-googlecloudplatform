import { Component, Input } from '@angular/core';

import { Report } from '../../../models/report.model';
import { CapitalizePipe } from '../../../pipes/capitalize.pipe';
import { ActivePipe } from '../../../pipes/active.pipe';

@Component({
	selector: 'report-details',
	templateUrl: './report-details.component.html',
	styles: ['./report-details.component.scss']
})
export class ReportDetailsComponent {

	@Input()
	report: Report;
	
}