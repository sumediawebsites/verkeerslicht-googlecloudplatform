import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Report } from '../../../models/report.model';

// interface SelectItem { 
// 	value: string;
// 	label: string;
// }

@Component({
	selector: 'report-form',
	templateUrl: './report-form.component.html',
	styles: ['./report-form.component.scss']
})
export class ReportFormComponent {

	@Input()
	report: Report = <Report>{};

	@Output()
	saveReport: EventEmitter<Report> = new EventEmitter<Report>();

	@Output()
	deleteReport: EventEmitter<Report> = new EventEmitter<Report>();

	@Output()
	cancelReport: EventEmitter<Report> = new EventEmitter<Report>();

	// colors: SelectItem[] = [
	// 	{ value: 'red', label: 'Red' },
	// 	{ value: 'blue', label: 'Blue' },
	// 	{ value: 'green', label: 'Green' },
	// 	{ value: 'orange', label: 'Orange' },
	// 	{ value: 'yellow', label: 'Yellow' },
	// 	{ value: 'purple', label: 'Purple' }
	// ] 

	// sizes: SelectItem[] = [
	// 	{ value: 'tiny', label: 'Tiny' },
	// 	{ value: 'small', label: 'Small' },
	// 	{ value: 'medium', label: 'Medium' },
	// 	{ value: 'large', label: 'Large' },
	// 	{ value: 'huge', label: 'Huge' }
	// ] 

	saveReportButton(report: Report) {
		
		this.saveReport.emit(report);
	}

	deleteReportButton(report: Report) {
		this.deleteReport.emit(report);
	}

	cancelReportButton(report: Report) {
		this.cancelReport.emit(report);
	}

}