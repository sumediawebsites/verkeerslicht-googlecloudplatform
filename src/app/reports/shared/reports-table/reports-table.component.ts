import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Report } from '../../../models/report.model';
import { CapitalizePipe } from '../../../pipes/capitalize.pipe';

@Component({
	selector: 'reports-table',
	templateUrl: './reports-table.component.html',
	styles: ['./reports-table.component.scss']
})
export class ReportsTableComponent {

	@Input()
	reports: Report[];

	@Output()
	viewReport: EventEmitter<number> = new EventEmitter<number>();

	@Output()
	editReport: EventEmitter<number> = new EventEmitter<number>();

	viewReportButton(reportId: number) {
		this.viewReport.emit(reportId)
	}

	editReportButton(reportId: number) {
		this.editReport.emit(reportId)
	}

}