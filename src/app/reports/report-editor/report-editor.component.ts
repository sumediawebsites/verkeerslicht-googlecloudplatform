import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Report } from '../../models/report.model';
import { Reports } from '../../services/reports.service';

import { ReportFormComponent } from '../shared/report-form/report-form.component';

@Component({
	selector: 'report-editor',
	templateUrl: './report-editor.component.html',
	styleUrls: ['./report-editor.component.scss'],
	providers: [ Reports ]
})
export class ReportEditorComponent implements OnInit {

	report: Report = <Report>{};

	constructor(
		private reportsData: Reports,
		private router: Router,
		private route: ActivatedRoute
	) { }

	ngOnInit() {
		this.route.params.subscribe(params =>
			this.reportsData.get(parseInt(params['id'],10)).subscribe(report =>
				this.report = report));
	}

	saveReport(report: Report) {

		(report.id
			? this.reportsData.update(report)
			: this.reportsData.insert(report))
			.subscribe(report => {
				this.router.navigate(["/"]);
			});
		
	}

	deleteReport(report: Report) {

		if (confirm('Weet je zeker dat je het volgende wil verwijderen?: this report?')) {
			this.reportsData.delete(report.id).subscribe(report =>
				this.router.navigate(["/"]));
		}

	}

	cancelReport(report: Report) {
		this.router.navigate(["/"]);
	}

}