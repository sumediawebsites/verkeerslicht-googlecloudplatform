import { Component, OnInit, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Report } from '../../models/report.model';
import { Reports } from '../../services/reports.service';
import { ReportsCustomListGroupComponent } from '../shared/reports-custom-list-group/reports-custom-list-group.component';
import { Device } from '../../models/device.model';
import { Devices } from '../../services/devices.service';
import { GoogleMapComponent } from '../../google-map/google-map.component';

@Component({
	selector: 'reports-viewer',
	templateUrl: './reports-viewer.component.html',
	styleUrls: ['./reports-viewer.component.scss'],
	providers: [ Reports, Devices ]
})

export class ReportsViewerComponent implements OnInit {

    // Used by pagination
	count: number = 0;
	offset: number = 0;
	limit: number = 25; // choose an appropriate number e.g. 25
    loading: boolean = false;
	failed: boolean = false;

	isNewReportCount: number = 0;
	isOnHoldReportCount: number = 0;
	isPendingReportCount: number = 0;
	isProcessedPositiveReportCount: number = 0;
	isProcessedNeutralReportCount: number = 0;

    // Start out with all report filters set to true (i.e. checked), 
	// so all reports are shown
	reportFilters: any = {
		isNewReport: true, 
		isOnHoldReport: true, 
		isPendingReport: true, 
		isProcessedPositiveReport: true,
		isProcessedNeutralReport: true
	};
    reportMarkersStatusArray: any = [];

	currentReport: Report;
	map: google.maps.Map;
	position: google.maps.LatLng;
    searchFilter: string = '';
	deviceMarkersInfoWindowContentStrings: string[] = [];
	deviceMarkers: google.maps.MarkerOptions[] = []; // <-- USE THIS
	reportMarkers: google.maps.MarkerOptions[] = []; // <-- USE THIS
	//markers: google.maps.Marker[] = []; // <-- DON'T USE THIS
	reportMarkersVisibilityUpdated: google.maps.MarkerOptions[] = [];
	reportMarkersToBeDeleted: google.maps.MarkerOptions[] = [];
	deviceMarkersToBeDeleted: google.maps.MarkerOptions[] = [];
    zoom: number = 7; // 20 is max, is most zoomed in
    center: google.maps.LatLng = new google.maps.LatLng(52.258107, 5.600592); // center of Holland
	scrollWheel: boolean = false; // disable zooming on the map with mouse scroll wheel
	markerDraggable: boolean;
    markerTitle: string;
	markerLat: any;
	markerLng: any;
	markerPosition: google.maps.LatLng;
	reports: Report[];
	devices: Device[];
	searchInput: string;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private reportsData: Reports,
		private devicesData: Devices
	) { }

	ngOnInit() {
		// Retrieve reports	
        let pagination = true;
        this.retrieveReports(pagination);
		this.retrieveDevices();
	}

	onClickFilterReportMarkers = function(event) {
		let elementID = event['srcElement']['id'];
        let elementChecked = event['srcElement']['checked'];

        switch(elementID) {
			case 'filterIsNewReport':
			    this.reportFilters['isNewReport'] = elementChecked;
			    break;
			case 'filterIsOnHoldReport':
			    this.reportFilters['isOnHoldReport'] = elementChecked;
			    break;
			case 'filterIsPendingReport':
			    this.reportFilters['isPendingReport'] = elementChecked;
			    break;
			case 'filterIsProcessedPositiveReport':
			    this.reportFilters['isProcessedPositiveReport'] = elementChecked;
			    break;
			case 'filterIsProcessedNeutralReport':
			    this.reportFilters['isProcessedNeutralReport'] = elementChecked;
			    break;
            default:
			    break;
		}
        this.filterReportMarkers();
	}
	// Get a subset of the report filters that are set to true
	getTrueReportFiltersOptions = function() {
		let trueReportFiltersOptions = []
		for (let option in this.reportFilters) {
			if (this.reportFilters[option]) {
			    trueReportFiltersOptions.push(option)
			}
		}
		return trueReportFiltersOptions;
	}

    filterReportMarkers = function() {
        let setReportMarkerFilters = this.getTrueReportFiltersOptions();
        let trueFilterStatusStrings: any = [];
		let keyStatus:string = '';
		for(var key in this.reportFilters) {
            // Use only the filters that have a value of true
		    if(this.reportFilters[key]) {
		 	  if(key=='isNewReport') { keyStatus = 'is-new' }
		 	  if(key=='isOnHoldReport') { keyStatus = 'is-on-hold' }
		 	  if(key=='isPendingReport') { keyStatus = 'is-pending' }
		 	  if(key=='isProcessedPositiveReport') { keyStatus = 'is-processed-positive' }
		 	  if(key=='isProcessedNeutralReport') { keyStatus = 'is-processed-neutral' }
              trueFilterStatusStrings.push(keyStatus);
		    }
		}
        this.reportMarkersVisibilityUpdated = [];
		let reportMarkerStatus;

        // for each marker, check to see if all required options are set
		for (let i = 0; i < this.reportMarkersStatusArray.length; i++) {
            reportMarkerStatus = this.reportMarkersStatusArray[i];

            let marker: google.maps.MarkerOptions[] = [];

			// Set the marker with this index to visibile: false by default
            let options = { 
				draggable: this.reportMarkers[reportMarkerStatus['markerIndex']].draggable,
				position: this.reportMarkers[reportMarkerStatus['markerIndex']].position,
				icon: this.reportMarkers[reportMarkerStatus['markerIndex']].icon,
				animation: this.reportMarkers[reportMarkerStatus['markerIndex']].animation,
				label: this.reportMarkers[reportMarkerStatus['markerIndex']].label,
				title: this.reportMarkers[reportMarkerStatus['markerIndex']].title,
				visible: false 
			};

            for(let n=0; n < trueFilterStatusStrings.length; n++) {
                if(trueFilterStatusStrings[n] == reportMarkerStatus['status']) {
                    options.visible = true;
				}
			}
            marker = [options];
			this.reportMarkersVisibilityUpdated = this.reportMarkersVisibilityUpdated.concat(marker);
		}

        // This will update the map
		this.reportMarkers = this.reportMarkersVisibilityUpdated;
	}

    retrieveReports(pagination: boolean) {
		// Retrieve reports	
		let usePagination = pagination;
		if(usePagination) {
			// With pagination
			let observable = this.route.params
				.map(params => params['nr'])
				.map(pageNr => (pageNr - 1) * this.limit);
			observable.subscribe(offset => this.offset = offset);
			observable.share().subscribe(offset => this.findAll(offset, this.limit));
        } else {
			// Without pagination
			this.reportsData.getAll()
				.subscribe(
					reports => this.reports = reports,
					err =>console.log(err),
					() => this.addReportMarkers(this.reports)
				);
		}
	}

    retrieveDevices() {
		// Retrieve devices, no pagination
		this.devicesData.getAll()
		  .subscribe(
			  devices => this.devices = devices,
			  err =>console.log(err),
			  () => this.addDeviceMarkers(this.devices)
		  );
	}

    findAll(offset: number, limit: number) {
      this.reports = [];
      this.loading = true;
      this.failed = false;
      this.reportsData.findAll(offset, limit).subscribe(result => {
		this.reports = result['reports'];
		this.count = result['count'];
        this.loading = false;
		this.deleteAllReportMarkers(); // clean the map of report markers
		this.deleteAllDeviceMarkers(); // clean the map of device markers
        if(this.searchFilter.length !== 0) {
			// Remove reports which do not match the searchFilter
			for (var i = 0; i < this.reports.length; i++) {
			
				if (this.reports[i]['vri_place'].toLowerCase().indexOf(this.searchFilter.toLowerCase()) !== -1) {
					// keep the current report with this.reports
				}
				else {
					// remove the current report from this.reports
					this.reports.splice(i, 1);
					// reduce the index by 1
					i = i - 1;
				}
			
			}
		}
        // We add only those reports that fulfill the searchFilter (if applicable)
        this.addReportMarkers(this.reports);
		this.addDeviceMarkers(this.devices);
      }, () => {
        this.loading = false;
        this.failed = true;
      });
    }

    onPageChange(offset) {
      this.offset = offset;
      this.router.navigate(['/pagina', (offset / this.limit) + 1]);
    }

	viewReport(reportId: number) {
		this.router.navigate(['report', reportId]);
	}

	editReport(reportId: number) {
		this.router.navigate(['report', reportId, 'edit']);
	}

    searchInputValue(searchInput: string) {
        this.searchInput = searchInput;
	}

	showOnMap(reportId: number) {
		this.currentReport = this.reports.find(x => x.id === reportId);
		this.showReport(this.currentReport);
	}

    setPaginationLimit(limit: number) {
        // set the limit for pagination
		this.limit = limit;
		// reset the offset to 0
		this.offset = 0;
		// navigate to the first page
		this.onPageChange(this.offset);
		// remove all markers from the map
		this.deleteAllReportMarkers();
        this.searchFilter = this.searchInput;
        let pagination = true;
        this.retrieveReports(pagination);
		this.retrieveDevices();
	}

	showReport(report:Report) {
		this.currentReport = report;
        let position: google.maps.LatLng = new google.maps.LatLng(parseFloat(this.currentReport['latitude']), parseFloat(this.currentReport['longitude']));
		this.setCenter(position);
		this.setZoom(17); // zoomed into street view
	}

    setCenter(position: google.maps.LatLng) {
		this.center = position;
	}

    setZoom(level: number) {
		this.zoom = level;
	}

    deleteAllReportMarkers() {
        this.reportMarkersToBeDeleted = this.reportMarkers;
		this.reportMarkers = [];
	}

    deleteAllDeviceMarkers() {
        this.deviceMarkersToBeDeleted = this.deviceMarkers;
		this.deviceMarkers = [];
	}

	addReportMarkers(reports) {
        this.isNewReportCount = 0;
		this.isOnHoldReportCount = 0;
		this.isPendingReportCount = 0;
		this.isProcessedNeutralReportCount = 0;
        this.isProcessedPositiveReportCount = 0;
		this.reportMarkersStatusArray = [];
		let markerIndex: number;

		for (var i = 0; i < reports.length; i++) {
			markerIndex = i;
			let draggable: boolean = false;
			let title: string = reports[i]['vri_place'];
			let label: any = [];
			let label_text: string = reports[i].id.toString();
			let label_color: string = 'white';
			label['text'] = label_text;
			label['color'] = label_color;
			let position: google.maps.LatLng = new google.maps.LatLng(reports[i]['latitude'], reports[i]['longitude']);
			let fillColor: string;
			let icon: any =  {
				path: 'M20, 10c0-5.5-4.5-10-10-10S0, 4.5, 0, 10c0, 1.4, 0.3, 2.7, 0.8, 3.9l0, 0c0, 0, 2.1, 5.2, 9.2, 16.1 c7.2-10.9, 9.2-16.1, 9.2-16.1l0, 0C19.7, 12.7, 20, 11.4, 20, 10z',
				fillOpacity: 1,
				strokeWeight: 0,
				size: [20, 30],
				labelOrigin: new google.maps.Point(10, 10),
				anchor: new google.maps.Point(10, 30)
			};

			switch(reports[i]['report_status'].class) {
	            case 'is-new':
					icon['fillColor'] = '#242e42';
					status = 'is-new';
					this.isNewReportCount++;
					break;
				case 'is-on-hold':
					icon['fillColor'] = '#f90';
					status = 'is-on-hold';
					this.isOnHoldReportCount++;					  
					break;
				case 'is-pending':
					icon['fillColor'] = '#f7412d';
					status = 'is-pending';
					this.isPendingReportCount++;				  
					break;
				case 'is-processed-positive':
					icon['fillColor'] = '#79c410';
					status = 'is-processed-positive';
					this.isProcessedPositiveReportCount++;				  
					break;
				case 'is-processed-neutral':
					icon['fillColor'] = '#ffcf00'; 					  
					label['color'] = '#242e42';
					status = 'is-processed-neutral';
					this.isProcessedNeutralReportCount++;
					break;
				default:
					break;
			}

			let animation: google.maps.Animation = google.maps.Animation.DROP;
			this.addReportMarker(status, draggable, title, label, position, icon, animation, markerIndex);
		}
	}

	addReportMarker(status: string, draggable: boolean, title: string, label: string, position: google.maps.LatLng, icon: any, animation: google.maps.Animation, markerIndex: number) {
        let marker: google.maps.MarkerOptions[] = [{
			draggable: draggable,
			position: position,
			icon: icon,
			animation: animation,
			label: label,
			title: title,
			visible: true
		}];
        // Append to the lookup table [status, markerIndex]
        this.reportMarkersStatusArray = this.reportMarkersStatusArray.concat([{status: status, markerIndex: markerIndex}]);
		this.reportMarkers = this.reportMarkers.concat(marker);
	}

    addDeviceMarkers(devices) {
		if (typeof(devices) != 'undefined' && devices != null) {
			for (var i = 0; i < devices.length; i++) {
				let draggable: boolean = false;
				let title: string = devices[i]['name'];
				let label: any = [];
				let label_text: string = devices[i].id.toString();
				let label_color: string = 'black';
				label['text'] = label_text;
				label['color'] = label_color;
				let position: google.maps.LatLng = new google.maps.LatLng(devices[i]['location']['latitude'], devices[i]['location']['longitude']);
				// let fillColor: string = 'lightgrey';
				// let icon: any = {
				// 	path: 'M20, 10c0-5.5-4.5-10-10-10S0, 4.5, 0, 10c0, 1.4, 0.3, 2.7, 0.8, 3.9l0, 0c0, 0, 2.1, 5.2, 9.2, 16.1 c7.2-10.9, 9.2-16.1, 9.2-16.1l0, 0C19.7, 12.7, 20, 11.4, 20, 10z',
				// 	fillOpacity: 1,
				// 	fillColor: fillColor,
				// 	strokeWeight: 0,
				// 	size: [20, 30],
				// 	labelOrigin: new google.maps.Point(10, 10),
				// 	anchor: new google.maps.Point(10, 30)
				// };
				let fillColor: string = '#242e42';
 				let icon: any = {
					path: 'M4.2,0C1.9,0,0,1.9,0,4.2l0,17.9C0,24.4,4.2,30,4.2,30s4.2-5.6,4.2-7.9l0-17.9C8.3,1.9,6.5,0,4.2,0z M5.8,24.4 c-1.5,0.9-3.6,0.4-4.5-1.2c-0.9-1.5-0.4-3.6,1.2-4.5s3.6-0.4,4.5,1.2C8,21.5,7.4,23.5,5.8,24.4z M5.9,15.7c-1.5,0.9-3.6,0.4-4.5-1.2 S1.1,11,2.6,10.1S6.1,9.7,7,11.3S7.5,14.8,5.9,15.7z M5.9,7.3C4.4,8.2,2.4,7.7,1.5,6.1S1.1,2.6,2.6,1.7s3.6-0.4,4.5,1.2 S7.5,6.4,5.9,7.3z',
 					fillOpacity: 1,
 					fillColor: fillColor,
 					strokeWeight: 0,
					size: [8, 12],
 					labelOrigin: new google.maps.Point(18, 10),
					anchor: new google.maps.Point(4, 12)
 				};
				let animation: google.maps.Animation = google.maps.Animation.DROP;
				let contentString: string = devices[i].disclaimer.toString();
                this.addDeviceMarkerInfoWindowContentString(contentString);
				this.addDeviceMarker(draggable, title, label, position, icon, animation);
			}
		}
	}

	addDeviceMarkerInfoWindowContentString(contentString: string) {
		this.deviceMarkersInfoWindowContentStrings = this.deviceMarkersInfoWindowContentStrings.concat(contentString);
	}

	addDeviceMarker(draggable: boolean, title: string, label: string, position: google.maps.LatLng, icon: any, animation: google.maps.Animation) {
		this.deviceMarkers = this.deviceMarkers.concat([{
			draggable: draggable,
			position: position,
			icon: icon,
			animation: animation,
			label: label,
			title: title
		}]);
	}	

}
