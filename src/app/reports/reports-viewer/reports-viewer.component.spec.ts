import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { CapitalizePipe } from '../../pipes/capitalize.pipe';
import { ReportsTableComponent } from '../shared/reports-table/reports-table.component';
import { ReportsViewerComponent } from './reports-viewer.component';
import { Reports } from '../../services/reports.service';

class MockHttp { }
class MockRouter { createUrlTree() {} }

describe('Reports Viewer Tests', () => {

	let fixture: ComponentFixture<ReportsViewerComponent>;
	let theComponent: ReportsViewerComponent;
	let de: DebugElement;
	let el: HTMLElement;
	let reports: Reports;

	beforeEach(() => {

		TestBed.configureTestingModule({
			declarations: [
				ReportsViewerComponent,
				ReportsTableComponent,
				CapitalizePipe
			],
			providers: [
				Reports,
				{ provide: Http, useClass: MockHttp },
				{ provide: Router, useClass: MockRouter }
			]
		});

		fixture = TestBed.createComponent(ReportsViewerComponent);
		theComponent = fixture.componentInstance;

		reports = fixture.debugElement.injector.get(Reports);

	});

  it ('reports viewer initialization', () => {

		spyOn(reports, 'getAll').and.returnValue(Observable.create(
			<(observer: any) => any> (observer => observer.next([{ name: 'Report 1' }]))
		));

		fixture.detectChanges();

		expect(reports.getAll).toHaveBeenCalled();

  });

});
