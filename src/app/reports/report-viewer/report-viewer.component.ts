import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Report } from '../../models/report.model';
import { Reports } from '../../services/reports.service';
import { ReportDetailsComponent } from '../shared/report-details/report-details.component';

@Component({
	templateUrl: './report-viewer.component.html',
	styles: ['./report-viewer.component.scss'],
	providers: [ Reports ]
})
export class ReportViewerComponent implements OnInit {

	report: Report;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private reportsData: Reports
	) { }

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.reportsData.get(parseInt(params['id'], 10)).subscribe(report => this.report = report);
		});
	}

	editReport(reportId: number) {
		this.router.navigate(["/report", reportId, "/edit"]);
	}

	returnToList() {
		this.router.navigate(["/"]);
	}
}