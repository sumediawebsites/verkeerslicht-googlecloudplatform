import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { AppComponent } from './app.component';

import { GoogleMapComponent } from './google-map/google-map.component';
import { PageHeaderComponent } from './shared/page-header/page-header.component';
import { PageFooterComponent } from './shared/page-footer/page-footer.component';
import { ReportsViewerComponent } from './reports/reports-viewer/reports-viewer.component';
import { ReportViewerComponent } from './reports/report-viewer/report-viewer.component';
import { ReportEditorComponent } from './reports/report-editor/report-editor.component';
import { ReportsTableComponent } from './reports/shared/reports-table/reports-table.component';
import { ReportsCustomListGroupComponent } from './reports/shared/reports-custom-list-group/reports-custom-list-group.component';
import { ReportDetailsComponent } from './reports/shared/report-details/report-details.component';
import { ReportFormComponent } from './reports/shared/report-form/report-form.component';
import { LoaderComponent } from './shared/loader/loader.component';

import { CapitalizePipe } from './pipes/capitalize.pipe';
import { ActivePipe } from './pipes/active.pipe';
import { FilterPipe } from './pipes/filter.pipe';

import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';

import { AppRoutingModule }  from './app.routes';
import { PaginationComponent } from './shared/pagination/pagination.component';

import 'intl';
import 'intl/locale-data/jsonp/en';

@NgModule({
  declarations: [
    AppComponent,
    GoogleMapComponent,
    PageHeaderComponent,
    PageFooterComponent,
    ReportsViewerComponent,
    ReportViewerComponent,
    ReportEditorComponent,
    ReportsTableComponent,
    ReportsCustomListGroupComponent,
    ReportDetailsComponent,
    ReportFormComponent,
    CapitalizePipe,
    ActivePipe,
    FilterPipe,
    PaginationComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
