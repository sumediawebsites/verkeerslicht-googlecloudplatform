// /* tslint:disable:no-unused-variable */

// import { TestBed, async } from '@angular/core/testing';
// import { AppComponent } from './app.component';

// describe('AppComponent', () => {
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       declarations: [
//         AppComponent
//       ],
//     });
//     TestBed.compileComponents();
//   });

//   it('should create the app', async(() => {
//     let fixture = TestBed.createComponent(AppComponent);
//     let app = fixture.debugElement.componentInstance;
//     expect(app).toBeTruthy();
//   }));

//   it(`should have as title 'app works!'`, async(() => {
//     let fixture = TestBed.createComponent(AppComponent);
//     let app = fixture.debugElement.componentInstance;
//     expect(app.title).toEqual('app works!');
//   }));

//   it('should render title in a h1 tag', async(() => {
//     let fixture = TestBed.createComponent(AppComponent);
//     fixture.detectChanges();
//     let compiled = fixture.debugElement.nativeElement;
//     expect(compiled.querySelector('h1').textContent).toContain('app works!');
//   }));
// });

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';

import { AppComponent } from './app.component';

class MockRouter { createUrlTree() {} }

describe('App', () => {

	let fixture: ComponentFixture<AppComponent>;
	let theComponent: AppComponent;

	beforeEach(() => {

		TestBed.configureTestingModule({
			declarations: [ AppComponent ],
			providers: [
				{ provide: Router, useClass: MockRouter }
			],
			schemas: [ NO_ERRORS_SCHEMA ]
		});

		fixture = TestBed.createComponent(AppComponent);
		theComponent = fixture.componentInstance;

	});

  it ('app initialization', () => {

    fixture.detectChanges();

    expect(theComponent.app.name).toBe('Reports Manager');
    expect(theComponent.app.creator).toBe('Our Company, Inc.');
    expect(theComponent.app.copyrightYear).toBe((new Date()).getFullYear());

  });

});
