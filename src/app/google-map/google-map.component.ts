// See also http://developertips.blogspot.nl/2016/05/angular-2-building-google-map-component.html
// Source https://github.com/huguogang/ng2tsdemo
// Demo  https://developertips.firebaseapp.com/google_map.html
import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    NgZone,
    OnInit,
    Output,
    Renderer
} from '@angular/core';

import { Report } from '../models/report.model';

// Reference to external javascript files
declare var MarkerClusterer: any;
declare var MarkerManager: any;

/**
 * A component wrapper of Google Maps.
 */
@Component({
    selector: 'app-google-map',
    templateUrl: './google-map.component.html',
    styleUrls: ['./google-map.component.scss'],
    providers: []
})
export class GoogleMapComponent implements OnInit {

    @Input() report:Report;

    @Input()
    set deleteAllReportMarkers(value: google.maps.MarkerOptions[]) {
        // Delete only report markers, not device markers
        if (this._reportMarkers) {
            this._reportMarkers.forEach(marker => marker.setMap(null));
            this._reportMarkers = [];
            if (typeof this.reportMarkerClusterer !== 'undefined') {
                this.reportMarkerClusterer.clearMarkers();
            }
        }
    }

    @Input()
    set center(value: google.maps.LatLng) {
        this._center = value;
        if (this.map) {
            this.map.setCenter(this._center);
        }
    }

    @Input()
    set zoom(value: number) {
        this._zoom = value;
        if (this.map) {
            this.map.setZoom(this._zoom);
        }
    }

    @Input()
    set scrollWheel(value: boolean) {
        // this._scrollWheel = value;
        // if (this.map) {
        //     this.map.setOptions({ scrollwheel: this._scrollWheel })
        // }
    }

    @Input()
    set reportMarkers(value: google.maps.MarkerOptions[]) {
        // Remove report markers
        if (this._reportMarkers) {
            this._reportMarkers.forEach(marker => marker.setMap(null));
        }
        this._reportMarkers = [];
        let newMapNeeded: boolean = false;
        // Add report markers
        value.forEach(option => {
            let marker: google.maps.Marker = new google.maps.Marker(option);
            marker.setMap(this.map);
            if(option.visible == false) {
                // Map does not hide markers correctly with visibile: false
                // hence the need for a new map
                newMapNeeded = true;
            }
            this._reportMarkers = this._reportMarkers.concat(marker);
        });
        if(newMapNeeded) {
                let targetElement: any = this._renderer.selectRootElement(".map");
                this.map = new google.maps.Map(targetElement, {
                    center: this.map.getCenter(),
                    zoom: this.map.getZoom(),
                    //scrollwheel: this._scrollWheel,
                    mapTypeControl: this._mapTypeControl,
                    fullscreenControl: false,
                    styles: this.styles
                });
                // Re-add the deviceMarkers
                if (this._deviceMarkers) {
                    this._deviceMarkers.forEach(marker => marker.setMap(this.map));
                }
        }
        this._ref.detectChanges();
        // If map is undefined here we cannot use this.reportMarkerClusterer
        if(typeof this.map !== 'undefined') {
            this.reportMarkerClustererOptions = {
                styles: [
                    {
                        url: "assets/icons/marker-clusterer/m1.png",
                        width: 52,
                        height: 52,
                        textColor: 'white'
                    },
                    {
                        url: "assets/icons/marker-clusterer/m2.png",
                        width: 56,
                        height: 56,
                        textColor: 'white'
                    },
                    {
                        url: "assets/icons/marker-clusterer/m3.png",
                        width: 66,
                        height: 66,
                        textColor: 'white'
                    },
                    {
                        url: "assets/icons/marker-clusterer/m4.png",
                        width: 78,
                        height: 78,
                        textColor: 'white'
                    },
                    {
                        url: "assets/icons/marker-clusterer/m5.png",
                        width: 90,
                        height: 90,
                        textColor: 'white'
                    }
                ]
            };
            this.reportMarkerClusterer = new MarkerClusterer(this.map, this._reportMarkers, this.reportMarkerClustererOptions);
        }
    }

    @Input()
    set deviceMarkers(value: google.maps.MarkerOptions[]) {
        // Remove device markers
        if (this._deviceMarkers) {
            this._deviceMarkers.forEach(marker => marker.setMap(null));
        }
        this._deviceMarkers = [];
        // Add device markers
        let index = 0;
        value.forEach(option => {
            let marker: google.maps.Marker = new google.maps.Marker(option);
            let contentString = this._deviceMarkersInfoWindowContentStrings[index];
            let infoWindow = new google.maps.InfoWindow({ content: contentString });
            marker.addListener('click', function() {
                infoWindow.open(this.map, marker);
            });
            marker.setMap(this.map);
            this._deviceMarkers.push(marker);
            index++;
        });
        this._ref.detectChanges();
        // NO MARKER CLUSTERER FOR DEVICES
    }

    @Input()
    set deviceMarkersInfoWindowContentStrings(value: string[]) {
        this._deviceMarkersInfoWindowContentStrings = value;
    }

    private _reportMarkers: google.maps.Marker[];
    private _deviceMarkers: google.maps.Marker[];
    private _deviceMarkersInfoWindowContentStrings: any = [];
    private _zoom: number = 15;
    private _center: google.maps.LatLng = new google.maps.LatLng(0, 0);
    //private _scrollWheel: boolean = true; // true, to enable zooming on mouse wheel scrolling.
    private _mapTypeControl: boolean = false; // the initial enabled/disabled state of the Map type control.

    map: google.maps.Map;
    reportMarkerClusterer: any; // define markerClusterer outside of constructor
    deviceMarkerClusterer: any; // define markerClusterer outside of constructor
    reportMarkerClustererOptions: any; // define markerClustererOptions outside of constructor
    deviceMarkerClustererOptions: any; // define markerClustererOptions outside of constructor
    markerDraggable: boolean;
    markerTitle: string;
    markerLat: any;
    markerLng: any;
    markerPosition: google.maps.LatLng;
    // Map styles
    styles: any = [
                {
                    "featureType": "administrative.country",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#FFBB00"
                        },
                        {
                            "saturation": 43.400000000000006
                        },
                        {
                            "lightness": 37.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": -40
                        },
                        {
                            "lightness": 36
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": -77
                        },
                        {
                            "lightness": 28
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#00FF6A"
                        },
                        {
                            "saturation": -1.0989010989011234
                        },
                        {
                            "lightness": 11.200000000000017
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": -24
                        },
                        {
                            "lightness": 61
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#ffc200"
                        },
                        {
                            "saturation": -61.8
                        },
                        {
                            "lightness": 45.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#FF0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 51.19999999999999
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#ff0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 52
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#0078FF"
                        },
                        {
                            "saturation": -13.200000000000003
                        },
                        {
                            "lightness": 2.4000000000000057
                        },
                        {
                            "gamma": 1
                        }
                    ]
                }
            ]; 

    constructor(
        private _renderer: Renderer,
        private _ref: ChangeDetectorRef,
        private _ngZone: NgZone) { }

    ngOnInit() {
        let targetElement: any = this._renderer.selectRootElement(".map");
        let me: GoogleMapComponent = this;
        this.map = new google.maps.Map(targetElement, {
            center: this._center,
            zoom: this._zoom,
            //scrollwheel: this._scrollWheel,
            mapTypeControl: this._mapTypeControl,
            fullscreenControl: false,
            styles: this.styles
        });
    }
}
