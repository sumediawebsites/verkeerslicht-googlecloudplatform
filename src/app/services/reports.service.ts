import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import "rxjs/add/operator/map";

import { Report } from '../models/report.model';

@Injectable()
export class Reports {

    private baseUrl: string = 'https://verkeerslicht-165607.appspot.com/api'; // TEMP, RENAME TO FINAL URL!!

	requestOptions: RequestOptions = new RequestOptions({
		headers: new Headers({ 'Content-Type': 'application/json' })
	});

	constructor(private http: Http) { }
    // Used without pagination
	getAll(): Observable<Report[]> {
		return this.http.get(`${this.baseUrl}/reports`).map(this.extractData).catch(this.handleError);
	}
    // Used with pagination
	findAll(offset: number = 0, limit: number = 2): Observable<Reports> {
      return this.http
        .get(`${this.baseUrl}/reports/index/${offset}/${limit}`) // cakePHP expects index, offset and limit as /index/offset/limit
		.map(response => response.json())
		.map(results => this.getList(results));
	}

	get(reportId: number): Observable<Report> {
		return this.http.get('https://verkeerslicht.dev/api/reports/' + encodeURIComponent(reportId.toString())).map(this.extractData).catch(this.handleError);
	}

	insert(report: Report): Observable<Report> {
		return this.http.post('https://verkeerslicht.dev/api/reports/', JSON.stringify(report), this.requestOptions).map(res => res.json()).catch(this.handleError);
	}

	update(report: Report): Observable<Report> {
		return this.http.put('https://verkeerslicht.dev/api/reports/' + encodeURIComponent(report.id.toString()),
			JSON.stringify(report), this.requestOptions).map(res => res.json()).catch(this.handleError);		
	}

	delete(reportId: number): Observable<Report> {
		return this.http.delete('https://verkeerslicht.dev/api/reports/' + encodeURIComponent(reportId.toString())).map(res => res.json()).catch(this.handleError);
	}

    getList(data): Reports {
		// room for additional filtering
		return data;
	}

	/**
	 * Pick the array that belongs to the key 'reports'
	 * 
	 * e.g. { reports:[our data is in here] }
	 */
	private extractData(res: Response) {
		let body = res.json();
		return body.reports || {};
	}

	/**
	 * Handle error
	 */
	private handleError(error: any) {
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	return Observable.throw(errMsg);
	}

}