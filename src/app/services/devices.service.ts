import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import "rxjs/add/operator/map";

import { Device } from '../models/device.model';

@Injectable()
export class Devices {

    private baseUrl: string = 'https://verkeerslicht-165607.appspot.com/api'; // TEMP, RENAME TO FINAL URL!!

	requestOptions: RequestOptions = new RequestOptions({
		headers: new Headers({ 'Content-Type': 'application/json' })
	});

	constructor(private http: Http) { }
    // Used without pagination
	getAll(): Observable<Device[]> {
		return this.http.get(`${this.baseUrl}/devices`).map(this.extractData).catch(this.handleError);
	}
    // Used with pagination
	findAll(offset: number = 0, limit: number = 2): Observable<Devices> {
      return this.http
        .get(`${this.baseUrl}/devices/index/${offset}/${limit}`) // cakePHP expects index, offset and limit as /index/offset/limit
		.map(response => response.json())
		.map(results => this.getList(results));
	}

	get(deviceId: number): Observable<Device> {
		return this.http.get('https://verkeerslicht-165607.appspot.com/api/device/' + encodeURIComponent(deviceId.toString())).map(this.extractData).catch(this.handleError);
	}

	insert(device: Device): Observable<Device> {
		return this.http.post('https://verkeerslicht-165607.appspot.com/api/devices/', JSON.stringify(device), this.requestOptions).map(res => res.json()).catch(this.handleError);  // WORKS!!
	}

	update(device: Device): Observable<Device> {
		return this.http.put('https://verkeerslicht-165607.appspot.com/api/devices/' + encodeURIComponent(device.id.toString()),
			JSON.stringify(device), this.requestOptions).map(res => res.json()).catch(this.handleError);		
	}

	delete(deviceId: number): Observable<Device> {
		return this.http.delete('https://verkeerslicht-165607.appspot.com/api/devices/' + encodeURIComponent(deviceId.toString())).map(res => res.json()).catch(this.handleError);  // WORKS!!
	}

    getList(data): Devices {
		// room for additional filtering
		return data;
	}

	/**
	 * Pick the array that belongs to the key 'devices'
	 * 
	 * e.g. { devices:[our data is in here] }
	 */
	private extractData(res: Response) {
		let body = res.json();
		//console.log(body.devices);
		return body.devices || {};
	}

	/**
	 * Handle error
	 */
	private handleError(error: any) {
    	let errMsg = (error.message) ? error.message :
      	error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    	return Observable.throw(errMsg);
	}

}