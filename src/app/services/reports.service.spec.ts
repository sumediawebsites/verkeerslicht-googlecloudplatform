import { async, inject, TestBed } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';

import { Reports } from './reports.service';

const mockReportData = [
	{ id: 1, device_id: 1, report_status_id: 1, reporter_id: 1, report_type_id: 1,
    title: 'testTitle_ReportFromReporterID_1', body: 'testBody_ReportAboutDeviceID_1',
    created_by: null, modified_by: null, created_at: new Date(), modified_at: new Date(), deleted: null },
	{ id: 2, device_id: 2, report_status_id: 1, reporter_id: 2, report_type_id: 1,
    title: 'testTitle_ReportFromReporterID_2', body: 'testBody_ReportAboutDeviceID_2',
    created_by: null, modified_by: null, created_at: new Date(), modified_at: new Date(), deleted: null },
	{ id: 3, device_id: 3, report_status_id: 1, reporter_id: 3, report_type_id: 1,
    title: 'testTitle_ReportFromReporterID_3', body: 'testBody_ReportAboutDeviceID_3',
    created_by: null, modified_by: null, created_at: new Date(), modified_at: new Date(), deleted: null },
];

describe('Reports Service', () => {
	
	let reports: Reports;
	let mockBackend: MockBackend;
	let mockResponse: Response;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule ],
      providers: [
        Reports,
        { provide: XHRBackend, useClass: MockBackend }
      ]
    })
    .compileComponents();
  }));	

	beforeEach(inject([Http, XHRBackend], (http: Http, _mockBackend: MockBackend) => {

		mockBackend = _mockBackend;
		
		mockResponse = new Response(new ResponseOptions({
			status: 200,
			headers: new Headers({ 'Content-Type': 'application/json' }),
			body: JSON.stringify(mockReportData)
		}));

		reports = new Reports(http);
	
	}));	

	it('can fetch reports', async(inject([], () => {

		mockBackend.connections.subscribe((connection: MockConnection) => {

			if(connection.request.url.endsWith('/reports')) {
				connection.mockRespond(mockResponse);
			}

			connection.mockError(Error('request url did not match expected url'));
		
	});

		reports.getAll().toPromise().then(reports => {
			expect(reports.length).toEqual(mockReportData.length);
		}).catch((err: Error) => {
			fail(err.message);
		});

	})));

});