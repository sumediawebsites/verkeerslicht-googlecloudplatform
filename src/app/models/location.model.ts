export class Location {
	constructor(
		public id: number,
		public address: string,
		public council_id: number,
		public place_id: number,
		public longitude: number,
		public latitude: number,
		public created_by?: number,
		public modified_by?: number,
		public created_at?: Date,
		public modified_at?: Date,
		public deleted?: Date
	) { }
}