import { Device } from './device.model'; 

export class Report {
	constructor(
		public id: number,
		public device_id: number,
		public report_status_id: number,
		public reporter_id: number,
		public report_type_id: number,
		public title: string,
		public body: string,
		public device: Device[],
		public created_by?: number,
		public modified_by?: number,
		public created_at?: Date,
		public modified_at?: Date,
		public deleted?: Date
	) { }
}