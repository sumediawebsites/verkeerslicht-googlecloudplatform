import { Location } from './location.model';

export class Device {
	constructor(
		public id: number,
		public location_id: number,
		public supplier_id: number,
		public user_id: number,
		public name: string,
		public location: Location[],
		public created_by?: number,
		public modified_by?: number,
		public created_at?: Date,
		public modified_at?: Date,
		public deleted?: Date
	) { }
}