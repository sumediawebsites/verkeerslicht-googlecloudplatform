import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoogleMapComponent } from './google-map/google-map.component';
import { ReportsViewerComponent } from './reports/reports-viewer/reports-viewer.component';
import { ReportViewerComponent } from './reports/report-viewer/report-viewer.component';
import { ReportEditorComponent } from './reports/report-editor/report-editor.component';

const appRoutes: Routes = [
    { path: '', redirectTo: '/pagina/1', pathMatch: 'full' },
	{ path: 'pagina/:nr', component: ReportsViewerComponent },
    { path: 'report/:id', component: ReportViewerComponent },
	{ path: 'report/:id/edit', component: ReportEditorComponent }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true });