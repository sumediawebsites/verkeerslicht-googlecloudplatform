import { Component, Input } from '@angular/core';

import { App } from '../../models/app.model';

@Component({
	selector: 'page-footer',
	templateUrl: './page-footer.component.html',
	styles: ['./page-footer.component.scss']
})
export class PageFooterComponent {

	@Input()
	app: App;

}