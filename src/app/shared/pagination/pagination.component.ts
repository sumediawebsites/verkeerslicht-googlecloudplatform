import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';

/* Based on 'https://g00glen00b.be/pagination-component-angular-2/' */

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() offset: number = 0;
  @Input() limit: number = 1;
  @Input() size: number = 1;
  @Input() range: number = 3;
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  currentPage: number;
  totalPages: number;
  pages: Observable<number[]>;  

  constructor() { }

  ngOnInit() {
    this.getPages(this.offset, this.limit, this.size);
  }

  ngOnChanges() {
    this.getPages(this.offset, this.limit, this.size);    
  }

  getPages(offset: number, limit: number, size: number) {
    this.currentPage = this.getCurrentPage(offset, limit);
    this.totalPages = this.getTotalPages(limit, size);
    this.pages = Observable.range(-this.range, this.range * 2 + 1)
      .map(offset => this.currentPage + offset)
      .filter(page => this.isValidPageNumber(page, this.totalPages))
      .toArray();    
  }

  isValidPageNumber(page: number, totalPages: number): boolean {
    return page > 0 && page <= totalPages;
  }

  getCurrentPage(offset: number, limit: number): number {
    // For example, if the offset is 20 and the limit is 10, then the page number would be 3 
    // (because offset 0 = page 1, offset 10 = page 2 and offset 20 = page 3).
    return Math.floor(offset / limit) + 1;
  }

  getTotalPages(limit: number, size: number): number {
    // To get the total amount of pages we divide the size by the limit. 
    // If there are 810 items and the page size is 20, then there are 41 pages, 
    // of which there are 40 whole pages and 1 half page. 
    // To be sure that we don’t have a limit of 0 and we’re dividing by zero, 
    // I’m using Math.max() to make sure that both the size and the limit are always at least 1.
    return Math.ceil(Math.max(size, 1) / Math.max(limit, 1));
  }

  selectPage(page: number, event) {
    this.cancelEvent(event);
    if (this.isValidPageNumber(page, this.totalPages)) {
      this.pageChange.emit((page - 1) * this.limit);
    }
  }

  cancelEvent(event) {
    event.preventDefault();
  }

}
