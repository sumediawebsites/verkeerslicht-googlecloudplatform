import { Component, Input } from '@angular/core';

import { App } from '../../models/app.model';

@Component({
	selector: 'page-header',
	templateUrl: './page-header.component.html',
	styles: ['./page-header.component.scss']
})
export class PageHeaderComponent {

	@Input()
	app: App;

}