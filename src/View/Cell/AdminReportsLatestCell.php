<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * ReportsLatest cell
 */
class AdminReportsLatestCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display( $conditions = array() )
    {
        $reportsTable = TableRegistry::get('Reports');

        $reports = $reportsTable->find('all', [
            'order' => [ 'Reports.created_at' => 'DESC' ],
            'contain' => [ 'ReportStatuses', 'ReportTypes', 'Devices' ],
            'conditions' => [
                'Reports.vri_street_1 IS NOT' => '',
                'Reports.vri_place IS NOT' => null,
                'Reports.date IS NOT' => null,
                'Reports.time IS NOT' => null,
                'ReportTypes.name IS NOT' => null,
                $conditions
            ],
            'limit' => 3,
        ])->toArray();


        $this->set(compact('reports'));
        $this->set('_serialize', ['reports']);
    }
}
