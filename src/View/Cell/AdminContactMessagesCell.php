<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * AdminContactMessages cell
 */
class AdminContactMessagesCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $contactsTable = TableRegistry::get('Contact');

        $contactMessages = $contactsTable->find('all', [
            'order' => [ 'Contact.created_at' => 'DESC' ],
            'contain' => [],
            'conditions' => [
                'Contact.is_answered' => false,
                'Contact.is_admin' => false,
            ],
        ])->toArray();

        $this->set(compact('contactMessages'));
        $this->set('_serialize', ['contactMessages']);
    }
}
