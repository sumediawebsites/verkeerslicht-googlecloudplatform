<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\Chronos\Chronos;

/**
 * PendingReports cell
 */
class PendingReportsCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $device_id = 3;
        $reportsTable = TableRegistry::get( 'reports' );
        $managersReports = $reportsTable->find('all')->where( [ 'device_id =' => $device_id ] );
        $managersReports = $managersReports->toArray();

        $this->set(compact('managersReports'));

    }
}
