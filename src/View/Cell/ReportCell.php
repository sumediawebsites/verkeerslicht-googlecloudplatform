<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Report cell
 */
class ReportCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $now = Time::now();
        $now->timezone = 'Europe/Amsterdam';

        $this->loadModel('Reports');
        $this->loadModel('ReportTypes');
        $report = TableRegistry::get('ReportTypes');
        $reportTypes = $this->Reports->ReportTypes->find('list', ['limit' => 200]);

        $this->set(compact('report', 'reportTypes', 'now' ));
        $this->set('_serialize', ['report', 'now']);
    }
}