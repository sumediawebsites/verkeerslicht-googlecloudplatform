<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * AdminReportTypePiecharts cell
 */
class AdminReportTypePiechartsCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display( $conditions = array() )
    {
        $reportsTable = TableRegistry::get('Reports');

        $reportsQuery = $reportsTable->find('all', [
            'contain' => [ 'Devices' ],
            'conditions' => [
                $conditions
            ]
        ]);
        $totalReports = count($reportsQuery->toArray());

        $newReports = count($reportsTable->find('all', [
            'contain' => [ 'Devices' ],
            'conditions' => [
                'report_status_id' => 3,
                $conditions
            ]
        ])->toArray());

        $positiveReports = count($reportsTable->find('all', [
            'contain' => [ 'Devices' ],
            'conditions' => [
                'report_status_id' => 5,
                $conditions
            ]
        ])->toArray());

        $negativeReports = count($reportsTable->find('all', [
            'contain' => [ 'Devices' ],
            'conditions' => [
                'report_status_id' => 6,
                $conditions
            ]
        ])->toArray());

        $heldReports = count($reportsTable->find('all', [
            'contain' => [ 'Devices' ],
            'conditions' => [
                'report_status_id' => 4,
                $conditions
            ]
        ])->toArray());

        $rejectedReports = count($reportsTable->find('all', [
            'contain' => [ 'Devices' ],
            'conditions' => [
                'report_status_id' => 8,
                $conditions
            ]
        ])->toArray());

        $this->set(compact('totalReports', 'newReports', 'positiveReports', 'negativeReports', 'heldReports', 'rejectedReports'));
        $this->set('_serialize', ['reports']);
    }
}
