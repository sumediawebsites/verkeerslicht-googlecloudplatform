<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * AdminCounter cell
 */
class AdminCounterCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display( $title = null, $table = null, $conditions = array(), $className = '' )
    {
        $this->loadModel( $table );
        $result = $this->$table->find('all', [
            'contain' => [
                'Devices'
            ],
            'conditions' => [
                $conditions
            ]
        ]);
        $this->set('count', $result->count() );
        $this->set('title', $title );
        $this->set('className', $className );
    }
}
