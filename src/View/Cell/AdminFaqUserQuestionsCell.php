<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * AdminFaqUserQuestions cell
 */
class AdminFaqUserQuestionsCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $faqUserQuestionsTable = TableRegistry::get('FaqUserQuestions');

        $faqUserQuestions = $faqUserQuestionsTable->find('all', [
            'order' => [ 'FaqUserQuestions.created_at' => 'DESC' ],
            'contain' => [ 'FaqCategories' ],
            'conditions' => [
                'FaqUserQuestions.is_response' => false,
                'FaqUserQuestions.answered' => false,
            ],
        ])->toArray();


        $this->set(compact('faqUserQuestions'));
        $this->set('_serialize', ['faqUserQuestions']);
    }
}
