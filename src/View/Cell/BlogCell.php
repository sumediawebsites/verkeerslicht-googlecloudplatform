<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * Blog cell
 */
class BlogCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {

        $blogTable = TableRegistry::get('BlogArticles');

        $blogArticles = $blogTable->find('all', [
            'order' => [ 'BlogArticles.id' => 'DESC' ],
            'limit' => 3,
        ])->toArray();


        $this->set(compact('blogArticles'));
        $this->set('_serialize', ['blogArticles']);

    }
}
