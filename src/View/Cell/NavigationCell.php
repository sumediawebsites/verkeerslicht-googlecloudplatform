<?php
namespace App\View\Cell;

use Aura\Intl\Exception;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\View\Cell;

/**
 * Navigation cell
 */
class NavigationCell extends Cell {

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display( $locale = null ) {
        if ( empty( $locale ) ) {
            $locale = 'nl_NL';
        }

        $this->loadModel( 'Cms.Menus' );
        $this->loadModel( 'Cms.MenuItems' );

        $menus = $this->Menus->findByLocale( $locale );
        if ( $menus->count() > 0 ) {
            $menu      = $menus->first();
            $menuItems = $this->MenuItems->find( 'threaded', [
                'conditions' => [
                    'menu_id' => $menu->id
                ],
                'order'      => [ 'lft' => 'ASC' ]
            ] )->toArray();

            foreach ( $menuItems as $menuItem ) {
                $menuItem->parsedUrl = Router::parse( $menuItem->url );
            }

            $this->set( compact( 'menuItems' ) );
        } else {
            Throw New Exception( __( 'No menu set for locale ' . $locale ) );
        }
    }
}