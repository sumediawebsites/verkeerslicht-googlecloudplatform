<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\Chronos\Chronos;
use Cake\I18n\Date;

/**
 * AdminGraph cell
 */
class AdminGraphCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display( $days = 7, $conditions = array() )
    {
        $reportsTable = TableRegistry::get('Reports');

        $startDate = new Date('-'.$days.' days');

        $conditions_preset = [
            'Reports.vri_street_1 IS NOT' => '',
            'Reports.vri_place IS NOT' => null,
            'ReportTypes.name IS NOT' => null,
            'Reports.created_at >=' => $startDate
        ];
        if( isEmpty($conditions) ){
            $conditions = $conditions_preset;
        } else {
            $conditions = array_merge( $conditions_preset, $conditions );
        }

        $reports = $reportsTable->find('all', [
            'select' => 'Reports.created_at',
            'order' => [ 'Reports.created_at' => 'DESC' ],
            'contain' => [ 'ReportStatuses', 'ReportTypes', 'Devices' ],
            'conditions' => $conditions
        ])->toArray();

        $groupedReports = [];

        for ($i = 0; $i <= $days; $i++) {
            $date = new Date( '-'.($days-$i).' days' );
            $dateString = $date->format('d M');
            $groupedReports[$dateString] = 0;
        }

        foreach($reports as $report){
            $date = new Date($report->created_at);
            $dateString = $date->format('d M');

            if( isset($groupedReports[$dateString]) ){
                $groupedReports[$dateString] ++;
            } else {
                $groupedReports[$dateString] = 1;
            };
        }

        $this->set(compact('groupedReports'));
        $this->set('_serialize', ['groupedReports']);
    }
}
