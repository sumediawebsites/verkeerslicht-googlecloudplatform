<?php
namespace App\Shell;

use Cake\Console\Shell;

/**
 * AdminUserCreate shell command.
 */
class AdminUserCreateShell extends Shell {

	public function initialize() {
		parent::initialize();
		$this->loadModel( 'Users' );
	}

	/**
	 * Manage the available sub-commands along with their arguments and help
	 *
	 * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
	 *
	 * @return \Cake\Console\ConsoleOptionParser
	 */
	public function getOptionParser() {
		$parser = parent::getOptionParser();

		return $parser;
	}

	/**
	 * main() method.
	 *
	 * @return bool|int Success or error code.
	 */
	public function main() {
		if ( empty( $this->args[0] ) ) {
			return $this->abort( 'Please enter a username' );
		} elseif ( empty( $this->args[1] ) ) {
			return $this->abort( 'Please enter an emailaddress' );
		} elseif ( empty( $this->args[2] ) ) {
			return $this->abort( 'Please enter a password' );
		} else {
			$this->Users->addBehavior( 'Tools.Passwordable' );
			$user = $this->Users->newEntity();
			$user = $this->Users->patchEntity( $user, [
				'firstname'  => $this->args[0],
				'username'   => $this->args[0],
				'email'      => $this->args[1],
				'pwd'        => $this->args[2],
				'pwd_repeat' => $this->args[2],
				'role_id'    => 1
			] );
			if ( $this->Users->save( $user ) ) {
				$this->out( 'Successfully created user ' . $this->args[0] );
			} else {
				debug( $user );
			}
		}

	}
}
