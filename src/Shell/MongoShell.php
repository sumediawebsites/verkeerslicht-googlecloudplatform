<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Settings\Core\Setting;

/**
 * Mongo shell command.
 */
class MongoShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Reports');
        $this->loadModel('Reporters');
        $this->loadModel('ReportStatuses');
        $this->loadModel('ReportTypes');
        $this->_io->styles('success', ['text' => 'green']);
        $this->_io->styles('error', ['text' => 'red']);
    }

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
    }

    public function process(){

      // Connect to MongoDB
      $mongo = new \MongoDB\Client(Setting::read('App.Mongo.DB.Server'));

          // Set vars
          $entryCount = 0;
          $processedCount = 0;

          $dbName = 'devices';
          $this->out("Database name: " . $dbName);

          $db = $mongo->$dbName;
          $coll = $db->reports;

          // Connect to cake / mySQL DB
          $reportsTable = TableRegistry::get('Reports');

          // Search mongo DB
          $queueQuery = array('queue_status' => 'queued');
          $cursor = $coll->find($queueQuery);

          foreach ($cursor as $id => $object) {
              $entryCount++;

              // Get Mongo vars
              $mongo_id = $object->_id;
              $created_at = $object->created_at;

              $reporter_email = $object->reporter_email;
              $reporter_firstname = $object->reporter_firstname;
              $reporter_lastname = $object->reporter_lastname;
              $keep_me_informed = $object->keep_me_informed;

              $report_type_id = $object->report_type_id;
              $body = $object->body;
              $latitude = $object->latitude;
              $longitude = $object->longitude;
              $vri_place = $object->vri_place;
              $vri_street_1 = $object->vri_street_1;
              $vri_street_2 = $object->vri_street_2;
              $date = $object->date;
              $time = $object->time;
              $vehicle = $object->vehicle;

              // determine of the report is complete or not
              if ( $vehicle == null OR $vehicle == '' ){
                  $report_is_completed = false;
                  $this->out( "Report is not complete." );
              } else {
                  $report_is_completed = true;
                  $this->out( "Report is complete." );
              }

              // check if user already exsists, if not: create new user
              $exists = $this->Reporters->exists(['Reporters.email' => $reporter_email]);
              if ($exists) {
                  // add report to existing user

                  $this->out("User " . $reporter_email . " already exists.");

                  $reporter = $this->Reporters->find()
                      ->where(['Reporters.email' => $reporter_email])
                      ->first();
                  $reporter_id = $reporter->id;

              } else {
                  // create new user, send them a registration email, add report to this new user

                  $this->out("User " . $reporter_email . " does not exist.");

                  $reporter = $this->Reporters->newEntity();
                  $reporter->email = $reporter_email;
                  $reporter->firstname = $reporter_firstname;
                  $reporter->lastnamel = $reporter_lastname;

                  $passwordToken = $reporter->createNewPasswordToken();
                  $reporter->password_token = $passwordToken;

                  if ($this->Reporters->save($reporter)) {
                      $reporter->sendConfirmRegistrationMail($passwordToken);

                      $this->out("<success>Password token has been sent to: " . $reporter_email . "</success>");

                      $reporter_id = $reporter->id;
                  } else {
                      $this->out("<error>Error: Could not create Reporter.</error>");
                  }

              }

              // Set vars to Cake entity
              $report = $reportsTable->newEntity();

              $reportStatusName = 'Nieuwe melding';
              $reportStatusEntity = $this->ReportStatuses->find()
                  ->where(['ReportStatuses.name' => $reportStatusName])
                  ->first();
              $reportStatusId = $reportStatusEntity->id;

              $report->report_status_id = $reportStatusId;
              $report->reporter_id = $reporter_id;
              $report->report_type_id = $report_type_id;
              $report->body = $body;
              $report->created_at = $created_at;
              $report->latitude = $latitude;
              $report->longitude = $longitude;
              $report->vri_place = $vri_place;
              $report->vri_street_1 = $vri_street_1;
              $report->vri_street_2 = $vri_street_2;
              $report->date = $date;
              $report->time = $time;
              $report->vehicle = $vehicle;
              $report->keep_me_informed = $keep_me_informed;

              // Save entity
              if ($reportsTable->save($report)) {
                  // Update queue status of entry
                  $queue_status = "processed";
                  $coll->updateOne(['_id' => new \MongoDB\BSON\ObjectID($mongo_id)], ['$set' => ["queue_status" => $queue_status]]);
                  $processedCount++;
              } else {
                  $this->out( "<error>Error, saving entity.</error>" );
              };

              // remind the reporter to finish their report, if it was not complete yet.
              if ( $report_is_completed === false ){
                  $reporter->sendCompleteReportMail( $report->id );
                  $this->out("<success>A reminder has been sent to " . $reporter_email . "</success>");
              }

          }
          $this->out($entryCount . " entries found.");
          $this->out("<success>" . $processedCount . " entries processed!</success>");
          $this->out("");
    }
}
