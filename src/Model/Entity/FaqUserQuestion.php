<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Mailer\Email;
use Settings\Core\Setting;

/**
 * FaqUserQuestion Entity
 *
 * @property int $id
 * @property int $faq_category_id
 * @property string $subject
 * @property string $message
 * @property string $response
 * @property string $email
 * @property bool $answered
 * @property bool $published
 * @property bool $highlighted
 * @property int $created_by
 * @property int $modified_by
 * @property int $parent_id
 * @property int $lft
 * @property int $rght
 * @property bool $is_response
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property \Cake\I18n\Time $deleted
 *
 * @property \App\Model\Entity\FaqCategory $faq_category
 * @property \App\Model\Entity\User $createdBy
 * @property \App\Model\Entity\User $modifiedBy
 */
class FaqUserQuestion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_virtual = [
        'fullname'
    ];

    protected function _getFullname(){
        if( isset( $this->firstname ) OR isset( $this->lastname ) ) {
            return $this->firstname . ' ' . $this->lastname;
        } elseif ( isset( $this->email )){
            return $this->email;
        }
    }

    /**
     *
     * Send question to admin
     *
     */
    public function sendQuestionMail() {
        $email = new Email();
        $email->emailFormat( 'html' )
            ->from( $this->email )
            ->to( [ Setting::Read( 'App.Email.DefaultSenderEmail' ) => Setting::Read( 'App.Email.DefaultSenderName' ) ] )
            ->subject( __( 'New question' ) );

        $email->template( 'default', 'question' );

        $email->viewVars( [
            'title'     => __( 'New question' ),
            'message'   => $this->message
        ] );

        $email->send();
    }

    /**
     *
     * Send question response to user
     *
     */
    public function sendResponseMail() {
        $email = new Email();
        $email->emailFormat( 'html' )
            ->from( [ Setting::Read( 'App.Email.DefaultSenderEmail' ) => Setting::Read( 'App.Email.DefaultSenderName' ) ] )
            ->to( $this->email )
            ->subject( __( 'Response to your question' ) );

        $email->template( 'default', 'question' );

        $email->viewVars( [
            'title'     => __( 'Response to your question' ),
            'message'   => $this->response
        ] );

        $email->send();
    }
}
