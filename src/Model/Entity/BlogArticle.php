<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BlogArticle Entity
 *
 * @property int $id
 * @property int $blog_category_id
 * @property string $title
 * @property string $slug
 * @property string $body
 * @property int $created_by
 * @property int $modified_by
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property \Cake\I18n\Time $deleted
 * @property string $image_url
 *
 * @property \App\Model\Entity\BlogCategory $blog_category
 */
class BlogArticle extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
