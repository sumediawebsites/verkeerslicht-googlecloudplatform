<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReportType Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $created_by
 * @property int $modified_by
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property \Cake\I18n\Time $deleted
 *
 * @property \App\Model\Entity\Report[] $reports
 */
class ReportType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
