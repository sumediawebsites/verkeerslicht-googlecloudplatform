<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Role Entity
 *
 * @property int $id
 * @property string $name
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property int $created_by
 * @property int $modified_by
 * @property \Cake\I18n\Time $deleted
 *
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Right[] $rights
 */
class Role extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

	public function getRights(){
		$rolesTable = TableRegistry::get('roles');
		$currentRole = $rolesTable->find('all', [
			'conditions' => [
				'roles.id' => $this->id,
			],
			'contain' => 'Rights'
		])->first();

		$allowRights = [];
		$denyRights = [];

		foreach($currentRole->rights as $right){
			if($right->_joinData->allow){
				$allowRights[] = $right->code;
			} elseif($right->_joinData->deny){
				$denyRights[] = $right->code;
			}
		}

		return [
			'allow' => $allowRights,
			'deny' => $denyRights
		];
	}
}
