<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contact Entity
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $subject
 * @property string $message
 * @property int $parent_id
 * @property bool $is_answered
 * @property bool $is_admin
 * @property int $created_by
 * @property int $modified_by
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property \Cake\I18n\Time $deleted
 *
 * @property \App\Model\Entity\Contact $parent_contact
 * @property \App\Model\Entity\Contact[] $child_contact
 */
class Contact extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_virtual = [
        'fullname'
    ];

    protected function _getFullname(){
        if( isset( $this->firstname ) OR isset( $this->lastname ) ) {
            return $this->firstname . ' ' . $this->lastname;
        } elseif ( isset( $this->email )){
            return $this->email;
        }
    }
}
