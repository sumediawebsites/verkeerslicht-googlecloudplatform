<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Routing\Router;
use Notifier\Utility\NotificationManager;
use Cake\ORM\TableRegistry;

/**
 * ChatMessage Entity
 *
 * @property int $id
 * @property int $parent_id
 * @property int $sender_id
 * @property int $receiver_id
 * @property string $message
 * @property \Cake\I18n\Time $read
 * @property int $created_by
 * @property int $modified_by
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property \Cake\I18n\Time $deleted
 *
 * @property \App\Model\Entity\ChatMessage $parent_chat_message
 * @property \App\Model\Entity\Sender $sender
 * @property \App\Model\Entity\Receiver $receiver
 * @property \App\Model\Entity\ChatMessage[] $child_chat_messages
 * @property \App\Model\Entity\User $createdBy
 * @property \App\Model\Entity\User $modifiedBy
 */
class ChatMessage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function sendNotification( $sender = null, $receiver = null ){

        $indexURL = Router::url( [ 'controller' => 'ChatMessages', 'action' => 'index', 'prefix' => 'admin', 'plugin' => false ] );
        $editURL = Router::url( [ 'controller' => 'ChatMessages', 'action' => 'edit', 'prefix' => 'admin', 'plugin' => false ] );
        if(isset($this->parent_id)){
            $link = $editURL.'/'.$this->parent_id;
        } elseif(isset($this->id)) {
            $link = $editURL.'/'.$this->id;
        } else {
            $link = $indexURL;
        }

        // Send notification
        $notificationManager = new NotificationManager();
        $notificationManager->notify([
            'users' => [ $receiver->id ],
            'template' => 'chatMessage',
            'vars' => [
                'user_name' => $sender->fullname,
                'body' => $this->message,
                'url' => $link,
            ],
        ]);

    }
}
