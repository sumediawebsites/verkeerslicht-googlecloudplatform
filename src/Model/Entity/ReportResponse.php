<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReportResponse Entity
 *
 * @property int $id
 * @property int $reporter_id
 * @property int $user_id
 * @property int $device_id
 * @property string $subject
 * @property string $message
 * @property int $created_by
 * @property int $modified_by
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property \Cake\I18n\Time $deleted
 *
 * @property \App\Model\Entity\Reporter $reporter
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Device $device
 */
class ReportResponse extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
