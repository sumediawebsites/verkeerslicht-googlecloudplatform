<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Report Entity
 *
 * @property int $id
 * @property int $device_id
 * @property int $report_status_id
 * @property int $reporter_id
 * @property int $report_type_id
 * @property string $title
 * @property string $body
 * @property int $created_by
 * @property int $modified_by
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property \Cake\I18n\Time $deleted
 * @property string $from
 * @property string $to
 * @property float $longitude
 * @property float $latitude
 * @property string $vri_city
 * @property string $vri_street_1
 * @property string $vri_street_2
 * @property string $vehicle
 * @property \Cake\I18n\Time $date
 * @property \Cake\I18n\Time $time
 *
 * @property \App\Model\Entity\Device $device
 * @property \App\Model\Entity\ReportStatus $report_status
 * @property \App\Model\Entity\Reporter $reporter
 * @property \App\Model\Entity\ReportType $report_type
 * @property \App\Model\Entity\User $createdBy
 * @property \App\Model\Entity\User $modifiedBy
 */
class Report extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
