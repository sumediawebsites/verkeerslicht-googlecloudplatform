<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Mailer\Email;
use Settings\Core\Setting;
use Cake\ORM\TableRegistry;

/**
 * Reporter Entity
 *
 * @property int $id
 * @property string $initials
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $username
 * @property string $phone
 * @property string $email
 * @property string $password
 * @property string $settings
 * @property string $picture
 * @property bool $is_active
 * @property int $created_by
 * @property int $modified_by
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property \Cake\I18n\Time $deleted
 * @property string $password_token
 *
 * @property \App\Model\Entity\Report[] $reports
 */
class Reporter extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'pwd',
        'pwd_repeat'
    ];

    protected $_virtual = [
        'fullname'
    ];

    protected function _getFullname() {
        if( isset( $this->firstname ) AND isset( $this->lastname ) ) {
            return $this->firstname . ' ' . $this->lastname;
        } elseif ( isset( $this->firstname )){
            return $this->firstname;
        } elseif ( isset( $this->lastname )){
            return $this->lastname;
        } elseif ( isset( $this->email )) {
            return $this->email;
        }
    }

    /**
     *
     * Send welcome mail method
     *
     */
    public function sendWelcomeMail() {
        $email = new Email();
        $email->emailFormat( 'html' )
            ->from( [ Setting::Read( 'App.Email.DefaultSenderEmail' ) => Setting::Read( 'App.Email.DefaultSenderName' ) ] )
            ->to( $this->email )
            ->subject( __( 'Welkom bij Verkeerslicht.nl' ) );

        $email->template( 'default', 'welcome' );

        $email->viewVars( [
            'fullname'  => self::_getFullname(),
            'title'     => __( 'Welkom bij Verkeerslicht.nl' ),
            'message'   => __( 'Bedankt voor het deelnemen!' )
        ] );

        $email->send();
    }

    /**
     *
     * Send mail to confirm registration
     *
     */
    public function sendConfirmRegistrationMail( $registration_token ) {

        $email = new Email();
        $subject = 'Bevestig alstublieft uw registratie.';
        $email->emailFormat( 'html' )
            ->from( [ Setting::Read( 'App.Email.DefaultSenderEmail' ) => Setting::Read( 'App.Email.DefaultSenderName' ) ] )
            ->to( $this->email )
            ->subject( $subject );

        $email->template( 'default', 'confirm_registration' );

        $email->viewVars( [
            'fullname'  => self::_getFullname(),
            'registration_token'  => $registration_token,
            'email'  => $this->email,
            'title'     => $subject,
            'message'   => __( 'Bedankt voor uw melding.<br>Om op elk gewenst moment de status van uw melding in te zien, klikt u op de onderstaande link.' )
        ] );

        $email->send();
    }

    /**
     *
     * Send mail to remind the reporter to finish their report
     *
     */
    public function sendCompleteReportMail( $report_id ) {

        $this->Reports = TableRegistry::get('Reports');
        $report = $this->Reports->get($report_id, [
            'contain' => [
                'ReportTypes'
            ]
        ]);

        $email = new Email();
        $email->emailFormat( 'html' )
            ->from( [ Setting::Read( 'App.Email.DefaultSenderEmail' ) => Setting::Read( 'App.Email.DefaultSenderName' ) ] )
            ->to( $this->email )
            ->subject( __( 'Afmaken van uw melding.' ) );

        $email->template( 'default', 'complete_report' );

        $email->viewVars( [
            'fullname'  => self::_getFullname(),
            'report_id' => $report->id,
            'report_type_name' => $report->report_type->name,
            'report_location' => $report->vri_place,
        ] );

        $email->send();
    }

    /**
     *
     * Send welcome mail method
     *
     */
    public function sendReportStatusUpdateMail( $reportStatusName ) {
        $email = new Email();
        $email->emailFormat( 'html' )
            ->from( [ Setting::Read( 'App.Email.DefaultSenderEmail' ) => Setting::Read( 'App.Email.DefaultSenderName' ) ] )
            ->to( $this->email )
            ->subject( __( 'Status van melding is veranderd.' ) );

        $email->template( 'default', 'report_status_change' );
        $email->viewVars( [
            'fullname'  => self::_getFullname(),
            'title'     => __( 'De status van uw melding is bijgewerkt.' ),
            'message'   => __( 'Uw melding heeft nu de volgende status: ' . $reportStatusName )
        ] );

        $email->send();
    }

    /**
     *
     * Send new password mail
     *
     */
    public function sendPasswordResetTokenMail( $token ) {

        $email = new Email();
        $email->emailFormat( 'html' )
            ->from( [ Setting::Read( 'App.Email.DefaultSenderEmail' ) => Setting::Read( 'App.Email.DefaultSenderName' ) ] )
            ->to( $this->email )
            ->subject( __( 'Wachtwoord herstellen' ) );

        $email->template( 'default', 'password' );

        $email->viewVars( [
            'fullname'  => self::_getFullname(),
            'token'  => $token,
            'title'     => __( 'Wachtwoord herstellen' ),
            'message'   => __( 'Deze code heb je nodig om het wachtwoord te wijzigen.' )
        ] );

        $email->send();
    }

    public function createNewPasswordToken( $length = 12 ) {
        $token        = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen( $codeAlphabet ); // edited

        for ( $i = 0; $i < $length; $i ++ ) {
            $token .= $codeAlphabet[ $this->_crypto_rand_secure( 0, $max ) ];
        }

        return $token;
    }

    private function _crypto_rand_secure( $min, $max ) {
        $range = $max - $min;
        if ( $range < 1 ) {
            return $min;
        } // not so random...
        $log    = ceil( log( $range, 2 ) );
        $bytes  = (int) ( $log / 8 ) + 1; // length in bytes
        $bits   = (int) $log + 1; // length in bits
        $filter = (int) ( 1 << $bits ) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec( bin2hex( openssl_random_pseudo_bytes( $bytes ) ) );
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ( $rnd >= $range );

        return $min + $rnd;
    }


}
