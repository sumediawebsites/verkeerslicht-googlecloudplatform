<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property int $role_id
 * @property string $initials
 * @property string $firstname
 * @property string $insertion
 * @property string $lastname
 * @property string $username
 * @property string $phone
 * @property string $email
 * @property string $password
 * @property string $settings
 * @property string $picture
 * @property bool $is_active
 * @property int $created_by
 * @property int $modified_by
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 * @property \Cake\I18n\Time $deleted
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\Filter[] $filters
 * @property \App\Model\Entity\Notification[] $notifications
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_virtual = [
       'fullname'
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _getFullname(){
        if(isset($this->insertion)){
            return $this->firstname . ' ' . $this->insertion . '' . $this->lastname;
        } else {
            return $this->firstname . ' ' . $this->lastname;
        }
    }
}
