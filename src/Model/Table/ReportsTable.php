<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Reports Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Devices
 * @property \Cake\ORM\Association\BelongsTo $ReportStatuses
 * @property \Cake\ORM\Association\BelongsTo $Reporters
 * @property \Cake\ORM\Association\BelongsTo $ReportTypes
 *
 * @method \App\Model\Entity\Report get($primaryKey, $options = [])
 * @method \App\Model\Entity\Report newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Report[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Report|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Report patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Report[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Report findOrCreate($search, callable $callback = null, $options = [])
 */
class ReportsTable extends Table
{
    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('reports');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('Devices', [
            'foreignKey' => 'device_id'
        ]);
        $this->belongsTo('ReportStatuses', [
            'foreignKey' => 'report_status_id'
        ]);
        $this->belongsTo('Reporters', [
            'foreignKey' => 'reporter_id'
        ]);
        $this->belongsTo('ReportTypes', [
            'foreignKey' => 'report_type_id'
        ]);

        $this->addBehavior( 'Utils.WhoDidIt', [
            'fields' => [ 'id', 'username' ]
        ] );
        $this->addBehavior( 'Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'modified_at' => 'always',
                ]
            ]
        ] );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('body');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        $validator
            ->allowEmpty('from');

        $validator
            ->allowEmpty('to');

        $validator
            ->numeric('longitude')
            ->allowEmpty('longitude');

        $validator
            ->numeric('latitude')
            ->allowEmpty('latitude');

        $validator
            ->allowEmpty('vri_city');

        $validator
            ->allowEmpty('vri_street_1');

        $validator
            ->allowEmpty('vri_street_2');

        $validator
            ->boolean('keep_me_informed')
            ->allowEmpty('keep_me_informed');

        $validator
            ->allowEmpty('vehicle');

        $validator
            ->allowEmpty('date');

        $validator
            ->allowEmpty('time');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['report_status_id'], 'ReportStatuses'));
        $rules->add($rules->existsIn(['reporter_id'], 'Reporters'));
        $rules->add($rules->existsIn(['report_type_id'], 'ReportTypes'));

        return $rules;
    }

    /**
     * @param $event
     * @param $entity
     */
    public function beforeSave($event, $entity ){

        // link the report to a device, if one is near
        $devices = TableRegistry::get('Devices')->find('all', [
            'contain' => 'Locations'
        ]);

        $suggestedDevices = [];

        foreach ( $devices as $device ){

            $latitudeFrom = $device->location->latitude;
            $longitudeFrom = $device->location->longitude;
            $latitudeTo = $entity->latitude;
            $longitudeTo = $entity->longitude;
            $earthRadius = 6371;

            // convert from degrees to radians
            $latFrom = deg2rad($latitudeFrom);
            $lonFrom = deg2rad($longitudeFrom);
            $latTo = deg2rad($latitudeTo);
            $lonTo = deg2rad($longitudeTo);

            $latDelta = $latTo - $latFrom;
            $lonDelta = $lonTo - $lonFrom;

            $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                    cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
            $distance = round($angle * $earthRadius);

            $device->distance = $distance*1000; // in KM

            if( intval($device->distance) < 200 ){
                $suggestedDevices[] = $device;
            }
        }

        if ( count($suggestedDevices) == 1 ){
            $entity->device_id = $suggestedDevices[0]->id;
        }

    }

    public function afterSave( $event, $entity )
    {

        // Send email to Reporter when the reportstatus is changed
        if ( $entity->extractOriginalChanged(['report_status_id']) != NULL && $entity->keep_me_informed === true ) {

            $reportStatusEntity = $this->ReportStatuses->find()
                ->where([ 'ReportStatuses.id' => $entity->report_status_id ])
                ->first();

            $reportStatusName = $reportStatusEntity->name;
            $reporter = $this->Reporters->get($entity->reporter_id);
            $reporter->sendReportStatusUpdateMail($reportStatusName);

        }
    }

}
