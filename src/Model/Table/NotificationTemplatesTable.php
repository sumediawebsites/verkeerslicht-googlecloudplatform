<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * NotificationTemplates Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Users
 *
 * @method \App\Model\Entity\NotificationTemplate get($primaryKey, $options = [])
 * @method \App\Model\Entity\NotificationTemplate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NotificationTemplate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NotificationTemplate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NotificationTemplate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NotificationTemplate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NotificationTemplate findOrCreate($search, callable $callback = null, $options = [])
 */
class NotificationTemplatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('notification_templates');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsToMany('Users', [
            'foreignKey' => 'notification_template_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_notification_templates'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('template');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('body');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        return $validator;
    }

    public function afterSave($event, $entity, $options){
        if($entity->isNew()){
            $users = TableRegistry::get('Users')->find('all');
            $usersNotificationTemplatesTable = TableRegistry::get('UsersNotificationTemplates');

            foreach ($users as $user) {
                $userNotificationTemplate = $usersNotificationTemplatesTable->newEntity();
                $userNotificationTemplate->user_id = $user->id;
                $userNotificationTemplate->notification_template_id = $entity->get('id');
                $userNotificationTemplate->show_notification = true;

                $usersNotificationTemplatesTable->save($userNotificationTemplate);
            }
        }
    }
}
