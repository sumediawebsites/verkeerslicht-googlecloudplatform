<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;
use Search\Manager;

/**
 * BlogArticles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $BlogCategories
 *
 * @method \App\Model\Entity\BlogArticle get($primaryKey, $options = [])
 * @method \App\Model\Entity\BlogArticle newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BlogArticle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BlogArticle|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BlogArticle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BlogArticle[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BlogArticle findOrCreate($search, callable $callback = null, $options = [])
 */
class BlogArticlesTable extends Table
{
    use SoftDeleteTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('blog_articles');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('BlogCategories', [
            'foreignKey' => 'blog_category_id'
        ]);

        $this->addBehavior( 'Utils.WhoDidIt', [
             'fields' => [ 'id', 'username' ]
         ] );
         $this->addBehavior( 'Timestamp', [
             'events' => [
                 'Model.beforeSave' => [
                     'created_at' => 'new',
                     'modified_at' => 'always',
                 ]
             ]
         ] );

        $this->addBehavior( 'Utils.Uploadable', [
            'image_url'        => [
                'removeFileOnDelete' => true,
                'removeFileOnUpdate' => true,
                'fileName'           => '{field}.{extension}',
                'path'               => '{ROOT}{DS}{WEBROOT}{DS}uploads{DS}{model}{DS}{field}{DS}'
            ]
        ]);

        // SEARCH

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->value('blog_category_id',
                [ 'filterEmpty' => true ]
            )
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
            ->add('q', 'Search.Like', [
                'before' => true,
                'after' => true,
                'mode' => 'or',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['title', 'body']
            ])
            ->add('foo', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    // Modify $query as required
                }
            ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('slug');

        $validator
            ->allowEmpty('body');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        $validator
            ->allowEmpty('image_url');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['blog_category_id'], 'BlogCategories'));

        return $rules;
    }
}
