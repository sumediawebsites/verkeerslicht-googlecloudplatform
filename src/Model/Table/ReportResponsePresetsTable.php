<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * ReportResponsePresets Model
 *
 * @method \App\Model\Entity\ReportResponsePreset get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReportResponsePreset newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReportResponsePreset[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReportResponsePreset|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportResponsePreset patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReportResponsePreset[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReportResponsePreset findOrCreate($search, callable $callback = null, $options = [])
 */
class ReportResponsePresetsTable extends Table
{
    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('report_response_presets');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior( 'Utils.WhoDidIt', [
            'fields' => [ 'id', 'username' ]
        ] );
        $this->addBehavior( 'Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'modified_at' => 'always',
                ]
            ]
        ] );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('subject');

        $validator
            ->allowEmpty('message');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        return $validator;
    }
}
