<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * ReportTypes Model
 *
 * @property \Cake\ORM\Association\HasMany $Reports
 *
 * @method \App\Model\Entity\ReportType get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReportType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReportType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReportType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReportType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReportType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReportType findOrCreate($search, callable $callback = null, $options = [])
 */
class ReportTypesTable extends Table
{
    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('report_types');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Reports', [
            'foreignKey' => 'report_type_id'
        ]);

        $this->addBehavior( 'Utils.WhoDidIt', [
          'fields' => [ 'id', 'username' ]
         ] );
         $this->addBehavior( 'Timestamp', [
             'events' => [
                 'Model.beforeSave' => [
                     'created_at' => 'new',
                     'modified_at' => 'always',
                 ]
             ]
         ] );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        return $validator;
    }
}
