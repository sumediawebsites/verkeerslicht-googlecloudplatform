<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;
use Search\Manager;

/**
 * FaqQuestions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FaqCategories
 *
 * @method \App\Model\Entity\FaqQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\FaqQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FaqQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FaqQuestion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FaqQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FaqQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FaqQuestion findOrCreate($search, callable $callback = null, $options = [])
 */
class FaqQuestionsTable extends Table
{
    use SoftDeleteTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('faq_questions');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('FaqCategories', [
            'foreignKey' => 'faq_category_id'
        ]);

        $this->addBehavior( 'Utils.WhoDidIt', [
          'fields' => [ 'id', 'username' ]
         ] );
         $this->addBehavior( 'Timestamp', [
             'events' => [
                 'Model.beforeSave' => [
                     'created_at' => 'new',
                     'modified_at' => 'always',
                 ]
             ]
         ] );

        // SEARCH

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->value('blog_category_id',
                [ 'filterEmpty' => true ]
            )
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
            ->add('q', 'Search.Like', [
                'before' => true,
                'after' => true,
                'mode' => 'or',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['title', 'body']
            ])
            ->add('foo', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    // Modify $query as required
                }
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('body');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['faq_category_id'], 'FaqCategories'));

        return $rules;
    }
}
