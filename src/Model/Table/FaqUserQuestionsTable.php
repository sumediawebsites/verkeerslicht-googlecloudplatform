<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * FaqUserQuestions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FaqCategories
 *
 * @method \App\Model\Entity\FaqUserQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\FaqUserQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FaqUserQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FaqUserQuestion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FaqUserQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FaqUserQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FaqUserQuestion findOrCreate($search, callable $callback = null, $options = [])
 */
class FaqUserQuestionsTable extends Table
{
    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('faq_user_questions');
        $this->displayField('subject');
        $this->primaryKey('id');

        $this->belongsTo('FaqCategories', [
            'foreignKey' => 'faq_category_id'
        ]);

        $this->addBehavior( 'Utils.WhoDidIt', [
            'fields' => [ 'id', 'username' ]
        ] );
        $this->addBehavior( 'Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'modified_at' => 'always',
                ]
            ]
        ] );

        $this->addBehavior('Tree');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('subject');

        $validator
            ->notEmpty('message');

        $validator
            ->allowEmpty('response');

        $validator
            ->email('email')
            ->notEmpty('email');

        $validator
            ->notEmpty('faq_category_id');

        $validator
            ->boolean('answered')
            ->allowEmpty('answered');

        $validator
            ->boolean('is_response')
            ->allowEmpty('is_response');

        $validator
            ->integer('parent_id')
            ->allowEmpty('parent_id');
        $validator
            ->integer('lft')
            ->allowEmpty('lft');
        $validator
            ->integer('rght')
            ->allowEmpty('rght');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['faq_category_id'], 'FaqCategories'));

        return $rules;
    }
}
