<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Roles
 * @property \Cake\ORM\Association\HasMany $Filters
 * @property \Cake\ORM\Association\HasMany $Notifications
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 */
class UsersTable extends Table
{

    use SoftDeleteTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('username');
        $this->primaryKey('id');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Filters', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Notifications', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Devices', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsToMany( 'NotificationTemplates', [
            'alias'            => 'NotificationTemplates',
            'foreignKey'       => 'user_id',
            'targetForeignKey' => 'notification_template_id',
            'joinTable'        => 'users_notification_templates',
            'through'          => 'UsersNotificationTemplates'
        ] );

        $this->addBehavior( 'Utils.Uploadable', [
            'picture'        => [
                'removeFileOnDelete' => true,
                'removeFileOnUpdate' => true,
                'fileName'           => '{ORIGINAL}',
                'path'               => '{ROOT}{DS}{WEBROOT}{DS}uploads{DS}{model}{DS}{field}{DS}'
            ]
        ]);

    }
    
    public function getUsersWithRoles() {
        $users = $this->find( 'all', [
            'contain'    => [ 'Roles' ],
            'conditions' => [
                $this->_alias . '.is_active' => true
            ],
            'order'      => $this->_alias . '.lastname'
        ] );

        foreach ( $users as $user ) {
            $usersArray[ $user->id ] = $user->fullname . ' (' . $user->role->name . ')';
        }

        return $usersArray;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('initials');

        $validator
            ->allowEmpty('firstname');

        $validator
            ->allowEmpty('insertion');

        $validator
            ->allowEmpty('lastname');

        $validator
            ->allowEmpty('username');

        $validator
            ->allowEmpty('phone');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('password');

        $validator
            ->allowEmpty('settings');

        $validator
            ->allowEmpty('picture');

        $validator
            ->boolean('is_active')
            ->allowEmpty('is_active');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }

    public function afterSave( $event, $entity, $options ){

        if( $entity->isNew() ){
            $notificationsTemplatesTable = TableRegistry::get('NotificationTemplates');
            $notificationTemplates = $notificationsTemplatesTable->find()->toArray();

            $this->NotificationTemplates->link( $entity, $notificationTemplates );
        }
    }

}
