<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;
use Notifier\Utility\NotificationManager;
use Notifier\Utility\Pusher\PusherSdkClient;

/**
 * ChatMessages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentChatMessages
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $ChildChatMessages
 *
 * @method \App\Model\Entity\ChatMessage get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChatMessage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ChatMessage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChatMessage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChatMessage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChatMessage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChatMessage findOrCreate($search, callable $callback = null, $options = [])
 */
class ChatMessagesTable extends Table
{
    use SoftDeleteTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('chat_messages');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('ParentChatMessages', [
            'className' => 'ChatMessages',
            'foreignKey' => 'parent_id'
        ]);
        $this->belongsTo('Senders', [
            'className' => 'Users',
            'foreignKey' => 'sender_id',
            'propertyName' => 'sender',
        ]);
        $this->belongsTo('Receivers', [
            'className' => 'Users',
            'foreignKey' => 'receiver_id',
            'propertyName' => 'receiver',
        ]);
        $this->hasMany('ChildChatMessages', [
            'className' => 'ChatMessages',
            'foreignKey' => 'parent_id'
        ]);
        $this->belongsToMany( 'NotificationTemplates', [
            'alias'            => 'NotificationTemplates',
            'foreignKey'       => 'user_id',
            'targetForeignKey' => 'notification_template_id',
            'joinTable'        => 'users_notification_templates',
            'through'          => 'UsersNotificationTemplates'
        ] );

        $this->addBehavior( 'Utils.WhoDidIt', [
            'fields' => [ 'id', 'username' ]
        ]);
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'modified_at' => 'always',
                ]
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('message');

        $validator
            ->dateTime('read')
            ->allowEmpty('read');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentChatMessages'));
        $rules->add($rules->existsIn(['sender_id'], 'Senders'));
        $rules->add($rules->existsIn(['receiver_id'], 'Receivers'));

        return $rules;
    }
}
