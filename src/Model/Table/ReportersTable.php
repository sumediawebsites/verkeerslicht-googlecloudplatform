<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Reporters Model
 *
 * @property \Cake\ORM\Association\HasMany $Reports
 *
 * @method \App\Model\Entity\Reporter get($primaryKey, $options = [])
 * @method \App\Model\Entity\Reporter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Reporter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Reporter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Reporter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Reporter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Reporter findOrCreate($search, callable $callback = null, $options = [])
 */
class ReportersTable extends Table
{
    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('reporters');
        $this->displayField('username');
        $this->primaryKey('id');

        $this->hasMany('Reports', [
            'foreignKey' => 'reporter_id'
        ]);

        $this->addBehavior( 'Utils.WhoDidIt', [
          'fields' => [ 'id', 'username' ]
         ] );
         $this->addBehavior( 'Timestamp', [
             'events' => [
                 'Model.beforeSave' => [
                     'created_at' => 'new',
                     'modified_at' => 'always',
                 ]
             ]
         ] );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('initials');

        $validator
            ->allowEmpty('firstname');

        $validator
            ->allowEmpty('middlename');

        $validator
            ->allowEmpty('lastname');

        $validator
            ->allowEmpty('username');

        $validator
            ->allowEmpty('phone');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('password');

        $validator
            ->allowEmpty('settings');

        $validator
            ->allowEmpty('picture');

        $validator
            ->boolean('is_active')
            ->allowEmpty('is_active');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('modified_at')
            ->allowEmpty('modified_at');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        $validator
            ->allowEmpty('password_token');

        $validator
            ->boolean('remember_me')
            ->allowEmpty('remember_me');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
