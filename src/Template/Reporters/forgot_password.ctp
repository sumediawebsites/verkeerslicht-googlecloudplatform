<?= $this->assign('title', 'Wachtwoord vergeten'); ?>

<?= $this->Flash->render( 'auth') ?>
<?= $this->Flash->render() ?>

<div class="banner banner--hero banner--hero--default hidden-sm-down"></div>

<div class="container">
    <section class="page__section section--white section--first section--fields">
        <!-- Step 1 -->
        <?php if ( !isset($step) ): ?>
            <?= $this->Form->create( null, ['class' => 'form form--forgot-password'] ) ?>
                <div class="row">
                    <div class="col-lg-7 col-xl-6">
                        <h1 class="h1"><?= __('Wachtwoord vergeten') ?></h1>

                        <p>Vul je e-mailadres in en klik op de "Door naar Stap 2" knop. We sturen je dan een e-mail met een wachtwoord-code die je bij Stap 2 kan invullen.</p>

                        <?php if($user_exists == 'user_exists'){ ?>
                            <p><?= __('Er bestaat al een account met dat e-mailadres.') ?></p>
                        <?php } ?>
                    </div>

                    <div class="col-lg-4 push-lg-1">
                        <div class="form-group email">
                            <?= $this->Form->input( 'email' , [ 'label' => false, 'type' => 'email', 'value' => $email_exists, 'placeholder' => 'Je e-mailadres' ] ); ?>
                            <?= $this->Form->hidden('step', ['value'=>'email']); ?>
                        </div>

                        <?= $this->Form->button( __( 'Door naar Stap 2<i class="icon-arrow-right"></i>' ), ['class' => 'btn btn--secondary btn--block'] ); ?>
                    </div>
                </div>
            <?= $this->Form->end() ?>

        <!--  Step 2  -->
        <?php elseif ( $step == 'token' ): ?>
            <?= $this->Form->create($reporter, ['class' => 'form form--forgot-password'] ) ?>
                <div class="row">
                    <div class="col-lg-7 col-xl-6">
                        <h1 class="h1"><?= __('Wachtwoord vergeten') ?></h1>

                        <p>Hier in Stap 2 vul je de velden en de wachtwoord-code uit onze verstuurde e-mail in en klik je op de "Wijzig wachtwoord" knop.</p>
                    </div>

                    <div class="col-lg-4 push-lg-1">
                        <div class="form-group password">
                            <?= $this->Form->input( 'pwd' , [ 'label' => false, 'type' => 'password', 'placeholder' => 'Nieuw wachtwoord' ] ); ?>
                        </div>

                        <div class="form-group password">
                            <?= $this->Form->input( 'pwd_repeat', [ 'label' => false, 'type' => 'password', 'placeholder' => 'Herhaal nieuw wachtwoord'  ] ); ?>
                            <?= $this->Form->hidden('step', ['value'=>'token']); ?>
                            <?= $this->Form->hidden('email'); ?>
                        </div>

                        <div class="form-group token">
                            <?= $this->Form->input( 'password_token' , [ 'label' => false, 'type' => 'text', 'value' => '', 'placeholder' => 'Wachtwoord-code' ] ); ?>
                        </div>

                        <?= $this->Form->button( __( 'Wijzig wachtwoord<i class="icon-arrow-right"></i>' ), [ 'class' => 'btn btn--secondary btn--block' ] ); ?>
                    </div>
                </div>
            <?= $this->Form->end() ?>
        <?php endif; ?>
    </section>
</div>
