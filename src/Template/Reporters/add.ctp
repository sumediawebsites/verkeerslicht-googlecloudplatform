<?= $this->assign('title', 'Account aanmaken'); ?>

<?= $this->Flash->render( 'auth') ?>
<?= $this->Flash->render() ?>

<div class="banner banner--hero banner--hero--registration hidden-sm-down"></div>

<div class="container">
    <section class="page__section section--white section--first section--fields">
        <?= $this->Form->create($reporter, ['class' => 'form form--registration'] ) ?>
            <div class="row">
                <div class="col-lg-7 col-xl-6">
                    <h1 class="h1"><?= __('Overal snel een melding plaatsen') ?></h1>

                    <p>Maak een account aan voor een overzicht van al je meldingen en om de status hiervan te bekijken.</p>

                    <p class="hidden-md-down">
                        Heb je al een account?
                    </p>

                    <?= $this->Html->link( 'Log hier in<i class="icon-arrow-right"></i>', ['controller' => 'Reporters', 'action' => 'login' ], [ 'class' => 'btn btn--primary hidden-md-down', 'escape' => false ] );?>
                </div>

                <div class="col-lg-4 push-lg-1">
                    <div class="form-group">
                        <?= $this->Form->input( 'firstname', [ 'label' => false, 'type' => 'text', 'placeholder' => 'Je voornaam' ] ); ?>
                    </div>

                    <div class="form-group">
                        <?= $this->Form->input( 'lastname', [ 'label' => false, 'type' => 'text', 'placeholder' => 'Je achternaam' ] ); ?>
                    </div>

                    <div class="form-group email">
                        <?= $this->Form->input( 'email' , [ 'label' => false, 'type' => 'email', 'placeholder' => 'Je e-mailadres' ]); ?>
                    </div>

                    <div class="form-group password">
                        <?= $this->Form->input( 'pwd' , [ 'label' => false, 'type' => 'password', 'placeholder' => 'Gewenst wachtwoord' ] ); ?>
                    </div>

                    <div class="form-group password">
                        <?= $this->Form->input( 'pwd_repeat', [ 'label' => false, 'type' => 'password', 'placeholder' => 'Herhaal gewenst wachtwoord' ] ); ?>
                    </div>

<!--                    <label class="checkbox checkbox--text">-->
<!--                        --><?//= $this->Form->checkbox( 'remember_me' , [ 'label' => false ] ); ?>
<!--                        <span>Onthoud mijn gegevens</span>-->
<!--                    </label>-->

                    <?= $this->Form->button( __( 'Maak account aan<i class="icon-arrow-right"></i>' ), [ 'class' => 'btn btn--secondary btn--block' ] ); ?>
                </div>
            </div>
        <?= $this->Form->end() ?>

        <span class="link-create-account hidden-md-up">
            Heb je al een account?

            <?= $this->Html->link(
                __( 'Log hier in' ),
                ['controller' => 'Reporters', 'action' => 'login' ]
            );?>
        </span>
    </section>
</div>
