<?= $this->assign('title', 'Mijn account'); ?>

<?= $this->Flash->render() ?>

<div class="banner banner--hero banner--hero--registration hidden-sm-down"></div>

<div class="container">
    <section class="page__section section--white section--first section--fields">
        <div class="row">
            <div class="col-lg-7 col-xl-6">
                <h1 class="h1"><?= __('Mijn account') ?></h1>

                <p>Bekijk het overzicht van je meldingen + bijhorende status en je persoonlijke kennisbank.</p>

                <a href="/logout" class="btn btn--secondary has-icon-left"><i class="icon-arrow-left"></i>Uitloggen</a>
            </div>

            <div class="col-lg-4 push-lg-1">
                <ul class="account-details">
                    <li>Naam: <?= $reporter->fullname;?></li>
                    <li>E-mailadres: <?= $reporter->email;?></li>
                </ul>

                <a href="/account/change_password" class="btn btn--primary btn--block">Wachtwoord wijzigen<i class="icon-arrow-right"></i></a>
            </div>
        </div>
    </section>

    <section class="page__section section--white section--fields">
        <h2 class="h2">Mijn meldingen</h2>

        <ul class="reports-list">
            <?php
            $reports = $reporter->reports;
            foreach( $reports as $report ):?>

                <li class="report <?= $report->report_status->class;?>">
                    <div class="report__content">
                        <?= $report->vri_street_1;?><?= $report->vri_place;?>
                        <span><?= $report->report_type->name;?></span>
                    </div>
                    <?php if( $report->report_status->class == 'is-new') {
                        echo "<p>"."Deze melding is nog niet compleet."."</p>";
                        echo $this->Html->link(
                            'Melding afmaken',
                            ['controller' => 'Reports', 'action' => 'complete', $report->id ]
                        );
                    }?>
                    <span class="report__timestamp"><?= $report->date;?> | <?= $report->time;?></span>
                </li>

            <?php endforeach;?>
        </ul>
    </section>

    <section class="page__section section--white section--fields">
        <h2 class="h2">Mijn kennisbank</h2>
        <dl class="accordion">
            <?php $i = 0;?>
            <?php foreach( $faqUserQuestions as $faqUserQuestion ):?>
                <?php $i++?>
                <?php $i .= "_faq"?>
                <div class="ac">
                    <input id="ac-<?=$i?>" type="checkbox" name="acs" />

                    <label for="ac-<?=$i?>">
                        <span><?= h($faqUserQuestion->fullname);?></span>
                        <br>
                        <span><?= h($faqUserQuestion->subject) ?></span>
                        <span><?= h($faqUserQuestion->faq_category->name);?></span>
                        <?php
                        echo $this->Text->truncate(
                            $faqUserQuestion->message,
                            50,
                            [
                                'ellipsis' => '...',
                                'exact' => false,
                                'html' => true
                            ]
                        );
                        ?>
                        <span class="report__timestamp"><?= $faqUserQuestion->created_at->format('d-m-y');?> | <?= $faqUserQuestion->created_at->format('H:i');?></span>
                    </label>

                    <div class="ac-content">
                    <?php foreach( $faqUserResponses as $faqUserResponse ){
                        if ( $faqUserResponse->parent_id === $faqUserQuestion->id ) { ?>
                            <p><?= $faqUserResponse->response;?></p>
                            <span class="report__timestamp"><?= $faqUserResponse->created_at->format('d-m-y'); ?> | <?= $faqUserResponse->created_at->format('H:i'); ?></span>
                            <?php
                        } // endif
                    } // endforeach
                    ?>
                    </div>
                </div>
            <?php endforeach;?>
        </dl>
    </section>

    <section class="page__section section--white section--fields">
        <h2 class="h2">Mijn contact</h2>
        <ul class="reports-list">
            <dl class="accordion">
                <?php $i = 0;?>
                <?php foreach( $contactMessages as $contactMessage ):?>
                <?php $i++?>
                <?php $i .= "_contact"?>
                <div class="ac">
                    <input id="ac-<?=$i?>" type="checkbox" name="acs" />

                    <label for="ac-<?=$i?>">
                        <span><?= h($contactMessage->fullname);?></span>
                        <br>
                        <span><?= h($contactMessage->subject) ?></span>
                        <?php
                        echo $this->Text->truncate(
                            $contactMessage->message,
                            50,
                            [
                                'ellipsis' => '...',
                                'exact' => false,
                                'html' => true
                            ]
                        );
                        ?>
                        <span class="report__timestamp"><?= $contactMessage->created_at->format('d-m-y | H:i');?></span>
                    </label>

                    <div class="ac-content">
                        <?php foreach( $contactResponses as $contactResponse ){
                            if ( $contactResponse->parent_id === $contactMessage->id ) { ?>
                                    <p><?= $contactResponse->message;?></p>
                                <span class="report__timestamp"><?= $contactResponse->created_at->format('d-m-y | H:i');?></span>
                                <?php
                                } // endif
                            } // endforeach
                            ?>
                        <?php endforeach;?>
                    </div>
                </div>
            </dl>
        </ul>
    </section>

</div>
