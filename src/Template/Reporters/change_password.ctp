<?= $this->assign('title', 'Wachtwoord wijzigen'); ?>

<?= $this->Flash->render( 'auth') ?>
<?= $this->Flash->render() ?>

<div class="banner banner--hero banner--hero--registration hidden-sm-down"></div>

<div class="container">
    <section class="page__section section--white section--first section--fields">
        <?= $this->Form->create() ?>
            <div class="row">
                <div class="col-lg-7 col-xl-6">
                    <h1 class="h1"><?= __('Wachtwoord wijzigen') ?></h1>

                    <p>Vul de velden in en klik je op de "Wijzig wachtwoord" knop.</p>
                </div>

                <div class="col-lg-4 push-lg-1">
                    <div class="form-group password">
                        <?= $this->Form->input( 'pwd' , [ 'label' => false, 'type' => 'password', 'placeholder' => 'Nieuw wachtwoord' ] ); ?>
                    </div>

                    <div class="form-group password">
                        <?= $this->Form->input( 'pwd_repeat', [ 'label' => false, 'type' => 'password', 'placeholder' => 'Herhaal nieuw wachtwoord'  ] ); ?>
                    </div>

                    <?= $this->Form->button( __( 'Wijzig wachtwoord<i class="icon-arrow-right"></i>' ), [ 'class' => 'btn btn--secondary btn--block' ] ); ?>
        	    </div>
            </div>
        <?= $this->Form->end() ?>
    </section>
</div>
