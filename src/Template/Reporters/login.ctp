<?= $this->assign('title', 'Inloggen'); ?>

<?= $this->Flash->render( 'auth') ?>
<?= $this->Flash->render() ?>

<div class="banner banner--hero banner--hero--default hidden-sm-down"></div>

<div class="container">
    <section class="page__section section--white section--first section--fields">
        <?= $this->Form->create( null, ['class' => 'form form--login'] ); ?>
            <div class="row">
                <div class="col-lg-7 col-xl-6">
                    <h1 class="h1"><?= __('Inloggen') ?></h1>

                    <p>Log in op je account voor een overzicht van je meldingen + bijhorende status en je persoonlijke kennisbank.</p>

                    <p class="hidden-md-down">
                        Heb je nog geen account?
                    </p>

                    <?= $this->Html->link( 'Maak er een aan<i class="icon-arrow-right"></i>', [ 'controller' => 'Reporters', 'action' => 'add' ], [ 'class' => 'btn btn--primary hidden-md-down', 'escape' => false ] );?>
                </div>

                <div class="col-lg-4 push-lg-1">
                    <?= $this->Form->input( 'email', [ 'label' => false, 'type' => 'email', 'placeholder' => 'E-mailadres' ] ); ?>
                    <?= $this->Form->input( 'password' , [ 'label' => false, 'type' => 'password', 'placeholder' => 'Wachtwoord' ] ); ?>

                    <?= $this->Html->link( __( 'Vergeten?' ), ['controller' => 'Reporters', 'action' => 'forgotPassword' ], [ 'class' => 'link-forgot-password' ] ); ?>

<!--                    <label class="checkbox checkbox--text">-->
<!--                        --><?//= $this->Form->checkbox( 'remember_me' , [ 'label' => false ] ); ?>
<!--                        <span>Onthoud mijn gegevens</span>-->
<!--                    </label>-->

                    <?= $this->Form->button( __( 'Log in<i class="icon-arrow-right"></i>' ), ['class' => 'btn btn--secondary btn--block' ] ); ?>
                <?= $this->Form->end() ?>

                <span class="link-create-account hidden-md-up">
                    Heb je nog geen account?

                    <?= $this->Html->link( __( 'Maak er een aan' ), [ 'controller' => 'Reporters', 'action' => 'add' ] ); ?>
                </span>
            </div>
        </div>
    </section>
</div>

<style>
    .footer {
        padding-bottom: 0;
    }

    .btn--footer {
        display: none;
    }
</style>
