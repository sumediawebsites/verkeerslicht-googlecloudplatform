<?= $this->assign('title', $blogArticle->title); ?>

<a href="/" class="btn btn--back btn--back--blog-detail has-icon-left hidden-md-up"><i class="icon-arrow-left"></i>Terug</a>

<div class="banner banner--hero banner--hero--blog-detail" style="background-image: url('/assets/images/banners/tempbanner_hero_blog-detail.png');"></div>

<div class="container blog-detail">
    <nav class="breadcrumbs hidden-sm-down">
        <?php $this->Breadcrumbs->add([
                ['title' => 'Home', 'url' => [ 'controller' => 'Pages', 'action' => 'display', 'home', 'plugin' => 'Cms' ], 'class' => 'hidden-md-down'],
                ['title' => 'Blog', 'url' => [ 'controller' => 'BlogArticles', 'action' => 'index' ]],
                ['title' => $blogArticle->title, 'url' => [ 'controller' => 'Pages', 'action' => 'view',  ]]
            ]
        );?>
        <?= $this->Breadcrumbs->render(
            ['class' => '']
        );?>
    </nav>

    <section class="page__section section--white section--first section--content-page">
        <article class="content-page">
            <div class="row">
                <div class="col-lg-9">
                    <header>
                        <h1 class="h1"><?= h($blogArticle->title) ?></h1>

                        <small>
                            <time datetime="<?= h($blogArticle->created_at->format('Y-m-d')) ?>">
                                <span class="hidden-md-down"><?= h($blogArticle->blog_category->name) ?> - </span>
                                Gepubliceerd op: <?= h($blogArticle->created_at->format('d-m-Y')) ?>
                            </time>
                        </small>
                    </header>

                    <div class="content-page__body">
                        <!-- <?= $this->Text->autoParagraph($blogArticle->body); ?> -->
                        <?= $blogArticle->body?>
                    </div>

                    <footer>
                        <a href="/blog" class="btn btn--primary has-icon-left hidden-md-down"><i class="icon-arrow-left"></i>Terug naar overzicht</a>

                        <div class="addthis_inline_share_toolbox_rymw"></div>
                    </footer>
                </div>
            </div>
        </article>
    </section>
</div>
