<?= $this->assign('title', 'Blog'); ?>

<div class="banner banner--hero banner--hero--blog hidden-sm-down"></div>

<div class="container">
    <a href="/" class="btn btn--back has-icon-left hidden-md-up"><i class="icon-arrow-left"></i>Terug</a>

    <nav class="breadcrumbs hidden-sm-down">
        <?php $this->Breadcrumbs->add([
                ['title' => 'Home', 'url' => [ 'controller' => 'Pages', 'action' => 'display', 'home', 'plugin' => 'Cms' ], 'class' => 'hidden-md-down'],
                ['title' => 'Blog', 'url' => [ 'controller' => 'BlogArticles', 'action' => 'index' ]]
            ]
        );?>
        <?= $this->Breadcrumbs->render(
            ['class' => '']
        );?>
    </nav>

    <section class="page__section section--white section--first section--blog">
        <header>
            <h1 class="h1">Blijf op de hoogte</h1>
        </header>

        <?= $this->Form->create( null, ['class' => 'hidden-sm-down']); ?>
            <div class="search-wrapper">
                <?php
                    echo $this->Form->input('q', [ 'templates' => ['inputContainer' => '{{content}}'],'label'=> false, 'type' => 'search', 'placeholder' => 'Zoeken in blog en publicaties']);

                    echo $this->Form->button('Zoeken<i class="icon-search"></i>', [ 'type' => 'submit', 'class' => 'btn btn--secondary']);

                    if ($isSearch) {
                        echo $this->Html->link('Reset', ['action' => 'index'], ['type' => 'submit', 'class' => 'btn btn--reset btn--primary'] );
                    }
                ?>
            </div>
        <?php echo $this->Form->end(); ?>

        <?php foreach ($blogArticles as $blogArticle): ?>
            <?php if($blogArticle->highlighted == true): ?>
                <div class="post-featured">
                    <h2 class="h3 hidden-md-down">Uitgelicht</h2>

                    <a href="<?php echo $this->Url->build([ "controller" => "blog_articles", "action" => "view", $blogArticle->id ]); ?>" class="post-block is-featured hidden-md-down">
                        <!--<figure>
                            <div class="post-block__img" style="background-image: url('<?=$blogArticle->image_url;?>');"></div>
                        </figure>-->

                        <div class="post-block__img post-block__img--featured" style="background-image: url('<?=$blogArticle->image_url;?>');"></div>

                        <div class="card card--blue">
                            <h3 class="h3"><?=$blogArticle->title;?></h3>

                            <p>
                                <?=$this->Text->truncate(
                                    $blogArticle->slug,
                                    200,
                                    [
                                        'ellipsis' => '...',
                                        'exact' => false,
                                        'html' => true
                                    ]
                                );?>
                            </p>

                            <footer>
                                <ul class="post__metadata">
                                    <li><i class="icon-eye"></i><span><?=$blogArticle->views;?> gelezen</span></li>
                                    <li><i class="icon-clock"></i><span><?=$blogArticle->read_time;?></span> min gem. leestijd</li>
                                </ul>

                                <i class="icon-arrow-right"></i>
                            </footer>
                        </div>
                    </a>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>

		<div class="post-overview">

            <?php if ($isSearch): ?>
                <?php if ( count($blogArticles) >= 1 ){ ?>
                    <h2 class="h3 hidden-md-down">Overzicht</h2>
                <?php }else{ ?>
                    <p>We hebben geen vergelijkbare vragen kunnen vinden.</p>
                <?php }// endif ?>
            <?php else: ?>
                <h2 class="h3 hidden-md-down">Overzicht</h2>
            <?php endif; ?>


	        <div class="row post-row">
                <?php $repeat = 3; // times to repeat column inside of row
                $blocks = array_chunk($blogArticles->toArray(), $repeat);
                foreach ($blocks as $block) { ?>
                    <?php foreach ($block as $blogArticle => $b) { ?>
                        <div class="col-xl-4 post-block-wrapper">
                            <article class="post-block">
                                <a href="<?php echo $this->Url->build([ "controller" => "blog_articles", "action" => "view", $b['id'] ]); ?>">
                                    <!--<figure>
                                        <img class="post-block__img" src="<?php echo $b['image_url']; ?>" />
                                    </figure>-->

                                    <div class="post-block__img" style="background-image: url('<?php echo $b['image_url']; ?>');"></div>

                                    <h3 class="h3"><?php echo $b['title']; ?></h3>

                                    <p>
                                        <?=$this->Text->truncate(
                                            $b['slug'],
                                            140,
                                            [
                                                'ellipsis' => '...',
                                                'exact' => false,
                                                'html' => true
                                            ]
                                        );?>
                                    </p>

                                    <footer>
                                        <ul class="post__metadata">
                                            <li><i class="icon-eye"></i><span><?php echo $b['views']; ?> gelezen</span></li>
                                            <li><i class="icon-clock"></i><span><?php echo $b['read_time']; ?></span> min gem. leestijd</li>
                                        </ul>

                                        <i class="icon-arrow-right"></i>
                                    </footer>
                                </a>
                            </article>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
