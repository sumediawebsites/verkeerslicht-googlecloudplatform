<section class="page__section section--white section--home--stay-informed">
    <header class="has-btn">
        <h2 class="h2">{{Home_3_title}}</h2>

        <a href="/blog" class="btn btn--primary hidden-md-down">Meer nieuws<i class="icon-arrow-right"></i></a>
    </header>

    <div class="row">
        <?php foreach( $blogArticles as $blogArticle ):?>
            <div class="col-xl-4">
                <article class="post-block">
                    <a href="<?php echo $this->Url->build([ "controller" => "blog_articles", "action" => "view", $blogArticle->id ]); ?>">
                        <!--<figure>
                            <img class="post-block__img" src="<?=$blogArticle->image_url;?>" />
                        </figure>-->
                        <div class="post-block__img" style="background-image:url('<?=$blogArticle->image_url;?>');"></div>
                        <h3 class="h3"><?=$blogArticle->title;?></h3>

                        <p>
                            <?=$this->Text->truncate(
                                $blogArticle->slug,
                                140,
                                [
                                    'ellipsis' => '...',
                                    'exact' => false,
                                    'html' => true
                                ]
                            );?>
                        </p>

                        <footer>
                            <ul class="post__metadata">
                                <li><i class="icon-eye"></i><span><?=$blogArticle->views;?> gelezen</span></li>
                                <li><i class="icon-clock"></i><span><?=$blogArticle->read_time;?></span> min gem. leestijd</li>
                            </ul>

                            <i class="icon-arrow-right"></i>
                        </footer>
                    </a>
                </article>
            </div>
        <?php endforeach;?>
    </div>
</section>
