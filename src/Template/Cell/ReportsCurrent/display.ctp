<section class="page__section section--white section--current-reports hidden-sm-down">
    <div class="row">
        <div class="col-xl-7">
            <header>
                <h2 class="h2">Actuele meldingen</h2>
            </header>

            <p>
                Benieuwd naar actuele meldingen, en of deze al in behandeling
                zijn genomen? Bekijk onze actuele radar en blijf volledig op
                de hoogte van alle updates over de melding bij jou in de buurt.
            </p>

            <ul class="reports-legend">
                <li class="is-new">Nieuw</li>
                <li class="is-on-hold">In de wacht</li>
                <li class="is-pending">Openstaand</li>
                <li class="is-processed-positive">Behandeld en positief</li>
                <li class="is-processed-neutral">Behandeld maar neutraal</li>
            </ul>

            <a href="/meldingen" class="btn btn--primary">Ga naar alle meldingen<i class="icon-arrow-right"></i></a>
        </div>
    </div>
</section>
