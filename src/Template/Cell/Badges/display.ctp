<div class="badges">
    <a href="https://itunes.apple.com/nl/genre/ios/id36?mt=8" target="_blank">
        <img src="/assets/images/badges/badge_app-store_nl.svg" />
    </a>

    <a href="https://play.google.com/store/apps?hl=nl" target="_blank">
        <img src="/assets/images/badges/badge_play-store_nl.svg" />
    </a>
</div>
