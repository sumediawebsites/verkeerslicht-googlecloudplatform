<header>
    <h2 class="h3">Kennisbankvragen</h2>

    <?= $this->Html->link( 'Alle vragen', [ 'controller' => 'FaqUserQuestions', 'action' => 'index' ], [ 'class' => 'btn btn--link' ] ); ?>
</header>

<div class="card card--white">
    <ul class="widget-list">
        <?php foreach( $faqUserQuestions as $faqUserQuestion ):?>
            <li class="widget-list__item is-new">
                <a href="<?= $this->Url->build( ['controller' => 'FaqUserQuestions', 'action' => 'edit', $faqUserQuestion->id ] ); ?>">
                    <div class="widget-list__item__content">
                        <span class="widget-list__item__subject"><?= $faqUserQuestion->subject;?></span>

                        <span class="widget-list__item__category"><?= $faqUserQuestion->faq_category->name;?></span>
                    </div>

                    <span class="widget-list__item__timestamp"><?= $faqUserQuestion->created_at->format('d-m-y');?> | <?= $faqUserQuestion->created_at->format('H:i');?></span>

                    <span class="btn btn--primary">Antwoorden</span>
                </a>
            </li>
        <?php endforeach;?>
    </ul>
</div>
