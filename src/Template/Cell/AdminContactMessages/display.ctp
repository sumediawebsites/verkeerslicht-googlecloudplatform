<header>
    <h2 class="h3">Contactberichten</h2>

    <?= $this->Html->link( 'Alle berichten', [ 'controller' => 'Contact', 'action' => 'index' ], [ 'class' => 'btn btn--link' ] ); ?>
</header>

<div class="card card--white">
    <ul class="widget-list">
        <?php foreach( $contactMessages as $contactMessage ):?>
            <li class="widget-list__item is-new">
                <a href="<?= $this->Url->build( ['controller' => 'Contact', 'action' => 'respond', $contactMessage->id ] ); ?>">
                    <div class="widget-list__item__content">
                        <span class="widget-list__item__name"><?= $contactMessage->fullname;?></span>

                        <span class="widget-list__item__subject">Onderwerp: <?= $contactMessage->subject;?></span>

                        <?=$this->Text->truncate(
                            $contactMessage->message,
                            100,
                            [
                                'ellipsis' => '...',
                                'exact' => false,
                                'html' => true
                            ]
                        );?>
                    </div>

                    <span class="widget-list__item__timestamp"><?= $contactMessage->created_at->format('d-m-y | H:i');?></span>

                    <span class="btn btn--primary">Antwoorden</span>
                </a>
            </li>
        <?php endforeach;?>
    </ul>
</div>
