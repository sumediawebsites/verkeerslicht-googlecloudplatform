<header>
    <h2 class="h3">Laatste meldingen</h2>

    <?= $this->Html->link( 'Alle openstaande meldingen', [ 'controller' => 'Reports', 'action' => 'index' ], [ 'class' => 'btn btn--link' ] ); ?>
</header>

<div class="card card--white">
    <ul class="widget-list">
        <?php if( count($reports) > 0 ):?>
            <?php foreach( $reports as $report ):?>
                <li class="widget-list__item <?= $report->report_status->class;?>">
                    <a href="<?= $this->Url->build( ['controller' => 'Reports', 'action' => 'Edit', $report->id ] ); ?>">
                        <div class="widget-list__item__content">
                            <span class="widget-list__item__place"><?= $report->vri_street_1;?> <?= $report->vri_place;?></span>
                            <span class="widget-list__item__description"><?= $report->report_type->name;?></span>
                        </div>

                        <span class="widget-list__item__timestamp"><?= $report->date->format('d-m-y');?> | <?= $report->time->format('H:i');?></span>

                        <i class="icon-arrow-right"></i>
                    </a>
                </li>
            <?php endforeach;?>
        <?php else: ?>
            <li class="widget-list__item">
                <a href="<?= $this->Url->build( ['controller' => 'Reports', 'action' => 'index'] ); ?>">
                    <div class="widget-list__item__content">
                    Er zijn geen nieuwe meldingen.
                    </div>
                </a>
            </li>
        <?php endif;?>
    </ul>
</div>
