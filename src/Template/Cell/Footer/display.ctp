<footer class="site-footer">
    <section class="footer-top">
        <div class="container">
            <div class="footer-top__content">
                <h2 class="h2 hidden-md-down">Plaats nu jouw melding</h2>

                <p>
                    <span class="hidden-lg-up">Download gratis onze app</span>

                    <span class="hidden-md-down">en help mee de wegen te optimaliseren</span>
                </p>
            </div>

            <div class="hidden-lg-up">
                <?= $this->Cell( 'Badges' ); ?>
            </div>

            <button data-remodal-target="modal-report" onclick="modalReportShowReportModal();" class="btn btn--secondary btn--lg hidden-md-down">Direct melden<i class="icon-arrow-right"></i></button>
        </div>
    </section>

    <section class="footer-bottom hidden-md-down">
        <div class="container">
            <ul>
                <li><span>&copy; <?php echo date("Y"); ?> Verkeerslicht.nl</span></li>
                <li><a href="/disclaimer"><span>Disclaimer</span></a></li>
                <li><a href="/sitemap"><span>Sitemap</span></a></li>
                <li><a href="/contact"><span>Contact</span></a></li>
            </ul>

            <a href="http://www.sumedia.nl/" target="_blank" class="logo-developer">
                <img src="/assets/images/logos/logo_sumedia.svg" />
            </a>
        </div>
    </section>
</footer>
