<h2 class="h2">Laatste meldingen</h2>

<ul class="reports-list">
    <?php foreach( $reports as $report ):?>
        <li class="report <?= $report->report_status->class;?>">
            <div class="report__content">
                <span class="report__marker"></span>

                <?= $report->vri_place;?>
                <span class="report__description"><?= $report->report_type->name;?></span>
            </div>

            <span class="report__timestamp"><?= $report->date->format('d-m-y');?> | <?= $report->time->format('H:i');?></span>
        </li>
    <?php endforeach;?>
</ul>

<a href="/meldingen" class="btn btn--primary">Alle meldingen<i class="icon-arrow-right"></i></a>
