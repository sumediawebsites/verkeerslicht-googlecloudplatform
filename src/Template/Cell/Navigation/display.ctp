<?php if(sizeof($menuItems) > 0) { ?>
        <ul>
            <?php

            $controllerName = $this->request->param('controller');
            $actionName = $this->request->param('action');
            $passName = $this->request->param('pass');
            $pluginName = $this->request->param('plugin');

            foreach($menuItems as $menuItem){

                // check if this item belongs to the active page
                $is_active = false;
                if (  $menuItem->parsedUrl['pass'] == $passName && $menuItem->parsedUrl['plugin'] == 'Cms'
                ) {
                    $is_active = true;

                } elseif ( $menuItem->parsedUrl['plugin'] != 'Cms' &&  $controllerName === $menuItem->parsedUrl['controller'] ){
                    $is_active = true;

                } else {
                    $is_active = false;
                }

                if(sizeof($menuItem->children) > 0):
                    $hasSub = true;

                    // check if child is active
                    $is_active_sub = false;
                    foreach($menuItem->children as $child) {
                        $child_name = strtolower($child->name);
                        $child_name = str_replace(' ', '-', $child_name);
                        if($passName) {
                            $passName = strtolower($passName[0]);
                        }

                        if ( $child_name == $passName ){
                            $is_active_sub = true;
                        }
                    }
                    ?>

                    <li class="has-sub <?php if($is_active_sub){echo "is-active";}?>">
                    <span class="nav-mobile__toggle-sub">
                        <?=$menuItem->name?>
                    </span>

                    <ul class="nav__sub">
                        <?php
                        foreach($menuItem->children as $child) {

                            ?>
                            <li>
                                <?php echo $this->Html->link($child->name, $child->url); ?>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                <?php else: ?>
                    <li class="<?php if($is_active){echo "is-active";}?>" ><?php echo $this->Html->link($menuItem->name, $menuItem->url); ?>
                <?php endif;?>

                </li>
                <?php
            }
            ?>
            <li><button data-remodal-target="modal-report" onclick="modalReportShowReportModal();" class="btn btn--secondary">Direct melden<i class="icon-arrow-right"></i></button></li>
        </ul>
<?php }?>
