<div class="report">
    <div class="remodal" data-remodal-id="modal-report" data-remodal-options="closeOnEscape: false, closeOnOutsideClick: false">
        <div class="modal__header modal-report-step-3-exclude">
           <header>
               <h2 class="h2">Melding plaatsen</h2>
           </header>

           <button data-remodal-action="close"><i class="icon-close"></i></button>
       </div>

       <div class="modal__content">
           <form class="form validate-form modalReport">

                <input id="current_step" name="current_step_do_not_use" type="hidden" value="1">

                <div class="tab-content">
                    <div class="tab-pane is-active" id="modal-report-tab-a">
                        <section class="form__section modal-report-step-2-only">
                            <h3 class="h3">Je gegevens</h3>

                            <?php $loggedin_user = $this->request->session()->read('Auth.User');?>

                            <?php if( !isset($loggedin_user)):?>
                                <p>Je bent niet ingelogd. <a href="/login">Log in</a> om op de hoogte te blijven van je melding.</p>
                            <?php endif;?>

                            <div class="form-group row">
                                <div class="col-md-6 col-md-p-left-less">
                                    <input id="reporter_firstname" type="text" name="reporter_firstname" class="form-control custom" placeholder="Voornaam" value="<?=$loggedin_user['firstname'];?>" required />
                                </div>

                                <div class="col-md-6 col-md-p-right-less">
                                    <input id="reporter_lastname" type="text" name="reporter_lastname" class="form-control custom" placeholder="Achternaam" value="<?=$loggedin_user['lastname'];?>" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 col-md-p-left-less">
                                    <input id="reporter_email_step_2" type="email" name="reporter_email_step_2_do_not_use" class="form-control custom" placeholder="E-mailadres"  value="<?=$loggedin_user['email'];?>" required />
                                </div>
                            </div>
                        </section>

                        <section class="form__section">
                            <h3 class="h3">Locatie van het Verkeerslicht</h3>

                            <!-- START Use my location button for Mobile, hide on Desktop -->
                            <div class="form-group">
                                <button class="btn btn--default toggle--google-map--use-location hidden-md-up" onclick="modalReportGetCurrentPosition('mobile'); return false;">Gebruik mijn locatie<i class="icon-location"></i></button>
                            </div>
                            <!-- END Use my location button for Mobile, hide on Desktop -->

                            <!-- START Google Map for Desktop, hide on Mobile -->
                            <fieldset id="latlonPickerDesktopReportDirect" class="gllpLatlonPickerDesktopReportDirect gllpLatlonPicker--use-location hidden-md-down">
                                <div class="search-wrapper">
                                    <input id="searchInputDesktopReportDirect" type="text" class="gllpSearchField form-control pac-input" placeholder="Zoek op locatie ... voor nieuwe melding" />

                                    <button id="searchButtonDesktopReportDirect" type="button" class="gllpSearchButton btn btn--secondary">Zoeken<i class="icon-search"></i></button>

                                    <button class="btn btn--default btn--locate hidden-md-down" alt="Gebruik mijn locatie" onclick="modalReportGetCurrentPosition('desktop'); return false;"><i class="icon-location"></i></button>
                                </div>

                                <div id="desktopReportDirectMap" class="google-map"></div>

                                <input type="hidden" class="gllpDevice"/>
                                <input type="hidden" class="gllpLatitude"/>
                                <input type="hidden" class="gllpLongitude"/>
                                <input type="hidden" class="gllpZoom"/>
                                <input type="hidden" class="gllpLocationName"/>
                                <input type="hidden" class="gllpElevation"/>
                                <input type="button" class="gllpUpdateButton" value="update map" style="display:none;">
                            </fieldset>
                            <!-- END Google Map for Desktop, hide on Mobile -->

                            <div class="form-group form-subsection modal-report-step-2-only hidden-sm-down">
                                <div class="row">
                                    <div class="col-md-6 col-md-p-left-less">
                                        <div class="coordinate-wrapper">
                                            <label for="latitude">Breedtegraad:</label>

                                            <input id="latitude" type="text" class="form-control custom" name="latitude" value="" readonly />
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-md-p-right-less">
                                        <div class="coordinate-wrapper">
                                            <label for="longitude">Lengtegraad:</label>

                                            <input id="longitude" type="text" class="form-control custom" name="longitude" value="" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group hidden-md-up">

                                <!-- START Pin on Map button for Mobile, hide on Desktop -->
                                <button class="btn btn--default toggle--google-map--pin-on-map" onclick="modalReportPinOnMap('mobile'); return false;">Of pin op kaart<i class="icon-marker"></i></button>
                                <!-- END Pin on Map button for Mobile, hide on Desktop -->

                                <!-- START Find location on Map button for Desktop, hide on Mobile -->
                                <button class="btn btn--default btn--block toggle--google-map--use-location hidden-md-down" onclick="modalReportGetCurrentPosition('desktop'); return false;">Zoek locatie op kaart<i class="icon-location"></i></button>
                                <!-- END Find location on Map button for Desktop, hide on Mobile -->

                                <!-- START Google Map for Mobile, hide on Desktop -->
                               <fieldset id="latlonPickerMobileReportDirect" class="gllpLatlonPickerMobileReportDirect gllpLatlonPicker--pin-on-map hidden-md-up">
                                    <div class="google-map">Google Maps</div>

                                    <input type="hidden" class="gllpDevice"/>
                                    <input type="hidden" class="gllpLatitude"/>
                                    <input type="hidden" class="gllpLongitude"/>
                                    <input type="hidden" class="gllpZoom"/>
                                    <input type="hidden" class="gllpLocationName"/>
                                    <input type="hidden" class="gllpElevation"/>
                                    <input type="button" class="gllpUpdateButton" value="update map" style="display: none;">
                                </fieldset>
                                <!-- END Google Map for Mobile -->
                            </div>

                            <div class="form-group form-subsection">
                                <input id="place" type="text" name="place" class="form-control custom" placeholder="Plaats" required readonly />
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 col-md-p-left-less">
                                    <input id="streetname1" type="text" name="streetname1" class="form-control custom" placeholder="Ik ging van (straat)" /><!-- WAS required -->
                                </div>

                                <div class="col-md-6 col-md-p-right-less">
                                    <input id="streetname2" type="text" name="streetname2" class="form-control custom" placeholder="Ik ging naar (straat)" /><!-- WAS required -->
                                </div>
                            </div>
                        </section>

                        <section class="form__section modal-report-step-2-only">
                            <div class="row">
                                <div class="col-md-6 col-md-p-left-less col-v-align-items">
                                    <h3 class="h3">Ik was daar</h3>
                                </div>

                                <div class="col-md-6 col-md-p-right-less">
                                    <div class="form-group">
                                        <div class="select-wrapper">
                                            <select id="reporter_vehicle" name="vehicle" class="form-control">
                                                <option disabled selected value="">Kies vervoersmiddel</option>
                                                <option value="car">Met de auto</option>
                                                <option value="motorcycle">Met de motor</option>
                                                <option value="scooter">Met de scooter/brommer</option>
                                                <option value="moped">Met de bromfiets</option>
                                                <option value="bicycle">Met de fiets</option>
                                                <option value="feet">Lopend</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="form__section">
                            <h3 class="h3">Selecteer het probleem</h3>

                            <div class="row">
                                <div class="col-md-6 col-md-p-left-less">
                                    <div class="form-group radio radio--problem radio--long-red">
                                        <input type="radio" id="radio-long-red" name="radio_problem" value="2" />

                                        <label for="radio-long-red"><i class="icon-slow"></i>Te lang rood</label>
                                    </div>

                                    <div class="radio radio--problem radio--short-green">
                                        <input type="radio" id="radio-short-green" name="radio_problem" value="3" />

                                        <label for="radio-short-green"><i class="icon-fast"></i>Te kort groen</label>
                                    </div>
                                </div>

                                <div class="col-md-6 col-md-p-right-less">
                                    <div class="form-group radio radio--problem radio--unsafe">
                                        <input type="radio" id="radio-unsafe" name="radio_problem" value="4" />

                                        <label for="radio-unsafe"><i class="icon-alert"></i>Onveilige situatie</label>
                                    </div>

                                    <div class="radio radio--problem radio--other">
                                        <input type="radio" id="radio-other" name="radio_problem" value="5" />

                                        <label for="radio-other"><i class="icon-cone"></i>Overig</label>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="form__section modal-report-step-2-only">
                            <div class="row">
                                <div class="col-md-6 col-md-p-left-less">
                                    <h3 class="h3">Datum</h3>

                                    <div class="form-group">
                                        <input type="date" name="date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" />
                                    </div>
                                </div>

                                <div class="col-md-6 col-md-p-right-less form-subsection subsection--time">
                                    <h3 class="h3">Tijdstip</h3>

                                    <div class="form-group">
                                        <input type="time" name="time" class="form-control timepicker" value="<?=$now->format('H:i');?>">
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="form__section">
                            <h3 class="h3 modal-report-step-1-only">Je gegevens</h3>

                            <div class="form-group row modal-report-step-1-only">
                                <div class="col-md-6 col-md-p-left-less">
                                    <input id="reporter_email_step_1" type="email" name="reporter_email_step_1_do_not_use" class="form-control custom" placeholder="E-mailadres" value="<?=$loggedin_user['email'];?>" />
                                    <!-- store reporter_email in below element -->
                                    <input id="reporter_email_step_1_and_2" type="hidden" name="reporter_email" class="form-control" placeholder="E-mailadres hidden" value="<?=$loggedin_user['email'];?>" />
                                </div>
                            </div>

                            <h3 class="h3 modal-report-step-2-only">Opmerkingen</h3>

                            <div class="form-group modal-report-step-2-only">
                                <textarea name="body" class="form-control" placeholder="Wil je verder nog iets kwijt?"></textarea>
                            </div>

                            <label class="checkbox checkbox--text">
                                <input type="checkbox" name="keep_me_informed" value="1" class="form-control" />

                                <span>Ja, ik wil op de hoogte blijven van de verbeteringen van mijn melding.</span>
                            </label>

                            <div class="modal-report-step-1-only hidden-md-up">
                                <button type="submit" class="btn btn--secondary" >Opslaan en later afmaken<i class="icon-arrow-right"></i></button>

                                <button class="btn btn--primary" onclick="modalReportGoToStep(2); return false;">Ga door naar stap 2<i class="icon-arrow-right"></i></button>
                            </div>

                            <div class="hidden-md-down">
                                <button class="btn btn--secondary modal-report-step-1-only" onclick="modalReportGoToStep(2); return false;">Ga door naar stap 2<i class="icon-arrow-right"></i></button>
                            </div>

                            <div>
                                <button type="submit" class="btn btn--secondary modal-report-step-2-only">Melding plaatsen<i class="icon-arrow-right"></i></button>

                                <p><span id="status"></span></p>

                            </div>
                        </section>
                    </div>

                    <div class="tab-pane" id="modal-report-tab-b">
                        <div class="great-succes">
                            <div class="banner banner--hero banner--hero--default"></div>

                            <div class="form__section">
                                <h2 class="h2 modal-report-step-3-only-complete" style="display:none;">Bedankt voor uw melding!</h2>
                                <h2 class="h2 modal-report-step-3-only-incomplete" style="display:block;">Bedankt voor uw [incomplete] melding!</h2>

                                <p class="modal-report-step-3-only-complete" style="display:none;">Bedankt voor het plaatsen van uw melding! Met deze melding heeft u zojuist de doorstroom van het verkeer verbeterd voor u en uw weggebruikers. Samen met Verkeerslicht.nl optimaliseren we de wegen.</p>
                                <p class="modal-report-step-3-only-incomplete" style="display:block;">Bedankt voor het plaatsen van uw [incomplete] melding! Met deze melding heeft u zojuist de doorstroom van het verkeer verbeterd voor u en uw weggebruikers. Samen met Verkeerslicht.nl optimaliseren we de wegen.</p>

                                <footer>
                                    <a href="/" class="btn btn--primary has-icon-left"><i class="icon-arrow-left"></i>Terug naar home</a>

                                    <a href="/meldingen" class="btn btn--secondary">Bekijk andere meldingen<i class="icon-arrow-right"></i></a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
           </form>
       </div>
    </div>
</div>

<script type="text/javascript">

    function initMaps() {

        // For Desktop - Complete Report
        $(".gllpLatlonPickerDesktopCompleteReport").each(function () {
            $obj = $(document).gMapsLatLonPicker();
            $obj.init( $(this) );
            // Add devices to map
            $obj.addMarkers('devices');
        });

        // For Desktop - Report Direct
        $(".gllpLatlonPickerDesktopReportDirect").each(function () {
            $obj = $(document).gMapsLatLonPicker();
            $obj.init( $(this) );
            // Add devices to map
            $obj.addMarkers('devices');
        });

        // For Mobile - Report Direct
        $(".gllpLatlonPickerMobileReportDirect").each(function () {
            $obj = $(document).gMapsLatLonPicker();
            $obj.init( $(this) );
            // Add devices to map
            $obj.addMarkers('devices');
        });
    }

    function initPlaces() {
        // initiate Google Places here

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initAutocomplete() {

            var elements = document.getElementsByClassName('pac-input');
            for(var i = 0; i < elements.length; i++) {
                // Create the autocomplete object, restricting the search to geographical location types.
                switch(elements[i].id) {
                    case 'searchInputDesktopCompleteReport':
                        autocompleteDesktopCompleteReport = new google.maps.places.Autocomplete(
                            /** @type {!HTMLInputElement} */(document.getElementById('searchInputDesktopCompleteReport')),
                            {types: ['geocode']});
                        break;
                    case 'searchInputDesktopReportDirect':
                        autocompleteDesktopReportDirect = new google.maps.places.Autocomplete(
                            /** @type {!HTMLInputElement} */(document.getElementById('searchInputDesktopReportDirect')),
                            {types: ['geocode']});
                        break;
                    case 'searchInputMobileReportDirect':
                        autocompleteMobileReportDirect = new google.maps.places.Autocomplete(
                            /** @type {!HTMLInputElement} */(document.getElementById('searchInputMobileReportDirect')),
                            {types: ['geocode']});
                        break;
                    default:
                        break;
                }
            }
        }
        // Call the initAutocomplete function
        initAutocomplete();
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    /**
     *  DEPENDING ON THE MODAL BEING OPEN OR CLOSED
     *
     *  When open, use the maps of the 'report direct modal'
     *  if closed, use the maps of the 'complete report page'
     */
    window.MODAL_REPORT_DIRECT_IS_OPEN = false; // Global variable

    $(document).on('opened', '.remodal', function () {
        // initiate the maps and places here,
        // and in addition on document ready
        // to force initiation,
        // otherwise we end up with a grayed out map
        // and no autocomplete places search
        initMaps();
        initPlaces();
        window.MODAL_REPORT_DIRECT_IS_OPEN = true;
    });

    $(document).on('closing', '.remodal', function (e) {
        // Reason: 'confirmation', 'cancellation'
        window.MODAL_REPORT_DIRECT_IS_OPEN = false;
    });

    function modalReportShowReportModal(){
        modalReportToggleTabs("modal-report-tab-a");
        modalReportGoToStep(1);
    }
    function modalReportHideReportModal(){
        modalReportGoToStep(1);
        modalReportToggleTabs("modal-report-tab-a");
    }

    function modalReportGoToStep(stepToShow) {
        switch(stepToShow) {
            case 1:
                document.getElementById("current_step").value = 1;
                // make elements not required for incomplete submission of report
                document.getElementById("reporter_firstname").required = false;
                document.getElementById("reporter_lastname").required = false;
                document.getElementById("reporter_email_step_2").required = false;
                document.getElementById("reporter_vehicle").required = false;

                elementsToShowByClass = "modal-report-step-1-only";
                elementsToHideByClass = "modal-report-step-2-only";
                break;
            case 2:
                document.getElementById("current_step").value = 2;
                // hide 'incomplete' fields on tab-b
                elements = document.getElementsByClassName("modal-report-step-3-only-incomplete");
                for (var i = 0; i < elements.length; i++) {
                    elements[i].style.display = "none";
                }
                // show 'complete' fields on tab-b
                elements = document.getElementsByClassName("modal-report-step-3-only-complete");
                for (var i = 0; i < elements.length; i++) {
                    elements[i].style.display = "block";
                }
                // make elements required for complete submission of report
                document.getElementById("reporter_firstname").required = true;
                document.getElementById("reporter_lastname").required = true;

                document.getElementById('streetname1').required = true;
                document.getElementById('streetname2').required = true;

                document.getElementById("reporter_email_step_2").required = true;
                document.getElementById("reporter_vehicle").required = true;

                elementsToHideByClass = "modal-report-step-1-only";
                elementsToShowByClass = "modal-report-step-2-only";

                document.getElementById('reporter_email_step_2').value = document.getElementById('reporter_email_step_1').value;
                document.getElementById('reporter_email_step_1_and_2').value = document.getElementById('reporter_email_step_1').value;
                break;
            case 3:
                document.getElementById("current_step").value = 3;
                elementsToHideByClass = "modal-report-step-2-only";
                elementsToShowByClass = "modal-report-step-3-only";
                document.getElementById('reporter_email_step_1_and_2').value = document.getElementById('reporter_email_step_2').value;
                elements = document.getElementsByClassName("modal-report-step-3-exclude");
                for (var i = 0; i < elements.length; i++) {
                    elements[i].style.display = "none";
                }
                break;
            default:
        }
        // hide all elements with elementsToHideByClass class
        elements = document.getElementsByClassName(elementsToHideByClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].style.display = "none";
        }
        // show all elements with elementsToShowByClass class
        elements = document.getElementsByClassName(elementsToShowByClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].style.display = "block";
        }
    }

    function modalReportPinOnMap(device) {
        // Pin on Map needs the Map re-initiated
        initMaps();
        // Complete the location info to move the marker into view
        setTimeout(function() {
          modalReportGetCurrentPosition(device);
        }, 1000); // 1 second, adjust to fit
    }

    function modalReportGetCurrentPosition(device) {
        // THIS WORKS WITH BOTH http AND https
        $.getJSON( "https://freegeoip.net/json/", function( data ) {
            // Aspire to a more acurate location, by consulting Google Geocoder
            // BELOW DOESN'T WORK FOR http REQUESTS, https ONLY
            var _geocoder = new google.maps.Geocoder();
            var _latitude = "";
            var _longitude = "";
            var _place = "";
            var _zoom;
            var _device;

            function codeLatLng(lat, lng) {
                var latlng = new google.maps.LatLng(lat, lng);
                _geocoder.geocode({'latLng': latlng}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                     if (results[0]) {
                       //formatted address
                       _place = results[0].formatted_address;
                        if(_latitude == "") { _latitude = data.latitude }
                        if(_longitude == "") { _longitude = data.longitude }
                        if(_place == "") { _place = data.city }
                        modalReportSetCurrentLatitude(_latitude);
                        modalReportSetCurrentLongitude(_longitude);
                        modalReportSetCurrentPlace(_place);
                        _zoom = 12; // Set a default value
                        _device = device; // desktop or mobile
                        // Allow time for the input fields to be updated
                        setTimeout(function() {
                          modalReportUpdateMaps(_latitude, _longitude, _zoom, _device);
                        }, 1000); // 1 second, adjust to fit
                     } else {
                       //console.log("codeLatLng - Geocoder failed due to: " + status);
                     }
                  }
                });
            }

            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
                //Get latitude and longitude;
                function successFunction(position) {
                    _latitude = position.coords.latitude;
                    _longitude = position.coords.longitude;
                    codeLatLng(_latitude, _longitude);
                }
                function errorFunction(error) {
                    //console.log("navigator.geolocation.getCurrentPosition, an error has occured : ", error);
                }
            }
        });
    }

    function modalReportUpdateMaps(latitude, longitude, zoom, device) {
        latitudeClass = "gllpLatitude";
        longitudeClass = "gllpLongitude";
        zoomClass = "gllpZoom";
        updateButtonClass = "gllpUpdateButton";
        deviceClass = "gllpDevice";

        elements = document.getElementsByClassName(latitudeClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].value = latitude;
        }

        elements = document.getElementsByClassName(longitudeClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].value = longitude;
        }

        elements = document.getElementsByClassName(zoomClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].value = zoom;
        }

        elements = document.getElementsByClassName(deviceClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].value = device;
        }

        // Update the map, by clicking the Update button
        elements = document.getElementsByClassName(updateButtonClass);
        for (var i = 0; i < elements.length; i++) {
            elements[i].click();
        }

    }

    function modalReportSetCurrentLatitude(latitude) {
        if (typeof(document.getElementById('latitude')) != 'undefined' && document.getElementById('latitude') != null)
        {
            document.getElementById('latitude').value = latitude;
        }

        latitudeClass = "gllpLatitude";
        elements = document.getElementsByClassName(latitudeClass);
        for(var i = 0; i < elements.length; i++) {
          elements[i].value = latitude;
        }

        if (typeof(document.getElementById('complete_report_latitude')) != 'undefined' && document.getElementById('complete_report_latitude') != null)
        {
            document.getElementById('complete_report_latitude').value = latitude;
        }
    }

    function modalReportSetCurrentLongitude(longitude) {
        if (typeof(document.getElementById('longitude')) != 'undefined' && document.getElementById('longitude') != null)
        {
            document.getElementById('longitude').value = longitude;
        }

        longitudeClass = "gllpLongitude";
        elements = document.getElementsByClassName(longitudeClass);
        for(var i = 0; i < elements.length; i++) {
          elements[i].value = longitude;
        }

        if (typeof(document.getElementById('complete_report_longitude')) != 'undefined' && document.getElementById('complete_report_longitude') != null)
        {
            document.getElementById('complete_report_longitude').value = longitude;
        }
    }

    function modalReportSetCurrentPlace(place) {
        if (typeof(document.getElementById('place')) != 'undefined' && document.getElementById('place') != null)
        {
            document.getElementById('place').value = place;
        }

        locationNameClass = "gllpLocationName";
        elements = document.getElementsByClassName(locationNameClass);
        for(var i = 0; i < elements.length; i++) {
          elements[i].value = place;
        }

        if (typeof(document.getElementById('complete_report_place')) != 'undefined' && document.getElementById('complete_report_place') != null)
        {
            document.getElementById('complete_report_place').value = place;
        }
    }

    function modalReportSubmitForm() {
        modalReportGoToStep(3);
        modalReportToggleTabs("modal-report-tab-b");
    }

    function modalReportToggleTabs(IdOfTabToShow) {
        var IdOfTabToHide = "";
        switch(IdOfTabToShow) {
            case "modal-report-tab-a":
                IdOfTabToHide = "modal-report-tab-b";
                break;
            case "modal-report-tab-b":
                IdOfTabToHide = "modal-report-tab-a";
                break;
            default:
        }
        // remove is-active from IdOfTabToHide
        var modalReportTabToHide = document.getElementById(IdOfTabToHide);
        modalReportTabToHide.className = "tab-pane";
        // set IdOfTabToShow to is-active
        var modalReportTabToShow = document.getElementById(IdOfTabToShow);
        modalReportTabToShow.className = "tab-pane is-active";
    }
</script>

<script>
    /**
     *  Add support for form field validation for Safari browsers (iPhone, iPad, desktop)
     *  for the 'required' attribute on input fields, which is currently not supported by Safari
     *
     *  Source: http://blueashes.com/2013/web-development/html5-form-validation-fallback/
     */
    function hasHtml5Validation () {
        return typeof document.createElement('input').checkValidity === 'function';
    }

    if (hasHtml5Validation()) {
        $( "form.modalReport" ).submit(function(event) {
            // Conclude from the below element which step we are in currently
            var element = document.getElementById("current_step");

            var currentStep = element.value;
            var allRequiredFieldsAreValid = false;
            function validateRadio(radios)
            {
                for (i = 0; i < radios.length; ++ i)
                {
                    if (radios [i].checked) return true;
                }
                return false;
            }
            function validateSelect(element)
            {
                if (element.options[element.selectedIndex].value != "") {
                    return true
                }
                return false;
            }
            switch(currentStep) {
                case "1":
                  // validate required fields of step 1 only
                  if( document.getElementById('place').value != ""
                      // NOT REQUIRED AT THIS STEP && document.getElementById('streetname1').value != ""
                      // NOT REQUIRED AT THIS STEP && document.getElementById('streetname2').value != ""
                      // validate if one of the radio options are checked
                      && validateRadio(document.getElementsByName('radio_problem'))  // Use name!, not id
                      && document.getElementById('reporter_email_step_1').value != ""
                      ) {
                      allRequiredFieldsAreValid = true;
                  } else {
                      //console.log("Form submit - not all required fields of step 1 are valid.");
                  }
                  break;
                case "2":
                  // validate required fields of step 1 and 2
                  if( document.getElementById('place').value != ""
                      && document.getElementById('streetname1').value != ""
                      && document.getElementById('streetname2').value != ""
                      // validate if one of the radio options are checked
                      && validateRadio(document.getElementsByName('radio_problem'))  // Use name!, not id
                      && document.getElementById('reporter_email_step_2').value != ""
                      && validateSelect(document.getElementsByName('vehicle')[0])  // Use name!, not id
                      ) {
                      allRequiredFieldsAreValid = true;
                  } else {
                      //console.log("Form submit - not all required fields of step 2 are valid.");
                  }
                  break;
                default:
                  //console.log("Form submit - default: currentStep = ", currentStep);
                  break;
            }
            if(!allRequiredFieldsAreValid) {
                event.preventDefault();
                $(this).addClass('invalid');
                $('#status').html('Sommige verplichte velden zijn nog niet ingevuld.');
            } else {
                $(this).addClass('valid');
                $('#status').html('Verstuurd.');

                var reporterEmailStep1 = document.getElementById('reporter_email_step_1');
                var reporterEmailStep2; // do not assign to a value yet
                var reporterEmailStep1And2 = document.getElementById('reporter_email_step_1_and_2');
                if(reporterEmailStep1And2.value == "") {
                    if(reporterEmailStep1.value !== "") {
                        reporterEmailStep1And2.value = reporterEmailStep1.value;
                    }
                    if(reporterEmailStep1.value == ""){
                        reporterEmailStep1.value = reporterEmailStep1And2.value;
                    }
                }
                if (typeof(document.getElementById('reporter_email_step_2')) != 'undefined' && document.getElementById('reporter_email_step_2') != null) {
                    // only in step 2 will reporter_email_step_2 be required
                    if(document.getElementById('reporter_email_step_2').required) {
                        reporterEmailStep2 = document.getElementById('reporter_email_step_2');
                        if(reporterEmailStep1And2.value == "") {
                            if(reporterEmailStep2.value !== "") {
                                reporterEmailStep1And2.value = reporterEmailStep2.value;
                            }
                        }
                    }
                }

                $.ajax({
                    type: "post",
                    url: "https://verkeerslicht-165607.appspot.com/api/reports/add",
                    data: $(this).serialize(),
                    success: function(response) {
                        var result = response['response'];
                        if(result.toString() == "success") {
                            //alert("Form submitted successfully! with response: " + result);
                        } else {
                            //alert("Form submission failed! with response: " + result);
                        }
                    },
                    error: function(response) {
                        var result = response['response'];
                    }
                });
                event.preventDefault();
                modalReportSubmitForm();
                return false;

            }

        }); // eof $( "form.modalReport" ).submit

    } // eof if(hasHtml5Validation())
</script>

<script type="text/javascript">

/**
 * A JQUERY GOOGLE MAPS LATITUDE AND LONGITUDE LOCATION PICKER
 * version 1.2
 *
 * Supports multiple maps. Works on touchscreen. Easy to customize markup and CSS.
 *
 * Courtesy by Richard Dancsi
 */

(function($) {
// for ie9 doesn't support debug console >>>
if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };
// ^^^

$.fn.gMapsLatLonPicker = (function() {

	var _self = this;

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PARAMETERS (MODIFY THIS PART) //////////////////////////////////////////////////////////////
	_self.params = {
		defLat : 52.258107, // Centre of Holland
		defLng : 5.600592, // Centre of Holland
		defZoom : 7, // See all of Holland
		queryLocationNameWhenLatLngChanges: true,
		queryElevationWhenLatLngChanges: true,
		mapOptions : {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			disableDoubleClickZoom: true,
			zoomControlOptions: true,
			streetViewControl: false,
            scrollwheel: false,
            styles: [
                {
                    "featureType": "administrative.country",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#FFBB00"
                        },
                        {
                            "saturation": 43.400000000000006
                        },
                        {
                            "lightness": 37.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": "-40"
                        },
                        {
                            "lightness": "36"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": "-77"
                        },
                        {
                            "lightness": "28"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#00FF6A"
                        },
                        {
                            "saturation": -1.0989010989011234
                        },
                        {
                            "lightness": 11.200000000000017
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": "-24"
                        },
                        {
                            "lightness": "61"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#ffc200"
                        },
                        {
                            "saturation": -61.8
                        },
                        {
                            "lightness": 45.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#FF0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 51.19999999999999
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#ff0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 52
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#0078FF"
                        },
                        {
                            "saturation": -13.200000000000003
                        },
                        {
                            "lightness": 2.4000000000000057
                        },
                        {
                            "gamma": 1
                        }
                    ]
                }
            ]
		},
		strings : {
			markerText : "Verplaats deze Markeerder",
			error_empty_field : "Kon geen coordinaten vinden voor deze plaats",
			error_no_results : "Kon geen coordinaten vinden voor deze plaats"
		},
		displayError : function(message) {
			alert(message);
		}
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// VARIABLES USED BY THE FUNCTION (DON'T MODIFY THIS PART) ////////////////////////////////////
	_self.vars = {
		ID : null,
		LATLNG : null,
		map : null,
		marker : null,
		geocoder : null
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PRIVATE FUNCTIONS FOR MANIPULATING DATA ////////////////////////////////////////////////////
	//ORIGINAL var setPosition = function(position) {
	var setPosition = function(position) {
		_self.vars.marker.setPosition(position);
		_self.vars.map.panTo(position);

		$(_self.vars.cssID + ".gllpZoom").val( _self.vars.map.getZoom() );
		$(_self.vars.cssID + ".gllpLongitude").val( position.lng() );
		$(_self.vars.cssID + ".gllpLatitude").val( position.lat() );

		$(_self.vars.cssID).trigger("location_changed", $(_self.vars.cssID));

		if (_self.params.queryLocationNameWhenLatLngChanges) {
			getLocationName(position);
		}
		if (_self.params.queryElevationWhenLatLngChanges) {
			getElevation(position);
		}
	};

	// for reverse geocoding
	var getLocationName = function(position) {
		var latlng = new google.maps.LatLng(position.lat(), position.lng());
		_self.vars.geocoder.geocode({'latLng': latlng}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK && results[1]) {
				$(_self.vars.cssID + ".gllpLocationName").val(results[1].formatted_address);
			} else {
				$(_self.vars.cssID + ".gllpLocationName").val("");
			}
			$(_self.vars.cssID).trigger("location_name_changed", $(_self.vars.cssID));
		});
	};

	// for getting the elevation value for a position
	var getElevation = function(position) {
		var latlng = new google.maps.LatLng(position.lat(), position.lng());

		var locations = [latlng];

		var positionalRequest = { 'locations': locations };

		_self.vars.elevator.getElevationForLocations(positionalRequest, function(results, status) {
			if (status == google.maps.ElevationStatus.OK) {
				if (results[0]) {
					$(_self.vars.cssID + ".gllpElevation").val( results[0].elevation.toFixed(3));
				} else {
					$(_self.vars.cssID + ".gllpElevation").val("");
				}
			} else {
				$(_self.vars.cssID + ".gllpElevation").val("");
			}
			$(_self.vars.cssID).trigger("elevation_changed", $(_self.vars.cssID));
		});
	};

	// search function
	var performSearch = function(string, silent) {
		if (string == "") {
			if (!silent) {
				_self.params.displayError( _self.params.strings.error_empty_field );
			}
			return;
		}
		_self.vars.geocoder.geocode(
			{"address": string},
			function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					$(_self.vars.cssID + ".gllpZoom").val(11);
					_self.vars.map.setZoom( parseInt($(_self.vars.cssID + ".gllpZoom").val()) );
					setPosition( results[0].geometry.location );
				} else {
					if (!silent) {
						_self.params.displayError( _self.params.strings.error_no_results );
					}
				}
			}
		);
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PUBLIC FUNCTIONS  //////////////////////////////////////////////////////////////////////////
	var publicfunc = {

		// INITIALIZE MAP ON DIV //////////////////////////////////////////////////////////////////
		init : function(object) {

			if ( !$(object).attr("id") ) {
				if ( $(object).attr("name") ) {
					$(object).attr("id", $(object).attr("name") );
				} else {
					$(object).attr("id", "_MAP_" + Math.ceil(Math.random() * 10000) );
				}
			}

			_self.vars.ID = $(object).attr("id");
			_self.vars.cssID = "#" + _self.vars.ID + " ";

			_self.params.defLat  = $(_self.vars.cssID + ".gllpLatitude").val()  ? $(_self.vars.cssID + ".gllpLatitude").val()		: _self.params.defLat;
			_self.params.defLng  = $(_self.vars.cssID + ".gllpLongitude").val() ? $(_self.vars.cssID + ".gllpLongitude").val()	    : _self.params.defLng;
			_self.params.defZoom = $(_self.vars.cssID + ".gllpZoom").val()      ? parseInt($(_self.vars.cssID + ".gllpZoom").val()) : _self.params.defZoom;
			_self.vars.LATLNG = new google.maps.LatLng(_self.params.defLat, _self.params.defLng);

			_self.vars.MAPOPTIONS		 = _self.params.mapOptions;
			_self.vars.MAPOPTIONS.zoom   = _self.params.defZoom;
			_self.vars.MAPOPTIONS.center = _self.vars.LATLNG;

			_self.vars.map = new google.maps.Map($(_self.vars.cssID + ".google-map").get(0), _self.vars.MAPOPTIONS);
			_self.vars.geocoder = new google.maps.Geocoder();
			_self.vars.elevator = new google.maps.ElevationService();

			_self.vars.marker = new google.maps.Marker({
				position: _self.vars.LATLNG,
				map: _self.vars.map,
				title: _self.params.strings.markerText,
				draggable: true
			});

             _self.searchableMapReportDirect=[];
             _self.searchableMapCompleteReport=[];

            // Create the search box and link it to the UI element.
            elements = document.getElementsByClassName('pac-input');
            for (var i = 0; i < elements.length; i++) {
                if(elements[i].id == 'searchInputDesktopCompleteReport') {
                    _self.searchableMapCompleteReport.input = elements[i];
                    _self.searchableMapCompleteReport.searchBox = new google.maps.places.SearchBox(_self.searchableMapCompleteReport.input);
                    _self.searchableMapCompleteReport.searchBox.addListener('places_changed', function() {
                        _self.searchableMapCompleteReport.places = _self.searchableMapCompleteReport.searchBox.getPlaces();
                    });
                }
                if(elements[i].id == 'searchInputDesktopReportDirect') {
                    _self.searchableMapReportDirect.input = elements[i];
                    _self.searchableMapReportDirect.searchBox = new google.maps.places.SearchBox(_self.searchableMapReportDirect.input);
                    _self.searchableMapReportDirect.searchBox.addListener('places_changed', function() {
                        _self.searchableMapReportDirect.places = _self.searchableMapReportDirect.searchBox.getPlaces();
                    });
                }
            }

			// Forces the map to (re-)draw itself when idle (i.e. showing a grey square)
            google.maps.event.addListenerOnce(_self.vars.map, 'idle', function() {
                // Put the marker on default position on the map (i.e. center of Holland)
                // Except for 'complete report' page, which may have been provided a position
                // from the backend
                //setPosition(_self.vars.marker.position);
                google.maps.event.trigger(_self.vars.map, 'resize');
            });

			// Set position on doubleclick
			google.maps.event.addListener(_self.vars.map, 'dblclick', function(event) {
				setPosition(event.latLng);
			});

			// Set position on marker move
			google.maps.event.addListener(_self.vars.marker, 'dragend', function(event) {
				setPosition(_self.vars.marker.position);
			});

			// Set zoom feld's value when user changes zoom on the map
			google.maps.event.addListener(_self.vars.map, 'zoom_changed', function(event) {
				$(_self.vars.cssID + ".gllpZoom").val( _self.vars.map.getZoom() );
				$(_self.vars.cssID).trigger("location_changed", $(_self.vars.cssID));
			});

			// Update location and zoom values based on input field's value
			$(_self.vars.cssID + ".gllpUpdateButton").bind("click", function() {
				var lat = $(_self.vars.cssID + ".gllpLatitude").val();
				var lng = $(_self.vars.cssID + ".gllpLongitude").val();
				var latlng = new google.maps.LatLng(lat, lng);
				_self.vars.map.setZoom( parseInt( $(_self.vars.cssID + ".gllpZoom").val() ) );
				setPosition(latlng);
			});

			// Search function by search button
			$(_self.vars.cssID + ".gllpSearchButton").bind("click", function() {
				performSearch( $(_self.vars.cssID + ".gllpSearchField").val(), false );
			});

			// Search function by gllp_perform_search listener
			$(document).bind("gllp_perform_search", function(event, object) {
				performSearch( $(object).attr('string'), true );
			});

			// Zoom function triggered by gllp_perform_zoom listener
			$(document).bind("gllp_update_fields", function(event) {
				var lat = $(_self.vars.cssID + ".gllpLatitude").val();
				var lng = $(_self.vars.cssID + ".gllpLongitude").val();
				var latlng = new google.maps.LatLng(lat, lng);
				_self.vars.map.setZoom( parseInt( $(_self.vars.cssID + ".gllpZoom").val() ) );
				setPosition(latlng);
			});
		},

		addMarkers : function(markers) {
            // The marker collection, adds below markers to the map
            _self.vars.markers = [];
            $.ajax({
                type: "get",
                url: "https://verkeerslicht-165607.appspot.com/api/devices",
                success: function(response) {
                    var result = response[markers];
                    for(var i = 0; i < result.length; i++) {
                        // The marker collection, adds below markers to the map
                        var newMarker = _self.vars.markers[i] = new google.maps.Marker({
                            position: new google.maps.LatLng(result[i]['location']['latitude'], result[i]['location']['longitude']), // WAS _self.vars.LATLNG,
                            map: _self.vars.map, // The map
                            title: result[i]['name'],
                            icon: {
                                path: 'M4.2,0C1.9,0,0,1.9,0,4.2l0,17.9C0,24.4,4.2,30,4.2,30s4.2-5.6,4.2-7.9l0-17.9C8.3,1.9,6.5,0,4.2,0z M5.8,24.4 c-1.5,0.9-3.6,0.4-4.5-1.2c-0.9-1.5-0.4-3.6,1.2-4.5s3.6-0.4,4.5,1.2C8,21.5,7.4,23.5,5.8,24.4z M5.9,15.7c-1.5,0.9-3.6,0.4-4.5-1.2 S1.1,11,2.6,10.1S6.1,9.7,7,11.3S7.5,14.8,5.9,15.7z M5.9,7.3C4.4,8.2,2.4,7.7,1.5,6.1S1.1,2.6,2.6,1.7s3.6-0.4,4.5,1.2 S7.5,6.4,5.9,7.3z',
                                fillOpacity: 1,
                                fillColor: '#242e42',
                                strokeWeight: 0,
                                size: [8, 12],
                                labelOrigin: new google.maps.Point(18, 10),
                                anchor: new google.maps.Point(4, 12)
                            },
                            draggable: false
                        });
                        newMarker.disclaimer = result[i]['disclaimer'];
                        var infoWindow = new google.maps.InfoWindow();
                        google.maps.event.addListener(newMarker, 'click', (function(newMarker, infoWindow){
                            return function () {
                                /* close the previous infoWindow */
                                // closeInfos();
                                infoWindow.setContent(newMarker.disclaimer);
                                infoWindow.open(this.map, newMarker);
                                /* keep the handle, in order to close it on next click event */
                                // infos[0] = infoWindow;
                            };
                        }(newMarker, infoWindow)));
                    }
                },
                error: function(response) {
                    var result = response['response'];
                }
            });
            return;
        },
		// EXPORT PARAMS TO EASILY MODIFY THEM ////////////////////////////////////////////////////
		params : _self.params
	};
	return publicfunc;
});

}(jQuery));

$(document).ready( function() {

	if (!$.gMapsLatLonPickerNoAutoInit) {

        if($('.pageReport').length) {
            // initiate the maps and places here,
            // and in addition when the modal opens
            // to force initiation,
            // otherwise we end up with a grayed out map
            initMaps();
            initPlaces();
        }
	}
});

$(document).bind("location_changed", function(event, object) {
    var locationNameClass = "gllpLocationName";
    var latitudeClass = "gllpLatitude";
    var longitudeClass = "gllpLongitude";
    var deviceClass = "gllpDevice";
    var mapIndex = 0; // default

    // Allow the location name to be set by the update
    setTimeout(function(){
        elements = document.getElementsByClassName(deviceClass);
        for (var i = 0; i < elements.length; i++) {
            // The 'complete report' page has three maps in total
            if(elements.length == 3) {
                switch(elements[i].value) {
                    case 'desktop':
                        mapIndex = 0; // the desktop map on the 'complete report' page
                        break;
                    case 'mobile':
                        mapIndex = 2; // the mobile map on the page
                        break;
                    default:
                }
            }
            // All other pages have two maps in total
            if(elements.length == 2) {
                switch(elements[i].value) {
                    case 'desktop':
                        mapIndex = 0; // the first map on the page
                        break;
                    case 'mobile':
                        // Check again that we are not on desktop, there is a screen width at which we are back on desktop
                        // Less than or equal to 992px is mobile, otherwise it is desktop
                        if ($(window).width() < 768) {
                          // do something for small screens
                          mapIndex = 1; // the second map on the page
                          break;
                        }
                        else if ($(window).width() >= 768 &&  $(window).width() <= 992) {
                          // do something for medium screens
                          mapIndex = 1; // the second map on the page
                          break;
                        }
                        else if ($(window).width() > 992 &&  $(window).width() <= 1200) {
                          // do something for big screens
                          mapIndex = 0; // the first map on the page
                          break;
                        }
                        else  {
                          // do something for huge screens
                          mapIndex = 0; // the first map on the page
                          break;
                        }
                    default:
                        mapIndex = 0; // the first map on the page
                        break;
                }
            }
        }

        elements = document.getElementsByClassName(locationNameClass);
        for (var i = 0; i < elements.length; i++) {
            // Set this location to the 'place' input field
            document.getElementById('place').value = elements[mapIndex].value;
            if (typeof(document.getElementById('complete_report_place')) != 'undefined' && document.getElementById('complete_report_place') != null) {
                document.getElementById('complete_report_place').value = elements[mapIndex].value;
            }
        }
        elements = document.getElementsByClassName(latitudeClass);
        for (var i = 0; i < elements.length; i++) {
            // Set this latitude to the 'latitude' input field
            document.getElementById('latitude').value = elements[mapIndex].value;
            if (typeof(document.getElementById('complete_report_latitude')) != 'undefined' && document.getElementById('complete_report_latitude') != null) {
                document.getElementById('complete_report_latitude').value = elements[mapIndex].value;
            }
        }
        elements = document.getElementsByClassName(longitudeClass);
        for (var i = 0; i < elements.length; i++) {
            // Set this longitude to the 'longitude' input field
            document.getElementById('longitude').value = elements[mapIndex].value;
            if (typeof(document.getElementById('complete_report_longitude')) != 'undefined' && document.getElementById('complete_report_longitude') != null) {
                document.getElementById('complete_report_longitude').value = elements[mapIndex].value;
            }
        }
    }, 1000); // 1 second, adjust to fit, for the map to finish updating to new position
});
</script>
<script>
document.getElementById('searchInputDesktopReportDirect').onkeypress = function(e){
  if (!e) e = window.event;
  var keyCode = e.keyCode || e.which;
  if (keyCode == '13'){
    // Enter pressed
    document.getElementById('searchButtonDesktopReportDirect').click();
    return false;
  }
}
</script>

<!-- Part of Safari support for HTML5 form validation for attribute 'required' -->
<style>
.invalid input:required:invalid {
    border: 1px solid #BE4C54;
}

.invalid input:required:valid {
    border: 1px solid #17D654 ;
}

input .custom {
  display: block;
  margin-bottom: 10px;
}
</style>
