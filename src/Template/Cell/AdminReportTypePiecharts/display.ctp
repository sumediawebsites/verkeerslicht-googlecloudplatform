<header>
    <h2 class="h3">Statistieken</h2>
</header>

<div class="card card--white">
    <div class="graph-grid">
        <div class="graph graph--donut">
            <div class="graph__chart">
                <canvas id="graphChart_new"></canvas>
                <span class="chart__percentage"><?=round($newReports/$totalReports*100)?></span>
            </div>

            <span class="graph__label">Nieuwe meldingen</span>

            <script>
                var ctx = document.getElementById("graphChart_new");

                var graphChart_new = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: [ 'Nieuw', 'Totaal' ],
                        datasets: [{
                            data: [ <?=$newReports?>, <?=$totalReports-$newReports?> ],
                            backgroundColor: [
                                "#242e42",
                                "#d2d4d8"
                            ],
                            hoverBackgroundColor: [
                                "#242e42",
                                "#d2d4d8"
                            ]
                        }]
                    },
                    options: {
                        responsive: false,
                        legend: {
                            display: false
                        },
                        tooltips: {
                            enabled: false
                        },
                        cutoutPercentage: 75
                    }
                });
            </script>
        </div>

        <div class="graph graph--donut">
            <div class="graph__chart">
                <canvas id="graphChart_hold"></canvas>
                <span class="chart__percentage"><?=round($heldReports/$totalReports*100)?></span>
            </div>

            <span class="graph__label">In de wacht</span>

            <script>
                var ctx = document.getElementById("graphChart_hold");
                var graphChart_hold = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: [ 'In de wacht', 'Totaal' ],
                        datasets: [{
                            data: [ <?=$heldReports?>, <?=$totalReports-$heldReports?> ],
                            backgroundColor: [
                                "#f90",
                                "#d2d4d8"
                            ],
                            hoverBackgroundColor: [
                                "#f90",
                                "#d2d4d8"
                            ]
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        tooltips: {
                            enabled: false
                        },
                        cutoutPercentage: 75
                    }
                });
            </script>
        </div>

        <div class="graph graph--donut">
            <div class="graph__chart">
                <canvas id="graphChart_rejected"></canvas>
                <span class="chart__percentage"><?=round($rejectedReports/$totalReports*100)?></span>
            </div>

            <span class="graph__label">Afgewezen</span>

            <script>
                var ctx = document.getElementById("graphChart_rejected");
                var graphChart_rejected = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: [ 'Afgewezen', 'Totaal' ],
                        datasets: [{
                            data: [ <?=$rejectedReports?>, <?=$totalReports-$rejectedReports?> ],
                            backgroundColor: [
                                "#f7412d",
                                "#d2d4d8"
                            ],
                            hoverBackgroundColor: [
                                "#f7412d",
                                "#d2d4d8"
                            ]
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        tooltips: {
                            enabled: false
                        },
                        cutoutPercentage: 75
                    }
                });
            </script>
        </div>

        <div class="graph graph--donut">
            <div class="graph__chart">
                <canvas id="graphChart_positive"></canvas>
                <span class="chart__percentage"><?=round($positiveReports/$totalReports*100)?></span>
            </div>

            <span class="graph__label">Behandeld en positief</span>

            <script>
                var ctx = document.getElementById("graphChart_positive");
                var graphChart_positive = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: [ 'Positief behandeld', 'Totaal' ],
                        datasets: [{
                            data: [ <?=$positiveReports?> , <?=$totalReports-$positiveReports?> ],
                            backgroundColor: [
                                "#79c410",
                                "#d2d4d8"
                            ],
                            hoverBackgroundColor: [
                                "#79c410",
                                "#d2d4d8"
                            ]
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        tooltips: {
                            enabled: false
                        },
                        cutoutPercentage: 75
                    }
                });
            </script>
        </div>

        <div class="graph graph--donut">
            <div class="graph__chart">
                <canvas id="graphChart_neutral"></canvas>
                <span class="chart__percentage"><?=round($negativeReports/$totalReports*100)?></span>
            </div>

            <span class="graph__label">Behandeld maar neutraal</span>

            <script>
                var ctx = document.getElementById("graphChart_neutral");
                var graphChart_neutral = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: [ 'Neutraal behandeld', 'Totaal' ],
                        datasets: [{
                            data: [ <?=$negativeReports?> , <?=$totalReports-$negativeReports?> ],
                            backgroundColor: [
                                "#ffcf00",
                                "#d2d4d8"
                            ],
                            hoverBackgroundColor: [
                                "#ffcf00",
                                "#d2d4d8"
                            ]
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        tooltips: {
                            enabled: false
                        },
                        cutoutPercentage: 75
                    }
                });
            </script>
        </div>
    </div>
</div>
