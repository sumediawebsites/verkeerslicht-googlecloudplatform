<header>
    <h2 class="h3">Meldingen</h2>

    <form name="days" method="post">
        <div class="select-wrapper">
            <select class="form-control" name="days" onchange="moreData(this.value)">
                <option value="7">Per dag</option>
                <option value="14">Per 2 weken</option>
                <option value="30">Per maand</option>
                <option value="365">Per jaar</option>
            </select>
        </div>
    </form>
</header>

<div class="card card--white">
    <?php $startingLabels = '["'.rtrim(implode('", "', array_keys($groupedReports)), '", "').'"]';?>
    <?php $startingData = '["'.rtrim(implode('", "', array_values($groupedReports)), '", "').'"]';?>

    <script>
        function moreData( numberOfDays ){

            $.get( "https://verkeerslicht.dev/admin/reports/getReportsFromDay/"+numberOfDays, function( result ) {
                // empty data
                while (chartData.length) { chartData.pop(); }
                while (chartLabels.length) { chartLabels.pop(); }

                // fill data
                result['labels'].forEach(function(labels, index){
                    chartLabels.push(labels);
                    lineChart_totalReports.update();
                });
                result['data'].forEach(function(data, index){
                    chartData.push(data);
                    lineChart_totalReports.update();
                });

                lineChart_totalReports.update();
            }).fail(function() {
            }).always(function() {
            });
        };
    </script>

    <canvas id="lineChart_totalReports"></canvas>

    <script>
        var chartData = <?= $startingData; ?>,
            chartLabels = <?= $startingLabels; ?>;

        var ctx = document.getElementById("lineChart_totalReports");
        var lineChart_totalReports = new Chart(ctx, {
            type: 'line',
            data: {
                labels: chartLabels,
                datasets: [{
                    data: chartData,
                    fill: "#f6f7f8",
                    lineTension: 0,
                    borderWidth: 3,
                    borderColor: "#242e42",
                    borderCapStyle: "butt",
                    pointRadius: 10,
                    pointBorderWidth: 3,
                    pointBackgroundColor: "#f2f2f2",
                    pointBorderColor: "#242e42",
                    pointHitRadius: 3,
                    pointHoverRadius: 11,
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        gridLines: {
                          tickMarkLength: 15
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                          tickMarkLength: 15
                        },
                        ticks: {
                            beginAtZero: true,
                            //suggestedMax: 60,
                            //stepSize: 10
                        }
                    }]
                },
                layout: {
                    padding: {
                        top: 15
                    }
                },
                legend: {
                    display: false
                }
            }
        });
    </script>
</div>
