<?php
/**
 * @var \Cake\View\View $this
 * @var \Cake\Model\Table\Entity\BlogCategory $blogCategory
 **/
?>

<div class="blogCategories view large-9 medium-8 columns content">
    <h3><?= h($blogCategory->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Parent Blog Category') ?></th>
            <td><?= $blogCategory->has('parent_blog_category') ? $this->Html->link($blogCategory->parent_blog_category->name, ['controller' => 'BlogCategories', 'action' => 'view', $blogCategory->parent_blog_category->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($blogCategory->name) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($blogCategory->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Blog Articles') ?></h4>
        <?php if (!empty($blogCategory->blog_articles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Body') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($blogCategory->blog_articles as $blogArticles): ?>
            <tr>
                <td><?= h($blogArticles->title) ?></td>
                <td><?= h($blogArticles->slug) ?></td>
                <td><?= h($blogArticles->body) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BlogArticles', 'action' => 'view', $blogArticles->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Blog Categories') ?></h4>
        <?php if (!empty($blogCategory->child_blog_categories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Description') ?></th>
            </tr>
            <?php foreach ($blogCategory->child_blog_categories as $childBlogCategories): ?>
            <tr>
                <td><?= $this->Html->link($childBlogCategories->name, ['controller' => 'BlogCategories', 'action' => 'view', $childBlogCategories->id]) ?></td>
                <td><?= h($childBlogCategories->description) ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
