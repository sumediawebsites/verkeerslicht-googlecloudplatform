<div class="blogQuestions index large-9 medium-8 columns content">
    <h3><?= __('Blog Categories') ?></h3>

    <ul>
      <?php foreach ($blogCategories as $blogCategoryChild): ?>
        <li>
          <ul>
            <li><?= $this->Html->link($blogCategoryChild->name, ['controller' => 'BlogCategories', 'action' => 'view', $blogCategoryChild->id]); ?></li>
            <li><?= $blogCategoryChild->description ?></li>
          </ul>
        <li>
    <?php endforeach; ?>
  </ul>

</div>
