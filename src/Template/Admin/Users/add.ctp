<header class="page__header">
	<h1 class="h3"><?= __( 'Gebruiker Toevoegen' ) ?></h1>

	<ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($user, ['type' => 'file']) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Add User') ?></h4>
	    </div>
		
	    <div class="panel-body">
	        <?php
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'role_id', 'label' => __('Role Id'), 'options' => $roles]);
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'initials', 'label' => __('Initials')]);
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'firstname', 'label' => __('Firstname')]);
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'insertion', 'label' => __('Insertion')]);
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'lastname', 'label' => __('Lastname')]);
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'username', 'label' => __('Username')]);
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'phone', 'label' => __('Phone')]);
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'email', 'label' => __('Email')]);
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'pwd', 'label' => __('Password'), 'type' => 'password' ]);
	            echo $this->element('AdminBootstrap.input_field', ['field' => 'pwd_repeat', 'label' => __('Repeat password'), 'type' => 'password' ]);
                echo $this->element( 'AdminBootstrap.input_field', [
                    'field'           => 'picture',
                    'label'           => __( 'Image' ),
                    'type'            => 'file'
                ] );
	    	?>
        </div>

        <div class="panel-footer">
    		<?= $this->Form->button(__('Submit')) ?>
	    </div>
    </div>
	<?= $this->Form->end() ?>
</section>
