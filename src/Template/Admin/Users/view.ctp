<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nieuwe User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nieuwe Role'), ['controller' => 'Roles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Filters'), ['controller' => 'Filters', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nieuwe Filter'), ['controller' => 'Filters', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Notifications'), ['controller' => 'Notifications', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nieuwe Notification'), ['controller' => 'Notifications', 'action' => 'add']) ?> </li>
    </ul>
</nav>

<section class="page__content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Role') ?></th>
            <td><?= $user->has('role') ? $this->Html->link($user->role->name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Initials') ?></th>
            <td><?= h($user->initials) ?></td>
        </tr>
        <tr>
            <th><?= __('fullname') ?></th>
            <td><?= h($user->fullname) ?></td>
        </tr>
        <tr>
            <th><?= __('Firstname') ?></th>
            <td><?= h($user->firstname) ?></td>
        </tr>
        <tr>
            <th><?= __('Insertion') ?></th>
            <td><?= h($user->insertion) ?></td>
        </tr>
        <tr>
            <th><?= __('Lastname') ?></th>
            <td><?= h($user->lastname) ?></td>
        </tr>
        <tr>
            <th><?= __('Username') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th><?= __('Phone') ?></th>
            <td><?= h($user->phone) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th><?= __('Picture') ?></th>
            <td><?= h($user->picture) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created By') ?></th>
            <td><?= $this->Number->format($user->created_by) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($user->modified_by) ?></td>
        </tr>
        <tr>
            <th><?= __('Created At') ?></th>
            <td><?= h($user->created_at) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified At') ?></th>
            <td><?= h($user->modified_at) ?></td>
        </tr>
        <tr>
            <th><?= __('Deleted') ?></th>
            <td><?= h($user->deleted) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Active') ?></th>
            <td><?= $user->is_active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Settings') ?></h4>
        <?= $this->Text->autoParagraph(h($user->settings)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Filters') ?></h4>
        <?php if (!empty($user->filters)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Filter Group Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Public') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Sort Order') ?></th>
                <th><?= __('Grid') ?></th>
                <th><?= __('Content') ?></th>
                <th><?= __('Created By') ?></th>
                <th><?= __('Modified By') ?></th>
                <th><?= __('Created At') ?></th>
                <th><?= __('Modified At') ?></th>
                <th><?= __('Deleted') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->filters as $filters): ?>
            <tr>
                <td><?= h($filters->id) ?></td>
                <td><?= h($filters->filter_group_id) ?></td>
                <td><?= h($filters->name) ?></td>
                <td><?= h($filters->public) ?></td>
                <td><?= h($filters->user_id) ?></td>
                <td><?= h($filters->sort_order) ?></td>
                <td><?= h($filters->grid) ?></td>
                <td><?= h($filters->content) ?></td>
                <td><?= h($filters->created_by) ?></td>
                <td><?= h($filters->modified_by) ?></td>
                <td><?= h($filters->created_at) ?></td>
                <td><?= h($filters->modified_at) ?></td>
                <td><?= h($filters->deleted) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Filters', 'action' => 'view', $filters->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Filters', 'action' => 'edit', $filters->id]) ?>
                    <?= $this->Form->postLink(__('Verwijderen'), ['controller' => 'Filters', 'action' => 'delete', $filters->id], ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $filters->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Notifications') ?></h4>
        <?php if (!empty($user->notifications)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Template') ?></th>
                <th><?= __('Vars') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('State') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Tracking Id') ?></th>
                <th><?= __('Deleted') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->notifications as $notifications): ?>
            <tr>
                <td><?= h($notifications->id) ?></td>
                <td><?= h($notifications->template) ?></td>
                <td><?= h($notifications->vars) ?></td>
                <td><?= h($notifications->user_id) ?></td>
                <td><?= h($notifications->state) ?></td>
                <td><?= h($notifications->created) ?></td>
                <td><?= h($notifications->modified) ?></td>
                <td><?= h($notifications->tracking_id) ?></td>
                <td><?= h($notifications->deleted) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Notifications', 'action' => 'view', $notifications->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Notifications', 'action' => 'edit', $notifications->id]) ?>
                    <?= $this->Form->postLink(__('Verwijderen'), ['controller' => 'Notifications', 'action' => 'delete', $notifications->id], ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $notifications->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</section>
