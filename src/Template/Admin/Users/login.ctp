<div class="users form form-signin">
    <?= $this->Flash->render( 'auth' ) ?>
    <?= $this->Form->create() ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><?= __( 'Please enter your username and password' ) ?></h4>
        </div>
        <div class="panel-body">
            <?= $this->Form->input( 'username' ) ?>
            <?= $this->Form->input( 'password' ) ?>
            <?= $this->Form->button( __( 'Login' ), ['class' => 'btn btn--primary btn--block btn--lg'] ); ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
