<header class="page__header">
	<h1 class="h3"><?= __( 'Wachtwoord Wijzigen' ) ?></h1>
</header>

<section class="page__section">
    <?= $this->Form->create($user) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('New password') ?></h4>
		    </div>
	    <div class="panel-body">
            <?= $this->Form->input( 'pwd' , [ 'label' => 'Password', 'type' => 'password' ] ); ?>
            <?= $this->Form->input( 'pwd_repeat', [ 'label' => 'Repeat password', 'type' => 'password' ] ); ?>
        </div>
        <div class="panel-footer">
            <?= $this->Form->button( __( 'Change password' ), ['class' => 'button'] ); ?>
            <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
