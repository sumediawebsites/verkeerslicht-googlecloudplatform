<header class="page__header">
	<h1 class="h3"><?= __( 'Gebruiker Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($user, ['type' => 'file']) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit User') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
                echo $this->element('AdminBootstrap.input_field', ['field' => 'role_id', 'label' => __('Role Id'), 'options' => $roles]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'initials', 'label' => __('Initials')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'firstname', 'label' => __('Firstname')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'insertion', 'label' => __('Insertion')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'lastname', 'label' => __('Lastname')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'username', 'label' => __('Username')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'phone', 'label' => __('Phone')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'email', 'label' => __('Email')]);
                $imageUrlBeforeValueHtml = '
                                    <div class="image-url-container">
                                        <div class="image-url">
                                            ' . $this->Html->Image('/' . $user->picture, [
                        'class' => 'preview',
                        'id' => 'image-url-image-picture'
                    ]) . '
                                        </div>
                                        <div class="image-url-actions">
                                            ' .
                    $this->Html->link(__('Verwijderen'), '#', [
                        'class' => 'remove-image-url',
                        'data-image-type' => 'picture'
                    ]) .
                    $this->Form->hidden('delete_image_picture', ['value' => '']);

                $imageUrlAfterValueHtml = '
                                        </div>
                                    </div>
                                ';
                echo $this->element( 'AdminBootstrap.input_field', [
                    'field'           => 'picture',
                    'label'           => __( 'Image' ),
                    'type'            => 'file',
                    'beforeValueHtml' => $user->picture != null ? $imageUrlBeforeValueHtml : '',
                    'afterValueHtml'  => $user->picture != null ? $imageUrlAfterValueHtml : ''
                ] );

        ?>
        </div>
        <div class="panel-footer">
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
	    </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $(".remove-image-url").click(function (event) {
            event.preventDefault();
            $("#image-url-image-" + $(this).data('image-type')).attr("src", "");
            $("input[name='delete_image_" + $(this).data('image-type') + "']").val("deleted");
        });
    });
</script>
