<header class="page__header">
    <h1 class="h3">Dashboard</h1>
</header>

<section class="page__section section--stats">
    <?= $this->Cell( 'AdminCounter', [ 'Totaal meldingen', 'Reports', [ 'Devices.user_id' => $manager->id ], 'reportCounter counter--total'] ); ?>
    <?= $this->Cell( 'AdminCounter', [ 'Te lang rood', 'Reports', [ 'report_type_id' => '2', 'Devices.user_id' => $manager->id ], 'reportCounter counter--red'] ); ?>
    <?= $this->Cell( 'AdminCounter', [ 'Te kort groen', 'Reports', [ 'report_type_id' => '3', 'Devices.user_id' => $manager->id  ], 'reportCounter counter--green'] ); ?>
    <?= $this->Cell( 'AdminCounter', [ 'Onveilige situatie', 'Reports', [ 'report_type_id' => '4', 'Devices.user_id' => $manager->id  ], 'reportCounter counter--unsafe'] ); ?>
    <?= $this->Cell( 'AdminCounter', [ 'Overige', 'Reports', [ 'report_type_id' => '5', 'Devices.user_id' => $manager->id ], 'reportCounter counter--other'] ); ?>
</section>

<section class="page__section">
    <div class="row">
        <div class="col-xl-6">
            <div class="widget widget--graph">
                <?= $this->Cell( 'AdminGraph', [ 7, [ 'Devices.user_id' => $manager->id ] ] ); ?>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="widget widget--list">
                <?= $this->Cell( 'AdminReportsLatest', [ ['Devices.user_id' => $manager->id ] ]); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <div class="widget">
                <?= $this->Cell( 'AdminReportTypePiecharts', [ ['Devices.user_id' => $manager->id ] ]); ?>
            </div>
        </div>
    </div>
</section>
