<header class="page__header">
	<h1 class="h3"><?= __( 'Kennisbankgeruikersvragen' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuwe Kennisbankgebruikersvraag'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
            <?= $this->Cell( 'Bootgrid.Bootgrid', [
                [
                        [
                            'column-id' => 'id',
            'visible' => false,
                                                'type' => 'integer',
                                    'header' => __('Id'),
            ],
                        [
                            'column-id' => 'FaqCategories__name',
            'filter_field' => 'FaqUserQuestions.faq_category_id',
            'input' => 'select',
            'values' => $faqCategories,
                                            'type' => 'integer',
                                    'header' => __('Faq Category Id'),
            ],
                        [
                            'column-id' => 'FaqUserQuestions__subject',
                                                'type' => 'string',
                                    'header' => __('Subject'),
            ],
                        [
                            'column-id' => 'FaqUserQuestions__email',
                                                'type' => 'string',
                                    'header' => __('Email'),
            ],
                        [
                            'column-id' => 'FaqUserQuestions__answered',
                                                'type' => 'boolean',
                                    'formatter' => 'boolean',
                        'header' => __('Answered'),
            ],
                        [
                            'column-id' => 'CreatedBy__username',
            'filter_field' => 'FaqUserQuestions.created_by',
            'input' => 'select',
            'values' => $createdBy,
                                            'type' => 'integer',
                                    'header' => __('Created By'),
            ],
                        [
                            'column-id' => 'ModifiedBy__username',
            'filter_field' => 'FaqUserQuestions.modified_by',
            'input' => 'select',
            'values' => $modifiedBy,
                                            'type' => 'integer',
                                    'header' => __('Modified By'),
            ],
                        [
                            'column-id' => 'FaqUserQuestions__created_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Created At'),
            ],
                        [
                            'column-id' => 'FaqUserQuestions__modified_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Modified At'),
            ],

                ]]);?>
        </div>
</section>
