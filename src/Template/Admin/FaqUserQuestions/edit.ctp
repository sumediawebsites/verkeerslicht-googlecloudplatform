<?php
/**
  * @var \App\View\AppView $this
  */
?>
<header class="page__header">
	<h1 class="h3"><?= __( 'Kennisbankgebuikersvraag Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Form->postLink(
                __('Verwijderen'),
                ['action' => 'delete', $faqUserQuestion->id],
                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $faqUserQuestion->id)]
            )
        ?></li>
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($faqUserQuestion) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Faq User Question') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'faq_category_id', 'label' => __('Faq Category Id'), 'options' => $faqCategories, 'empty' => true]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'email', 'label' => __('Email')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'subject', 'label' => __('Subject')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'message', 'label' => __('Message'), 'type' => 'wysiwyg']);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'answered', 'label' => __('Answered'), 'type' => 'checkbox' ]);
                ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>

	    </div>
    </div>
    <?= $this->Form->end() ?>

    <?= $this->Form->create($faqUserQuestion, [
        'url' => ['action' => 'respond']
    ]) ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><?= __('Respond to question') ?></h4>
        </div>
        <div class="panel-body">
            <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'email', 'label' => __('Recepients email')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'subject', 'label' => __('Subject')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'response', 'label' => __('Response'), 'type' => 'wysiwyg']);
            ?>
        </div>
        <div class="panel-footer">
            <?= $this->Form->button(__('Send')) ?>
        </div>
    </div>

</section>
