<header class="page__header">
	<h1 class="h3"><?= __( 'Meldingstatussen' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuwe Meldingstatus'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
            <?= $this->Cell( 'Bootgrid.Bootgrid', [
                [
                        [
                                'column-id' => 'id',
                'visible' => false,
                                                'type' => 'integer',
                                    'header' => __('Id'),
            ],
                        [
                                'column-id' => 'ReportStatuses__name',
                                                'type' => 'string',
                                    'header' => __('Name'),
            ],
                    [
                        'column-id' => 'ReportStatuses__class',
                        'type' => 'string',
                        'header' => __('Class name'),
                    ],
                        [
                                    'column-id' => 'CreatedBy__id',
                    'filter_field' => 'created_by',
                    'input' => 'select',
                    'values' => $createdBy,
                                            'type' => 'integer',
                                    'header' => __('Created By'),
            ],
                        [
                                    'column-id' => 'ModifiedBy__id',
                    'filter_field' => 'modified_by',
                    'input' => 'select',
                    'values' => $modifiedBy,
                                            'type' => 'integer',
                                    'header' => __('Modified By'),
            ],
                        [
                                'column-id' => 'ReportStatuses__created_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Created At'),
            ],
                        [
                                'column-id' => 'ReportStatuses__modified_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Modified At'),
            ],
                        [
                                'column-id' => 'ReportStatuses__deleted',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Deleted'),
            ],

                ]]);?>
        </div>
</section>
