<header class="page__header">
	<h1 class="h3"><?= __( 'Meldingstatus Toevoegen' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($reportStatus) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Add Report Status') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
        echo $this->element('AdminBootstrap.input_field', ['field' => 'name', 'label' => __('Name')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'description', 'label' => __('Description')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'class', 'label' => __('Class name')]);
        ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
