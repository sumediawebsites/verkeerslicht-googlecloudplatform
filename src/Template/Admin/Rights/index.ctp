<header class="page__header">
	<h1 class="h3"><?= __( 'Rechten' ) ?></h1>

	<!-- <ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Nieuw Recht' ), [ 'action' => 'add' ] ) ?></li>
    </ul> -->
</header>

<section class="page__section section--table">
	<div class="card card--white">
        <?= $this->Cell('Bootgrid.Bootgrid', [
            [
                [
                    'column-id' => 'Rights__id',
                    'formatter' => null,
                    'visible' => false,
                    'header' => __('Id')
                ],
                [
                    'column-id' => 'Rights__name',
                    'formatter' => null,
                    'visible' => true,
                    'header' => __('Name')
                ],
                [
                    'column-id' => 'Rights__code',
                    'formatter' => null,
                    'visible' => true,
                    'header' => __('Code')
                ],
            ]
        ]);
        ?>
    </div>
</section>
