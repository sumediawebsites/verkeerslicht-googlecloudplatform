<header class="page__header">
	<h1 class="h3"><?= __( 'Recht Toevoegen' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($right) ?>
	    <fieldset>
			<legend><?= __('Add Right') ?></legend>

			<div class="form_style_element">
		    	<div class="form_style_label">
			        <?= __('Name') ?>
			    </div>

			    <div class="form_style_two first">
			        <?php echo $this->Form->input('name', ['label' => false]); ?>
			    </div>

			    <div class="form_style_label">
			        <?= __('Code') ?>
			    </div>

			    <div class="form_style_two">
			        <?php echo $this->Form->input('code', ['label' => false]); ?>
			    </div>
			</div>

            <?php echo $this->Form->input('roles._ids', ['options' => $roles]); ?>
	    </fieldset>

    	<?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</section>
