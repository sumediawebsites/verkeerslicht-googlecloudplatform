<header class="page__header">
	<h1 class="h3"><?= __( 'Recht Bewerken' ) ?></h1>
</header>

<section class="page__section">
	<?= $this->Form->create( $right ) ?>
		<fieldset>
			<legend><?= __( 'Edit Right' ) ?></legend>
			<?= $this->element( 'AdminBootstrap.input_field', [
				'field'      => 'name',
				'label'      => __( 'Name' ),
				'twoColumns' => true,
				'first'      => true,
			] ); ?>
			<?= $this->element( 'AdminBootstrap.input_field', [
				'field'      => 'code',
				'label'      => __( 'Code' ),
				'twoColumns' => true,
			] ); ?>
			<?= $this->element( 'AdminBootstrap.input_field', [
				'field'      => 'roles._ids',
				'label'      => __( 'Roles' ),
				'twoColumns' => true,
				'options'    => $roles,
				'type'       => 'selectpicker',
				'first'      => true,
				'last'       => true,
			] ); ?>
		</fieldset>

		<?= $this->Form->button( __( 'Submit' ) ) ?>
	<?= $this->Form->end() ?>
</section>
