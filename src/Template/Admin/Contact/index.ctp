<header class="page__header">
	<h1 class="h3"><?= __( 'Contactberichten' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuwe Contact'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
        <?= $this->Cell( 'Bootgrid.Bootgrid', [
            [
                [
                    'column-id' => 'id',
                    'visible' => false,
                    'type' => 'integer',
                    'header' => __('Id'),
                ],
                [
                    'column-id' => 'Contact__firstname',
                    'type' => 'string',
                    'header' => __('Firstname'),
                ],
                [
                    'column-id' => 'Contact__lastname',
                    'type' => 'string',
                    'header' => __('Lastname'),
                ],
                [
                    'column-id' => 'Contact__email',
                    'type' => 'string',
                    'header' => __('Email'),
                ],
                [
                    'column-id' => 'Contact__subject',
                    'type' => 'string',
                    'header' => __('Subject'),
                ],
                [
                    'column-id' => 'Contact__is_answered',
                    'type' => 'boolean',
                    'formatter' => 'boolean',
                    'header' => __('Is Answered'),
                ],
                [
                    'column-id' => 'Contact__created_at',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Created At'),
                ],
                [
                    'column-id' => 'Contact__modified_at',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Modified At'),
                ],
            ]
        ]);?>
    </div>
</section>
