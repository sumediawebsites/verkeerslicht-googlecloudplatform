<?php
/**
  * @var \App\View\AppView $this
  */
?>
<header class="page__header">
	<h1 class="h3"><?= __( 'Contactbericht Bewerken ' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Form->postLink(
                __('Verwijderen'),
                ['action' => 'delete', $contact->id],
                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $contact->id)]
            )
        ?></li>
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($contact) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Contact') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
        echo $this->element('AdminBootstrap.input_field', ['field' => 'firstname', 'label' => __('Firstname')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'lastname', 'label' => __('Lastname')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'email', 'label' => __('Email')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'subject', 'label' => __('Subject')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'message', 'label' => __('Message'), 'type' => 'wysiwyg']);
        ?>
        </div>
        <div class="panel-footer">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Html->link(
                'Beantwoorden',
                ['controller' => 'Contact', 'action' => 'respond', $contact->id],
                ['class' => 'btn btn--primary']
            );
            ?>
	    </div>
    </div>
    <?= $this->Form->end() ?>
</section>
