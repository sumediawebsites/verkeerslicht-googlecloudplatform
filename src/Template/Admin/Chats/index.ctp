<header class="page__header">
	<h1 class="h3"><?= __( 'Gebruikers' ) ?></h1>

    <ul class="page__header__controls">
		<li><?= $this->Html->link( __( 'Nieuwe Gebruiker' ), [ 'action' => 'add' ] ) ?></li>
	</ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
		<?= $this->Cell( 'Bootgrid.Bootgrid', [
			[
				[
					'column-id' => 'id',
					'visible'   => false,
					'type'      => 'integer',
					'header'    => __( 'Id' ),
				],
				[
					'column-id'    => 'Roles__name',
					'filter_field' => 'role_id',
					'input'        => 'select',
					'values'       => $roles,
					'type'         => 'integer',
					'header'       => __( 'Role Id' ),
				],
				[
					'column-id' => 'Users__firstname',
					'type'      => 'string',
					'header'    => __( 'Firstname' ),
				],
				[
					'column-id' => 'Users__lastname',
					'type'      => 'string',
					'header'    => __( 'Lastname' ),
				],
				[
					'column-id' => 'Users__username',
					'type'      => 'string',
					'header'    => __( 'Username' ),
				],
				[
					'column-id' => 'Users__phone',
					'type'      => 'string',
					'header'    => __( 'Phone' ),
				],
				[
					'column-id' => 'Users__email',
					'type'      => 'string',
					'header'    => __( 'Email' ),
				],
				[
					'column-id' => 'Users__picture',
					'type'      => 'image',
					'header'    => __( 'Picture' ),
				],
			]
		] ); ?>
	</div>
</section>
