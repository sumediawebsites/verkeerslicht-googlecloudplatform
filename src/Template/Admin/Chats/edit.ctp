<header class="page__header">
	<h1 class="h3"><?=$this->Html->Image('/' . $user->picture, [
            'class' => 'avatar',
            'id' => 'image-url-image-picture'
        ]);?><?=$user->fullname; ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= __( 'Messages' ) ?></h3>
        </div>
        <?php if ( count($myMessages) > 0 ): ?>
            <table class="table table-striped">
                <tbody>
                <?php foreach ( $myMessages as $message ): ?>
                    <tr>
                        <td colspan="2" class="notificationBody">
                            <?= $message->body ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <div class="panel-body">
                <p><?= __( 'There are no messages for you at this moment.' ); ?></p>
            </div>
        <?php endif; ?>
    </div>

    <div class="panel panel-default">
        <?= $this->Form->create() ?>
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Bericht sturen') ?></h4>
		    </div>
	    <div class="panel-body">
            <?= $this->element('AdminBootstrap.input_field', ['field' => 'message', 'label' => false, 'type' => 'wysiwyg']);?>
        </div>
        <div class="panel-footer">
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
	    </div>
    </div>
</section>