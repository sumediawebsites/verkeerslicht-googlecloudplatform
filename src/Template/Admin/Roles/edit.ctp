<header class="page__header">
	<h1 class="h3"><?= __( 'Rol Bewerken' ) ?></h1>
</header>

<section class="page__section">
	<?= $this->Form->create( $role ) ?>
	<div class="panel panel-default">
		<div class="panel-body">
			<?= $this->element( 'AdminBootstrap.input_field', [
				'field' => 'name',
				'label' => __( 'Naam' ),
			] ); ?>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title"><?= __( 'Rechten' ); ?></h4>
		</div>
		<?php
		if ( ! empty( $rights ) ): ?>
		<table class="table table-striped">
			<!-- <tr>
				<th><?= __( 'Naam' ); ?></th>
				<th><?= __( 'Toestaan' ); ?></th>
				<th><?= __( 'Weigeren' ); ?></th>
			</tr> -->
			<?php foreach ( $rights as $right ):
				if ( in_array( $right->id, array_keys( $currentRights ) ) ) {
					$currentRR = $currentRights[ $right->id ];
				} else {
					$currentRR = null;
				}
				?>
				<tr>
					<td><?= $right->name; ?></td>
					<td><label class="radiobtn"><input type="radio" value="allow" name="rights[<?= $right->id; ?>][]" <?= $currentRR == null ? '' : $currentRR['allow'] ? 'checked' : ''; ?>/><span><?= __( 'Toestaan' ); ?></span></label></td>
					<td><label class="radiobtn"><input type="radio" value="deny" name="rights[<?= $right->id; ?>][]" <?= $currentRR == null ? '' : $currentRR['deny'] ? 'checked' : ''; ?>/><span><?= __( 'Weigeren' ); ?></span></label></td>
				</tr>
			<?php endforeach; ?>
		</table>
		<div class="panel-footer">
			<?php endif; ?>
			<?= $this->Form->button( __( 'Submit' ) ) ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
</section>

<script type="text/javascript">
	$('#check-all_1').on('change', function () {
		$('input[class="checkbox_1"]').prop('checked', $(this).prop('checked'));
		$('#check-all_2').prop('checked', false)
	});
	$('#check-all_2').on('change', function () {
		$('input[class="checkbox_2"]').prop('checked', $(this).prop('checked'));
		$('#check-all_1').prop('checked', false)
	});
	$("#check-all_1").change(function () {
		if (this.checked) {
			$('input[class="checkbox_2"]').prop("checked", false);
		}
	});
	$("#check-all_2").change(function () {
		if (this.checked) {
			$('input[class="checkbox_1"]').prop("checked", false);
		}
	});
</script>
