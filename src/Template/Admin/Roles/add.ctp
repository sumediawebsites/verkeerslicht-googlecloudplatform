<header class="page__header">
	<h1 class="h3"><?= __( 'Rol Toevoegen' ) ?></h1>

	<ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
	<?= $this->Form->create( $role ) ?>
		<fieldset>
			<legend><?= __( 'Add Role' ) ?></legend>
			<?= $this->element( 'AdminBootstrap.input_field', [
				'field' => 'name',
				'label' => __( 'Name' ),
			] ); ?>
			<?php echo $this->Form->hidden( 'rights_ids', [ 'options' => $rights ] ); ?>
		</fieldset>
		
		<?= $this->Form->button( __( 'Submit' ) ) ?>
	<?= $this->Form->end() ?>
</section>
