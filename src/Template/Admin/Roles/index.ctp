<header class="page__header">
	<h1 class="h3"><?= __( 'Rollen' ) ?></h1>

    <!-- <ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Nieuwe Rol' ), [ 'action' => 'add' ] ) ?></li>
    </ul> -->
</header>

<section class="page__section section--table">
	<div class="card card--white">
        <?= $this->Cell('Bootgrid.Bootgrid', [
            [
                [
                    'column-id' => 'Roles__id',
                    'formatter' => null,
                    'visible' => false,
                    'header' => __('Id')
                ],
                [
                    'column-id' => 'Roles__name',
                    'formatter' => null,
                    'visible' => true,
                    'header' => __('Name')
                ],
            ]
        ]);
        ?>
    </div>
</section>
