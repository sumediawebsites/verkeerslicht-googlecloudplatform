<header class="page__header">
	<h1 class="h3"><?= __( 'Melders' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuwe Melder'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
            <?= $this->Cell( 'Bootgrid.Bootgrid', [
                [
                        [
                                'column-id' => 'id',
                'visible' => false,
                                                'type' => 'integer',
                                    'header' => __('Id'),
            ],
                        [
                                'column-id' => 'Reporters__initials',
                                                'type' => 'string',
                                    'header' => __('Initials'),
            ],
                        [
                                'column-id' => 'Reporters__firstname',
                                                'type' => 'string',
                                    'header' => __('Firstname'),
            ],
                        [
                                'column-id' => 'Reporters__middlename',
                                                'type' => 'string',
                                    'header' => __('Middlename'),
            ],
                        [
                                'column-id' => 'Reporters__lastname',
                                                'type' => 'string',
                                    'header' => __('Lastname'),
            ],
                        [
                                'column-id' => 'Reporters__username',
                                                'type' => 'string',
                                    'header' => __('Username'),
            ],
                        [
                                'column-id' => 'Reporters__phone',
                                                'type' => 'string',
                                    'header' => __('Phone'),
            ],
                        [
                                'column-id' => 'Reporters__email',
                                                'type' => 'string',
                                    'header' => __('Email'),
            ],
                        [
                                'column-id' => 'Reporters__picture',
                                                'type' => 'string',
                                    'header' => __('Picture'),
            ],
                        [
                                'column-id' => 'Reporters__is_active',
                                                'type' => 'boolean',
                                    'formatter' => 'boolean',
                        'header' => __('Is Active'),
            ],
                        [
                                    'column-id' => 'CreatedBy__id',
                    'filter_field' => 'created_by',
                    'input' => 'select',
                    'values' => $createdBy,
                                            'type' => 'integer',
                                    'header' => __('Created By'),
            ],
                        [
                                    'column-id' => 'ModifiedBy__id',
                    'filter_field' => 'modified_by',
                    'input' => 'select',
                    'values' => $modifiedBy,
                                            'type' => 'integer',
                                    'header' => __('Modified By'),
            ],
                        [
                                'column-id' => 'Reporters__created_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Created At'),
            ],
                        [
                                'column-id' => 'Reporters__modified_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Modified At'),
            ],
                        [
                                'column-id' => 'Reporters__deleted',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Deleted'),
            ],

                ]]);?>
        </div>
</section>
