<header class="page__header">
	<h1 class="h3"><?= __( 'Melder Toevoegen' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($reporter) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Add Reporter') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
        echo $this->element('AdminBootstrap.input_field', ['field' => 'initials', 'label' => __('Initials')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'firstname', 'label' => __('Firstname')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'middlename', 'label' => __('Middlename')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'lastname', 'label' => __('Lastname')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'username', 'label' => __('Username')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'phone', 'label' => __('Phone')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'email', 'label' => __('Email')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'password', 'label' => __('Password')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'settings', 'label' => __('Settings')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'picture', 'label' => __('Picture')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'is_active', 'label' => __('Is Active')]);
        ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
