<header class="page__header">
	<h1 class="h3"><?= __( 'Kennisbankvraag Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Form->postLink(
                __('Verwijderen'),
                ['action' => 'delete', $faqQuestion->id],
                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $faqQuestion->id)]
            )
        ?></li>
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($faqQuestion) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Faq Question') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'faq_category_id', 'label' => __('Faq Category Id'), 'options' => $faqCategories, 'empty' => true]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'title', 'label' => __('Title')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'body', 'label' => __('Body')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'highlighted', 'label' => __('Highlighted'), 'type' => 'checkbox' ]);
        ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
