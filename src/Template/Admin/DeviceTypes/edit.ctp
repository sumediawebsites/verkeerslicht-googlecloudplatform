<header class="page__header">
	<h1 class="h3"><?= __( 'VRI-type Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li>
			<?= $this->Form->postLink(
	                __('Verwijderen'),
	                ['action' => 'delete', $deviceType->id],
	                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $deviceType->id)]
	            )
	        ?>
		</li>
	    <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
	</ul>
</header>

<section class="page__section">
    <?= $this->Form->create($deviceType) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Device Type') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
        echo $this->element('AdminBootstrap.input_field', ['field' => 'name', 'label' => __('Name')]);
        ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
