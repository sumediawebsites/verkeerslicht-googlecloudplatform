<nav class="col-lg-3 col-xs-4 columns" id="actions-sidebar">
    <?= $this->Cell( 'Bootgrid.FilterSidebar', [ $this->AuthUser->id() ] ); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title open"><?= __('Actions') ?>
                <span class="pull-right panel-toggle" id="actions-toggle">
				<i class="fa fa-arrow-down"></i>
			</span></h4>
        </div>
    <ul class="list-group">
        <li class="list-group-item"><?= $this->Html->link(__('New Notification Template'), ['action' => 'add']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
    </div>
</nav>
<div class="notificationTemplates index col-lg-9 col-xs-8 columns content">
    <div class="panel panel-default">
        <div class="panel-heading">
    <h3 class="panel-title"><?= __('Notification Templates') ?></h3></div>
        <div class="panel-body">
            <?= $this->Cell( 'Bootgrid.Bootgrid', [
                [
                        [
                                'column-id' => 'id',
                'visible' => false,
                                    'type' => 'integer',
                        'header' => __('Id'),
            ],
                        [
                                'column-id' => 'NotificationTemplates__name',
                                    'type' => 'string',
                        'header' => __('Name'),
            ],
                        [
                                'column-id' => 'NotificationTemplates__template',
                                    'type' => 'string',
                        'header' => __('Template'),
            ],
                        [
                                'column-id' => 'NotificationTemplates__title',
                                    'type' => 'string',
                        'header' => __('Title'),
            ],
                        [
                                'column-id' => 'NotificationTemplates__created_by',
                                    'type' => 'integer',
                        'header' => __('Created By'),
            ],
                        [
                                'column-id' => 'NotificationTemplates__modified_by',
                                    'type' => 'integer',
                        'header' => __('Modified By'),
            ],
                        [
                                'column-id' => 'NotificationTemplates__created_at',
                                    'type' => 'datetime',
                        'formatter' => 'datetime',
                        'header' => __('Created At'),
            ],
                        [
                                'column-id' => 'NotificationTemplates__modified_at',
                                    'type' => 'datetime',
                        'formatter' => 'datetime',
                        'header' => __('Modified At'),
            ],
            
                ]]);?>
        </div>
        </div>
</div>
