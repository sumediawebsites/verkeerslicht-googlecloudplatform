<nav class="col-lg-3 col-xs-4 columns" id="actions-sidebar">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title open"><?= __( 'Actions' ) ?>
				<span class="pull-right panel-toggle" id="actions-toggle">
				<i class="fa fa-arrow-down"></i>
			</span></h4>
		</div>
		<ul class="list-group">
			<li class="list-group-item"><?= $this->Form->postLink(
					__( 'Delete' ),
					[ 'action' => 'delete', $notificationTemplate->id ],
					[ 'confirm' => __( 'Are you sure you want to delete # {0}?', $notificationTemplate->id ) ]
				)
				?></li>
			<li class="list-group-item"><?= $this->Html->link( __( 'List Notification Templates' ), [ 'action' => 'index' ] ) ?></li>
			<li class="list-group-item"><?= $this->Html->link( __( 'List Users' ), [ 'controller' => 'Users', 'action' => 'index' ] ) ?></li>
			<li class="list-group-item"><?= $this->Html->link( __( 'New User' ), [ 'controller' => 'Users', 'action' => 'add' ] ) ?></li>
		</ul>
	</div>
</nav>
<div class="notificationTemplates form col-lg-9 col-xs-8 columns content">
	<?= $this->Form->create( $notificationTemplate ) ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title"><?= __( 'Edit Notification Template' ) ?></h4>
		</div>
		<div class="panel-body">
			<?php
			echo $this->element( 'AdminBootstrap.input_field', [ 'field' => 'name', 'label' => __( 'Name' ) ] );
			echo $this->element( 'AdminBootstrap.input_field', [ 'field' => 'template', 'label' => __( 'Template' ) ] );
			echo $this->element( 'AdminBootstrap.input_field', [ 'field' => 'title', 'label' => __( 'Title' ) ] );
			echo $this->element( 'AdminBootstrap.input_field', [ 'field' => 'body', 'label' => __( 'Body' ) ] );
			?>
		</div>
		<div class="panel-footer">
			<?= $this->Form->button( __( 'Submit' ) ) ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>
