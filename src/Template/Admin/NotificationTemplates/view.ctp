<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Notification Template'), ['action' => 'edit', $notificationTemplate->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Notification Template'), ['action' => 'delete', $notificationTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notificationTemplate->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Notification Templates'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Notification Template'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="notificationTemplates view large-9 medium-8 columns content">
    <h3><?= h($notificationTemplate->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($notificationTemplate->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Template') ?></th>
            <td><?= h($notificationTemplate->template) ?></td>
        </tr>
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($notificationTemplate->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($notificationTemplate->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created By') ?></th>
            <td><?= $this->Number->format($notificationTemplate->created_by) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($notificationTemplate->modified_by) ?></td>
        </tr>
        <tr>
            <th><?= __('Created At') ?></th>
            <td><?= h($notificationTemplate->created_at) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified At') ?></th>
            <td><?= h($notificationTemplate->modified_at) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Body') ?></h4>
        <?= $this->Text->autoParagraph(h($notificationTemplate->body)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($notificationTemplate->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Role Id') ?></th>
                <th><?= __('Buddy Id') ?></th>
                <th><?= __('Initials') ?></th>
                <th><?= __('Firstname') ?></th>
                <th><?= __('Insertion') ?></th>
                <th><?= __('Lastname') ?></th>
                <th><?= __('Username') ?></th>
                <th><?= __('Phone') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Password') ?></th>
                <th><?= __('Settings') ?></th>
                <th><?= __('Check Quotes') ?></th>
                <th><?= __('Created By') ?></th>
                <th><?= __('Modified By') ?></th>
                <th><?= __('Created At') ?></th>
                <th><?= __('Modified At') ?></th>
                <th><?= __('Is Active') ?></th>
                <th><?= __('Picture') ?></th>
                <th><?= __('Deleted') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($notificationTemplate->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->role_id) ?></td>
                <td><?= h($users->buddy_id) ?></td>
                <td><?= h($users->initials) ?></td>
                <td><?= h($users->firstname) ?></td>
                <td><?= h($users->insertion) ?></td>
                <td><?= h($users->lastname) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->phone) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->settings) ?></td>
                <td><?= h($users->check_quotes) ?></td>
                <td><?= h($users->created_by) ?></td>
                <td><?= h($users->modified_by) ?></td>
                <td><?= h($users->created_at) ?></td>
                <td><?= h($users->modified_at) ?></td>
                <td><?= h($users->is_active) ?></td>
                <td><?= h($users->picture) ?></td>
                <td><?= h($users->deleted) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
