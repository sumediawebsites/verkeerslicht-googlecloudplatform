<nav class="col-lg-3 col-xs-4 columns" id="actions-sidebar">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title open"><?= __('Actions') ?>
                <span class="pull-right panel-toggle" id="actions-toggle">
				<i class="fa fa-arrow-down"></i>
			</span></h4>
            </div>
<ul class="list-group">
        <li class="list-group-item"><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $usersNotificationTemplate->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $usersNotificationTemplate->id)]
            )
        ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('List Users Notification Templates'), ['action' => 'index']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('List Notification Templates'), ['controller' => 'NotificationTemplates', 'action' => 'index']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('New Notification Template'), ['controller' => 'NotificationTemplates', 'action' => 'add']) ?></li>
    </ul>
	    </div>
</nav>
<div class="usersNotificationTemplates form col-lg-9 col-xs-8 columns content">
    <?= $this->Form->create($usersNotificationTemplate) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Users Notification Template') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'user_id', 'label' => __('User Id'), 'options' => $users, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'notification_template_id', 'label' => __('Notification Template Id'), 'options' => $notificationTemplates, 'empty' => true]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'show_notification', 'label' => __('Show Notification')]);
                ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</div>
