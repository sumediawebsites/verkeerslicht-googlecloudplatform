<nav class="col-lg-3 col-xs-4 columns" id="actions-sidebar">
    <?= $this->Cell( 'Bootgrid.FilterSidebar', [ $this->AuthUser->id() ] ); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title open"><?= __('Actions') ?>
                <span class="pull-right panel-toggle" id="actions-toggle">
				<i class="fa fa-arrow-down"></i>
			</span></h4>
        </div>
    <ul class="list-group">
        <li class="list-group-item"><?= $this->Html->link(__('New Users Notification Template'), ['action' => 'add']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('List Notification Templates'), ['controller' => 'NotificationTemplates', 'action' => 'index']) ?></li>
        <li class="list-group-item"><?= $this->Html->link(__('New Notification Template'), ['controller' => 'NotificationTemplates', 'action' => 'add']) ?></li>
    </ul>
    </div>
</nav>
<div class="usersNotificationTemplates index col-lg-9 col-xs-8 columns content">
    <div class="panel panel-default">
        <div class="panel-heading">
    <h3 class="panel-title"><?= __('Users Notification Templates') ?></h3></div>
        <div class="panel-body">
            <?= $this->Cell( 'Bootgrid.Bootgrid', [
                [
                        [
                                'column-id' => 'id',
                'visible' => false,
                                    'type' => 'integer',
                        'header' => __('Id'),
            ],
                        [
                                    'column-id' => 'Users__fullname',
                    'filter_field' => 'user_id',
                    'input' => 'select',
                    'values' => $users,
                                'type' => 'integer',
                        'header' => __('User Id'),
            ],
                        [
                                    'column-id' => 'NotificationTemplates__name',
                    'filter_field' => 'notification_template_id',
                    'input' => 'select',
                    'values' => $notificationTemplates,
                                'type' => 'integer',
                        'header' => __('Notification Template Id'),
            ],
                        [
                                'column-id' => 'UsersNotificationTemplates__show_notification',
                                    'type' => 'boolean',
                        'formatter' => 'boolean',
                        'header' => __('Show Notification'),
            ],
            
                ]]);?>
        </div>
        </div>
</div>
