<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Users Notification Template'), ['action' => 'edit', $usersNotificationTemplate->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Notification Template'), ['action' => 'delete', $usersNotificationTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersNotificationTemplate->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Notification Templates'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Notification Template'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Notification Templates'), ['controller' => 'NotificationTemplates', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Notification Template'), ['controller' => 'NotificationTemplates', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersNotificationTemplates view large-9 medium-8 columns content">
    <h3><?= h($usersNotificationTemplate->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $usersNotificationTemplate->has('user') ? $this->Html->link($usersNotificationTemplate->user->fullname, ['controller' => 'Users', 'action' => 'view', $usersNotificationTemplate->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Notification Template') ?></th>
            <td><?= $usersNotificationTemplate->has('notification_template') ? $this->Html->link($usersNotificationTemplate->notification_template->name, ['controller' => 'NotificationTemplates', 'action' => 'view', $usersNotificationTemplate->notification_template->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersNotificationTemplate->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Show Notification') ?></th>
            <td><?= $usersNotificationTemplate->show_notification ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
