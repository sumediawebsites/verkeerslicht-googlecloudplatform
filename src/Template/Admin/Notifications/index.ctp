<div class="notifications index col-xs-12 content">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?= __( 'Notifications' ) ?></h3>
		</div>
		<div class="panel-body">
			<?= $this->Form->postLink( __( 'Mark all as read' ), [ 'action' => 'mark_all_read' ], [
				'class'   => 'btn btn-primary',
				'confirm' => __( 'Are you sure you want to mark all notifications as read?' )
			] ); ?>
			<?= $this->Html->link( __( 'Delete all notifications' ), [ 'action' => 'delete_all' ], [
				'class'   => 'btn btn-danger',
				'confirm' => __( 'Are you sure you want to delete all notifications?' )
			] ); ?>
		</div>
		<?php if ( count($notifications) > 0 ): ?>
			<table class="table table-striped">
				<thead>
				<tr>
					<th><?= __( 'Notification' ) ?></th>
					<th><?= __( 'Created' ) ?></th>
					<th class="actions"><?= __( 'Actions' ) ?></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ( $notifications as $notification ): ?>
					<tr class="<?= $notification->state == 1 ? "unread" : "read"; ?>">
						<td><?= h( $notification->title ) ?></td>
						<td><?= h( $notification->created ) ?></td>
						<td class="actions" rowspan="2">
							<?php if ( $notification->state == 1 ): ?>
								<?= $this->Html->link( __( 'Mark as read' ), [
									'action' => 'mark_as_read',
									$notification->id
								] ) ?><br/>
							<?php endif; ?>
							<?= $this->Html->link( __( 'Delete' ), [ 'action' => 'delete', $notification->id ] ) ?>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="notificationBody">
							<?= $notification->body ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<div class="panel-body">
				<p><?= __( 'There are no notifications for you at this moment.' ); ?></p>
			</div>
		<?php endif; ?>
	</div>
</div>
