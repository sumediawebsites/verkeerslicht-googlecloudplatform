<header class="page__header">
	<h1 class="h3"><?= __( 'VRI\'s' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuwe VRI'), ['controller' => 'Devices', 'action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
            <?= $this->Cell( 'Bootgrid.Bootgrid', [
                [
                        [
                                'column-id' => 'id',
                'visible' => false,
                                                'type' => 'integer',
                                    'header' => __('Id'),
            ],
                        [
                                    'column-id' => 'Locations__address',
                    'filter_field' => 'location_id',
                    'input' => 'select',
                    'values' => $locations,
                                            'type' => 'integer',
                                    'header' => __('Location Id'),
            ],
                        [
                                    'column-id' => 'Suppliers__name',
                    'filter_field' => 'supplier_id',
                    'input' => 'select',
                    'values' => $suppliers,
                                            'type' => 'integer',
                                    'header' => __('Supplier Id'),
            ],
                        [
                                    'column-id' => 'Users__username',
                    'filter_field' => 'user_id',
                    'input' => 'select',
                    'values' => $managers,
                                            'type' => 'integer',
                                    'header' => __('Manager Id'),
            ],
                        [
                                'column-id' => 'Devices__name',
                                                'type' => 'string',
                                    'header' => __('Name'),
            ],
                        [
                                    'column-id' => 'CreatedBy__fullname',
                    'filter_field' => 'created_by',
                    'input' => 'select',
                    'values' => $createdBy,
                                            'type' => 'integer',
                                            'formatter' => 'fullname',
                                    'header' => __('Created By'),
            ],
                        [
                                    'column-id' => 'ModifiedBy__id',
                    'filter_field' => 'modified_by',
                    'input' => 'select',
                    'values' => $modifiedBy,
                                            'type' => 'integer',
                                    'header' => __('Modified By'),
            ],
                        [
                                'column-id' => 'Devices__created_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Created At'),
            ],
                        [
                                'column-id' => 'Devices__modified_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Modified At'),
            ],
                        [
                                'column-id' => 'Devices__deleted',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Deleted'),
            ],

                ]]);?>
        </div>
</section>
