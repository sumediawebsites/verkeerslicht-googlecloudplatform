<header class="page__header">
	<h1 class="h3"><?= __( 'VRI Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Form->postLink(
                __('Verwijderen'),
                ['action' => 'delete', $device->id],
                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $device->id)]
            )
        ?></li>
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($device) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Device') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'location_id', 'label' => __('Location Id'), 'options' => $locations, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'supplier_id', 'label' => __('Supplier Id'), 'options' => $suppliers, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'user_id', 'label' => __('Manager Id'), 'options' => $managers, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'name', 'label' => __('Name')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'disclaimer', 'label' => __('Disclaimer'), 'type' => 'wysiwyg']);
        ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
