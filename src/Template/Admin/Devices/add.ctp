<header class="page__header">
	<h1 class="h3"><?= __( 'VRI Toevoegen' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($device) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Add Device') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'location_id', 'label' => __('Location Id'), 'options' => $locations, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'supplier_id', 'label' => __('Supplier Id'), 'options' => $suppliers, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'user_id', 'label' => __('Manager Id'), 'options' => $managers, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'name', 'label' => __('Name')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'disclaimer', 'label' => __('Disclaimer'), 'type' => 'wysiwyg']);
        ?>
        </div>
        <input type="hidden" name="referer" value="<?=$referer?>">
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
