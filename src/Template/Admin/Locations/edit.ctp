<header class="page__header">
	<h1 class="h3"><?= __( 'Locatie Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Form->postLink(
                __('Verwijderen'),
                ['action' => 'delete', $location->id],
                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $location->id)]
            )
        ?></li>
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($location) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Location') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
        echo $this->element('AdminBootstrap.input_field', ['field' => 'address', 'label' => __('Address')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'council_id', 'label' => __('Council Id'), 'options' => $councils, 'empty' => true]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'place_id', 'label' => __('Place Id'), 'options' => $places, 'empty' => true]);
        ?>
        <div class="row form-group">
            <label class="control-label col-md-3" for="map"></label>           
            <div class="col-md-9">
                <div class="form-group map">
                    <div id="map">
                    <?php
                    $options = [
                      'zoom' => 6,
                      'type' => 'R',
                      'geolocate' => true,
                      'div' => ['id' => 'map_canvas'],
                      'map' => ['navOptions' => ['style' => 'SMALL'], 'typeOptions' => ['style' => 'HORIZONTAL_BAR', 'pos' => 'RIGHT_CENTER']]                   
                    ];
                    $map = $this->GoogleMap->map($options);
                    echo $map;
                    // Add marker for Center of Holland: 'lat' => 52.258107, 'lng' => 5.600592
                    $this->GoogleMap->addMarker(['lat' => $location->latitude, 'lng' => $location->longitude, 'title' => 'Some Title', 'content' => $location->address, 'icon' => $this->GoogleMap->iconSet('green', 'E'), 'draggable' => 'true']);
                    // Store the final JS in a HtmlHelper script block
                    $this->GoogleMap->finalize();
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        echo $this->element('AdminBootstrap.input_field', ['field' => 'latitude', 'label' => __('Latitude')]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'longitude', 'label' => __('Longitude')]);
        ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
<script type="text/javascript">
  function setLatLng(lat, lng) {
    $("input[name='latitude']").val(lat);
    $("input[name='longitude']").val(lng);
  }
</script>