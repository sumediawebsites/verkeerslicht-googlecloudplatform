<header class="page__header">
	<h1 class="h3"><?= __( 'Plaatsen' ) ?></h1>

    <ul class="page__header__controls">
      <li><?= $this->Html->link(__('Nieuwe Plaats'), ['action' => 'add']) ?></li>
  </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
            <?= $this->Cell( 'Bootgrid.Bootgrid', [
                [
                        [
                                'column-id' => 'id',
                'visible' => false,
                                                'type' => 'integer',
                                    'header' => __('Id'),
            ],
                        [
                                    'column-id' => 'Councils__name',
                    'filter_field' => 'council_id',
                    'input' => 'select',
                    'values' => $councils,
                                            'type' => 'integer',
                                    'header' => __('Council Id'),
            ],
                        [
                                'column-id' => 'Places__name',
                                                'type' => 'string',
                                    'header' => __('Name'),
            ],
                        [
                                    'column-id' => 'CreatedBy__id',
                    'filter_field' => 'created_by',
                    'input' => 'select',
                    'values' => $createdBy,
                                            'type' => 'integer',
                                    'header' => __('Created By'),
            ],
                        [
                                    'column-id' => 'ModifiedBy__id',
                    'filter_field' => 'modified_by',
                    'input' => 'select',
                    'values' => $modifiedBy,
                                            'type' => 'integer',
                                    'header' => __('Modified By'),
            ],
                        [
                                'column-id' => 'Places__created_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Created At'),
            ],
                        [
                                'column-id' => 'Places__modified_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Modified At'),
            ],
                        [
                                'column-id' => 'Places__deleted',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Deleted'),
            ],

                ]]);?>
        </div>
</section>
