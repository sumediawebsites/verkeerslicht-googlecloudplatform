<header class="page__header">
	<h1 class="h3"><?= __( 'Meldingtype Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Form->postLink(
                __('Verwijderen'),
                ['action' => 'delete', $reportType->id],
                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $reportType->id)]
            )
        ?></li>
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($reportType) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Report Type') ?></h4>
	    </div>

	    <div class="panel-body">
        	<?php
        		echo $this->element('AdminBootstrap.input_field', ['field' => 'name', 'label' => __('Name')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'description', 'label' => __('Description')]);
        	?>
        </div>

        <div class="panel-footer">
    		<?= $this->Form->button(__('Submit')) ?>
	    </div>
    </div>
	<?= $this->Form->end() ?>
</section>
