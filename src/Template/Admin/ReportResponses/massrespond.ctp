<?php
/**
  * @var \App\View\AppView $this
  */
?>
<header class="page__header">
	<h1 class="h3"><?= __( 'Melderantwoord Massaal E-mailen' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Annuleren'), ['controller' => 'reports']) ?></li>
    </ul>
</header>

<section class="page__section">
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Respond to reports') ?></h4>
		    </div>
	    <div class="panel-body">

            <div class="row form-group">
                <label class="control-label col-md-3 ">Melders</label>
                <div class="col-md-9">
                    <ul></ul>
                    <?php foreach( $reports as $report){
                        echo "<li>".h($report->reporter->fullname)." (".h($report->reporter->email).")</li>";
                    }?>
                    </ul>
                </div>
            </div>

            <hr>

            <?= $this->element('AdminBootstrap.input_field', ['field' => 'reportresponsepreset', 'class' => 'preset_picker', 'label' => __('Preset'), 'options' => $reportResponsePresets, 'empty' => true]); ?>

            <?php
            echo $this->Form->create($reportResponse);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'subject', 'class' => 'subject_field', 'label' => __('Onderwerp')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'message', 'class' => 'CKE_message', 'label' => __('Bericht'), 'type' => 'wysiwyg']);
            ?>

            <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'change_report_status', 'class' => 'change_status', 'label' => __('Change report status'), 'type' => 'checkbox']);
            ?>
            <div class="status_picker_wrapper">
                <?= $this->element('AdminBootstrap.input_field', ['field' => 'report_status', 'class' => 'status_picker', 'label' => __('Report status'), 'options' => $reportStatuses, 'empty' => true]); ?>
            </div>

        </div>

        <div class="panel-footer">
            <?= $this->Form->button(__('Send')) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</section>

<script>
    $(document).ready(function () {
        $('.status_picker_wrapper').hide();
    });

    $('.change_status').change(function (e) {
        if ($(this).is(":checked")) {
            $('.status_picker_wrapper').show();
        } else {
            $('.status_picker_wrapper').hide();
        }
    });
</script>

<script>
    $('.preset_picker').change(function (e) {
        var presetId = $(this).val();
        var CKEInstance = $('.CKE_message').get(1).id;
        var subjectField = $('.subject_field');

        if (presetId !== '') {
            $.get('<?=$this->Url->build([
                    'controller' => 'ReportResponsePresets',
                    'action' => 'get',
                    'prefix' => 'api'
                ])?>/' + presetId)
                .done(function (data) {
                    var subjectText = data.reportResponsePreset.subject;
                    var messageText = data.reportResponsePreset.message;

                    CKEDITOR.instances[CKEInstance].setData(messageText);
                    subjectField.val(subjectText);
                });
        }
    });
</script>