<?php
/**
  * @var \App\View\AppView $this
  */
?>
<header class="page__header">
	<h1 class="h3"><?= __( 'Melderantwoord Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Form->postLink(
                __('Verwijderen'),
                ['action' => 'delete', $reportResponse->id],
                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $reportResponse->id)]
            )
        ?></li>
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($reportResponse) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Report Response') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'reporter_id', 'label' => __('Reporter Id'), 'options' => $reporters, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'user_id', 'label' => __('User Id'), 'options' => $users, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'device_id', 'label' => __('Device Id'), 'options' => $devices, 'empty' => true]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'subject', 'label' => __('Subject')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'message', 'label' => __('Message')]);
                ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>

	    </div>
    </div>
    <?= $this->Form->end() ?>
</section>
