<header class="page__header">
	<h1 class="h3"><?= __( 'Melderantwoorden' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuw Melderantwoord'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
            <?= $this->Cell( 'Bootgrid.Bootgrid', [
                [
                        [
                            'column-id' => 'id',
            'visible' => false,
                                                'type' => 'integer',
                                    'header' => __('Id'),
            ],
                        [
                            'column-id' => 'Reporters__username',
            'filter_field' => 'ReportResponses.reporter_id',
            'input' => 'select',
            'values' => $reporters,
                                            'type' => 'integer',
                                    'header' => __('Reporter Id'),
            ],
                        [
                            'column-id' => 'Users__username',
            'filter_field' => 'ReportResponses.user_id',
            'input' => 'select',
            'values' => $users,
                                            'type' => 'integer',
                                    'header' => __('User Id'),
            ],
                        [
                            'column-id' => 'Devices__name',
            'filter_field' => 'ReportResponses.device_id',
            'input' => 'select',
            'values' => $devices,
                                            'type' => 'integer',
                                    'header' => __('Device Id'),
            ],
                        [
                            'column-id' => 'ReportResponses__subject',
                                                'type' => 'string',
                                    'header' => __('Subject'),
            ],
                        [
                            'column-id' => 'ReportResponses__message',
                                                'type' => 'string',
                                    'header' => __('Message'),
            ],
                        [
                            'column-id' => 'ReportResponses__created_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Created At'),
            ],
                        [
                            'column-id' => 'ReportResponses__modified_at',
                                                'type' => 'date',
                                    'formatter' => 'datetime',
                        'header' => __('Modified At'),
            ],

                ]]);?>
        </div>
</section>
