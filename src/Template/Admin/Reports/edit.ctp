<header class="page__header">
	<h1 class="h3"><?= __( 'Melding Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Form->postLink(
                __('Verwijderen'),
                ['action' => 'delete', $report->id],
                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $report->id)]
            )
        ?></li>
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($report) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Report') ?></h4>
		    </div>
	    <div class="panel-body">
            <div class="suggested-device">
                <p><strong>VRI's in de buurt:</strong></p>
                <?php foreach( $suggestedDevices as $device ):?>
                    <div class="suggested-device-item">
                        <p>
                            <span><?=$device->name;?></span><br>
                            <span><?=$device->location->address;?></span><br>
                            <span>Latitude: <?=$device->location->latitude;?></span><br>
                            <span>Longitude: <?=$device->location->longitude;?></span><br>
                            <span>Afstand: <?=$device->distance;?> km</span><br>
                        </p>
                    </div>
                    <hr>
                <?php endforeach;?>
            </div>
        <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'device_id', 'label' => __('Device Id'), 'options' => $devices, 'empty' => true]);
            echo  $this->Html->link(
                'Nieuwe VRI toevoegen',
                ['controller' => 'Devices', 'action' => 'add' ],
                ['class' => 'btn btn--primary']
            );
            echo '<hr>';
            echo $this->element('AdminBootstrap.input_field', ['field' => 'report_status_id', 'label' => __('Report Status Id'), 'options' => $reportStatuses, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'reporter_id', 'label' => __('Reporter Id'), 'options' => $reporters, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'report_type_id', 'label' => __('Report Type Id'), 'options' => $reportTypes, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'body', 'label' => __('Body')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'latitude', 'label' => __('Latitude'), 'type' => 'decimal' ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'longitude', 'label' => __('Longitude'), 'type' => 'decimal' ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'vri_place', 'label' => __('Place') ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'vri_street_1', 'label' => __('Street 1') ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'vri_street_2', 'label' => __('Street 2') ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'date', 'label' => __('Date'), 'type' => 'date', 'value' => $report->date->format('d-m-Y') ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'time', 'label' => __('Time'), 'type' => 'time', 'value' => $report->time->format('H:i') ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'keep_me_informed', 'label' => __('Keep me informed'), 'type' => 'checkbox']);
        ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Html->link(
        'Beantwoord met email',
        ['controller' => 'ReportResponses', 'action' => 'respond', $report->id ],
        ['class' => 'btn btn--primary']
    );
    ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
