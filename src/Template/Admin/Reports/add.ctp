<header class="page__header">
	<h1 class="h3"><?= __( 'Melding Toevoegen' ) ?></h1>

    <ul class="page__header__controls">
	    <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
	</ul>
</header>

<section class="page__section">
    <?= $this->Form->create($report) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Add Report') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'device_id', 'label' => __('Device Id'), 'options' => $devices, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'report_status_id', 'label' => __('Report Status Id'), 'options' => $reportStatuses, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'reporter_id', 'label' => __('Reporter Id'), 'options' => $reporters, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'report_type_id', 'label' => __('Report Type Id'), 'options' => $reportTypes, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'body', 'label' => __('Body')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'latitude', 'label' => __('Latitude'), 'type' => 'decimal' ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'longitude', 'label' => __('Longitude'), 'type' => 'decimal' ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'vri_place', 'label' => __('Place') ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'vri_street_1', 'label' => __('Street 1') ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'vri_street_2', 'label' => __('Street 2') ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'date', 'label' => __('Date'), 'type' => 'date' ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'time', 'label' => __('Time'), 'type' => 'time' ]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'keep_me_informed', 'label' => __('Keep me informed'), 'type' => 'checkbox']);
        ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
