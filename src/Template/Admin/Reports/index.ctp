<header class="page__header">
	<h1 class="h3"><?= __( 'Meldingen' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuwe Melding'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
        <?= $this->Cell( 'Bootgrid.Bootgrid', [
			[
				[
					'column-id' => 'id',
					'visible' => false,
					'type' => 'integer',
					'header' => __('Id'),
				],
				[
					'column-id' => 'Devices__name',
					'filter_field' => 'device_id',
					'input' => 'select',
					'values' => $devices,
					'type' => 'integer',
					'header' => __('Device'),
				],
				[
					'column-id' => 'ReportStatuses__name',
					'filter_field' => 'report_status_id',
					'input' => 'select',
					'values' => $reportStatuses,
					'type' => 'integer',
					'header' => __('Report Status'),
				],
				[
					'column-id' => 'Reporters__username',
					'filter_field' => 'reporter_id',
					'input' => 'select',
					'values' => $reporters,
					'type' => 'integer',
					'header' => __('Reporter'),
				],
				[
					'column-id' => 'ReportTypes__name',
					'filter_field' => 'report_type_id',
					'input' => 'select',
					'values' => $reportTypes,
					'type' => 'integer',
					'header' => __('Report Type'),
				],
				[
					'column-id' => 'Reports__created_at',
					'type' => 'date',
					'formatter' => 'datetime',
					'header' => __('Created At'),
				],
				[
					'column-id' => 'Reports__modified_at',
					'type' => 'date',
					'formatter' => 'datetime',
					'header' => __('Modified At'),
				],
			],
			[
				'checkbox_collection_options' => [
					'show'      => true,
					'url'       => 'https://verkeerslicht.dev/admin/reportresponses/massrespond/',
					'link_text' => 'E-mail verzenden',
					'header'    => 'E-mail Verzendlijst'
				]
			]
        ] );?>
    </div>
</section>
