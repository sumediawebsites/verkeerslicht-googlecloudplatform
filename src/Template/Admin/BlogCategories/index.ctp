<header class="page__header">
	<h1 class="h3"><?= __( 'Blog Categorieën' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuwe Blogcategorie'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
        <?= $this->Cell( 'Bootgrid.Bootgrid', [
            [
                [
                    'column-id' => 'id',
                    'visible' => false,
                    'type' => 'integer',
                    'header' => __('Id'),
                ],
                [
                    'column-id' => 'ParentBlogCategories__name',
                    'filter_field' => 'parent_id',
                    'input' => 'select',
                    'values' => $parentBlogCategories,
                    'type' => 'integer',
                    'header' => __('Parent Id'),
                ],
                [
                    'column-id' => 'BlogCategories__name',
                    'type' => 'string',
                    'header' => __('Name'),
                ],
                [
                    'column-id' => 'CreatedBies__username',
                    'filter_field' => 'created_by',
                    'input' => 'select',
                    'values' => $createdBy,
                    'type' => 'integer',
                    'header' => __('Created By'),
                ],
                [
                    'column-id' => 'ModifiedBy__id',
                    'filter_field' => 'modified_by',
                    'input' => 'select',
                    'values' => $modifiedBy,
                    'type' => 'integer',
                    'header' => __('Modified By'),
                ],
                [
                    'column-id' => 'BlogCategories__created_at',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Created At'),
                ],
                [
                    'column-id' => 'BlogCategories__modified_at',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Modified At'),
                ],
                [
                    'column-id' => 'BlogCategories__deleted',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Deleted'),
                ],
            ]
        ] );?>
    </div>
</section>
