<header class="page__header">
	<h1 class="h3"><?= __( 'Blogcategorie Bewerken' ) ?></h1>

    <ul class="page__header__controls">
        <li>
			<?= $this->Form->postLink(
                __('Verwijderen'),
                ['action' => 'delete', $blogCategory->id],
                ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $blogCategory->id)]
            )?>
		</li>
        <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($blogCategory) ?>
    <div class="panel panel-default">
	    <div class="panel-heading">
		    <h4 class="panel-title"><?= __('Edit Blog Category') ?></h4>
		    </div>
	    <div class="panel-body">
        <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'parent_id', 'label' => __('Parent Id'), 'options' => $parentBlogCategories, 'empty' => true]);
        echo $this->element('AdminBootstrap.input_field', ['field' => 'name', 'label' => __('Name')]);
                echo $this->element('AdminBootstrap.input_field', ['field' => 'description', 'label' => __('Description')]);
        ?>
        </div>
        <div class="panel-footer">
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
	    </div>
    </div>
</section>
