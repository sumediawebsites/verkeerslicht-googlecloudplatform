<header class="page__header">
    <h1 class="h3"><?= __('Add report response preset') ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Annuleren'), ['action' => 'index']) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($reportResponsePreset) ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><?= __('Add Report Response Preset') ?></h4>
        </div>
        <div class="panel-body">
            <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'name', 'label' => __('Name')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'subject', 'label' => __('Subject')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'message', 'label' => __('Message'), 'type' => 'wysiwyg']);
            ?>
        </div>
        <div class="panel-footer">
            <?= $this->Form->button(__('Submit')) ?>

        </div>
    </div>
    <?= $this->Form->end() ?>
</section>
