<header class="page__header">
    <h1 class="h3"><?= __('Report response presets') ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('New preset'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
    <div class="card card--white">
        <?= $this->Cell('Bootgrid.Bootgrid', [
            [
                [
                    'column-id' => 'id',
                    'visible' => false,
                    'type' => 'integer',
                    'header' => __('Id'),
                ],
                [
                    'column-id' => 'ReportResponsePresets__name',
                    'type' => 'string',
                    'header' => __('Name'),
                ],
                [
                    'column-id' => 'ReportResponsePresets__subject',
                    'type' => 'string',
                    'header' => __('Subject'),
                ],
                [
                    'column-id' => 'CreatedBy__username',
                    'filter_field' => 'ReportResponsePresets.created_by',
                    'input' => 'select',
                    'values' => $createdBy,
                    'type' => 'integer',
                    'header' => __('Created By'),
                ],
                [
                    'column-id' => 'ModifiedBy__username',
                    'filter_field' => 'ReportResponsePresets.modified_by',
                    'input' => 'select',
                    'values' => $modifiedBy,
                    'type' => 'integer',
                    'header' => __('Modified By'),
                ],
                [
                    'column-id' => 'ReportResponsePresets__created_at',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Created At'),
                ],
                [
                    'column-id' => 'ReportResponsePresets__modified_at',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Modified At'),
                ],

            ]]); ?>
    </div>
</section>
