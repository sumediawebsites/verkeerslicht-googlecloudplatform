<header class="page__header">
	<h1 class="h3"><?= __( 'Blogartikel bewerken' ) ?></h1>

    <ul class="page__header__controls">
            <li><?= $this->Form->postLink(
                    __('Verwijderen'),
                    ['action' => 'delete', $blogArticle->id],
                    ['confirm' => __('Weet je zeker dat je het volgende wil verwijderen?: # {0}?', $blogArticle->id)]
                )
                ?></li>
            <li><?= $this->Html->link( __( 'Annuleren' ), [ 'action' => 'index' ] ) ?></li>
        </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($blogArticle, ['type' => 'file']) ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><?= __('Edit Blog Article') ?></h4>
        </div>
        <div class="panel-body">
            <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'blog_category_id', 'label' => __('Blog Category Id'), 'type' => 'selectpicker', 'options' => $blogCategories, 'empty' => true]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'title', 'label' => __('Title')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'slug', 'label' => __('Slug')]);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'body', 'label' => __('Body'), 'type' => 'wysiwyg']);
            $imageUrlBeforeValueHtml = '
							<div class="image-url-container">
								<div class="image-url">
									' . $this->Html->Image('/' . $blogArticle->image_url, [
                    'class' => 'preview',
                    'id' => 'image-url-image-image_url'
                ]) . '
								</div>
								<div class="image-url-actions">
									' .
                $this->Html->link(__('Verwijderen'), '#', [
                    'class' => 'remove-image-url',
                    'data-image-type' => 'image_url'
                ]) .
                $this->Form->hidden('delete_image_image_url', ['value' => '']);

            $imageUrlAfterValueHtml = '
								</div>
							</div>
						';
            echo $this->element( 'AdminBootstrap.input_field', [
                'field'           => 'image_url',
                'label'           => __( 'Image' ),
                'type'            => 'file',
                'beforeValueHtml' => $blogArticle->image_url != null ? $imageUrlBeforeValueHtml : '',
                'afterValueHtml'  => $blogArticle->image_url != null ? $imageUrlAfterValueHtml : ''
            ] );

            echo $this->element('AdminBootstrap.input_field', ['field' => 'read_time', 'label' => __('Read time'), 'type' => 'text']);
            echo $this->element('AdminBootstrap.input_field', ['field' => 'highlighted', 'label' => __('Highlighted'), 'type' => 'checkbox']);
            ?>
        </div>
        <div class="panel-footer">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $(".remove-image-url").click(function (event) {
            event.preventDefault();
            $("#image-url-image-" + $(this).data('image-type')).attr("src", "");
            $("input[name='delete_image_" + $(this).data('image-type') + "']").val("deleted");
        });
    });
</script>
