<header class="page__header">
	<h1 class="h3"><?= __( 'Blogartikelen' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuw Blogartikel'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
	<div class="card card--white">
        <?= $this->Cell( 'Bootgrid.Bootgrid', [
            [
                [
                    'column-id' => 'id',
                    'visible' => false,
                    'type' => 'integer',
                    'header' => __('Id'),
                ],
                [
                    'column-id' => 'BlogCategories__name',
                    'filter_field' => 'blog_category_id',
                    'input' => 'select',
                    'values' => $blogCategories,
                    'type' => 'integer',
                    'header' => __('Blog Category Id'),
                ],
                [
                    'column-id' => 'BlogArticles__title',
                    'type' => 'string',
                    'header' => __('Title'),
                ],
                [
                    'column-id' => 'BlogArticles__highlighted',
                    'type' => 'boolean',
                    'header' => __('Highlighted'),
                ],
                [
                    'column-id' => 'BlogArticles__slug',
                    'type' => 'string',
                    'header' => __('Slug'),
                ],
                [
                    'column-id' => 'CreatedBy__id',
                    'filter_field' => 'created_by',
                    'input' => 'select',
                    'values' => $createdBy,
                    'type' => 'integer',
                    'header' => __('Created By'),
                ],
                [
                    'column-id' => 'ModifiedBy__id',
                    'filter_field' => 'modified_by',
                    'input' => 'select',
                    'values' => $modifiedBy,
                    'type' => 'integer',
                    'header' => __('Modified By'),
                ],
                [
                    'column-id' => 'BlogArticles__created_at',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Created At'),
                ],
                [
                    'column-id' => 'BlogArticles__modified_at',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Modified At'),
                ],
                [
                    'column-id' => 'BlogArticles__deleted',
                    'type' => 'date',
                    'formatter' => 'datetime',
                    'header' => __('Deleted'),
                ],
                [
                    'column-id' => 'BlogArticles__image_url',
                    'type' => 'string',
                    'header' => __('Image URL'),
                ],
                [
                    'column-id' => 'BlogArticles__read_time',
                    'type' => 'string',
                    'header' => __('Read time'),
                ],
            ]
        ] );?>
    </div>
</section>
