<?php
/**
  * @var \App\View\AppView $this
  */
?>
<header class="page__header">
    <h1 class="h3"><?= __( 'Gesprekken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuw gesprek'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Gesprekken'), ['action' => 'index']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
    <div class="card card--white">

        <?php if ( count($chatMessages) > 0 ): ?>
            <table class="table table-striped">
                <tbody>
                <?php foreach ( $chatMessages as $message ): ?>
                    <tr>
                        <td colspan="2" class="notificationBody">
                            <?php if( $message->sender_id == $user->id):?>
                                <strong>Ik: </strong>
                            <?php else: ?>
                                <strong><?= $message->sender->fullname; ?>: </strong>
                            <?php endif; ?>
                            <?= $message->message ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p><?= __( 'There are no messages for you at this moment.' ); ?></p>
        <?php endif; ?>
        <?= $this->Form->create($chatMessage) ?>
        <?php
        echo $this->element('AdminBootstrap.input_field', ['field' => 'message', 'label' => false]);
        ?>
        </div>
        <div class="panel-footer">
            <?= $this->Form->button(__('Submit')) ?>
        </div>
        <?= $this->Form->end() ?>

    </div>
</section>
