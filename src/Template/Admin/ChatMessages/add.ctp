<header class="page__header">
    <h1 class="h3"><?= __( 'Gesprekken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuw gesprek'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Gesprekken'), ['action' => 'index']) ?></li>
    </ul>
</header>

<section class="page__section">
    <?= $this->Form->create($chatMessage) ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title"><?= __('Add Blog Category') ?></h4>
        </div>
        <div class="panel-body">
            <?php
            echo $this->element('AdminBootstrap.input_field', ['field' => 'receivers._ids', 'label' => __('Ontvanger'), 'options' => $users, 'empty' => true, 'type' => 'selectpicker', 'multiple' => true ]);
            if($userData['role_id'] == 1) {
                echo $this->element('AdminBootstrap.input_field', ['field' => 'all_managers', 'label' => __('Alle wegbeheerders'), 'type' => 'checkbox']);
            }
            echo $this->element('AdminBootstrap.input_field', ['field' => 'message', 'label' => __('Bericht')]);
            ?>
        </div>
        <div class="panel-footer">
            <?= $this->Form->button(__('Submit')) ?>

        </div>
        <?= $this->Form->end() ?>
    </div>
</section>
