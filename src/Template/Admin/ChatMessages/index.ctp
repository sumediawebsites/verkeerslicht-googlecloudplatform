<header class="page__header">
    <h1 class="h3"><?= __( 'Gesprekken' ) ?></h1>

    <ul class="page__header__controls">
        <li><?= $this->Html->link(__('Nieuw gesprek'), ['action' => 'add']) ?></li>
    </ul>
</header>

<section class="page__section section--table">
    <div class="card card--white">
        <?= $this->Cell( 'Bootgrid.Bootgrid', [
            [
                [
                    'column-id' => 'Senders__username',
                    'type'      => 'string',
                    'header'    => __( 'Zender' ),
                ],
                [
                    'column-id' => 'Receivers__username',
                    'type'      => 'string',
                    'header'    => __( 'Ontvanger' ),
                ],
                [
                    'column-id' => 'Receivers__picture',
                    'type'      => 'image',
                    'formatter' => 'image',
                    'header'    => __( 'Picture' ),
                ],
            ]
        ] ); ?>
    </div>
</section>
