<app-root>Loading...</app-root>
<script type="text/javascript" src="js/app/inline.bundle.js"></script>
<script type="text/javascript" src="js/app/vendor.bundle.js"></script>
<script type="text/javascript" src="js/app/main.bundle.js"></script>

<style>
    @media screen and (min-width: 768px) {
        .header {
            box-shadow: none;
        }
        
        main {
            padding-bottom: 0;
        }

        .footer {
            display: none;
        }
    }
</style>
