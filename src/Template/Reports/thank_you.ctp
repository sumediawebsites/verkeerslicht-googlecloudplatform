<div class="container">
    <div class="row">
        <div class="col-md-8 push-md-2">
            <section class="section--thanks">
                <div class="banner banner--thanks">
                    <header>
                        <h1 class="thanks__title">Bedankt voor je melding!</h1>

                        <h2 class="thanks__subtitle">Samen met Verkeerslicht.nl de wegen optimaliseren.</h2>
                    </header>
                </div>

                <footer>
                    <a href="/" class="btn btn--primary has-icon-left"><i class="icon-arrow-left"></i>Terug naar home</a>

                    <a href="/meldingen" class="btn btn--secondary">Bekijk andere meldingen<i class="icon-arrow-right"></i></a>
                </footer>
            </section>
        </div>
    </div>
</div>
