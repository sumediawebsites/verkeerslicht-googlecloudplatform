<div class="banner banner--hero banner--hero--default hidden-sm-down"></div>

<div class="container">
    <section class="page__section section--white section--first section--complete-report">
        <h1 class="h1">Melding afmaken</h1>

        <?= $this->Form->create($report, [ 'class' => 'form validate-form pageReport']) ?>
        <section class="form__section">
            <h3 class="h3">Locatie van het Verkeerslicht</h3>

            <fieldset id="latlonPickerDesktopCompleteReport" class="gllpLatlonPickerDesktopCompleteReport gllpLatlonPicker--use-location">
                <div class="search-wrapper">
                    <input id="searchInputDesktopCompleteReport" type="text" class="gllpSearchField form-control pac-input" placeholder="Zoek op locatie ... voor melding afmaken" />

                    <button id="searchButtonDesktopCompleteReport" type="button" class="gllpSearchButton btn btn--secondary">Zoeken<i class="icon-search"></i></button>

                    <button class="btn btn--default btn--locate hidden-md-down" alt="Gebruik mijn locatie" onclick="modalReportGetCurrentPosition('desktop'); return false;"><i class="icon-location"></i></button>
                </div>

                <div class="google-map"></div>

                <input type="hidden" class="gllpSearchField">
                <input type="button" class="gllpSearchButton" value="search" style="display:none;">
                <input type="hidden" class="gllpDevice" value="desktop"/>
                <?php echo $this->Form->input('latitude', [ 'class' => 'gllpLatitude', 'label' => false, 'type' => 'hidden' ]); ?>
                <?php echo $this->Form->input('longitude', [ 'class' => 'gllpLongitude', 'label' => false, 'type' => 'hidden' ]); ?>
                <input type="hidden" class="gllpZoom" value="12"/>
                <?php echo $this->Form->input('vri_place', [ 'class' => 'gllpLocationName', 'label' => false, 'type' => 'hidden' ]); ?>
                <input type="hidden" class="gllpElevation"/>
                <input type="button" class="gllpUpdateButton" value="update map" style="display:none;">
            </fieldset>

            <div class="form-group form-subsection hidden-sm-down">
                <div class="row">
                    <div class="col-md-6 col-md-p-left-less">
                        <div class="coordinate-wrapper">
                            <label for="latitude">Breedtegraad:</label>

                            <?php echo $this->Form->input('latitude', [ 'id' => 'complete_report_latitude', 'label' => false, 'disabled' => true, 'readonly' => true ]); ?>
                        </div>
                    </div>

                    <div class="col-md-6 col-md-p-right-less">
                        <div class="coordinate-wrapper">
                            <label for="longitude">Lengtegraad:</label>

                            <?php echo $this->Form->input('longitude', [ 'id' => 'complete_report_longitude', 'label' => false, 'disabled' => true, 'readonly' => true ]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group form-subsection">
                <?php echo $this->Form->input('vri_place', [ 'id' => 'complete_report_place', 'label' => false, 'placeholder' => 'Plaats', 'disabled' => true ]); ?>
            </div>

            <div class="form-group row">
                <div class="col-md-6 col-md-p-left-less">
                    <?php echo $this->Form->input('vri_street_1', ['id' => 'complete_report_streetname1', 'label' => false, 'placeholder' => 'Straatnaam 1', 'required' => true ]); ?>
                </div>

                <div class="col-md-6 col-md-p-right-less">
                    <?php echo $this->Form->input('vri_street_2', ['id' => 'complete_report_streetname2', 'label' => false, 'placeholder' => 'Straatnaam 2', 'required' => true ]); ?>
                </div>
            </div>
        </section>

        <section class="form__section">

            <div class="form-group row">
                <div class="col-md-6 col-md-p-left-less">
                    <input id="reporter_firstname" type="text" name="reporter_firstname" class="form-control" placeholder="Voornaam" required />
                </div>

                <div class="col-md-6 col-md-p-right-less">
                    <input id="reporter_lastname" type="text" name="reporter_lastname" class="form-control" placeholder="Achternaam" required />
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-p-left-less col-v-align-items">
                    <h3 class="h3">Ik was daar</h3>
                </div>

                <div class="col-md-6 col-md-p-right-less">
                    <div class="form-group">
                        <div class="select-wrapper">
                            <?php echo $this->Form->select('vehicle', ['car' => 'Met de auto','motorcycle' => 'Met de motor','scooter' => 'Met de scooter/brommer','moped' => 'Met de bromfiets', 'bicycle' => 'Met de fiets', 'feet' => 'Lopend'] , ['label' => false, 'required' => true ]); ?>
                            <!-- THE ABOVE RESULTS IN THIS:
                            <select name="vehicle" class="form-control form-control" required>
                                <option value="car" selected="selected">Met de auto</option>
                                <option value="motorcycle">Met de motor</option>
                                <option value="scooter">Met de scooter/brommer</option>
                                <option value="moped">Met de bromfiets</option>
                                <option value="bicycle">Met de fiets</option>
                                <option value="feet">Lopend</option>
                            </select>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="form__section">
            <h3 class="h3">Selecteer het probleem</h3>

            <div class="row">
                <div class="col-md-6 col-md-p-left-less">
                    <div class="form-group radio radio--problem radio--long-red">
                        <input type="radio" id="radio-long-red" name="report_type_id" value="2" required <?php if( $report->report_type_id == 2 ){ echo "checked"; } ?> />

                        <label for="radio-long-red"><i class="icon-slow"></i>Te lang rood</label>
                    </div>

                    <div class="col-md-6 col-md-p-left-less">
                        <div class="radio radio--problem radio--short-green">
                            <input type="radio" id="radio-short-green" name="report_type_id" value="3" <?php if( $report->report_type_id == 4 ){ echo "checked"; } ?> />

                            <label for="radio-short-green"><i class="icon-fast"></i>Te kort groen</label>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-md-p-right-less">
                    <div class="form-group radio radio--problem radio--unsafe">
                        <input type="radio" id="radio-unsafe" name="report_type_id" value="4" <?php if( $report->report_type_id == 4 ){ echo "checked"; } ?> />

                        <label for="radio-unsafe"><i class="icon-alert"></i>Onveilige situatie</label>
                    </div>

                    <div class="col-md-6 col-md-p-right-less">
                        <div class="radio radio--problem radio--other">
                            <input type="radio" id="radio-other" name="report_type_id" value="5" <?php if( $report->report_type_id == 4 ){ echo "checked"; } ?> />

                            <label for="radio-other"><i class="icon-cone"></i>Overig</label>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="form__section">
            <div class="row">
                <div class="col-md-6 col-md-p-left-less">
                    <h3 class="h3">Datum</h3>

                    <div class="form-group">
                        <input type="date" name="date" class="form-control timepicker" value="<?=$report->date->format('Y-m-d');?>">
                    </div>
                </div>

                <div class="col-md-6 col-md-p-right-less form-subsection subsection--time">
                    <h3 class="h3">Tijdstip</h3>

                    <div class="form-group">
                        <input type="time" name="time" class="form-control timepicker" value="<?=$report->time->format('H:i');?>">
                    </div>
                </div>
            </div>
        </section>

        <section class="form__section">
            <h3 class="h3">Opmerkingen</h3>

            <div class="form-group">
                <?php echo $this->Form->input('body', ['label' => false, 'placeholder' => 'Wil je verder nog iets kwijt?' ]); ?>
            </div>

            <label class="checkbox checkbox--text">
                <?php echo $this->Form->checkbox('keep_me_informed', ['label' => false ]); ?>

                <span>Ja, ik wil op de hoogte blijven van de verbeteringen van mijn melding.</span>
            </label>

            <div>
                <button type="submit" class="btn btn--secondary">Melding plaatsen<i class="icon-arrow-right"></i></button>
                <p><span id="status"></span></p>
            </div>
        </section>
        <?= $this->Form->end() ?>
    </section>
</div>

<script>
    function hasHtml5Validation () {
        return typeof document.createElement('input').checkValidity === 'function';
    }

    if (hasHtml5Validation()) {
        $('.validate-form').submit(function (e) {
            if (!this.checkValidity()) {
                e.preventDefault();
                $(this).addClass('invalid');
                $('#status').html('Sommige verplichte velden zijn nog niet ingevuld.');
            } else {
                $(this).removeClass('invalid');
                $('#status').html('Verstuurd');
            }
        });
    }
</script>
<script>
    document.getElementById('searchInputDesktopCompleteReport').onkeypress = function(e){
        if (!e) e = window.event;
        var keyCode = e.keyCode || e.which;
        if (keyCode == '13'){
        // Enter pressed
        document.getElementById('searchButtonDesktopCompleteReport').click();
            return false;
        }
    }
</script>

<style>
.invalid input:required:invalid {
    border: 1px solid #BE4C54;
}

.invalid input:required:valid {
    border: 1px solid #17D654 ;
}

input {
  display: block;
  margin-bottom: 10px;
}
</style>
