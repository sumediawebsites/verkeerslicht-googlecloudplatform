<?php

use Cake\Core\Configure;

/**
 * Default `html` block.
 */
if ( ! $this->fetch( 'html' ) ) {
	$this->start( 'html' );
	printf( '<html lang="%s" class="no-js">', Configure::read( 'App.language' ) );
	$this->end();
}

/**
 * Default `title` block.
 */
if ( ! $this->fetch( 'title' ) ) {
	$this->start( 'title' );
	echo Configure::read( 'App.title' );
	$this->end();
}

/**
 * Default `footer` block.
 */
if ( ! $this->fetch( 'tb_footer' ) ) {
	$this->start( 'tb_footer' );
	printf( '&copy;%s %s', date( 'Y' ), Configure::read( 'App.title' ) );
	$this->end();
}

/**
 * Default `body` block.
 */
$this->prepend( 'tb_body_attrs', ' class="' . implode( ' ', [
		$this->request->controller,
		$this->request->action
	] ) . '" ' );
if ( ! $this->fetch( 'tb_body_start' ) ) {
	$this->start( 'tb_body_start' );
	echo '<body' . $this->fetch( 'tb_body_attrs' ) . '>';
	$this->end();
}
/**
 * Default `flash` block.
 */
if ( ! $this->fetch( 'tb_flash' ) ) {
	$this->start( 'tb_flash' );
	if ( isset( $this->Flash ) ) {
		echo $this->Flash->render();
	}
	$this->end();
}
if ( ! $this->fetch( 'tb_body_end' ) ) {
	$this->start( 'tb_body_end' );
	echo '</body>';
	$this->end();
}

/**
 * Prepend `meta` block with `author` and `favicon`.
 */
$this->prepend( 'meta', $this->Html->meta( 'author', null, [
	'name'    => 'author',
	'content' => Configure::read( 'App.author' )
] ) );
$this->prepend( 'meta', $this->Html->meta( 'favicon.ico', '/favicon.ico', [ 'type' => 'icon' ] ) );

/**
 * Prepend `css` block with Bootstrap stylesheets and append
 * the `$html5Shim`.
 */
$html5Shim =
	<<<HTML

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
HTML;
$this->prepend( 'css', $this->Html->css( [
	'bootstrap/bootstrap',
	'Bootgrid.jquery.bootgrid.min',
	'Bootgrid.query-builder.default.min',
	'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css',
	'jquery.datetimepicker.min.css',
	"Notifier.dhtmlx",
	'admin',
	'/dist/styles/lms.min.css'
] ) );

$this->append( 'css', $html5Shim );

/**
 * Prepend `script` block with jQuery and Bootstrap scripts
 */
$this->prepend( 'script', $this->Html->script( [
		'https://use.fontawesome.com/9375559305.js',
        'https://code.jquery.com/jquery-2.2.4.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js'
] ) );
$this->prepend( 'scriptBottom', $this->Html->script( [
	'bootstrap/bootstrap',
	'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js',
	'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/i18n/defaults-nl_NL.min.js',
	'jquery.datetimepicker.full.min.js',
	'Bootgrid.query-builder.standalone.min',
	'Bootgrid.jquery.bootgrid',
	'Bootgrid.doT.min',
	'Bootgrid.jquery.bootgrid.fa.min',
	'Bootgrid.jQuery.extendext.min',
	'Bootgrid.moment-with-locales.min',
	'Bootgrid.sql-parser',
	"https://js.pusher.com/3.2/pusher.min.js",
	'Notifier.dhtmlx',
	'jquery.validate.min',
	'messages_nl',
	'additional-methods.min',
	'admin',
] ) );

?>
<!DOCTYPE html>
<?= $this->fetch( 'html' ) ?>

<head>
	<?= $this->Html->charset() ?>

	<title><?= $this->fetch( 'title' ) ?></title>

	<?= $this->fetch( 'meta' ) ?>
	<?= $this->fetch( 'css' ) ?>
	<?= $this->fetch( 'script' ); ?>
    <?php echo $this->CKEditor->loadJs(); ?>
    <!--< ?= $this->Cell( 'Notifier.Pusher', [ $_authUser['id'] ] ); ? >-->
</head>

<?php echo $this->fetch( 'tb_body_start' ); ?>

<!-- Top nav -->
<header class="header header--dark">
	<a href="/" target="_blank" class="header__logo"><img src="/assets/images/logos/logo_verkeerslicht.svg" /></a>

	<nav class="nav nav--top">
		<!-- Collect the nav links, forms, and other content for toggling -->
		<?= $this->Menu->render( 'user', [
			'branch_class' => 'has-sub',
			'matcher'      => new \Gourmet\KnpMenu\Menu\Matcher\Matcher( $this->request ),
			'renderer'     => '\App\Menu\Renderer\ListRenderer',
		] ); ?>
	</nav>
</header>

<main>
	<!-- Side nav -->
	<aside>
		<nav class="nav nav--aside">
			<?= $this->Menu->render( 'admin', [
				'branch_class' => 'has-sub',
				'matcher'      => new \Gourmet\KnpMenu\Menu\Matcher\Matcher( $this->request ),
				'renderer'     => '\App\Menu\Renderer\ListRenderer',
			] ); ?>
		</nav>
	</aside>

	<div class="page-wrapper">
	    <?php
        echo $this->fetch( 'tb_flash' );
        echo $this->Flash->render();

	    echo $this->fetch( 'content' );
	    ?>

		<footer class="footer">
			<section class="footer__section">
				<ul>
					<li><span>&copy; <?= date( 'Y' ); ?> Verkeerslicht.nl</span></li>
					<li><span>Versie 1.0</span></li>
					<li><a href="http://www.sumedia.nl/contact/" target="_blank">Rapporteer een probleem</a></li>
				</ul>

				<a href="http://www.sumedia.nl/" target="_blank" class="logo-developer">
		            <img src="/assets/images/logos/logo_sumedia.svg" />
		        </a>
			</section>
		</footer>
	</div>
</main>

<?php
echo $this->fetch( 'scriptBottom' );
echo $this->fetch( 'tb_body_end' );
?>

</html>
