<!DOCTYPE html>
<html class="login">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->Html->css('bootstrap/bootstrap.css'); ?>
    <?= $this->Html->css('base.css') ?>
    <!-- ?= $this->Html->css('cake.css') ? -->
    <?= $this->Html->css('admin.css') ?>

    <?= $this->fetch('script') ?>
    <?= $this->Html->script("https://code.jquery.com/jquery-latest.min.js"); ?>
    <?= $this->Html->script("bootstrap/bootstrap.min.js", ['block' => 'scriptBottom']); ?>

</head>
<body>
<div class="wrap" role="document">
    <div class="login-holder">
        <div class="content row">
            <div class="form-signin">
                <main class="col-lg-12">
                    <?= $this->Flash->render() ?>
                    <div class="clearfix">
                        <?= $this->fetch('content') ?>
                    </div>
                </main>
            </div>
        </div>
    </div>
    <div class="login-bg"></div>
</div>
<?= $this->fetch('scriptBottom'); ?>
</body>
</html>