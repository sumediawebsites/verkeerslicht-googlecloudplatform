<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// TODO: Move to a global place?

 /**
 * Get paths for assets
 */
 class JsonManifest {
     private $manifest;

     public function __construct($manifest_path) {
         if (file_exists($manifest_path)) {
             $this->manifest = json_decode(file_get_contents($manifest_path), true);
         } else {
             $this->manifest = [];
         }
     }

     public function get() {
         return $this->manifest;
     }

     public function getPath($key = '', $default = null) {
         $collection = $this->manifest;
         if (is_null($key)) {
             return $collection;
         }
         if (isset($collection[$key])) {
             return $collection[$key];
         }
         foreach (explode('.', $key) as $segment) {
             if (!isset($collection[$segment])) {
                 return $default;
             } else {
                 $collection = $collection[$segment];
             }
         }
         return $collection;
     }
 }

 function asset_path($filename) {
     $dist_path = '/dist/';
     $directory = dirname($filename) . '/';
     $file = basename($filename);
     static $manifest;

     if (empty($manifest)) {
         $manifest_path = WWW_ROOT . '/dist/' . 'assets.json';
         $manifest = new JsonManifest($manifest_path);
     }

     if (array_key_exists($file, $manifest->get())) {
         return $dist_path . $directory . $manifest->get()[$file];
     } else {
         return $dist_path . $directory . $file;
     }
 }

$cakeDescription = 'Verkeerslicht';
echo $this->Html->docType();
?>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title><?= $this->fetch('title') ?> | <?= $cakeDescription ?></title>
    <meta name="description" content="Verkeerslicht.nl | Samen werken aan een goede doorstroom." />

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#242e42">
    <meta name="theme-color" content="#ffffff">

    <?= $this->fetch('meta') ?>

    <?= $this->Html->css( $this->Assets->asset_path('styles/main.min.css') ) ?>

    <!-- MAKE SURE TO PLACE THESE REFERENCES IN THE HEAD, SO THEIR LIBRARIES WILL BE LOADED IN TIME -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjRwGkHxGFOsIJQctKgKrvFc5xXk_uk3A&libraries=visualization,places&language=nl-NL"></script>
    <script src="/js/app/assets/js/markerclusterer.js"></script>
    <script src="/js/app/assets/js/markermanager.js"></script>
    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <?= $this->fetch( 'script' ); ?>

    <?= $this->Html->script( [
		$this->Assets->asset_path('scripts/vendors-non-bower.min.js'),
        $this->Assets->asset_path('scripts/main.min.js'),
        "//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5890a97865768642"
	], [ 'block' => 'scriptBottom' ] ); ?>

</head>
<body<?= isset( $bodyclass ) ? ' class="' . $bodyclass . '"' : '' ?>>
    <nav class="nav-mobile">
        <?= $this->Cell( 'Navigation' ); ?>
		<?= $this->Cell( 'Badges' ); ?>
	</nav>

    <div class="page-wrapper">
	    <?= $this->Cell( 'Header' ); ?> <!-- NOTE: Header is Case-Sensitive -->

        <main>
            <?= $this->Flash->render(); ?>
            <?= $this->fetch( 'content' ); ?>
        </main>

        <!-- Report Modal -->
	    <?= $this->Cell( 'Report' ); ?> <!-- NOTE: Report is Case-Sensitive -->
        <!-- WARNING: Above Cell( 'Report' ) is not loaded... find out why! -->
        <?= $this->Cell( 'Footer' ); ?> <!-- NOTE: Footer is Case-Sensitive -->
    </div>

    <button data-remodal-target="modal-report" onclick="modalReportShowReportModal();" class="btn btn--secondary btn--footer hidden-lg-up">
        <div class="container">
            Plaats nu een melding<i class="icon-arrow-right"></i>
        </div>
    </button>                    

	<?= $this->fetch( 'scriptBottom' ); ?>

</body>
</html>