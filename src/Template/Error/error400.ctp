<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'default';

if (Configure::read('debug')):
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?= $this->element('auto_table_warning') ?>
<?php
    if (extension_loaded('xdebug')):
        xdebug_print_function_stack();
    endif;

    $this->end();
endif;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8 push-md-2">
            <section class="section--thanks">
                <div class="banner banner--thanks">
                    <header>
                        <h1 class="thanks__title">Oeps!</h1>
                        <h2 class="thanks__subtitle">
                            <?= __d('cake', 'De opgevraagde pagina {0} kan niet gevonden worden op de server.', "<strong>'{$url}'</strong>") ?>
                        </h2>
                    </header>
                </div>

                <footer>
                    <a href="/" class="btn btn--primary has-icon-left"><i class="icon-arrow-left"></i>Terug naar home</a>
                </footer>
            </section>
        </div>
    </div>
</div>
