<?= $this->assign('title', 'Kennisbank'); ?>

<div class="banner banner--hero banner--hero--faq hidden-sm-down"></div>

<div class="container">
    <a href="/" class="btn btn--back has-icon-left hidden-md-up"><i class="icon-arrow-left"></i>Terug</a>

    <nav class="breadcrumbs hidden-sm-down">
        <?php $this->Breadcrumbs->add([
                ['title' => 'Home', 'url' => [ 'controller' => 'Pages', 'action' => 'display', 'home', 'plugin' => 'Cms' ], 'class' => 'hidden-md-down'],
                ['title' => 'Kennisbank', 'url' => '/kennisbank' ]
            ]
        );?>
        <?= $this->Breadcrumbs->render(
            ['class' => '']
        );?>
    </nav>

    <section class="page__section section--white section--first section--faq">
        <header class="has-btn">
            <h1 class="h1">Kennisbank</h1>

            <a href="#faq-user-submit" class="btn btn--primary hidden-md-down">Stel zelf een vraag<i class="icon-arrow-right"></i></a>
        </header>

        <p class="hidden-md-up">Bekijk hier alle reeds geplaatste meldingen door heel Nederland!</p>

        <?php echo $this->Form->create( null, ['class' => 'hidden-sm-down']); ?>
            <div class="search-wrapper">
                <?php
                    echo $this->Form->input('q', [ 'templates' => [ 'inputContainer' => '{{content}}' ], 'label' => false, 'type' => 'search', 'placeholder' => 'Zoek in onze kennisbank' ] );

                    echo $this->Form->button('Zoeken<i class="icon-search"></i>', [ 'type' => 'submit', 'class' => 'btn btn--secondary']);

                    if ($isSearch) {
                        echo $this->Html->link('Reset', ['action' => 'index'], ['type' => 'submit', 'class' => 'btn btn--reset btn--primary'] );
                    }
                ?>
            </div>
        <?php echo $this->Form->end(); ?>

		<div class="faq__overview">
            <?php if ($isSearch): ?>
                <?php if ( count($faqQuestions) >= 1 ){ ?>
			        <h2 class="h3">Gerelateerde vragen</h2>
                <?php }else{ ?>
                    <p>We hebben geen vergelijkbare vragen kunnen vinden.</p>
                <?php }// endif ?>
            <?php else: ?>
                <h2 class="h3">Top <?= count($faqQuestions); ?> meest gestelde vragen</h2>
            <?php endif; ?>

            <div class="accordion">
                <?php $i = 0;?>
                <?php foreach ($faqQuestions as $faqQuestion): ?>
                    <?php $i++?>
                    <div class="ac">
                        <input id="ac-<?=$i?>" type="checkbox" name="acs" />

                        <label for="ac-<?=$i?>"><?= h($faqQuestion->title) ?></label>

                        <div class="ac-content">
                            <p><?= h($faqQuestion->body) ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div id="faq-user-submit" class="faq__user-submit">
            <h2 class="h3">Stel zelf een vraag</h2>

            <div class="user-submit-form">
                <?=$this->Form->create('faqUserQuestion', [ 'url' => [ 'controller' => 'FaqUserQuestions', 'action' => 'add' ], 'class' => 'form form--contact' ] ); ?>
                    <div class="form-group">
                        <div class="select-wrapper">
                            <?= $this->Form->input( 'faq_category_id', [ 'label' => false, 'type' => 'select', 'options' => $faqCategories, 'empty' => 'Kies een categorie']); ?>
                        </div>
                    </div>

                    <?= $this->Form->input( 'email', [ 'label' => false, 'type' => 'email', 'placeholder' => 'Je e-mailadres' ] ); ?>

                    <?= $this->Form->input( 'subject', [ 'label' => false, 'type' => 'text', 'placeholder' => 'Onderwerp' ] ); ?>

                    <div class="form-group textarea">
                        <?= $this->Form->textarea( 'message', [ 'label' => false, 'escape' => true, 'placeholder' => 'Waar kunnen we je mee helpen?' ] ); ?>
                    </div>

                    <?= $this->Form->button('Verzenden<i class="icon-arrow-right"></i>', [ 'type' => 'submit', 'class' => 'btn btn--primary']); ?>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
