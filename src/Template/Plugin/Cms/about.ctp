<?= $this->assign('title', 'Wat wij doen'); ?>

<div class="banner banner--hero banner--hero--about hidden-sm-down"></div>

<div class="container">
    <a href="/" class="btn btn--back has-icon-left hidden-md-up"><i class="icon-arrow-left"></i>Terug</a>

    <nav class="breadcrumbs hidden-sm-down">
        <?php $this->Breadcrumbs->add([
                ['title' => 'Home', 'url' => [ 'controller' => 'Pages', 'action' => 'display', 'home', 'plugin' => 'Cms' ] ],
                ['title' => 'Wat doet verkeerslicht.nl', 'url' => [ 'controller' => 'Pages', 'action' => 'display', 'wat-doet-verkeerslicht', 'plugin' => 'Cms' ] ]
            ]
        );?>
        <?= $this->Breadcrumbs->render(
            ['class' => '']
        );?>
    </nav>

	<section class="page__section section--white section--first section--first--what-we-do">
        <div class="row">
            <div class="col-md-8">
        		<h1 class="h1">{{Title}}</h1>

        		<h4 class="h4">{{Subtitle}}</h4>

				<ul class="usp-icons">
					<li>
						<i class="icon-download"></i>

						Op afstand<br />
						aanpasbaar
					</li>

					<li>
						<i class="icon-data"></i>

						Optimalisatie<br />
						& beheer
					</li>

					<li>
						<i class="icon-graph"></i>

						Zichtbare<br />
						verbeteringen
					</li>
				</ul>

				<button data-remodal-target="modal-report" onclick="modalReportShowReportModal();" class="btn btn--secondary hidden-md-down">{{BTN_CTA_report}}<i class="icon-arrow-right"></i></button>
            </div>
        </div>

		<div class="persona hidden-md-down">
			<div class="name-badge">
				<div class="name-badge__content">
                    {{Persoon_naam}}
					<span>{{Persoon_titel}}r</span>
				</div>
			</div>
		</div>
    </section>

	<section class="page__section section--white section--what-we-do--testimonials">
		<h2 class="h2">{{Testimonials_titel}}</h2>

		<ul class="testimonials">
			<li>
				<blockquote cite="http://developer.mozilla.org">
					{{Testimonial_1_text}}

					<footer>— <cite>{{Testimonial_1_person}} </cite></footer>
				</blockquote>

				<figure>
					<img src="/assets/images/testimonials/testimonial_martijn-van-de-laar.png" />
				</figure>
			</li>
			<li>
				<blockquote cite="http://developer.mozilla.org">
					{{Testimonial_2_text}}

					<footer>— <cite>{{Testimonial_2_person}} </cite></footer>
				</blockquote>

				<figure>
					<img src="/assets/images/testimonials/testimonial_marly-flint.png" />
				</figure>
			</li>
			<li>
				<blockquote cite="http://developer.mozilla.org">
					{{Testimonial_3_text}}

					<footer>— <cite>{{Testimonial_3_person}}</cite></footer>
				</blockquote>

				<figure>
					<img src="/assets/images/testimonials/testimonial_mark-vlieringa.png" />
				</figure>
			</li>
		</ul>
    </section>

    <section class="page__section section--white latest-reports hidden-md-up">
        <?= $this->Cell( 'ReportsLatest' ); ?>
    </section>

    <?= $this->Cell( 'ReportsCurrent' ); ?>
</div>
