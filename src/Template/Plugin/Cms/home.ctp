<?= $this->assign('title', 'Home'); ?>

<div class="banner banner--hero banner--hero--default"></div>

<div class="container">
	<section class="page__section section--white section--first section--first--home">
		<div class="row">
			<div class="col-md-6 col-xl-7">
				<h1 class="h1">{{Home_1_title}}</h1>

				<h4 class="h4">
                    {{Home_1_subtitle}}
				</h4>

				<button data-remodal-target="modal-report" onclick="modalReportShowReportModal();" class="btn btn--secondary btn--lg hidden-sm-down">{{Home_1_btn}}<i class="icon-arrow-right"></i></button>
			</div>

			<div class="app__demo hidden-sm-down">
				<div class="slick-slider">
				    <div><div class="image"><img src="/assets/images/home/home_phone_screen-1.jpg"></div></div>
				    <div><div class="image"><img src="/assets/images/home/home_phone_screen-2.jpg"></div></div>
				    <div><div class="image"><img src="/assets/images/home/home_phone_screen-3.jpg"></div></div>
				</div>

				<?= $this->Cell( 'Badges' ); ?> <!-- NOTE: Badges is Case-Sensitive -->
			</div>
		</div>
	</section>

	<a href="/wat-doet-verkeerslicht" class="btn btn--block btn--section hidden-md-up">{{Home_nav_about}}<i class="icon-arrow-right"></i></a>
	<a href="/meldingen" class="btn btn--block btn--section hidden-md-up">{{Home_nav_meldingen}}<i class="icon-arrow-right"></i></a>
	<a href="https://itunes.apple.com/nl/genre/ios/id36?mt=8" target="_blank" class="btn btn--block btn--section hidden-md-up">{{Home_nav_apps}}<i class="icon-arrow-right"></i></a>

	<a href="#how-it-works" class="btn btn--scroll hidden-lg-down">
		{{Home_btn_scroll}}<span class="text-supernova">.nl</span>

		<div class="scroll-animation">
			<div class="scroll-animation__down-icons">
				<span></span>
				<span></span>
				<span></span>
			</div>

			<div class="scroll-animation__mouse">
				<div class="mouse__scrollWheel"></div>
			</div>
		</div>
	</a>

	<section class="page__section section--white section--home--how-it-works" id="how-it-works">
		<div class="row">
			<div class="col-xl-6">
				<h2 class="h2 has-leading-zero">{{Home_2_title}}<span class="leading-zero hidden-sm-down">01</span></h2>

				<ol class="list list--leading-zero hidden-md-up">
					<li>
						<h4 class="h4">{{Home_2_mobile_subtitle_1}}</h4>
            			{{Home_2_mobile_text_1}}
					</li>

					<li>
						<h4 class="h4">{{Home_2_mobile_subtitle_2}}</h4>
        				{{Home_2_mobile_text_2}}
					</li>

					<li>
						<h4 class="h4">{{Home_2_mobile_subtitle_3}}</h4>
                        {{Home_2_mobile_text_3}}
					</li>
				</ol>

				<span class="hidden-sm-down">
                    {{Home_2_desktop_introtext}}
				</span>

				<a href="/wat-doet-verkeerslicht" class="btn btn--block btn--primary hidden-sm-up">Meer informatie<i class="icon-arrow-right"></i></svg></a>

				<div class="btn-link-combo hidden-sm-down">
					<a href="/apps" class="btn btn--primary">Ga naar de App<i class="icon-arrow-right"></i></svg></a>
					<p>of <a href="https://www.youtube.com/watch?v=hXjBS-Hr6qo" target="_blank">Verkeerslicht.nl in 90 sec<span class="btn-play"></span></a></p>
				</div>
			</div>

			<div class="col-lg-6 app-screens hidden-lg-down">
				<img src="/assets/images/home/home_app-screens.png" />
			</div>
		</div>
	</section>

	<?= $this->Cell( 'ReportsCurrent' ); ?> <!-- NOTE: ReportsCurrent is Case-Sensitive -->

    <?= $this->Cell( 'Blog' ); ?> <!-- NOTE: Blog is Case-Sensitive -->
</div>
