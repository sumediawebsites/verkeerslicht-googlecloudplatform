<?= $this->assign('title', 'Sitemap'); ?>

<div class="banner banner--hero banner--hero--about hidden-sm-down"></div>

<div class="container">
    <a href="/" class="btn btn--back has-icon-left hidden-md-up"><i class="icon-arrow-left"></i>Terug</a>

    <nav class="breadcrumbs hidden-sm-down">
        <?php $this->Breadcrumbs->add([
                ['title' => 'Home', 'url' => [ 'controller' => 'Pages', 'action' => 'display', 'home', 'plugin' => 'Cms' ] ],
                ['title' => 'Sitemap', 'url' => [ 'controller' => 'Pages', 'action' => 'display', 'sitemap', 'plugin' => 'Cms' ] ]
            ]
        );?>
        <?= $this->Breadcrumbs->render(
            ['class' => '']
        );?>
    </nav>

	<section class="page__section section--white section--first section--content-page">
        <article class="content-page">
            <div class="row">
                <div class="col-lg-9">
                    <header>
                		<h1 class="h1">Sitemap</h1>

                		<h4 class="h4">{{Subtitle}}</h4>
                    </header>

                    <div class="content-page__body">
                        {{Content}}
                    </div>
                </div>
            </div>
        </article>
    </section>
</div>
