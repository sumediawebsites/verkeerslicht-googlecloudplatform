<?= $this->assign('title', 'Apps'); ?>

<div class="banner banner--hero banner--hero--apps"></div>

<div class="container">
	<section class="page__section section--white section--first section--first--apps">
        <div class="row">
            <div class="col-md-6 col-xl-7">
        		<h1 class="h1">{{Apps_1_title}}</h1>

                {{Apps_1_text}}

                <?= $this->Cell( 'Badges' ); ?>
            </div>

			<img src="/assets/images/apps/apps_iphone-1.png" class="img-phone" />
        </div>
	</section>
</div>

<div class="banner banner--middle banner--middle--apps">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h2 class="h2">
                    {{Apps_banner_title}}
                </h2>
            </div>
        </div>
    </div>
</div>

<div class="container">
	<section class="page__section section--white section--apps--safe-reporting">
		<div class="row">
			<div class="col-md-6 col-xl-7">
				<h2 class="h2">{{Apps_2_title}}</h2>

                {{Apps_2_text}}

				<?= $this->Cell( 'Badges' ); ?>
			</div>

			<img src="/assets/images/apps/apps_iphone-2.png" class="img-phone" />
		</div>
	</section>
</div>
