<?= $this->assign('title', 'Contact'); ?>

<?php if ( isset( $this->Flash ) ) {
echo $this->Flash->render();
}?>

<div class="banner banner--hero banner--hero--contact hidden-sm-down"></div>

<div class="container">
    <a href="/" class="btn btn--back has-icon-left hidden-md-up"><i class="icon-arrow-left"></i>Terug</a>

    <nav class="breadcrumbs hidden-sm-down">
        <?php $this->Breadcrumbs->add([
                ['title' => 'Home', 'url' => [ 'controller' => 'Pages', 'action' => 'display', 'home', 'plugin' => 'Cms' ], 'class' => 'hidden-md-down'],
                ['title' => 'Contact', 'url' => [ 'controller' => 'Contact', 'action' => 'index' ]],
            ]
        );?>
        <?= $this->Breadcrumbs->render(
            ['class' => '']
        );?>
    </nav>

    <section class="page__section section--white section--first section--contact">
        <h1 class="h1">Contact<span class="hidden-md-down"> opnemen</span></h1>

        <?= $this->Flash->render('all');?>

        <div class="row">
            <div class="col-lg-3 push-lg-9">
                <div class="row">
                    <div class="col-xs-6 col-lg-12">
                        <address>{{Address}}</address>
                    </div>

                    <div class="col-xs-6 col-lg-12">
                        <ul class="contact-info">
                            <li><a href="tel:{{Phone}}">{{Phone}}</a></li>
                            <li><a href="mailto:{{Email}}">{{Email}}</a></li>
                            <li><span>{{KvK}}</span></li>
                        <ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-9 pull-lg-3">
                <?= $this->Form->create($contact, [ 'url' => [ 'action' => 'add' ],'class' => 'form form--contact' ] ); ?>
                    <div class="row">
                        <div class="col-md-6 col-md-p-left-less">
                            <?= $this->Form->input( 'firstname', [ 'label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Je voornaam' ] ); ?>
                        </div>

                        <div class="col-md-6 col-md-p-right-less">
                            <?= $this->Form->input( 'lastname', [ 'label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Je achternaam' ] ); ?>
                        </div>
                    </div>

                    <?= $this->Form->input( 'email', [ 'label' => false, 'type' => 'email', 'class' => 'form-control', 'placeholder' => 'Je e-mailadres' ] ); ?>

                    <?= $this->Form->input( 'subject', [ 'label' => false, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Onderwerp' ] ); ?>

                    <div class="form-group textarea">
                        <?= $this->Form->textarea( 'message', [ 'label' => false, 'escape' => true, 'class' => 'form-control', 'placeholder' => 'Bericht' ] ); ?>
                    </div>

                    <?= $this->Form->button('Verzenden<i class="icon-arrow-right"></i>', [ 'class' => 'btn btn--primary' ] ); ?>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </section>
</div>
