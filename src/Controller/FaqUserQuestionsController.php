<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FaqUserQuestions Controller
 *
 * @property \App\Model\Table\FaqUserQuestionsTable $FaqUserQuestions
 */
class FaqUserQuestionsController extends AppController
{

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faqUserQuestion = $this->FaqUserQuestions->newEntity();
        if ($this->request->is('post')) {
            $faqUserQuestion = $this->FaqUserQuestions->patchEntity($faqUserQuestion, $this->request->data);
            if ($this->FaqUserQuestions->save($faqUserQuestion)) {
                $faqUserQuestion->sendQuestionMail();
                $this->Flash->success(__('Je vraag is verzonden.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Je vraag kon niet verzonden worden. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['controller' => 'FaqQuestions' , 'action' => 'index']);
    }

}
