<?php
namespace App\Controller\Admin;

/**
 * Reporters Controller
 *
 * @property \App\Model\Table\ReportersTable $Reporters
 */
class ReportersController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'Reporters.initials',
			'Reporters.firstname',
			'Reporters.middlename',
			'Reporters.lastname',
			'Reporters.username',
			'Reporters.phone',
			'Reporters.email',
			'Reporters.password',
			'Reporters.settings',
			'Reporters.picture',
			'Reporters.is_active',
			'Reporters.created_by',
			'Reporters.modified_by',
			'Reporters.created_at',
			'Reporters.modified_at',
			'Reporters.deleted',
             ],
             'defaultSort'   => [ 'Reporters.id' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['CreatedBy', 'ModifiedBy']
        ];
        $reporters = $this->paginate($this->Reporters);

        $createdBy = $this->Reporters->CreatedBy->find('list');
        $modifiedBy = $this->Reporters->ModifiedBy->find('list');
        $this->set(compact('reporter', 'createdBy', 'modifiedBy'));

        $this->set(compact('reporters'));
        $this->set('_serialize', ['reporters']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reporter = $this->Reporters->newEntity();
        if ($this->request->is('post')) {
            $reporter = $this->Reporters->patchEntity($reporter, $this->request->data);
            if ($this->Reporters->save($reporter)) {
                $this->Flash->success(__('De melder is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De melder kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->Reporters->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Reporters->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reporter', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reporter']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Reporter id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reporter = $this->Reporters->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reporter = $this->Reporters->patchEntity($reporter, $this->request->data);
            if ($this->Reporters->save($reporter)) {
                $this->Flash->success(__('De melder is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De melder kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->Reporters->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Reporters->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reporter', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reporter']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Reporter id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $reporter = $this->Reporters->get($id);
        if ($this->Reporters->delete($reporter)) {
            $this->Flash->success(__('De melder is verwijderd.'));
        } else {
            $this->Flash->error(__('De melder kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
