<?php
namespace App\Controller\Admin;

/**
 * Provinces Controller
 *
 * @property \App\Model\Table\ProvincesTable $Provinces
 */
class ProvincesController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'Provinces.name',
			'Provinces.created_by',
			'Provinces.modified_by',
			'Provinces.created_at',
			'Provinces.modified_at',
			'Provinces.deleted',
             ],
             'defaultSort'   => [ 'Provinces.name' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['CreatedBy', 'ModifiedBy']
        ];
        $provinces = $this->paginate($this->Provinces);

        $createdBy = $this->Provinces->CreatedBy->find('list');
        $modifiedBy = $this->Provinces->ModifiedBy->find('list');
        $this->set(compact('province', 'createdBy', 'modifiedBy'));

        $this->set(compact('provinces'));
        $this->set('_serialize', ['provinces']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $province = $this->Provinces->newEntity();
        if ($this->request->is('post')) {
            $province = $this->Provinces->patchEntity($province, $this->request->data);
            if ($this->Provinces->save($province)) {
                $this->Flash->success(__('De provincie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De provincie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->Provinces->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Provinces->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('province', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['province']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Province id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $province = $this->Provinces->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $province = $this->Provinces->patchEntity($province, $this->request->data);
            if ($this->Provinces->save($province)) {
                $this->Flash->success(__('De provincie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De provincie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->Provinces->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Provinces->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('province', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['province']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Province id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $province = $this->Provinces->get($id);
        if ($this->Provinces->delete($province)) {
            $this->Flash->success(__('De provincie is verwijderd.'));
        } else {
            $this->Flash->error(__('De provincie kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
