<?php
namespace App\Controller\Admin;
use Cake\Event\Event;
use Notifier\Utility\NotificationManager;
use Cake\ORM\TableRegistry;
use Notifier\Utility\Pusher\PusherSdkClient;

/**
 * ChatMessages Controller
 *
 * @property \App\Model\Table\ChatMessagesTable $ChatMessages
 */
class ChatMessagesController extends AdminController
{
    public function initialize() {
        parent::initialize();

        $conditions = [
            'ChatMessages.parent_id IS NULL',
            'OR' => [
                [ 'ChatMessages.sender_id' => $this->Auth->user('id') ],
                [ 'ChatMessages.receiver_id' => $this->Auth->user('id') ]
            ]
        ];
        $this->loadComponent( 'Bootgrid.Bootgrid', [
               'entity'        => $this->modelClass,
               'contain'       => [
                   'ParentChatMessages',
                   'Receivers',
                   'Senders'
               ],
                'conditions' => $conditions,
                'order' => [
                    'ChatMessages.id' => 'DESC'
                ],
                'searchColumns' => [
                            'ChatMessages.message',
                            'ChatMessages.read',
                            'ChatMessages.created_by',
                            'ChatMessages.modified_by',
                            'ChatMessages.created_at',
                            'ChatMessages.modified_at',
                            'Senders.username',
                            'Receivers.username',
                  ],
                 'defaultSort'   => [ 'ChatMessages__id' => 'DESC' ]
               ]
       );
    }

	
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ( $this->request->is( 'ajax' ) ) {
            $this->Bootgrid->getData();
        } else {
            $this->paginate = [
                'contain' => [
                    'ParentChatMessages',
                    'Senders',
                    'Receivers'
                ],
                'order' => [
                    'id' => 'DESC'
                ]
            ];
            $chatMessages = $this->paginate($this->ChatMessages);
            $parentChatMessages = $this->ChatMessages->ParentChatMessages->find('list');
            $users = $this->ChatMessages->Receivers->find('list');
            $this->set(compact('chatMessage', 'parentChatMessages', 'users', 'roles'));

            $this->set(compact('chatMessages'));
            $this->set('_serialize', ['chatMessages']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $chatMessage = $this->ChatMessages->newEntity();

        $this->set('userData', $this->Auth->user());

        // Managers mogen alleen met Admins communiceren
        $conditions = [];
        if( $this->Auth->user('role_id') == 1 ){
            $conditions = [];
        } elseif( $this->Auth->user('role_id') == 2 ) {
            $conditions = [ 'role_id' => 1 ];
        }
        $this->loadModel('Users');
        $users = $this->Users->find('list', [
            'limit' => 200,
            'conditions' => [
                $conditions
            ]
        ]);
        // Strictly just the managers, just to be sure
        $managers = $this->Users->find('all', [
            'conditions' => [
                'role_id' => 2
            ]
        ])->toArray();

        if ($this->request->is('post')) {

            if( $this->request->data('all_managers') == 1 ){
                foreach ( $managers as $manager ){
                    $massMessage = $this->ChatMessages->newEntity();
                    $massMessage = $this->ChatMessages->patchEntity($massMessage, $this->request->data);
                    $massMessage->sender_id = $this->Auth->user('id');
                    $massMessage->receiver_id = $manager->id;
                    $this->ChatMessages->save($massMessage);
                    $receiver = $this->Users->get($massMessage->receiver_id);
                    $chatMessage->sendNotification($receiver);
                }
                return $this->redirect(['action' => 'index']);

            } elseif( is_array($this->request->data('receivers._ids')) ) {
                foreach ( $this->request->data('receivers._ids') as $receiverId ){
                    $massMessage = $this->ChatMessages->newEntity();
                    $massMessage = $this->ChatMessages->patchEntity($massMessage, $this->request->data);
                    $massMessage->sender_id = $this->Auth->user('id');
                    $massMessage->receiver_id = $receiverId;
                    $this->ChatMessages->save($massMessage);
                    $receiver = $this->Users->get($receiverId);
                    $chatMessage->sendNotification($receiver);
                }
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chat message could not be sent. Please, try again.'));
            }
        }
        $this->set(compact('chatMessage', 'parentChatMessages', 'users'));
        $this->set('_serialize', ['chatMessage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Chat Message id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Users');

        $users = $this->Users->find('list', ['limit' => 200]);
        $user = $this->Users->get($this->Auth->user('id'));
        $chatMessages = $this->ChatMessages->find('all', [
            'conditions' => [
                'parent_id' => $id
            ],
            'contain' => [
                'Senders',
                'Receivers'
            ]
        ])->toArray();

        $chatMessage = $this->ChatMessages->newEntity();
        $chatMessage->parent_id = $id;
        $parentChatMessage = $this->ChatMessages->get($chatMessage->parent_id, [
            'contain' => [
                'Senders',
                'Receivers'
            ]
        ]);
        array_unshift($chatMessages, $parentChatMessage);

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($parentChatMessage->sender_id == $this->Auth->user('id')) {
                $otherUser = $parentChatMessage->receiver_id;

            } elseif ($parentChatMessage->receiver_id == $this->Auth->user('id')) {
                $otherUser = $parentChatMessage->sender_id;
            } else {
                $otherUser = null;

                $this->Flash->error(__('The chat message could not be saved. Please, try again.'));
                return $this->redirect(['action' => 'edit', $id]);
            }

            $chatMessage = $this->ChatMessages->patchEntity($chatMessage, $this->request->data);
            $chatMessage->sender_id = $this->Auth->user('id');
            $chatMessage->receiver_id = $otherUser;

            if ($this->ChatMessages->save($chatMessage)) {
                $this->Flash->success(__('The chat message has been saved.'));

                $receiver = $this->Users->get($chatMessage->receiver_id);
                $sender = $this->Users->get($chatMessage->sender_id);
                $chatMessage->sendNotification( $sender, $receiver );

                return $this->redirect(['action' => 'edit', $id]);
            } else {
                $this->Flash->error(__('The chat message could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('chatMessage', 'parentChatMessages', 'user', 'users', 'chatMessages'));
        $this->set('_serialize', ['chatMessage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Chat Message id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $chatMessage = $this->ChatMessages->get($id);
        if ($this->ChatMessages->delete($chatMessage)) {
            $this->Flash->success(__('The chat message has been deleted.'));
        } else {
            $this->Flash->error(__('The chat message could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
