<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;
use Notifier\Utility\Pusher\PusherSdkClient;
use Aura\Intl\Exception;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AdminController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Bootgrid.Bootgrid', [
                'entity' => 'Users',
                'contain' => [
                    'Roles'
                ],
                'searchColumns' => [
                    'Users.firstname',
                    'Users.lastname',
                    'Users.username'
                ],
                'defaultSort' => ['Users__lastname' => 'ASC']
            ]
        );

        // THE BELOW CODE CAUSES THE PAGE TO RETURN 500 INTERNAL SERVER ERROR !!
        // We have now registered with Pusher,
        // See https://dashboard.pusher.com/apps/340514/getting_started
        //
        // The below settings are defined in app.yaml

        $this->pusherClient = new PusherSdkClient(
            // Configure::read('Pusher.appKey'),
            // Configure::read('Pusher.appSecret'),
            // Configure::read('Pusher.appId')
            // Get environment variables from app.yaml
            Configure::read('pusher_app_key'),
            Configure::read('pusher_app_secret'),
            Configure::read('pusher_app_id')
        );
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('pusherAuth');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ($this->request->is('ajax')) {
            $this->Bootgrid->getData();
        } else {
            $roles = $this->Users->Roles->find('list');
            $this->set(compact('roles'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     *
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function account()
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => [
                'NotificationTemplates'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data, ['associated' => ['NotificationTemplates._joinData']]);
            if ($this->Users->save($user, ['associated' => ['NotificationTemplates']])) {
                $this->Flash->success(__('Het account is gewijzigd.'));

                return $this->redirect(['action' => 'account']);
            } else {
                $this->Flash->error(__('Het wachtwoord kon niet worden gewijzigd. Probeer het a.u.b. nog eens.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->Users->addBehavior('Tools.Passwordable');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('De gebruiker is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De gebruiker kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }

            // Pwd should not be passed to the view again for security reasons
            $user->unsetProperty('pwd');
            $user->unsetProperty('pwd_repeat');
        }
        $activeUsers = $this->Users->getUsersWithRoles();
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles', 'activeUsers'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     *
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $user = $this->Users->get($id, [
            'contain' => [
                'NotificationTemplates'
            ]
        ]);

        // non-admins can only edit their own accounts
        if( $this->Auth->user('role_id') != 1 && $id != $this->Auth->user('id') ){
            $this->Flash->error(__('Je hebt geen rechten om deze pagina te bekijken.'));
            return $this->redirect([ 'controller' => 'Users', 'action' => 'dashboard']);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {


            if (isset($this->request->data['delete_picture']) && $this->request->data['delete_picture'] == 'deleted' && !isset($this->request->data['picture'])) {
                $user->picture = null;
            }

            if (!isset($this->request->data['delete_picture']) && empty($this->request->data['picture']['name'])) {
                $user->dirty('picture', false);
                unset($this->request->data['picture']);
            }

            $user = $this->Users->patchEntity($user, $this->request->data);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('De gebruiker is opgeslagen.'));
                $this->Auth->setUser($user);

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De gebruiker kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }

        $activeUsers = $this->Users->getUsersWithRoles();
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles', 'activeUsers'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
//        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('De gebruiker is verwijderd.'));
        } else {
            $this->Flash->error(__('De gebruiker kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        $this->viewBuilder()->layout('login');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $userModel = $this->Users->get($this->Auth->user('id'));

                $rolesTable = TableRegistry::get('roles');
                $currentUserRole = $rolesTable->get($user['role_id']);

                $this->request->session()->write('Rights', $currentUserRole->getRights());
                $this->request->session()->write('Settings', $userModel->settings);

                $this->redirect(array(
                    'admin' => true,
                    'controller' => 'users',
                    'action' => 'dashboard'));
            } else {
            $this->Flash->error('Je e-mailadres of wachtwoord is incorrect.');
            }
        }
    }

    public function logout()
    {
        $this->request->session()->destroy();
        $this->Flash->success('Je bent nu uitgelogd.');

        return $this->redirect($this->Auth->logout());
    }

    public function change_password($id = null)
    {
        $this->Users->addBehavior('Tools.Passwordable');

        if ($id == null) {
            $id = $this->Auth->user('id');
        }

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Het wachtwoord is gewijzigd.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Het wachtwoord kon niet worden gewijzigd. Probeer het a.u.b. nog eens.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function saveTableSettings()
    {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->data;
        $columnSettings = [];
        $columnSettings[$data['tableId']] = [];
        foreach (json_decode($data['columnSettings']) as $column) {
            if ($column->id != 'commands') {
                $columnSettings[$data['tableId']][$column->id]['visible'] = (bool)$column->visible;
            }
        }
        $currentUser = $this->Users->get($this->Auth->user('id'));
        $userSettings = $currentUser->settings;
        debug($userSettings);
        if ($userSettings == null || $userSettings == 'null') {
            $userSettings = [];
        } else {
            $userSettings = json_decode($userSettings, true);
        }
        $currentUser->settings = json_encode(array_merge($userSettings, $columnSettings));

        $this->request->session()->write('Settings', $currentUser->settings);

        echo (bool)$this->Users->save($currentUser);
        die();
    }

    public function pusherAuth()
    {

        $this->autoRender = false;
        $channelName = $this->request->data('channel_name');
        preg_match("/(private-user)-(\d*)/", $channelName, $output_array);

        if (count($output_array) == 3) {
            if ($output_array[2] == $this->Auth->user('id')) {
                $result = $this->pusherClient->authenticate(
                    $this->request->data('channel_name'),
                    $this->request->data('socket_id')
                );;

                $this->response->type('json');
                $this->response->body($result);
            } else {
                Throw new Exception(__("No access to this channel"));
            }
        } else {
            Throw new Exception(__("Invalid channel name"));
        }
    }

    public function dashboard()
    {
        if ($this->Auth->user('role_id') == 2) {
            $this->redirect(array(
                'admin' => true,
                'controller' => 'Users',
                'action' => 'managerDashboard'));

        } elseif($this->Auth->user('role_id') == 1) {
            $this->redirect(array(
                'admin' => true,
                'controller' => 'Users',
                'action' => 'adminDashboard'));
        } else {
            $this->redirect(array(
                'admin' => true,
                'controller' => 'reports',
                'action' => 'index'));
        }
    }
    public function managerDashboard()
    {
        $manager = $this->Users->get( $this->Auth->user('id'), [
            'contain' => [
                'Devices' => [
                    'Reports' => [
                        'ReportStatuses'
                    ]
                ]
            ]
        ]);

        $this->set(compact('manager'));
        $this->set('_serialize', ['manager']);
    }

    public function adminDashboard()
    {
        $user = $this->Users->get( $this->Auth->user('id'), [
            'contain' => [
                'Devices' => [
                    'Reports' => [
                        'ReportStatuses'
                    ]
                ]
            ]
        ]);

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

}
