<?php
namespace App\Controller\Admin;

/**
 * FaqQuestions Controller
 *
 * @property \App\Model\Table\FaqQuestionsTable $FaqQuestions
 */
class FaqQuestionsController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'FaqCategories', 'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'FaqQuestions.title',
			'FaqQuestions.body',
			'FaqQuestions.created_by',
			'FaqQuestions.modified_by',
			'FaqQuestions.created_at',
			'FaqQuestions.modified_at',
			'FaqQuestions.deleted',
		'FaqCategories.name',
              ],
             'defaultSort'   => [ 'FaqQuestions.title' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['FaqCategories', 'CreatedBy', 'ModifiedBy']
        ];
        $faqQuestions = $this->paginate($this->FaqQuestions);

        $faqCategories = $this->FaqQuestions->FaqCategories->find('list');
        $createdBy = $this->FaqQuestions->CreatedBy->find('list');
        $modifiedBy = $this->FaqQuestions->ModifiedBy->find('list');
        $this->set(compact('faqQuestion', 'faqCategories', 'createdBy', 'modifiedBy'));

        $this->set(compact('faqQuestions'));
        $this->set('_serialize', ['faqQuestions']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faqQuestion = $this->FaqQuestions->newEntity();
        if ($this->request->is('post')) {
            $faqQuestion = $this->FaqQuestions->patchEntity($faqQuestion, $this->request->data);
            if ($this->FaqQuestions->save($faqQuestion)) {
                $this->Flash->success(__('De kennisbankvraag is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De kennisbankvraag kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $faqCategories = $this->FaqQuestions->FaqCategories->find('list', ['limit' => 200]);
        $createdBy = $this->FaqQuestions->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->FaqQuestions->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('faqQuestion', 'faqCategories', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['faqQuestion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Faq Question id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faqQuestion = $this->FaqQuestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faqQuestion = $this->FaqQuestions->patchEntity($faqQuestion, $this->request->data);
            if ($this->FaqQuestions->save($faqQuestion)) {
                $this->Flash->success(__('De kennisbankvraag is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De kennisbankvraag kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $faqCategories = $this->FaqQuestions->FaqCategories->find('list', ['limit' => 200]);
        $createdBy = $this->FaqQuestions->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->FaqQuestions->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('faqQuestion', 'faqCategories', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['faqQuestion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Faq Question id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $faqQuestion = $this->FaqQuestions->get($id);
        if ($this->FaqQuestions->delete($faqQuestion)) {
            $this->Flash->success(__('De kennisbankvraag is verwijderd.'));
        } else {
            $this->Flash->error(__('De kennisbankvraag kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
