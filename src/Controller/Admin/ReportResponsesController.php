<?php
namespace App\Controller\Admin;

use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Settings\Core\Setting;

/**
 * ReportResponses Controller
 *
 * @property \App\Model\Table\ReportResponsesTable $ReportResponses
 */
class ReportResponsesController extends AdminController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent( 'Bootgrid.Bootgrid', [
                                                       'entity'        => $this->modelClass,
                                                       'contain'       => [
                'Reporters', 'Users', 'Devices', 'CreatedBy', 'ModifiedBy'
        ],
        'searchColumns' => [
                'ReportResponses.subject',
                'ReportResponses.message',
                'ReportResponses.created_by',
                'ReportResponses.modified_by',
                'ReportResponses.created_at',
                'ReportResponses.modified_at',
            'Reporters.username',
            'Users.username',
            'Devices.name',
            'CreatedBy.username',
            'ModifiedBy.username',
                  ],
                 'defaultSort'   => [ 'ReportResponses.id' => 'ASC' ]
             ]
       );
     }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['Reporters', 'Users', 'Devices', 'CreatedBy', 'ModifiedBy']
        ];
        $reportResponses = $this->paginate($this->ReportResponses);

        $reporters = $this->ReportResponses->Reporters->find('list');
        $users = $this->ReportResponses->Users->find('list');
        $devices = $this->ReportResponses->Devices->find('list');
        $createdBy = $this->ReportResponses->CreatedBy->find('list');
        $modifiedBy = $this->ReportResponses->ModifiedBy->find('list');
        $this->set(compact('reportResponse', 'reporters', 'users', 'devices', 'createdBy', 'modifiedBy'));

        $this->set(compact('reportResponses'));
        $this->set('_serialize', ['reportResponses']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reportResponse = $this->ReportResponses->newEntity();
        if ($this->request->is('post')) {
            $reportResponse = $this->ReportResponses->patchEntity($reportResponse, $this->request->data);
            if ($this->ReportResponses->save($reportResponse)) {
                $this->Flash->success(__('De meldingreactie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('De meldingreactie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
        }
        $reporters = $this->ReportResponses->Reporters->find('list', ['limit' => 200]);
        $users = $this->ReportResponses->Users->find('list', ['limit' => 200]);
        $devices = $this->ReportResponses->Devices->find('list', ['limit' => 200]);
        $createdBy = $this->ReportResponses->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->ReportResponses->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reportResponse', 'reporters', 'users', 'devices', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reportResponse']);
    }

    /**
     * Respond method
     *
     * @param string|null $id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function respond( $report_id = null )
    {
        $this->loadModel('Reports');
        $report = $this->Reports->get($report_id, [
            'contain' => [
                'Reporters',
                'ReportTypes'
            ]
        ]);

        $this->loadModel('ReportResponsePresets');
        $reportResponsePresets = $this->ReportResponsePresets->find('list');

        $this->loadModel('Reporters');
        $reporter = $this->Reporters->find('all', [
            'conditions' => [
                'Reporters.id' => $report->reporter_id
            ]
        ])->first();

        $reportResponse = $this->ReportResponses->newEntity();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $reportResponse = $this->ReportResponses->patchEntity($reportResponse, $this->request->data);

            if ($this->ReportResponses->save($reportResponse)) {

                $user = TableRegistry::get('Users')->find('all', [
                    'conditions' => [
                        'Users.id' => $this->AuthUser->id()
                    ]
                ])->first();

                $email = new Email();
                $sender = [Setting::Read('App.Email.DefaultSenderEmail') => Setting::Read('App.Email.DefaultSenderName') . " | " . $user->fullname];
                $receiver = $reporter->email;
                $subject = $this->request->data(['subject']);

                $email->emailFormat('html')
                    ->from($sender)
                    ->to($receiver)
                    ->subject($subject);
                $email->template('default', 'report_response');
                $email->viewVars([
                    'title' => $subject,
                    'body' => $this->request->data(['message'])
                ]);
                $email->send();

                $this->Flash->success(__('De reactie is verzonden'));
                return $this->redirect(['controller' => 'reports', 'action' => 'index']);
            } else {
                $this->Flash->error(__('De reactie kon niet worden verzonden. Probeer het a.u.b. nog eens.'));
                return $this->redirect( $this->referer() );
            }
        }
        $this->set(compact('reportResponse', 'report', 'reportResponsePresets'));
        $this->set('_serialize', ['reportResponse', 'report']);
    }

    /**
     * Mass respond method
     *
     * @param string|null $report_ids.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function massRespond( $report_ids = '' )
    {
        $this->loadModel('ReportResponses'); // why tho?
        $reportResponse = $this->ReportResponses->newEntity();

        $this->loadModel('ReportResponsePresets');
        $reportResponsePresets = $this->ReportResponsePresets->find('list');

        $this->loadModel('Reports');
        $report_ids = explode(',', $report_ids);
        $reports = $this->Reports->find( 'all', [
            'contain' => [
                'Reporters' => function ($exp) {
                    return $exp
                        ->select([ 'id', 'email']);
                }
            ],
            'conditions' => [
                'Reports.id IN' => $report_ids
            ]
        ])->toArray();

        $this->loadModel('Users');
        $user = $this->Users->find('all', [
            'conditions' => [
                'Users.id' => $this->AuthUser->id()
            ]
        ])->first();

        $this->loadModel('ReportStatuses');
        $reportStatuses = $this->ReportStatuses->find('list');

        if ($this->request->is(['patch', 'post', 'put'])) {

            foreach ( $reports as $report ){

                $report->report_status_id = $this->request->data('report_status');

                if (!$this->Reports->save($report)) {
                    $this->Flash->error(__('De reactie kon niet worden verzonden. Er ging iets mis bij het wijzigen van de rapportage status.'));
                    return $this->redirect($this->referer());
                }
            }

            $reportResponse = $this->ReportResponses->patchEntity($reportResponse, $this->request->data);

            if ($this->ReportResponses->save($reportResponse)) {

                $reporter_emails = [];
                foreach( $reports as $report){
                    $reporter_emails[] = $report->reporter->email;
                }

                $email = new Email();
                $sender = [ Setting::Read( 'App.Email.DefaultSenderEmail' ) => Setting::Read( 'App.Email.DefaultSenderName' )." | ".$user->fullname ];
                $receiver = $reporter_emails;
                $subject = $this->request->data([ 'subject' ]);

                $email->emailFormat( 'html' )
                    ->from( $sender )
                    ->to( $receiver )
                    ->subject( $subject );
                $email->template( 'default', 'report_response' );
                $email->viewVars( [
                    'title'     => $subject,
                    'body'   => $this->request->data([ 'message' ])
                ] );
                $email->send();

                $this->Flash->success(__('De reactie is verzonden'));
                return $this->redirect([ 'controller' => 'reports' ,'action' => 'index']);
            } else {
                $this->Flash->error(__('De reactie kon niet worden verzonden. Probeer het a.u.b. nog eens.'));
                return $this->redirect( $this->referer() );
            }
        }
        $this->set(compact('reportResponse', 'reports', 'reportResponsePresets', 'reportStatuses'));
        $this->set('_serialize', ['reportResponse', 'reports']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Report Response id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reportResponse = $this->ReportResponses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reportResponse = $this->ReportResponses->patchEntity($reportResponse, $this->request->data);
            if ($this->ReportResponses->save($reportResponse)) {
                $this->Flash->success(__('De meldingreactie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De meldingreactie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $reporters = $this->ReportResponses->Reporters->find('list', ['limit' => 200]);
        $users = $this->ReportResponses->Users->find('list', ['limit' => 200]);
        $devices = $this->ReportResponses->Devices->find('list', ['limit' => 200]);
        $createdBy = $this->ReportResponses->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->ReportResponses->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reportResponse', 'reporters', 'users', 'devices', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reportResponse']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Report Response id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $reportResponse = $this->ReportResponses->get($id);
        if ($this->ReportResponses->delete($reportResponse)) {
            $this->Flash->success(__('De meldingreactie is verwijderd.'));
        } else {
            $this->Flash->error(__('De meldingreactie kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
