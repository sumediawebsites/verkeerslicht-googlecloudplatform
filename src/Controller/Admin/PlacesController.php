<?php
namespace App\Controller\Admin;

/**
 * Places Controller
 *
 * @property \App\Model\Table\PlacesTable $Places
 */
class PlacesController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'Councils', 'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'Places.name',
			'Places.created_by',
			'Places.modified_by',
			'Places.created_at',
			'Places.modified_at',
			'Places.deleted',
		'Councils.name',
              ],
             'defaultSort'   => [ 'Places.name' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['Councils', 'CreatedBy', 'ModifiedBy']
        ];
        $places = $this->paginate($this->Places);

        $councils = $this->Places->Councils->find('list');
        $createdBy = $this->Places->CreatedBy->find('list');
        $modifiedBy = $this->Places->ModifiedBy->find('list');
        $this->set(compact('place', 'councils', 'createdBy', 'modifiedBy'));

        $this->set(compact('places'));
        $this->set('_serialize', ['places']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $place = $this->Places->newEntity();
        if ($this->request->is('post')) {
            $place = $this->Places->patchEntity($place, $this->request->data);
            if ($this->Places->save($place)) {
                $this->Flash->success(__('De plaats is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De plaats kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $councils = $this->Places->Councils->find('list', ['limit' => 200]);
        $createdBy = $this->Places->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Places->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('place', 'councils', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['place']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Place id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $place = $this->Places->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $place = $this->Places->patchEntity($place, $this->request->data);
            if ($this->Places->save($place)) {
                $this->Flash->success(__('De plaats is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De plaats kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $councils = $this->Places->Councils->find('list', ['limit' => 200]);
        $createdBy = $this->Places->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Places->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('place', 'councils', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['place']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Place id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $place = $this->Places->get($id);
        if ($this->Places->delete($place)) {
            $this->Flash->success(__('De plaats is verwijderd.'));
        } else {
            $this->Flash->error(__('De plaats kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
