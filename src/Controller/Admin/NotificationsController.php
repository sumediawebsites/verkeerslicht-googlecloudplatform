<?php
namespace App\Controller\Admin;

use Cake\ORM\TableRegistry;

/**
 * Notifications Controller
 *
 * @property \App\Model\Table\NotificationsTable $Notifications
 */
class NotificationsController extends AdminController {
	/**
	 * Index method
	 *
	 * @return \Cake\Network\Response|null
	 */
	public function index() {
		$notifications = $this->Notifier->getNotifications();

		$this->set( compact( 'notifications' ) );
		$this->set( '_serialize', [ 'notifications' ] );
	}

	public function mark_as_read( $id ) {
		$this->Notifier->markAsRead( $id );
		$this->Flash->success( __( 'Notificatie is gemarkeerd als gelezen.' ) );

		return $this->redirect( $this->referer() );
	}

	public function delete( $id ) {
		$notificationsTable = TableRegistry::get( 'Notifier.Notifications' );
		$notification       = $notificationsTable->get( $id );
		$notificationsTable->delete( $notification );
		$this->Flash->success( __( 'Notificatie is verwijderd.' ) );

		return $this->redirect( $this->referer() );
	}

	public function mark_all_read() {
		$this->Notifier->markAsRead( null, $this->AuthUser->id() );
		$this->Flash->success( __( 'Alle notificaties zijn gemarkeerd als gelezen.' ) );

		return $this->redirect( $this->referer() );
	}

	public function delete_all() {
		$notificationsTable = TableRegistry::get( 'Notifier.Notifications' );
		$notifications      = $notificationsTable->findByUserId( $this->AuthUser->id() );
		foreach ( $notifications as $notification ):
			$notificationsTable->delete( $notification );
		endforeach;
		$this->Flash->success( __( 'Al je notificaties zijn verwijderd.' ) );

		return $this->redirect( $this->referer() );
	}
}
