<?php
namespace App\Controller\Admin;

/**
 * Widgets Controller
 *
 * @property \App\Model\Table\WidgetsTable $Widgets
 */
class WidgetsController extends AdminController
{
    public function initialize() {
        parent::initialize();
    }

	
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $roles = $this->Widgets->Roles->find('list');
        $this->set(compact('widget', 'roles'));

        $this->set(compact('widgets'));
        $this->set('_serialize', ['widgets']);
    }

}
