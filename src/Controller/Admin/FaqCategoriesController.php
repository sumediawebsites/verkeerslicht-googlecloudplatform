<?php
namespace App\Controller\Admin;

/**
 * FaqCategories Controller
 *
 * @property \App\Model\Table\FaqCategoriesTable $FaqCategories
 */
class FaqCategoriesController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'ParentFaqCategories', 'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'FaqCategories.name',
			'FaqCategories.description',
			'FaqCategories.created_by',
			'FaqCategories.modified_by',
			'FaqCategories.created_at',
			'FaqCategories.modified_at',
			'FaqCategories.deleted',
		'ParentFaqCategories.name',
              ],
             'defaultSort'   => [ 'FaqCategories.name' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['ParentFaqCategories', 'CreatedBy', 'ModifiedBy']
        ];
        $faqCategories = $this->paginate($this->FaqCategories);

        $parentFaqCategories = $this->FaqCategories->ParentFaqCategories->find('list');
        $createdBy = $this->FaqCategories->CreatedBy->find('list');
        $modifiedBy = $this->FaqCategories->ModifiedBy->find('list');
        $this->set(compact('faqCategory', 'parentFaqCategories', 'createdBy', 'modifiedBy'));

        $this->set(compact('faqCategories'));
        $this->set('_serialize', ['faqCategories']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faqCategory = $this->FaqCategories->newEntity();
        if ($this->request->is('post')) {
            $faqCategory = $this->FaqCategories->patchEntity($faqCategory, $this->request->data);
            if ($this->FaqCategories->save($faqCategory)) {
                $this->Flash->success(__('De kennisbankcategorie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De kennisbankcategorie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $parentFaqCategories = $this->FaqCategories->ParentFaqCategories->find('list', ['limit' => 200]);
        $createdBy = $this->FaqCategories->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->FaqCategories->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('faqCategory', 'parentFaqCategories', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['faqCategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Faq Category id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faqCategory = $this->FaqCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faqCategory = $this->FaqCategories->patchEntity($faqCategory, $this->request->data);
            if ($this->FaqCategories->save($faqCategory)) {
                $this->Flash->success(__('De kennisbankcategorie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De kennisbankcategorie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $parentFaqCategories = $this->FaqCategories->ParentFaqCategories->find('list', ['limit' => 200]);
        $createdBy = $this->FaqCategories->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->FaqCategories->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('faqCategory', 'parentFaqCategories', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['faqCategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Faq Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $faqCategory = $this->FaqCategories->get($id);
        if ($this->FaqCategories->delete($faqCategory)) {
            $this->Flash->success(__('De kennisbankcategorie is verwijderd.'));
        } else {
            $this->Flash->error(__('De kennisbankcategorie kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
