<?php
namespace App\Controller\Admin;

/**
 * FaqUserQuestions Controller
 *
 * @property \App\Model\Table\FaqUserQuestionsTable $FaqUserQuestions
 */
class FaqUserQuestionsController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'FaqCategories', 'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'FaqUserQuestions.subject',
			'FaqUserQuestions.message',
			'FaqUserQuestions.response',
			'FaqUserQuestions.email',
			'FaqUserQuestions.answered',
			'FaqUserQuestions.is_response',
			'FaqUserQuestions.created_by',
			'FaqUserQuestions.modified_by',
			'FaqUserQuestions.created_at',
			'FaqUserQuestions.modified_at',
		'FaqCategories.name',
              ],
             'defaultSort'   => [ 'FaqUserQuestions.id' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['FaqCategories', 'CreatedBy', 'ModifiedBy']
        ];
        $faqUserQuestions = $this->paginate($this->FaqUserQuestions);

        $faqCategories = $this->FaqUserQuestions->FaqCategories->find('list');
        $createdBy = $this->FaqUserQuestions->CreatedBy->find('list');
        $modifiedBy = $this->FaqUserQuestions->ModifiedBy->find('list');
        $this->set(compact('faqUserQuestion', 'faqCategories', 'createdBy', 'modifiedBy'));

        $this->set(compact('faqUserQuestions'));
        $this->set('_serialize', ['faqUserQuestions']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faqUserQuestion = $this->FaqUserQuestions->newEntity();
        if ($this->request->is('post')) {
            $faqUserQuestion = $this->FaqUserQuestions->patchEntity($faqUserQuestion, $this->request->data);
            if ($this->FaqUserQuestions->save($faqUserQuestion)) {
                $this->Flash->success(__('De kennisbankgebruikersvraag is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('De kennisbankgebruikersvraag kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
        }
        $faqCategories = $this->FaqUserQuestions->FaqCategories->find('list', ['limit' => 200]);
        $this->set(compact('faqUserQuestion', 'faqCategories'));
        $this->set('_serialize', ['faqUserQuestion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Faq User Question id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faqUserQuestion = $this->FaqUserQuestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faqUserQuestion = $this->FaqUserQuestions->patchEntity($faqUserQuestion, $this->request->data);
            if ($this->FaqUserQuestions->save($faqUserQuestion)) {
                $this->Flash->success(__('De kennisbankgebruikersvraag is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De kennisbankgebruikersvraag kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $faqCategories = $this->FaqUserQuestions->FaqCategories->find('list', ['limit' => 200]);
        $this->set(compact('faqUserQuestion', 'faqCategories'));
        $this->set('_serialize', ['faqUserQuestion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Faq User Question id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $faqUserQuestion = $this->FaqUserQuestions->get($id);
        if ($this->FaqUserQuestions->delete($faqUserQuestion)) {
            $this->Flash->success(__('De kennisbankgebruikersvraag is verwijderd.'));
        } else {
            $this->Flash->error(__('De kennisbankgebruikersvraag kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Respond method
     *
     * @param string|null $id Faq User Question id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function respond($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faqUserQuestion = $this->FaqUserQuestions->get($id, [
                'contain' => []
            ]);
            $faqUserResponse = $this->FaqUserQuestions->newEntity();
            $faqUserResponse = $this->FaqUserQuestions->patchEntity($faqUserResponse, $this->request->data);

            $faqUserResponse->parent_id = $faqUserQuestion->id;
            $faqUserResponse->is_response = true;
            $faqUserQuestion->answered = true;

            if ($this->FaqUserQuestions->save($faqUserResponse)) {

                if ($this->FaqUserQuestions->save($faqUserQuestion)){
                    $faqUserResponse->sendResponseMail();
                    $this->Flash->success(__('De kennisbankgebruikersvraag is opgeslagen.'));
                    return $this->redirect(['action' => 'index']);

                } else {
                $this->Flash->error(__('De kennisbankgebruikersvraag kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
                return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->Flash->error(__('De kennisbankgebruikersvraag kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
				        return $this->redirect(['action' => 'index']);
            }
        }
    }
}
