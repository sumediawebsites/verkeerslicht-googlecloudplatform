<?php
namespace App\Controller\Admin;

/**
 * DeviceTypes Controller
 *
 * @property \App\Model\Table\DeviceTypesTable $DeviceTypes
 */
class DeviceTypesController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'DeviceTypes.name',
			'DeviceTypes.created_by',
			'DeviceTypes.modified_by',
			'DeviceTypes.created_at',
			'DeviceTypes.modified_at',
			'DeviceTypes.deleted',
             ],
             'defaultSort'   => [ 'DeviceTypes.name' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['CreatedBy', 'ModifiedBy']
        ];
        $deviceTypes = $this->paginate($this->DeviceTypes);

        $createdBy = $this->DeviceTypes->CreatedBy->find('list');
        $modifiedBy = $this->DeviceTypes->ModifiedBy->find('list');
        $this->set(compact('deviceType', 'createdBy', 'modifiedBy'));

        $this->set(compact('deviceTypes'));
        $this->set('_serialize', ['deviceTypes']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deviceType = $this->DeviceTypes->newEntity();
        if ($this->request->is('post')) {
            $deviceType = $this->DeviceTypes->patchEntity($deviceType, $this->request->data);
            if ($this->DeviceTypes->save($deviceType)) {
                $this->Flash->success(__('Het VRI-type is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Het VRI-type kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->DeviceTypes->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->DeviceTypes->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('deviceType', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['deviceType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Device Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $deviceType = $this->DeviceTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $deviceType = $this->DeviceTypes->patchEntity($deviceType, $this->request->data);
            if ($this->DeviceTypes->save($deviceType)) {
                $this->Flash->success(__('Het VRI-type is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Het VRI-type kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->DeviceTypes->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->DeviceTypes->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('deviceType', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['deviceType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Device Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $deviceType = $this->DeviceTypes->get($id);
        if ($this->DeviceTypes->delete($deviceType)) {
            $this->Flash->success(__('Het VRI-type is verwijderd.'));
        } else {
            $this->Flash->error(__('Het VRI-type kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
