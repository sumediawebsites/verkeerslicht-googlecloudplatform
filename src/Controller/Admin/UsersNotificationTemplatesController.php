<?php
namespace App\Controller\Admin;

/**
 * UsersNotificationTemplates Controller
 *
 * @property \App\Model\Table\UsersNotificationTemplatesTable $UsersNotificationTemplates
 */
class UsersNotificationTemplatesController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'Users', 'NotificationTemplates'
	],
	'searchColumns' => [
			'UsersNotificationTemplates.show_notification',
		'Users.fullname',
 		'NotificationTemplates.name',
              ],
             'defaultSort'   => [ 'UsersNotificationTemplates.id' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['Users', 'NotificationTemplates']
        ];
        $usersNotificationTemplates = $this->paginate($this->UsersNotificationTemplates);

        $users = $this->UsersNotificationTemplates->Users->find('list');
        $notificationTemplates = $this->UsersNotificationTemplates->NotificationTemplates->find('list');
        $this->set(compact('usersNotificationTemplate', 'users', 'notificationTemplates'));

        $this->set(compact('usersNotificationTemplates'));
        $this->set('_serialize', ['usersNotificationTemplates']);
}
    }

    /**
     * View method
     *
     * @param string|null $id Users Notification Template id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersNotificationTemplate = $this->UsersNotificationTemplates->get($id, [
            'contain' => ['Users', 'NotificationTemplates']
        ]);

        $this->set('usersNotificationTemplate', $usersNotificationTemplate);
        $this->set('_serialize', ['usersNotificationTemplate']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersNotificationTemplate = $this->UsersNotificationTemplates->newEntity();
        if ($this->request->is('post')) {
            $usersNotificationTemplate = $this->UsersNotificationTemplates->patchEntity($usersNotificationTemplate, $this->request->data);
            if ($this->UsersNotificationTemplates->save($usersNotificationTemplate)) {
                $this->Flash->success(__('De gebruikersnotificatietemplate is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De gebruikersnotificatietemplate kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $users = $this->UsersNotificationTemplates->Users->find('list', ['limit' => 200]);
        $notificationTemplates = $this->UsersNotificationTemplates->NotificationTemplates->find('list', ['limit' => 200]);
        $this->set(compact('usersNotificationTemplate', 'users', 'notificationTemplates'));
        $this->set('_serialize', ['usersNotificationTemplate']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Notification Template id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersNotificationTemplate = $this->UsersNotificationTemplates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersNotificationTemplate = $this->UsersNotificationTemplates->patchEntity($usersNotificationTemplate, $this->request->data);
            if ($this->UsersNotificationTemplates->save($usersNotificationTemplate)) {
                $this->Flash->success(__('De gebruikersnotificatietemplate is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De gebruikersnotificatietemplate kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $users = $this->UsersNotificationTemplates->Users->find('list', ['limit' => 200]);
        $notificationTemplates = $this->UsersNotificationTemplates->NotificationTemplates->find('list', ['limit' => 200]);
        $this->set(compact('usersNotificationTemplate', 'users', 'notificationTemplates'));
        $this->set('_serialize', ['usersNotificationTemplate']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Notification Template id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $usersNotificationTemplate = $this->UsersNotificationTemplates->get($id);
        if ($this->UsersNotificationTemplates->delete($usersNotificationTemplate)) {
            $this->Flash->success(__('De gebruikersnotificatietemplate is verwijderd.'));
        } else {
            $this->Flash->error(__('De gebruikersnotificatietemplate kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
