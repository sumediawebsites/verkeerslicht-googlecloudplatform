<?php
namespace App\Controller\Admin;

/**
 * Locations Controller
 *
 * @property \App\Model\Table\LocationsTable $Locations
 */
class LocationsController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'Councils', 'Places', 'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'Locations.address',
			'Locations.longitude',
			'Locations.latitude',
			'Locations.created_by',
			'Locations.modified_by',
			'Locations.created_at',
			'Locations.modified_at',
			'Locations.deleted',
		'Councils.name',
 		'Places.name',
              ],
             'defaultSort'   => [ 'Locations.address' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['Councils', 'Places', 'CreatedBy', 'ModifiedBy']
        ];
        $locations = $this->paginate($this->Locations);

        $councils = $this->Locations->Councils->find('list');
        $places = $this->Locations->Places->find('list');
        $createdBy = $this->Locations->CreatedBy->find('list');
        $modifiedBy = $this->Locations->ModifiedBy->find('list');
        $this->set(compact('location', 'councils', 'places', 'createdBy', 'modifiedBy'));

        $this->set(compact('locations'));
        $this->set('_serialize', ['locations']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $location = $this->Locations->newEntity();
        if ($this->request->is('post')) {
            $location = $this->Locations->patchEntity($location, $this->request->data);
            if ($this->Locations->save($location)) {
                $this->Flash->success(__('De locatie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De locatie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $councils = $this->Locations->Councils->find('list', ['limit' => 200]);
        $places = $this->Locations->Places->find('list', ['limit' => 200]);
        $createdBy = $this->Locations->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Locations->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('location', 'councils', 'places', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['location']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Location id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $location = $this->Locations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $location = $this->Locations->patchEntity($location, $this->request->data);
            if ($this->Locations->save($location)) {
                $this->Flash->success(__('De locatie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De locatie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $councils = $this->Locations->Councils->find('list', ['limit' => 200]);
        $places = $this->Locations->Places->find('list', ['limit' => 200]);
        $createdBy = $this->Locations->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Locations->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('location', 'councils', 'places', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['location']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Location id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $location = $this->Locations->get($id);
        if ($this->Locations->delete($location)) {
            $this->Flash->success(__('De locatie is verwijderd.'));
        } else {
            $this->Flash->error(__('De locatie kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
