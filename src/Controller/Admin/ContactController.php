<?php
namespace App\Controller\Admin;

use Cake\Mailer\Email;
use Settings\Core\Setting;
use Cake\Core\Configure;
use Notifier\Utility\NotificationManager;
use Notifier\Utility\Pusher\PusherSdkClient;

/**
 * Contact Controller
 *
 * @property \App\Model\Table\ContactTable $Contact
 */
class ContactController extends AdminController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent( 'Bootgrid.Bootgrid', [
                                                       'entity'        => $this->modelClass,
                                                       'contain'       => [
                'ParentContact', 'CreatedBy', 'ModifiedBy'
        ],
        'searchColumns' => [
                'Contact.firstname',
                'Contact.lastname',
                'Contact.email',
                'Contact.subject',
                'Contact.message',
                'Contact.is_answered',
                'Contact.is_admin',
                'Contact.created_by',
                'Contact.modified_by',
                'Contact.created_at',
                'Contact.modified_at',
            'ParentContact.subject',
            'CreatedBy.username',
            'ModifiedBy.username',
                  ],
                 'defaultSort'   => [ 'Contact.created_at' => 'DESC' ]
             ]
        );
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['ParentContact', 'CreatedBy', 'ModifiedBy'],
            'conditions' => [ 'Contact.is_admin' => false ]
        ];
        $contact = $this->paginate($this->Contact);

        $parentContact = $this->Contact->ParentContact->find('list');
        $createdBy = $this->Contact->CreatedBy->find('list');
        $modifiedBy = $this->Contact->ModifiedBy->find('list');
        $this->set(compact('contact', 'parentContact', 'createdBy', 'modifiedBy'));

        $this->set(compact('contact'));
        $this->set('_serialize', ['contact']);
}
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contact = $this->Contact->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contact = $this->Contact->patchEntity($contact, $this->request->data);
            if ($this->Contact->save($contact)) {
                $this->Flash->success(__('Het contactbericht is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Het contactbericht kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $parentContact = $this->Contact->ParentContact->find('list', ['limit' => 200]);
        $createdBy = $this->Contact->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Contact->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('contact', 'parentContact', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['contact']);
    }

    /**
     * Respond method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function Respond($id = null)
    {
        $contact = $this->Contact->get($id);
        $contactResponse = $this->Contact->newEntity();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $contactResponse = $this->Contact->patchEntity($contactResponse, $this->request->data);
            $contactResponse->is_admin = true;
            $contactResponse->parent_id = $contact->id;
            $contact->is_answered = true;

            // save original
            if ($this->Contact->save($contact)) {
                $this->Flash->success(__('Het oorspronkelijke bericht is opgeslagen.'));

            } else {
                $this->Flash->error(__('Het oorspronkelijke bericht kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
            // save response
            if ($this->Contact->save($contactResponse)) {
                // Send an email.
                $email = new Email();
                $email->emailFormat( 'html' )
                    ->from( Setting::Read( 'App.Email.DefaultSenderEmail' ) )
                    ->to( $contactResponse->email )
                    ->subject( $contactResponse->subject );
                $email->template( 'default', 'contact' );
                $email->viewVars( [
                    'firstname'  => $contactResponse->firstname,
                    'lastname'  => $contactResponse->lastname,
                    'email'  => $contactResponse->email,
                    'subject'  => $contactResponse->subject,
                    'body'     => $contactResponse->message
                ] );
                $email->send();

                $this->Flash->success(__('Je reactie is opgeslagen.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Je reactie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }

        $contact->message = "<br/><br/><hr/>Origineel bericht:<br/>".$contact->message;

        $parentContact = $this->Contact->ParentContact->find('list', ['limit' => 200]);
        $createdBy = $this->Contact->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Contact->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('contact', 'parentContact', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['contact']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $contact = $this->Contact->get($id);
        if ($this->Contact->delete($contact)) {
            $this->Flash->success(__('Het contactbericht is verwijderd.'));
        } else {
            $this->Flash->error(__('Het contactbericht kon niet worden verwijdered. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
