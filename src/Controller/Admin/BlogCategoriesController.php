<?php
namespace App\Controller\Admin;

/**
 * BlogCategories Controller
 *
 * @property \App\Model\Table\BlogCategoriesTable $BlogCategories
 */
class BlogCategoriesController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'ParentBlogCategories', 'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'BlogCategories.name',
			'BlogCategories.description',
			'BlogCategories.created_by',
			'BlogCategories.modified_by',
			'BlogCategories.created_at',
			'BlogCategories.modified_at',
			'BlogCategories.deleted',
		'ParentBlogCategories.name',
              ],
             'defaultSort'   => [ 'BlogCategories.name' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['ParentBlogCategories', 'CreatedBy', 'ModifiedBy']
        ];
        $blogCategories = $this->paginate($this->BlogCategories);

        $parentBlogCategories = $this->BlogCategories->ParentBlogCategories->find('list');
        $createdBy = $this->BlogCategories->CreatedBy->find('list');
        $modifiedBy = $this->BlogCategories->ModifiedBy->find('list');
        $this->set(compact('blogCategory', 'parentBlogCategories', 'createdBy', 'modifiedBy'));

        $this->set(compact('blogCategories'));
        $this->set('_serialize', ['blogCategories']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $blogCategory = $this->BlogCategories->newEntity();
        if ($this->request->is('post')) {
            $blogCategory = $this->BlogCategories->patchEntity($blogCategory, $this->request->data);
            if ($this->BlogCategories->save($blogCategory)) {
                $this->Flash->success(__('De blogcategorie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De blogcategorie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $parentBlogCategories = $this->BlogCategories->ParentBlogCategories->find('list', ['limit' => 200]);
        $createdBy = $this->BlogCategories->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->BlogCategories->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('blogCategory', 'parentBlogCategories', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['blogCategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Blog Category id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $blogCategory = $this->BlogCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $blogCategory = $this->BlogCategories->patchEntity($blogCategory, $this->request->data);
            if ($this->BlogCategories->save($blogCategory)) {
                $this->Flash->success(__('De blogcategorie is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De blogcategorie kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $parentBlogCategories = $this->BlogCategories->ParentBlogCategories->find('list', ['limit' => 200]);
        $createdBy = $this->BlogCategories->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->BlogCategories->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('blogCategory', 'parentBlogCategories', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['blogCategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Blog Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $blogCategory = $this->BlogCategories->get($id);
        if ($this->BlogCategories->delete($blogCategory)) {
            $this->Flash->success(__('De blogcategorie is verwijderd.'));
        } else {
            $this->Flash->error(__('De blogcategorie kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
