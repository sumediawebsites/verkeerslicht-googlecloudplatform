<?php
namespace App\Controller\Admin;

/**
 * Councils Controller
 *
 * @property \App\Model\Table\CouncilsTable $Councils
 */
class CouncilsController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'Provinces', 'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'Councils.name',
			'Councils.created_by',
			'Councils.modified_by',
			'Councils.created_at',
			'Councils.modified_at',
			'Councils.deleted',
		'Provinces.name',
              ],
             'defaultSort'   => [ 'Councils.name' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['Provinces', 'CreatedBy', 'ModifiedBy']
        ];
        $councils = $this->paginate($this->Councils);

        $provinces = $this->Councils->Provinces->find('list');
        $createdBy = $this->Councils->CreatedBy->find('list');
        $modifiedBy = $this->Councils->ModifiedBy->find('list');
        $this->set(compact('council', 'provinces', 'createdBy', 'modifiedBy'));

        $this->set(compact('councils'));
        $this->set('_serialize', ['councils']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $council = $this->Councils->newEntity();
        if ($this->request->is('post')) {
            $council = $this->Councils->patchEntity($council, $this->request->data);
            if ($this->Councils->save($council)) {
                $this->Flash->success(__('De gemeente is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De gemeente kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $provinces = $this->Councils->Provinces->find('list', ['limit' => 200]);
        $createdBy = $this->Councils->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Councils->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('council', 'provinces', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['council']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Council id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $council = $this->Councils->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $council = $this->Councils->patchEntity($council, $this->request->data);
            if ($this->Councils->save($council)) {
                $this->Flash->success(__('De gemeente is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De gemeente kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $provinces = $this->Councils->Provinces->find('list', ['limit' => 200]);
        $createdBy = $this->Councils->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Councils->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('council', 'provinces', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['council']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Council id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $council = $this->Councils->get($id);
        if ($this->Councils->delete($council)) {
            $this->Flash->success(__('De gemeente is verwijderd.'));
        } else {
            $this->Flash->error(__('De gemeente kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
