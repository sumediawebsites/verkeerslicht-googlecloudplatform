<?php
namespace App\Controller\Admin;

/**
 * Devices Controller
 *
 * @property \App\Model\Table\DevicesTable $Devices
 */
class DevicesController extends AdminController
{
		public function initialize() {
            parent::initialize();

            $conditions = [];

            if ( $this->Auth->user('role_id') == 2 ){
                $conditions = [
                    'Devices.user_id' => $this->Auth->user('id')
                ];
            }

            $this->loadComponent( 'Bootgrid.Bootgrid', [
                                                           'entity'        => $this->modelClass,
                                                           'contain'       => [
                    'Locations', 'Suppliers', 'Users', 'CreatedBy', 'ModifiedBy'
            ],
            'conditions' => $conditions,
            'searchColumns' => [
                    'Devices.name',
                    'Devices.created_by',
                    'Devices.modified_by',
                    'Devices.created_at',
                    'Devices.modified_at',
                    'Devices.deleted',
                'Suppliers.name',
                'Users.username',
                      ],
                     'defaultSort'   => [ 'Devices.name' => 'ASC' ]
                 ]
           );
     }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['Locations', 'Suppliers', 'Users', 'CreatedBy', 'ModifiedBy']
        ];
        $devices = $this->paginate($this->Devices);

        $locations = $this->Devices->Locations->find('list');
        $suppliers = $this->Devices->Suppliers->find('list');
        $managers = $this->Devices->Users->find('list');
        $createdBy = $this->Devices->CreatedBy->find('list');
        $modifiedBy = $this->Devices->ModifiedBy->find('list');
        $this->set(compact('device', 'locations', 'suppliers', 'managers', 'createdBy', 'modifiedBy'));

        $this->set(compact('devices'));
        $this->set('_serialize', ['devices']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $device = $this->Devices->newEntity();
        if ($this->request->is('post')) {

            $device = $this->Devices->patchEntity($device, $this->request->data);
            if ($this->Devices->save($device)) {
                $this->Flash->success(__('De VRI is opgeslagen.'));

//                return $this->redirect(['action' => 'index']);
                return $this->redirect($this->request->data('referer'));
            } else {
                $this->Flash->error(__('De VRI kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        } else {
            $referer = $this->referer();
        }
        $locations = $this->Devices->Locations->find('list', ['limit' => 200]);
        $suppliers = $this->Devices->Suppliers->find('list', ['limit' => 200]);
        $managers = $this->Devices->Users->find('list', ['limit' => 200])->where([ 'role_id' => 2 ]);
        $createdBy = $this->Devices->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Devices->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('device', 'locations', 'suppliers', 'managers', 'createdBy', 'modifiedBy', 'referer'));
        $this->set('_serialize', ['device']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Device id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $device = $this->Devices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $device = $this->Devices->patchEntity($device, $this->request->data);
            if ($this->Devices->save($device)) {
                $this->Flash->success(__('De VRI is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De VRI kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $locations = $this->Devices->Locations->find('list', ['limit' => 200]);
        $suppliers = $this->Devices->Suppliers->find('list', ['limit' => 200]);
        $managers = $this->Devices->Users->find('list', ['limit' => 200])->where([ 'role_id' => 2 ]);
        $createdBy = $this->Devices->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Devices->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('device', 'locations', 'suppliers', 'managers', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['device']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Device id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $device = $this->Devices->get($id);
        if ($this->Devices->delete($device)) {
            $this->Flash->success(__('De VRI is verwijderd.'));
        } else {
            $this->Flash->error(__('De VRI kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
