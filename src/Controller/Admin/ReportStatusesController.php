<?php
namespace App\Controller\Admin;

/**
 * ReportStatuses Controller
 *
 * @property \App\Model\Table\ReportStatusesTable $ReportStatuses
 */
class ReportStatusesController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'ReportStatuses.name',
			'ReportStatuses.description',
			'ReportStatuses.created_by',
			'ReportStatuses.modified_by',
			'ReportStatuses.created_at',
			'ReportStatuses.modified_at',
			'ReportStatuses.deleted',
            'ReportStatuses.class',
             ],
             'defaultSort'   => [ 'ReportStatuses.name' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['CreatedBy', 'ModifiedBy']
        ];
        $reportStatuses = $this->paginate($this->ReportStatuses);

        $createdBy = $this->ReportStatuses->CreatedBy->find('list');
        $modifiedBy = $this->ReportStatuses->ModifiedBy->find('list');
        $this->set(compact('reportStatus', 'createdBy', 'modifiedBy'));

        $this->set(compact('reportStatuses'));
        $this->set('_serialize', ['reportStatuses']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reportStatus = $this->ReportStatuses->newEntity();
        if ($this->request->is('post')) {
            $reportStatus = $this->ReportStatuses->patchEntity($reportStatus, $this->request->data);
            if ($this->ReportStatuses->save($reportStatus)) {
                $this->Flash->success(__('De meldingstatus is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De meldingstatus kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->ReportStatuses->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->ReportStatuses->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reportStatus', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reportStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Report Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reportStatus = $this->ReportStatuses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reportStatus = $this->ReportStatuses->patchEntity($reportStatus, $this->request->data);
            if ($this->ReportStatuses->save($reportStatus)) {
                $this->Flash->success(__('De meldingstatus is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De meldingstatus kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->ReportStatuses->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->ReportStatuses->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reportStatus', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reportStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Report Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $reportStatus = $this->ReportStatuses->get($id);
        if ($this->ReportStatuses->delete($reportStatus)) {
            $this->Flash->success(__('De meldingstatus is verwijderd.'));
        } else {
            $this->Flash->error(__('De meldingstatus kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
