<?php
namespace App\Controller\Admin;

/**
 * Suppliers Controller
 *
 * @property \App\Model\Table\SuppliersTable $Suppliers
 */
class SuppliersController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'Suppliers.name',
			'Suppliers.description',
			'Suppliers.created_by',
			'Suppliers.modified_by',
			'Suppliers.created_at',
			'Suppliers.modified_at',
			'Suppliers.deleted',
             ],
             'defaultSort'   => [ 'Suppliers.name' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['CreatedBy', 'ModifiedBy']
        ];
        $suppliers = $this->paginate($this->Suppliers);

        $createdBy = $this->Suppliers->CreatedBy->find('list');
        $modifiedBy = $this->Suppliers->ModifiedBy->find('list');
        $this->set(compact('supplier', 'createdBy', 'modifiedBy'));

        $this->set(compact('suppliers'));
        $this->set('_serialize', ['suppliers']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $supplier = $this->Suppliers->newEntity();
        if ($this->request->is('post')) {
            $supplier = $this->Suppliers->patchEntity($supplier, $this->request->data);
            if ($this->Suppliers->save($supplier)) {
                $this->Flash->success(__('De leverancier is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De leverancier kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->Suppliers->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Suppliers->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('supplier', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['supplier']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Supplier id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $supplier = $this->Suppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $supplier = $this->Suppliers->patchEntity($supplier, $this->request->data);
            if ($this->Suppliers->save($supplier)) {
                $this->Flash->success(__('De leverancier is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De leverancier kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->Suppliers->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Suppliers->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('supplier', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['supplier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Supplier id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $supplier = $this->Suppliers->get($id);
        if ($this->Suppliers->delete($supplier)) {
            $this->Flash->success(__('De leverancier is verwijderd.'));
        } else {
            $this->Flash->error(__('De leverancier kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
