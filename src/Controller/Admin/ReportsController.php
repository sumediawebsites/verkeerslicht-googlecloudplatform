<?php
namespace App\Controller\Admin;

use Cake\I18n\Date;

/**
 * Reports Controller
 *
 * @property \App\Model\Table\ReportsTable $Reports
 */
class ReportsController extends AdminController
{
		public function initialize() {
            parent::initialize();

            $conditions = [];

            if ( $this->Auth->user('role_id') == 2 ){
                $conditions = [
                    'Devices.user_id' => $this->Auth->user('id')
                ];
            }

            $this->loadComponent( 'Bootgrid.Bootgrid', [
           'entity'        => $this->modelClass,
           'contain'       => [
                    'Devices', 'ReportStatuses', 'Reporters', 'ReportTypes', 'CreatedBy', 'ModifiedBy'
            ],
            'conditions' => $conditions,
            'searchColumns' => [
                'Reports.title',
                'Reports.body',
                'Reports.created_by',
                'Reports.modified_by',
                'Reports.created_at',
                'Reports.modified_at',
                'Reports.deleted',
            'Devices.name',
            'ReportStatuses.name',
            'ReportTypes.name',
            'Reporters.username',
                  ],
                 'defaultSort'   => [ 'Reports.created_at' => 'DESC' ]
             ]
           );
     }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['Devices', 'ReportStatuses', 'Reporters', 'ReportTypes', 'CreatedBy', 'ModifiedBy']
        ];
        $reports = $this->paginate($this->Reports);

        $devices = $this->Reports->Devices->find('list');
        $reportStatuses = $this->Reports->ReportStatuses->find('list');
        $reporters = $this->Reports->Reporters->find('list');
        $reportTypes = $this->Reports->ReportTypes->find('list');
        $createdBy = $this->Reports->CreatedBy->find('list');
        $modifiedBy = $this->Reports->ModifiedBy->find('list');
        $this->set(compact('report', 'devices', 'reportStatuses', 'reporters', 'reportTypes', 'createdBy', 'modifiedBy'));

        $this->set(compact('reports'));
        $this->set('_serialize', ['reports']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $report = $this->Reports->newEntity();
        if ($this->request->is('post')) {
            $report = $this->Reports->patchEntity($report, $this->request->data);
            if ($this->Reports->save($report)) {
                $this->Flash->success(__('De melding is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De melding kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $devices = $this->Reports->Devices->find('list', ['limit' => 200]);
        $reportStatuses = $this->Reports->ReportStatuses->find('list', ['limit' => 200]);
        $reporters = $this->Reports->Reporters->find('list', ['limit' => 200]);
        $reportTypes = $this->Reports->ReportTypes->find('list', ['limit' => 200]);
        $createdBy = $this->Reports->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Reports->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('report', 'devices', 'reportStatuses', 'reporters', 'reportTypes', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['report']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Report id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $report = $this->Reports->get($id);

        $this->loadModel('Devices');
        $devices = $this->Devices->find('all', [
            'contain' => 'Locations',
            'conditions' => []
        ]);

        $suggestedDevices = [];

        foreach ( $devices as $device ){

            $latitudeFrom = $device->location->latitude;
            $longitudeFrom = $device->location->longitude;
            $latitudeTo = $report->latitude;
            $longitudeTo = $report->longitude;
            $earthRadius = 6371;

            // convert from degrees to radians
            $latFrom = deg2rad($latitudeFrom);
            $lonFrom = deg2rad($longitudeFrom);
            $latTo = deg2rad($latitudeTo);
            $lonTo = deg2rad($longitudeTo);

            $latDelta = $latTo - $latFrom;
            $lonDelta = $lonTo - $lonFrom;

            $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                    cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
            $distance = $angle * $earthRadius;

            $device->distance = $distance*1000; // km to m

            if( intval($device->distance) < 200 ){
                $suggestedDevices[] = $device;
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $report = $this->Reports->patchEntity($report, $this->request->data);
            if ($this->Reports->save($report)) {
                $this->Flash->success(__('De melding is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De melding kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $devices = $this->Reports->Devices->find('list', ['limit' => 200]);
        $reportStatuses = $this->Reports->ReportStatuses->find('list', ['limit' => 200]);
        $reporters = $this->Reports->Reporters->find('list', ['limit' => 200]);
        $reportTypes = $this->Reports->ReportTypes->find('list', ['limit' => 200]);
        $createdBy = $this->Reports->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->Reports->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('report', 'devices', 'reportStatuses', 'reporters', 'reportTypes', 'createdBy', 'modifiedBy', 'suggestedDevices'));
        $this->set('_serialize', ['report']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Report id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $report = $this->Reports->get($id);
        if ($this->Reports->delete($report)) {
            $this->Flash->success(__('De melding is verwijderd.'));
        } else {
            $this->Flash->error(__('De melding kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * return reports from $day until now
     *
     * @return void
     */
    public function getReportsFromDay( $days = 7 )
    {
        $this->RequestHandler->renderAs($this, 'json');

        $startDate = new Date('-'.$days.' days');

        $reports = $this->Reports->find('all', [
            'select' => 'Reports.created_at',
            'order' => [ 'Reports.created_at' => 'DESC' ],
            'contain' => [ 'ReportStatuses', 'ReportTypes' ],
            'conditions' => [
                'Reports.vri_street_1 IS NOT' => '',
                'Reports.vri_place IS NOT' => null,
                'Reports.date IS NOT' => null,
                'ReportTypes.name IS NOT' => null,
                'Reports.created_at >=' => $startDate
            ]
        ])->toArray();

        $groupedReports = [];

        for ($i = 0; $i <= $days; $i++) {
            $date = new Date( '-'.($days-$i).' days' );
            $dateString = $date->format('d M');
            $groupedReports[$dateString] = 0;
        }

        foreach($reports as $report){
            $date = new Date($report->created_at);
            $dateString = $date->format('d M');

            if( isset($groupedReports[$dateString]) ){
                $groupedReports[$dateString] ++;
            } else {
                $groupedReports[$dateString] = 1;
            };
        }

        $labels = array_keys($groupedReports);
        $data = array_values($groupedReports);

        $this->set(compact('labels', 'data'));
        $this->set('_serialize', ['labels', 'data']);
    }

}
