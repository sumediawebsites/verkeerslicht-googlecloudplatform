<?php

namespace App\Controller\Admin;

/**
 * ReportResponsePresets Controller
 *
 * @property \App\Model\Table\ReportResponsePresetsTable $ReportResponsePresets
 */
class ReportResponsePresetsController extends AdminController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Bootgrid.Bootgrid', [
                'entity' => $this->modelClass,
                'contain' => [
                    'CreatedBy', 'ModifiedBy'
                ],
                'searchColumns' => [
                    'ReportResponsePresets.name',
                    'ReportResponsePresets.subject',
                    'ReportResponsePresets.message',
                    'ReportResponsePresets.created_by',
                    'ReportResponsePresets.modified_by',
                    'ReportResponsePresets.created_at',
                    'ReportResponsePresets.modified_at',
                    'CreatedBy.username',
                    'ModifiedBy.username',
                ],
                'defaultSort' => ['ReportResponsePresets.name' => 'ASC']
            ]
        );
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ($this->request->is('ajax')) {
            $this->Bootgrid->getData();
        } else {
            $this->paginate = [
                'contain' => ['CreatedBy', 'ModifiedBy']
            ];
            $reportResponsePresets = $this->paginate($this->ReportResponsePresets);

            $createdBy = $this->ReportResponsePresets->CreatedBy->find('list');
            $modifiedBy = $this->ReportResponsePresets->ModifiedBy->find('list');
            $this->set(compact('reportResponsePreset', 'createdBy', 'modifiedBy'));

            $this->set(compact('reportResponsePresets'));
            $this->set('_serialize', ['reportResponsePresets']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reportResponsePreset = $this->ReportResponsePresets->newEntity();
        if ($this->request->is('post')) {
            $reportResponsePreset = $this->ReportResponsePresets->patchEntity($reportResponsePreset, $this->request->data);
            if ($this->ReportResponsePresets->save($reportResponsePreset)) {
                $this->Flash->success(__('The report response preset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The report response preset could not be saved. Please, try again.'));
        }
        $createdBy = $this->ReportResponsePresets->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->ReportResponsePresets->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reportResponsePreset', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reportResponsePreset']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Report Response Preset id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reportResponsePreset = $this->ReportResponsePresets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reportResponsePreset = $this->ReportResponsePresets->patchEntity($reportResponsePreset, $this->request->data);
            if ($this->ReportResponsePresets->save($reportResponsePreset)) {
                $this->Flash->success(__('The report response preset has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The report response preset could not be saved. Please, try again.'));
            }
        }
        $createdBy = $this->ReportResponsePresets->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->ReportResponsePresets->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reportResponsePreset', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reportResponsePreset']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Report Response Preset id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $reportResponsePreset = $this->ReportResponsePresets->get($id);
        if ($this->ReportResponsePresets->delete($reportResponsePreset)) {
            $this->Flash->success(__('The report response preset has been deleted.'));
        } else {
            $this->Flash->error(__('The report response preset could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
