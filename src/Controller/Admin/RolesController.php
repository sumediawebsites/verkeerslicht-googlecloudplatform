<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;
use Cake\ORM\TableRegistry;

/**
 * Roles Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 */
class RolesController extends AdminController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Bootgrid.Bootgrid', [
                'entity' => 'Roles',
                'searchColumns' => [
                    'name',
                ],
                'defaultSort' => ['name' => 'ASC']
            ]
        );
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ($this->request->is('ajax')) {
            $this->Bootgrid->getData();
        } else {

        }

    }

    /**
     * View method
     *
     * @param string|null $id Role id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $role = $this->Roles->get($id, [
            'contain' => ['Rights', 'Users']
        ]);

        $this->set('role', $role);
        $this->set('_serialize', ['role']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $role = $this->Roles->newEntity();
        if ($this->request->is('post')) {
            $role = $this->Roles->patchEntity($role, $this->request->data);
            if ($this->Roles->save($role)) {
                $this->Flash->success(__('De rol is opgeslagen.'));
                return $this->redirect(['action' => 'edit', $role->id]);
            } else {
                $this->Flash->error(__('De rol kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $rights = $this->Roles->Rights->find('list', ['limit' => 200]);
        $this->set(compact('role', 'rights'));
        $this->set('_serialize', ['role']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Role id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     * @todo Rewrite the save allow/deny functions
     */
    public function edit($id = null)
    {
        $role = $this->Roles->get($id, [
            'contain' => ['Rights']
        ]);
        $rights = $this->Roles->Rights->find('all', ['order' => 'code']);

        $currentRights = [];

        foreach($role->rights as $right){
            $currentRights[$right->id] = [];
            $currentRights[$right->id]['allow'] = $right->_joinData->allow;
            $currentRights[$right->id]['deny'] = $right->_joinData->deny;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $postRights = $this->request->data['rights'];
            unset($this->request->data['rights']);
            $role = $this->Roles->patchEntity($role, $this->request->data, ['associated' => null]);
            if ($this->Roles->save($role)) {
                //save allow/deny data
                $rightRolesTable = TableRegistry::get('rights_roles');

                foreach($rights as $right){
                    $rrQuery = $rightRolesTable->findByRightIdAndRoleId($right->id, $role->id);
                    if($rrQuery->count() == 1){
                        $rr = $rrQuery->first();
                    } else {
                        $rr = $rightRolesTable->newEntity([
                            'right_id' => $right->id,
                            'role_id' => $role->id
                        ]);
                    }

                    $rr->allow = isset($postRights[$right->id][0]) && $postRights[$right->id][0] == 'allow' ? true : null;
                    $rr->deny = isset($postRights[$right->id][0]) && $postRights[$right->id][0] == 'deny' ? true : null;

                    $rightRolesTable->save($rr);
                }

                $this->Flash->success(__('De rol is opgeslagen.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('De rol kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $this->set(compact('role', 'rights', 'currentRights'));
        $this->set('_serialize', ['role']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Role id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete', 'get']);
        $role = $this->Roles->get($id);
        if ($this->Roles->delete($role)) {
            $this->Flash->success(__('De rol is verwijderd.'));
        } else {
            $this->Flash->error(__('De rol kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
