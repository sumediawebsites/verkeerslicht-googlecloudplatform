<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;

/**
 * Rights Controller
 *
 * @property \App\Model\Table\RightsTable $Rights
 */
class RightsController extends AdminController {

	public function initialize() {
		parent::initialize();
		$this->loadComponent('Bootgrid.Bootgrid', [
				'entity'        => 'Rights',
				'searchColumns' => [
					'Rights.code',
					'Rights.name'
				],
				'defaultSort'   => [ 'Rights__name' => 'ASC' ]
			]
		);
	}

	/**
	 * Index method
	 *
	 * @return \Cake\Network\Response|null
	 */
	public function index() {
		if ( $this->request->is( 'ajax' ) ) {
			$this->Bootgrid->getData();
		} else {

		}

	}

	/**
	 * View method
	 *
	 * @param string|null $id Right id.
	 *
	 * @return \Cake\Network\Response|null
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function view( $id = null ) {
		$right = $this->Rights->get( $id, [
			'contain' => [ 'Roles' ]
		] );

		$this->set( 'right', $right );
		$this->set( '_serialize', [ 'right' ] );
	}

	/**
	 * Add method
	 *
	 * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
	 */
	public function add() {
		$right = $this->Rights->newEntity();
		if ( $this->request->is( 'post' ) ) {
			$right = $this->Rights->patchEntity( $right, $this->request->data );
			if ( $this->Rights->save( $right ) ) {
				$this->Flash->success( __( 'Het recht is opgeslagen.' ) );

				return $this->redirect( [ 'action' => 'index' ] );
			} else {
				$this->Flash->error( __( 'Het recht kon niet worden opgeslagen. Probeer het a.u.b. nog eens.' ) );
			}
		}
		$roles = $this->Rights->Roles->find( 'list', [ 'limit' => 200 ] );
		$this->set( compact( 'right', 'roles' ) );
		$this->set( '_serialize', [ 'right' ] );
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Right id.
	 *
	 * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit( $id = null ) {
		$right = $this->Rights->get( $id, [
			'contain' => [ 'Roles' ]
		] );
		if ( $this->request->is( [ 'patch', 'post', 'put' ] ) ) {
			$right = $this->Rights->patchEntity( $right, $this->request->data );
			if ( $this->Rights->save( $right ) ) {
				$this->Flash->success( __( 'Het recht is opgeslagen.' ) );

				return $this->redirect( [ 'action' => 'index' ] );
			} else {
				$this->Flash->error( __( 'Het recht kon niet worden opgeslagen. Probeer het a.u.b. nog eens.' ) );
			}
		}
		$roles = $this->Rights->Roles->find( 'list', [ 'limit' => 200 ] );
		$this->set( compact( 'right', 'roles' ) );
		$this->set( '_serialize', [ 'right' ] );
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Right id.
	 *
	 * @return \Cake\Network\Response|null Redirects to index.
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function delete( $id = null ) {
		$this->request->allowMethod( [ 'post', 'delete', 'get' ] );
		$right = $this->Rights->get( $id );
		if ( $this->Rights->delete( $right ) ) {
			$this->Flash->success( __( 'Het recht is verwijderd.' ) );
		} else {
			$this->Flash->error( __( 'Het recht kon niet worden verwijderd. Probeer het a.u.b. nog eens.' ) );
		}

		return $this->redirect( [ 'action' => 'index' ] );
	}
}
