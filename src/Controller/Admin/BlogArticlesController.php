<?php
namespace App\Controller\Admin;

/**
 * BlogArticles Controller
 *
 * @property \App\Model\Table\BlogArticlesTable $BlogArticles
 */
class BlogArticlesController extends AdminController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Bootgrid.Bootgrid', [
                'entity' => $this->modelClass,
                'contain' => [
                    'BlogCategories', 'CreatedBy', 'ModifiedBy'
                ],
                'searchColumns' => [
                    'BlogArticles.title',
                    'BlogArticles.slug',
                    'BlogArticles.body',
                    'BlogArticles.created_by',
                    'BlogArticles.modified_by',
                    'BlogArticles.created_at',
                    'BlogArticles.modified_at',
                    'BlogArticles.deleted',
                    'BlogCategories.name',
                ],
                'defaultSort' => ['BlogArticles.title' => 'ASC']
            ]
        );
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ($this->request->is('ajax')) {
            $this->Bootgrid->getData();
        } else {
            $this->paginate = [
                'contain' => ['BlogCategories', 'CreatedBy', 'ModifiedBy']
            ];
            $blogArticles = $this->paginate($this->BlogArticles);

            $blogCategories = $this->BlogArticles->BlogCategories->find('list');
            $createdBy = $this->BlogArticles->CreatedBy->find('list');
            $modifiedBy = $this->BlogArticles->ModifiedBy->find('list');
            $this->set(compact('blogArticle', 'blogCategories', 'createdBy', 'modifiedBy'));

            $this->set(compact('blogArticles'));
            $this->set('_serialize', ['blogArticles']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $blogArticle = $this->BlogArticles->newEntity();
        if ($this->request->is('post')) {
            $blogArticle = $this->BlogArticles->patchEntity($blogArticle, $this->request->data);
            if ($this->BlogArticles->save($blogArticle)) {
                $this->Flash->success(__('Het blogartikel is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Het blogartikel kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $blogCategories = $this->BlogArticles->BlogCategories->find('list', ['limit' => 200]);
        $createdBy = $this->BlogArticles->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->BlogArticles->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('blogArticle', 'blogCategories', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['blogArticle']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Blog Article id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $blogArticle = $this->BlogArticles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            if (isset($this->request->data['delete_image_url']) && $this->request->data['delete_image_url'] == 'deleted' && !isset($this->request->data['image_url'])) {
                $blogArticle->image_url = null;
            }

            if (!isset($this->request->data['delete_image_url']) && empty($this->request->data['image_url']['name'])) {
                $blogArticle->dirty('image_url', false);
                unset($this->request->data['image_url']);
            }

            $blogArticle = $this->BlogArticles->patchEntity($blogArticle, $this->request->data);

            if ($this->BlogArticles->save($blogArticle)) {

                $this->Flash->success(__('Het blogartikel is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Het blogartikel kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $blogCategories = $this->BlogArticles->BlogCategories->find('list', ['limit' => 200]);
        $createdBy = $this->BlogArticles->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->BlogArticles->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('blogArticle', 'blogCategories', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['blogArticle']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Blog Article id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $blogArticle = $this->BlogArticles->get($id);
        if ($this->BlogArticles->delete($blogArticle)) {
            $this->Flash->success(__('Het blogartikel is verwijderd.'));
        } else {
            $this->Flash->error(__('Het blogartikel kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
