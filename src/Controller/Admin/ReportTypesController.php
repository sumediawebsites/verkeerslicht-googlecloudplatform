<?php
namespace App\Controller\Admin;

/**
 * ReportTypes Controller
 *
 * @property \App\Model\Table\ReportTypesTable $ReportTypes
 */
class ReportTypesController extends AdminController
{
		public function initialize() {
	parent::initialize();
	$this->loadComponent( 'Bootgrid.Bootgrid', [
		                                           'entity'        => $this->modelClass,
		                                           'contain'       => [
			'CreatedBy', 'ModifiedBy'
	],
	'searchColumns' => [
			'ReportTypes.name',
			'ReportTypes.description',
			'ReportTypes.created_by',
			'ReportTypes.modified_by',
			'ReportTypes.created_at',
			'ReportTypes.modified_at',
			'ReportTypes.deleted',
             ],
             'defaultSort'   => [ 'ReportTypes.name' => 'ASC' ]
         ]
   );
 }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    if ( $this->request->is( 'ajax' ) ) {
        $this->Bootgrid->getData();
    } else {
        $this->paginate = [
            'contain' => ['CreatedBy', 'ModifiedBy']
        ];
        $reportTypes = $this->paginate($this->ReportTypes);

        $createdBy = $this->ReportTypes->CreatedBy->find('list');
        $modifiedBy = $this->ReportTypes->ModifiedBy->find('list');
        $this->set(compact('reportType', 'createdBy', 'modifiedBy'));

        $this->set(compact('reportTypes'));
        $this->set('_serialize', ['reportTypes']);
}
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reportType = $this->ReportTypes->newEntity();
        if ($this->request->is('post')) {
            $reportType = $this->ReportTypes->patchEntity($reportType, $this->request->data);
            if ($this->ReportTypes->save($reportType)) {
                $this->Flash->success(__('Het meldingtype is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Het meldingtype kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->ReportTypes->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->ReportTypes->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reportType', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reportType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Report Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reportType = $this->ReportTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reportType = $this->ReportTypes->patchEntity($reportType, $this->request->data);
            if ($this->ReportTypes->save($reportType)) {
                $this->Flash->success(__('Het meldingtype is opgeslagen.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Het meldingtype kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            }
        }
        $createdBy = $this->ReportTypes->CreatedBy->find('list', ['limit' => 200]);
        $modifiedBy = $this->ReportTypes->ModifiedBy->find('list', ['limit' => 200]);
        $this->set(compact('reportType', 'createdBy', 'modifiedBy'));
        $this->set('_serialize', ['reportType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Report Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $reportType = $this->ReportTypes->get($id);
        if ($this->ReportTypes->delete($reportType)) {
            $this->Flash->success(__('Het meldingtype is verwijderd.'));
        } else {
            $this->Flash->error(__('Het meldingtype kon niet worden verwijderd. Probeer het a.u.b. nog eens.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
