<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\View\Helper\UrlHelper;
use \Notifier\Utility\NotificationManager;
use Cake\Core\Configure;
use Cake\Utility\Inflector;

class AdminController extends AppController {
	public $helpers = [
		'TinyAuth.AuthUser',
		'Gourmet/KnpMenu.Menu',
		'AkkaCKEditor.CKEditor' => [ 'distribution' => 'standard' ],
	];

	public function beforeFilter( \Cake\Event\Event $event ) {
		if ( ! $this->request->is( 'ajax' ) ) {

			$this->set( 'notifications', $this->Notifier->getNotifications() );
			$this->set( 'notificationCount', $this->Notifier->countNotifications() );
			$this->set( 'notifier', $this->Notifier );
			$this->set( 'unreadNotificationCount', $this->Notifier->countNotifications( $this->AuthUser->id(), true ) );

			$this->_setNotifications();

			$rightCode = strtolower( $this->request->params['controller'] . '_' . $this->request->params['action'] );
			if ( $rightCode != 'users_logout' && $rightCode != 'users_login' ) {

				$menu = $this->Menu->get( 'admin' );
				$userMenu = $this->Menu->get( 'user' );

				$items     = $this->_initMenuItems();
				$userItems = $this->_initUserMenuItems();

				$this->_buildMenu( $menu, $items );
				$this->_buildMenu( $userMenu, $userItems );
			}
		}
		parent::beforeFilter( $event );
	}

	public function initialize() {

		parent::initialize();
//		$this->loadComponent( 'Auth', [ 'authorize' => 'Controller' ] );

		$this->Auth->config( 'authenticate', [
			'Form' =>
				[ 'fields' => [ 'username' => 'username', 'password' => 'password' ], 'userModel' => 'Users' ]
		] );
		$this->Auth->config( 'loginAction', [
			'controller' => 'Users',
			'action'     => 'login',
			'prefix'     => 'admin',
			'plugin'     => false
		] );
		$this->Auth->config( 'unauthorizedRedirect', [
			'controller' => 'Users',
			'action'     => 'login',
			'prefix'     => 'admin',
			'plugin'     => false,
		] );
		$this->Auth->config( 'loginRedirect', [
			'controller' => 'Users',
			'action'     => 'dashboard',
			'prefix'     => 'admin',
			'plugin'     => false,
		] );

		$this->loadComponent( 'Utils.GlobalAuth' );
		$this->loadComponent( 'Utils.Authorizer' );
		$this->loadComponent( 'TinyAuth.AuthUser' );
		$this->loadComponent( 'Gourmet/KnpMenu.Menu' );
		$this->loadComponent( 'Notifier.Notifier' );
		$this->viewBuilder()->layout( 'admin' );
        $this->Auth->deny();
	}

	private function _setNotifications() {
		$notificationManager = new NotificationManager();

		$notificationTemplates = TableRegistry::get( 'NotificationTemplates' )->find( 'all' );

		foreach ( $notificationTemplates as $notificationTemplate ) {
			$notificationManager->addTemplate( $notificationTemplate->template, [
				'title' => $notificationTemplate->title,
				'body'  => $notificationTemplate->body
			] );
		}
	}

	public function isAuthorized( $user ) {

		if ( $this->request->session()->check( 'is_frontend' ) ) {
			return false;
		}
		$rightCode = strtolower( $this->request->params['controller'] . '_' . $this->request->params['action'] );
		if ( $rightCode == 'users_logout' ) {
			return true;
		}

		if ( ! $this->request->session()->check( 'Rights' ) ) {
			return false;
		}

		if ( in_array( $rightCode, $this->request->session()->Read( 'Rights' )['allow'] ) ) {
			return true;
		} elseif ( in_array( $rightCode, $this->request->session()->Read( 'Rights' )['deny'] ) ) {
			$this->Flash->error( __( 'Je hebt geen rechten om deze pagina te bekijken. Daarom wordt je doorgestuurd naar de vorige pagina.' ) );
			$this->redirect( $this->referer() );

			return false;
		} else {
			$rightsTable = TableRegistry::get( 'rights' );
			$right       = $rightsTable->findByCode( $rightCode );
			if ( ! $right->count() ) {
				$newRight                = $rightsTable->newEntity( [
					'name' => $rightCode,
					'code' => $rightCode
				] );
				$resultInsertRightsTable = $rightsTable->save( $newRight );

				/**
				 * Link role to right and write it into session when a user has Admin rights
				 * @todo when admin is complete remove this code
				 */
				if ( $user['role_id'] === 1 ) {
					$rightsRolesTable = TableRegistry::get( 'rights_roles' );
					$newRightRole     = $rightsRolesTable->newEntity( [
						'role_id'  => 1,
						'right_id' => $resultInsertRightsTable->id,
						'allow'    => 1,
						'deny'     => 0
					] );

					$resultInsertRolesRightsTable = $rightsRolesTable->save( $newRightRole );
					$this->request->session()->write( 'Rights', array_merge_recursive( $this->request->session()->Read( 'Rights' ), array( 'allow' => array( $resultInsertRolesRightsTable->id => $rightCode ) ) ) );

					return true;
				}

			}
		}
		$this->Flash->error( __( 'Je hebt geen rechten om deze pagina te bekijken. Daarom word je doorgestuurd naar de vorige pagina.' ) );
		$this->redirect( $this->referer() );

		return false;
	}

	private function _initMenuItems() {
		$_menuItems = [
            'dashboard' => [
                'title'		 => '<i class="icon-lms-dashboard"></i>'.__( 'Dashboard' ),
                'controller' => 'Users',
                'action'     => 'dashboard',
            ],
			'pages_index'     => [
				'title'    => '<i class="icon-lms-pages"></i>'.__( 'Alle pagina\'s' ),
				'children' => [
					'pages_index' => [
						'title'      => __( 'Pagina\'s' ),
						'controller' => 'Pages',
						'action'     => 'index',
						'plugin'     => 'Cms'
					],
					'templates_index' => [
						'title'      => __( 'Templates' ),
						'controller' => 'Templates',
						'action'     => 'index',
						'plugin'     => 'Cms'
					],
				]
			],
            'blogarticles_index' => [
				'title' => '<i class="icon-lms-blog"></i>'.__( 'Blog'),
				'children' => [
					'blogarticles_index' => [
						'title' => __('Artikelen'),
						'controller' => 'BlogArticles',
						'action' => 'index',
					],
						'blogcategories_index' => [
							'title' => __('Categorieën'),
							'controller' => 'BlogCategories',
							'action' => 'index',
						],
				],
			],
			'faqquestions_index' => [
				'title' => '<i class="icon-lms-question"></i>'.__( 'Kennisbank'),
				'children' => [
					'faqquestions_index' => [
						'title' => __('Vragen'),
						'controller' => 'FaqQuestions',
						'action' => 'index',
					],
					'faqcategories_index' => [
						'title' => __('Categorieën'),
						'controller' => 'FaqCategories',
						'action' => 'index',
					]
				],
			],
            'menus_index'     => [
                'title'    => '<i class="icon-lms-menus"></i>'.__( 'Menu\'s' ),
                'children' => [
                    'menus_index'     => [
                        'title'      => __( 'Alle menu\'s' ),
                        'controller' => 'Menus',
                        'action'     => 'index',
                        'plugin'     => 'Cms'
                    ],
                    'menuitems_index' => [
                        'title'      => __( 'Menu-items' ),
                        'controller' => 'MenuItems',
                        'action'     => 'index',
                        'plugin'     => 'Cms'
                    ],
                ]
            ],
			'devices_index' => [
				'title' => '<i class="icon-lms-devices"></i>'.__('VRI\'s').'</span>',
				'children' => [
					'devices_index' => [
						'title' => __('Alle VRI\'s'),
						'controller' => 'Devices',
						'action' => 'index',
					],
					'devicetypes_index' => [
						'title' => __('Types'),
						'controller' => 'DeviceTypes',
						'action' => 'index',
					],
					'suppliers_index' => [
						'title' => __('Leveranciers'),
						'controller' => 'Suppliers',
						'action' => 'index',
					],
				],
			],
			'reports_index' => [
				'title' => '<i class="icon-lms-reports"></i>'.__('Meldingen').'</span>',
				'children' => [
                    'reports_index' => [
                        'title' => __('Alle meldingen'),
                        'controller' => 'Reports',
                        'action' => 'index',
                    ],
					'reporters_index' => [
						'title' => __('Melders'),
						'controller' => 'Reporters',
						'action' => 'index',
					],
					'reporttypes_index' => [
						'title' => __('Types'),
						'controller' => 'ReportTypes',
						'action' => 'index',
					],
					'reportstatuses_index' => [
						'title' => __('Statussen'),
						'controller' => 'ReportStatuses',
						'action' => 'index',
					],
					'reportresponsepresets_index' => [
						'title' => __('Response presets'),
						'controller' => 'ReportResponsePresets',
						'action' => 'index',
					],
				],
			],
			'places_index' => [
				'title' => '<i class="icon-lms-places"></i>'.__('Plaatsen').'</span>',
				'children' => [
                    'locations_index' => [
                        'title' => __('Locaties'),
                        'controller' => 'Locations',
                        'action' => 'index',
                    ],
					'places_index' => [
						'title' => __('Plaatsen'),
						'controller' => 'Places',
						'action' => 'index',
					],
					'councils_index' => [
						'title' => __('Gemeentes'),
						'controller' => 'Councils',
						'action' => 'index',
					],
					'provinces_index' => [
						'title' => __('Provincies'),
						'controller' => 'Provinces',
						'action' => 'index',
					],
				],
			],
			'users_index' => [
				'title'    => '<i class="icon-lms-users"></i>'.__( 'Gebruikers' ),
				'children' => [
					'users_index' => [
						'title'      => __('Alle gebruikers'),
						'controller' => 'Users',
						'action'     => 'index',
					],
					'roles_index' => [
						'title'      => __('Rollen'),
						'controller' => 'Roles',
						'action'     => 'index'
					]
				],
			],
            'messages_index' => [
                'title'    => '<i class="icon-lms-messages"></i>'.__( 'Berichten' ),
                'children' => [
					'chatmessages_index' => [
		                'title'      => __('Gesprekken'),
		                'controller' => 'ChatMessages',
		                'action'     => 'index',
		            ],
                    'contact_index' => [
                        'title'      => __('Contact'),
                        'controller' => 'Contact',
                        'action'     => 'index',
                    ],
                    'faquserquestions_index' => [
                        'title'      => __('Kennisbank'),
                        'controller' => 'FaqUserQuestions',
                        'action'     => 'index'
                    ]
                ],
            ],
		];


		return $_menuItems;
	}

    /**
     * @return array
     */
    private function _initUserMenuItems() {

		$unreadNotificationCount = $this->Notifier->countNotifications( $this->AuthUser->id(), true );

		$notifications      = $this->Notifier->getNotifications();
		$notificationsArray = [];

		$counter = 0;
		foreach ( $notifications as $notification ) {
			if ( $notification->state == 1 ) {
				$counter ++;
				$item = [
					'title' => '<span id="notification-' . $notification->id . '">' . $notification->title . ' ' . '</span>',
					'url'   => $notification->url,
				];

				$notificationsArray[ 'notifications_view_' . $notification->id ] = $item;
				if ( $counter >= 15 ) {
					break;
				}
			}
		}
		$itemAll                                      = [
			'title' => __( 'All notifications' ),
			'url'   => Router::url( [ 'controller' => 'Notifications', 'action' => 'index', 'prefix' => 'admin', 'plugin' => false ] )
		];
		$notificationsArray['notifications_view_all'] = $itemAll;

		$_menuItems = [
			 'notifications_index' => [
			 	'title'      => 'Berichten<span class="badge">' . $unreadNotificationCount . '</span>',
			 	'prefix'     => 'admin',
			 	'controller' => 'Notifications',
			 	'action'     => 'index',
			 	'children'   => $notificationsArray,
			 ],
			'users_edit'        => [
				'title'    => '<img class="avatar" src="'.Router::url($this->Auth->user( 'picture' )).'" />'.ucfirst( $this->Auth->user( 'fullname' ) ),
				'children' => [
					'my_account'      => [
						'prefix'     => 'admin',
						'controller' => 'Users',
						'action'     => 'edit/'.$this->Auth->user( 'id' ),
						'title'      => __( 'Account-instellingen' ),
					],
					'change_password' => [
						'prefix'     => 'admin',
						'controller' => 'Users',
						'action'     => 'change_password',
						'title'      => __( 'Wijzig wachtwoord' )
					],
					'logout'          => [
						'prefix'     => 'admin',
						'controller' => 'Users',
						'action'     => 'logout',
						'title'      => __( 'Uitloggen' ),
					]
				]
			],
		];

		return $_menuItems;
	}

	private function _buildMenu( $menu, $items, $parentItem = null ) {
		foreach ( $items as $rightCode => $data ) {
			if ( $rightCode == 'separator' ) {
				if ( $parentItem == null ) {
					$newItem = $menu->addChild( null );
				} else {
					$newItem = $parentItem->addChild( null );
				}
			} else {
				if ( ( isset( $this->request->session()->Read( 'Rights' )['allow'] ) && in_array( $rightCode, $this->request->session()->Read( 'Rights' )['allow'] ) ) || ( strpos( $rightCode, 'notifications' ) !== false ) ) {
					if ( isset( $data['controller'] ) ) {
						$url = [
							'uri' => [
								'controller' => $data['controller'],
								'action'     => $data['action'],
								'prefix'     => isset( $data['prefix'] ) ? $data['prefix'] : 'admin',
								'plugin'     => isset( $data['plugin'] ) ? $data['plugin'] : false,
								isset( $data['args'] ) ? $data['args'] : null
							]
						];
					} elseif ( isset( $data['url'] ) ) {
						$route = Router::parse( $data['url'] );
						$url   = [
							'uri' => [
								'controller' => $route['controller'],
								'action'     => $route['action'],
								'prefix'     => isset( $route['prefix'] ) ? $route['prefix'] : ( isset( $route['plugin'] ) ? false : 'admin' ),
								'plugin'     => isset( $route['plugin'] ) ? $route['plugin'] : false,
								! empty( $route['pass'] ) ? $route['pass'][0] : null,
							]
						];
					} else {
						$url = [];
					}

					if ( $parentItem == null ) {
						$newItem = $menu->addChild( $data['title'], $url );
					} else {
						$newItem = $parentItem->addChild( $data['title'], $url );
					}
					if ( isset( $data['children'] ) ) {
						$newItem->setUri( '#' );
						$this->_buildMenu( $menu, $data['children'], $newItem );
					} else {

					}
				} else {
				}
			}
		}
	}

}
