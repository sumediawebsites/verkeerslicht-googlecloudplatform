<?php
namespace App\Controller\Admin;

/**
 * NotificationTemplates Controller
 *
 * @property \App\Model\Table\NotificationTemplatesTable $NotificationTemplates
 */
class NotificationTemplatesController extends AdminController {
	public function initialize() {
		parent::initialize();
		$this->loadComponent( 'Bootgrid.Bootgrid', [
				'entity'        => $this->modelClass,
				'contain'       => [
				],
				'searchColumns' => [
					'NotificationTemplates.name',
					'NotificationTemplates.template',
					'NotificationTemplates.title',
					'NotificationTemplates.body',
					'NotificationTemplates.created_by',
					'NotificationTemplates.modified_by',
					'NotificationTemplates.created_at',
					'NotificationTemplates.modified_at',
				],
				'defaultSort'   => [ 'NotificationTemplates.name' => 'ASC' ]
			]
		);
	}


	/**
	 * Index method
	 *
	 * @return \Cake\Network\Response|null
	 */
	public function index() {
		if ( $this->request->is( 'ajax' ) ) {
			$this->Bootgrid->getData();
		} else {
			$notificationTemplates = $this->paginate( $this->NotificationTemplates );

			$users = $this->NotificationTemplates->Users->find( 'list' );
			$this->set( compact( 'notificationTemplate', 'users' ) );

			$this->set( compact( 'notificationTemplates' ) );
			$this->set( '_serialize', [ 'notificationTemplates' ] );
		}
	}

	/**
	 * View method
	 *
	 * @param string|null $id Notification Template id.
	 *
	 * @return \Cake\Network\Response|null
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function view( $id = null ) {
		$notificationTemplate = $this->NotificationTemplates->get( $id, [
			'contain' => [ 'Users' ]
		] );

		$this->set( 'notificationTemplate', $notificationTemplate );
		$this->set( '_serialize', [ 'notificationTemplate' ] );
	}

	/**
	 * Add method
	 *
	 * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
	 */
	public function add() {
		$notificationTemplate = $this->NotificationTemplates->newEntity();
		if ( $this->request->is( 'post' ) ) {
			$notificationTemplate = $this->NotificationTemplates->patchEntity( $notificationTemplate, $this->request->data );
			if ( $this->NotificationTemplates->save( $notificationTemplate ) ) {
				$this->Flash->success( __( 'De notificatietemplate is opgeslagen.' ) );

				return $this->redirect( [ 'action' => 'index' ] );
			} else {
				$this->Flash->error( __( 'De notificatietemplate kon niet worden opgeslagen. Probeer het a.u.b. nog eens.' ) );
			}
		}
		$users = $this->NotificationTemplates->Users->find( 'list', [ 'limit' => 200 ] );
		$this->set( compact( 'notificationTemplate', 'users' ) );
		$this->set( '_serialize', [ 'notificationTemplate' ] );
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Notification Template id.
	 *
	 * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit( $id = null ) {
		$notificationTemplate = $this->NotificationTemplates->get( $id, [
			'contain' => [ 'Users' ]
		] );
		if ( $this->request->is( [ 'patch', 'post', 'put' ] ) ) {
			$notificationTemplate = $this->NotificationTemplates->patchEntity( $notificationTemplate, $this->request->data );
			if ( $this->NotificationTemplates->save( $notificationTemplate ) ) {
				$this->Flash->success( __( 'De notificatietemplate is opgeslagen.' ) );

				return $this->redirect( [ 'action' => 'index' ] );
			} else {
				$this->Flash->error( __( 'De notificatietemplate kon niet worden opgeslagen. Probeer het a.u.b. nog eens.' ) );
			}
		}
		$users = $this->NotificationTemplates->Users->find( 'list', [ 'limit' => 200 ] );
		$this->set( compact( 'notificationTemplate', 'users' ) );
		$this->set( '_serialize', [ 'notificationTemplate' ] );
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Notification Template id.
	 *
	 * @return \Cake\Network\Response|null Redirects to index.
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function delete( $id = null ) {
		$notificationTemplate = $this->NotificationTemplates->get( $id );
		if ( $this->NotificationTemplates->delete( $notificationTemplate ) ) {
			$this->Flash->success( __( 'De notificatietemplate is verwijderd.' ) );
		} else {
			$this->Flash->error( __( 'De notificatietemplate kon niet worden verwijderd. Probeer het a.u.b. nog eens.' ) );
		}

		return $this->redirect( [ 'action' => 'index' ] );
	}
}
