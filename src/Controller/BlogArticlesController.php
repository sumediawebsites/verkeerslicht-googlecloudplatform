<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Search\Manager;

/**
 * BlogArticles Controller
 *
 * @property \App\Model\Table\BlogArticlesTable $BlogArticles
 */
class BlogArticlesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            // This is default config. You can modify "actions" as needed to make
            // the PRG component work only for specified methods.
            'actions' => ['index']
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // Search by friendsofcake
        $query = $this->BlogArticles
            // Use the plugins 'search' custom finder and pass in the
            // processed query params
            ->find('search', ['search' => $this->request->query])
            // You can add extra things to the query if you need to
            ->contain(['BlogCategories'])
            ->where(['title IS NOT' => null]);

        $this->set('isSearch', $this->BlogArticles->isSearch());
        $this->set('blogArticles', $this->paginate($query));

    }

    /**
     * View method
     *
     * @param string|null $id Blog Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $blogArticle = $this->BlogArticles->get($id, [
            'contain' => ['BlogCategories', 'CreatedBy', 'ModifiedBy']
        ]);

        // update number of views
        $blogArticle->views = $blogArticle->views + 1;
        $this->BlogArticles->save($blogArticle);

        $this->set('blogArticle', $blogArticle);
        $this->set('_serialize', ['blogArticle']);
    }

}
