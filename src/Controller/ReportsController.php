<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Settings\Core\Setting;

/**
 * Reports Controller
 *
 * @property \App\Model\Table\ReportsTable $Reports
 */
class ReportsController extends AppController
{

    protected $_helpers = [
        'Auth'
    ];

    public function initialize() {
        parent::initialize();
        $this->Auth->deny( 'complete' );
        $this->Auth->allow( 'index' );
    }

    public function isAuthorized() {
        return true;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Devices', 'ReportStatuses', 'Reporters', 'ReportTypes', 'CreatedBy', 'ModifiedBy']
        ];
        $reports = $this->paginate($this->Reports);

        $this->set(compact('reports'));
        $this->set('_serialize', ['reports']);
    }

    /**
     * Complete method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function complete( $report_id ){

        $report = $this->Reports->get( $report_id );

        if( $report->reporter_id == $this->Auth->user('id') ) {

            if ( $this->request->is('put') ) {
                $this->request->data['date'] = date('d-m-Y', strtotime($this->request->data('date')));

                $report = $this->Reports->patchEntity($report, $this->request->data);

                if ($this->Reports->save($report)) {
                    $this->Flash->success(__('Je melding is bijgewerkt.'));

                    return $this->redirect(['action' => 'thankYou']);
                } else {
                    $this->Flash->error(__('Je melding kon niet worden bijgewerkt.'));
                }

            }
        } else {
            $this->Flash->error(__('Je hebt geen toestemming om deze melding te bewerken.'));
            return $this->redirect($this->Auth->redirectUrl());
        }

        // form validation / pre-fill
        $reportTypes = $this->Reports->ReportTypes->find('list', ['limit' => 200]);

        $this->set(compact('report', 'devices', 'reporters', 'reportTypes' ));
        $this->set('_serialize', ['report']);

    }

    /**
     * Thank You method
     *
     * Shows after a successful completion
     */
    public function thankYou( ){

        // to do

    }

}
