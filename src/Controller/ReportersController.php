<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Settings\Core\Setting;
use Cake\Controller\Component\CookieComponent;

/**
 * Reporters Controller
 *
 * @property \App\Model\Table\ReportsTable $Reporters
 */
class ReportersController extends AppController
{
    protected $_helpers = [
        'Auth'
    ];

    public function initialize() {
        parent::initialize();
        $this->Auth->deny( 'account' );
        $this->Auth->allow( 'add' );

        $this->loadComponent('Cookie', ['expires' => '+10 days', 'httpOnly' => true]);
        $this->Cookie->configKey('Reporter', 'path', '/');
        $this->Cookie->configKey('Reporter', [
            'expires' => '+10 days',
            'httpOnly' => true
        ]);
    }

    public function isAuthorized() {
        return true;
    }

    public function account()
    {
        $reporter = $this->Reporters->get( $this->Auth->user( 'id' ), [
            'contain' => [
                'Reports' => [
                    'ReportStatuses',
                    'ReportTypes'
                ]
            ]
        ]);

        $faqUserQuestionsTable = TableRegistry::get('FaqUserQuestions');
        $contactTable = TableRegistry::get('Contact');

        $faqUserQuestions = $faqUserQuestionsTable->find('threaded', [
            'order' => [ 'FaqUserQuestions.created_at' => 'DESC' ],
            'contain' => [ 'FaqCategories' ],
            'conditions' => [
                'FaqUserQuestions.is_response' => false,
                'FaqUserQuestions.email' => $reporter->email
            ],
        ])->toArray();

        $faqUserResponses = $faqUserQuestionsTable->find('threaded', [
            'order' => [ 'FaqUserQuestions.created_at' => 'DESC' ],
            'contain' => [ 'FaqCategories' ],
            'conditions' => [
                'FaqUserQuestions.is_response' => true,
                'FaqUserQuestions.email' => $reporter->email
            ],
        ])->toArray();

        $contactMessages = $contactTable->find('all', [
            'order' => [ 'Contact.created_at' => 'DESC' ],
            'conditions' => [
                'Contact.is_admin' => false,
                'Contact.email' => $reporter->email
            ],
        ])->toArray();

        $contactResponses = $contactTable->find('all', [
            'order' => [ 'Contact.created_at' => 'DESC' ],
            'conditions' => [
                'Contact.is_admin' => true,
                'Contact.email' => $reporter->email
            ],
        ])->toArray();

        $this->set(compact('reporter', 'faqUserQuestions', 'faqUserResponses', 'contactMessages', 'contactResponses' ));
        $this->set('_serialize', ['reporter', 'faqUserQuestions', 'faqUserResponses', 'contactMessages', 'contactResponses' ]);
    }

    public function login() {
        if( $this->request->is( [ 'post', 'put' ] ) ) {

            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);

//                $this->Cookie->write('Reporters.email', $this->Auth->user('email'));

                // Retrieve user from DB to update it
                if (isset($this->request->data['remember_me'])) {

                    $reporter = $this->Reporters->find('all', [
                        'conditions' => [
                            'Reporters.email' => $this->Auth->user('email')
                        ]
                    ])->first();

                    $reporter->remember_me = true;

                    if ($this->Reporters->save($reporter)) {
                    }
                }

                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('E-mailadres of wachtwoord is incorrect'), [
                    'key' => 'auth'
                ]);
            }

        } else {

            if ( $email = $this->Cookie->read('Reporters.email') ) {

                // Retrieve user from DB
                $user = $this->Reporters->find('all', [
                    'conditions' => [
                        'Reporters.email' => $email
                    ]
                ])->first();

                if ($user->remember_me === true) {
                    $this->Auth->setUser($user);
                }

                return $this->redirect($this->Auth->redirectUrl());
            }
        }
    }

    public function logout() {
        if ( $email = $this->Cookie->read('Reporters.email') ) {
            $this->Cookie->delete('Reporters');
        }
        $this->request->session()->destroy();
        $this->Flash->success( 'Je bent nu uitgelogd.' );

        return $this->redirect( $this->Auth->logout() );
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->Reporters->addBehavior( 'Tools.Passwordable' );

        // TODO: check if reporter already exists

        if ( $this->request->is( 'post' ) ) {

            if ( $reporter = $this->Reporters->find()
                ->where([ 'Reporters.email' => $this->request->data['email'] ])
                ->first()
            ){
                return $this->redirect(['action' => 'forgotPassword', "user_exists", $this->request->data['email'] ]);
            } else {
                $reporter = $this->Reporters->newEntity();
            }

            $reporter = $this->Reporters->patchEntity( $reporter, $this->request->data );

            if ( $this->Reporters->save( $reporter ) ) {
                $this->Flash->success( __( 'Je bent nu ingelogd met je nieuwe account.' ) );

                $reporter->sendWelcomeMail();

                // Log in right away
                $this->Auth->setUser($reporter->toArray());

                return $this->redirect( [ 'action' => 'account' ] );

            } else {
                $this->Flash->error( __( 'Je account kon niet worden aangemaakt. Probeer het a.u.b. nog eens.' ) );
            }

            // Pwd should not be passed to the view again for security reasons
            $reporter->unsetProperty('pwd');
            $reporter->unsetProperty('pwd_repeat');

        } else {
            $reporter = $this->Reporters->newEntity();
        }
        $this->set( compact( 'reporter' ) );
        $this->set( '_serialize', [ 'reporter' ] );
    }

    /**
     *
     * Users thas made a report without an account can activate their auto generated account with this method
     *
     */
    public function confirmRegistration( $reporter_email, $password_token ){
        $this->Reporters->addBehavior('Tools.Passwordable');

        $reporter_email = rawurldecode($reporter_email);

        if ( $this->request->is([ 'patch', 'post', 'put' ]) ) {

            $reporter = $this->Reporters->find()
                ->where(['Reporters.password_token' => $password_token])
                ->andWhere(['Reporters.email' => $reporter_email])
                ->first();

            $reporter = $this->Reporters->patchEntity($reporter, $this->request->data);

            if ($this->Reporters->save($reporter)) {
                $this->Flash->success(__('Je account is geactiveerd.'));

                // TODO: unset password token when done

                // Log in right away
                $this->Auth->setUser($reporter->toArray());
                return $this->redirect(['action' => 'account']);

            } else {
                $this->Flash->error(__('Je account kon niet worden geactiveerd. Probeer het a.u.b. nog eens.'));
            }

            // Pwd should not be passed to the view again for security reasons
            $reporter->unsetProperty('pwd');
            $reporter->unsetProperty('pwd_repeat');

        } else {
            $reporter = $this->Reporters->find()
                ->where(['Reporters.email' => $reporter_email ])
                ->first();
        }

        $this->set( compact( 'reporter' ) );
        $this->set( '_serialize', [ 'reporter' ] );

    }

    /**
     *
     * Forgot password method
     *
     */
    public function forgotPassword( $user_exists = null, $email_exists = null ){

        $this->Reporters->addBehavior('Tools.Passwordable');

//        Steps:
//        0. controller: do nothing
//           view: fill in email address
//        1. controller: catch email address, send token
//           view: fill in token and new password
//        2. controller: update password

        // Step 1
        if ( $this->request->is( [ 'patch', 'post', 'put' ] ) && $this->request->data['step'] == 'email' ) {

            $email = $this->request->data['email'];
            $reporter = $this->Reporters->find()
                ->where(['Reporters.email' => $email])
                ->first();

            $passwordToken = $reporter->createNewPasswordToken();
            $reporter->password_token = $passwordToken;

            if ($this->Reporters->save($reporter)) {

                $reporter->sendPasswordResetTokenMail( $passwordToken );
                $this->Flash->success(__('Er is een e-mail naaar je e-mailadres verzonden.'));

                // continue to form of step 2
                $step = 'token';
                $this->set( compact( 'step' ) );
                $this->set( compact( 'email' ) );

            } else {
                $this->Flash->error(__('Er is iets mis gegaan. Probeer het a.u.b. nog eens.'));
            }

        // Step 2
        } elseif ( $this->request->is( [ 'patch', 'post', 'put' ] ) && $this->request->data['step'] == 'token' ){

            if ($this->request->data['password_token'] != null) {

                $password_token = $this->request->data['password_token'];
                $email = $this->request->data['email'];

                $reporter = $this->Reporters->find()
                    ->where([ 'Reporters.password_token' => $password_token ])
                    ->andWhere([  'Reporters.email' => $email  ])
                    ->first();

                $reporter = $this->Reporters->patchEntity($reporter, $this->request->data);

                if ($this->Reporters->save($reporter)) {
                    $this->Flash->success(__('Je wachtwoord is gewijzigd.'));

                    // TODO: unset password token when done

                    // Log in right away
                    $this->Auth->setUser($reporter->toArray());
                    return $this->redirect(['action' => 'account']);
                } else {
                    $this->Flash->error(__('Je wachtwoord kon niet worden gewijzigd. Probeer het a.u.b. nog eens.'));
                    $step = 'token';
                    $this->set( compact( 'step' ) );
                }

                // Pwd should not be passed to the view again for security reasons
                $reporter->unsetProperty('pwd');
                $reporter->unsetProperty('pwd_repeat');
            }

        }

        $this->set( compact( 'reporter', 'user_exists', 'email_exists' ) );
        $this->set( '_serialize', [ 'reporter' ] );

    }

    /**
     *
     * Change password method
     *
     */
    public function changePassword(){
        $this->Reporters->addBehavior( 'Tools.Passwordable' );
        $reporter = $this->Reporters->get( $this->Auth->user( 'id' ) );

        if ( $this->request->is( [ 'patch', 'post', 'put' ] ) ) {
            $reporter = $this->Reporters->patchEntity($reporter, $this->request->data);

            if ($this->Reporters->save($reporter)) {
                $this->Flash->success(__('Je wachtwoord is gewijzigd.'));

                // Log in right away
                $this->Auth->setUser($reporter->toArray());
                return $this->redirect(['action' => 'account']);
            } else {
                $this->Flash->error(__('Je wachtwoord kon niet worden gewijzigd. Probeer het a.u.b. nog eens.'));
            }

            // Pwd should not be passed to the view again for security reasons
            $reporter->unsetProperty('pwd');
            $reporter->unsetProperty('pwd_repeat');
        }
        $this->set( compact( 'reporter' ) );
        $this->set( '_serialize', [ 'reporter' ] );

    }

}
