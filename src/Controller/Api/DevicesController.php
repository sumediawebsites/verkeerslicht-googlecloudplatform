<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Settings\Core\Setting;

/**
 * Devices Controller
 *
 * @property \App\Model\Table\DevicesTable $Devices
 */
class DevicesController extends ApiController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->RequestHandler->renderAs($this, 'json');

        $devices = $this->Devices->find( 'all', [
            'contain' => [
                'Locations'
            ]
        ]);

        $this->set(compact('devices'));
        $this->set('_serialize', ['devices']);
    }


}
