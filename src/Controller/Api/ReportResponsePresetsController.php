<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Settings\Core\Setting;

/**
 * Reports Controller
 *
 * @property \App\Model\Table\ReportsTable $Reports
 */
class ReportResponsePresetsController extends ApiController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * @param $id
     */
    public function get($id)
    {
        $this->RequestHandler->renderAs($this, 'json');

        $reportResponsePreset = $this->ReportResponsePresets->get($id);

        $this->set(compact('reportResponsePreset'));
        $this->set('_serialize', ['reportResponsePreset']);
    }
}
