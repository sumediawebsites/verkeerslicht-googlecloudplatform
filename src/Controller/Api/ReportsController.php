<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Settings\Core\Setting;

use Google\Auth\Credentials\GCECredentials;
use App\DataModel\CloudSql;
use App\DataModel\Datastore;
use App\DataModel\MongoDb;
use App\DataModel\DataModelInterface;

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Symfony\Component\Yaml\Yaml;

/**
 * Reports Controller
 *
 * @property \App\Model\Table\ReportsTable $Reports
 */
class ReportsController extends ApiController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Reporters'); // ADDED by wvh
        $this->loadModel('ReportStatuses'); // ADDED by wvh
        $this->loadModel('ReportTypes'); // ADDED by wvh
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index( $offset = 0, $limit = 25 )
    {
        $this->RequestHandler->renderAs($this, 'json');

        $count = $reports = $this->Reports->find( 'all', [
            'conditions' => [
                'device_id IS NOT' => null,
                'report_type_id IS NOT' => null,
            ]
        ])->count();

        $reports = $this->Reports->find( 'all', [
            'contain' => [
                'Devices' => [
                    'Locations'
                ],
                'ReportStatuses',
                'ReportTypes'
            ],
            'conditions' => [
                'device_id IS NOT' => null,
                'report_type_id IS NOT' => null,
            ],
            'order' => [
                'date' => 'DESC'
            ],
            'limit' => $limit,
            'offset' => $offset
        ]);
        $this->set(compact('reports', 'count'));
        $this->set('_serialize', ['reports', 'count']);
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {
        $this->RequestHandler->renderAs($this, 'json');

        if ($this->request->is('post')) {
            // catch post data
            $body = $this->request->data('body');
            $reporter_email = $this->request->data('reporter_email');
            $reporter_firstname = $this->request->data('reporter_firstname');
            $reporter_lastname = $this->request->data('reporter_lastname');
            $latitude = $this->request->data('latitude');
            $longitude = $this->request->data('longitude');
            $vri_place = $this->request->data('place');
            $vri_street_1 = $this->request->data('streetname1');
            $vri_street_2 = $this->request->data('streetname2');
            $date = $this->request->data('date');
            $time = $this->request->data('time');
            $vehicle = $this->request->data('vehicle');
            $keep_me_informed = $this->request->data('keep_me_informed');
            $report_type_id = $this->request->data('radio_problem');

            // security fallback
            if( $reporter_email == null OR $reporter_email == ''){
                die('error');
            }

            // make up some data
            $created_at = date("Y-m-d H:i:s");
            $queue_status = "queued";

            $app = new Application(); // Silex\Application

            // parse configuration
            $config = getenv('REPORT_CONFIG') ?:
                __DIR__ . '/../../../config/' . 'settings.yml';

            $app['config'] = Yaml::parse(file_get_contents($config));

            // determine the datamodel backend using the app configuration
            $app['report.model'] = function ($app) {
                /** @var array $config */
                $config = $app['config'];
                
                if (empty($config['reports_backend'])) {
                    throw new \DomainException('"reports_backend" must be set in report config');
                }

                // Data Model
                switch ($config['reports_backend']) {
                    case 'mongodb':
                        return new MongoDb(
                            $config['mongo_url'],
                            $config['mongo_database'],
                            $config['mongo_collection']
                        );
                    case 'datastore':
                        return new Datastore(
                            $config['google_project_id']
                        );
                    case 'cloudsql':
                        // Add Unix Socket for CloudSQL 2nd Gen when applicable
                        $socket = GCECredentials::onGce()
                            ? ';unix_socket=/cloudsql/' . $config['cloudsql_connection_name']
                            : '';
                        return new CloudSql(
                            $config['mysql_dsn'] . $socket,
                            $config['mysql_user'],
                            $config['mysql_password']
                        );
                    default:
                        throw new \DomainException("Invalid \"reports_backend\" given: $config[reports_backend]. "
                            . "Possible values are cloudsql, mongodb, or datastore.");
                }
            };

            // add service parameters
            $app['report.page_size'] = 10;

            /** @var DataModelInterface $model */
            $model = $app['report.model'];

            $report = array(); // Add info to report here, an associative array

            $report['reporter_email'] = $reporter_email;
            $report['reporter_firstname'] = $reporter_firstname;
            $report['reporter_lastname'] = $reporter_lastname;
            $report['report_type_id'] = $report_type_id;
            $report['body'] = $body;
            $report['created_at'] = $created_at;
            $report['queue_status'] = $queue_status;
            $report['latitude'] = $latitude;
            $report['longitude'] = $longitude;
            $report['vri_place'] = $vri_place;
            $report['vri_street_1'] = $vri_street_1;
            $report['vri_street_2'] = $vri_street_2;
            $report['date'] = $date;
            $report['time'] = $time;
            $report['vehicle'] = $vehicle;
            $report['keep_me_informed'] = $keep_me_informed;

            $id = $model->create($report);

            $this->Flash->set( __('Bedankt voor uw melding.'), [
                'element' => 'success'
            ]);

            $response = 'success';

        } else {
            $response = 'error';
        }

        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
     * Transfer method
     *
     * Transfers all reports in MongoDB with queue status 'queued' to MySQL (CloudSQL)
     * This function can be called by the following GET request:
     * https://verkeerslicht-165607.appspot.com/api/reports/transfer
     *
     * This call can be used in a cronjob, to automate the transfer(s)
     * See Google Cloud Platform, AppEngine, Task Queues, CronJob
     * The settings for a cron job are kept in the file cron.yaml
     * in the root of this repository. An example 'Hello World' is supplied.
     * NOTE: You have to refer to the cron.yaml from app.yaml
     * Deploy the cron.yaml as follows:
     * gcloud app deploy cron.yaml
     *
     * @return void
     */
    public function transfer()
    {
        $this->RequestHandler->renderAs($this, 'json');

        if ($this->request->is('get')) {

            $app = new Application(); // Silex\Application

            // Set vars
            $entryCount = 0;
            $processedCount = 0;

            // parse configuration
            $config = getenv('REPORT_CONFIG') ?:
                __DIR__ . '/../../../config/' . 'settings.yml';

            $app['config'] = Yaml::parse(file_get_contents($config));

            // determine the datamodel backend using the app configuration
            $app['report.model'] = function ($app) {
                /** @var array $config */
                $config = $app['config'];
                
                if (empty($config['reports_backend'])) {
                    throw new \DomainException('"reports_backend" must be set in report config');
                }

                // Data Model
                switch ($config['reports_backend']) {
                    case 'mongodb':
                        return new MongoDb(
                            $config['mongo_url'],
                            $config['mongo_database'],
                            $config['mongo_collection']
                        );
                    case 'datastore':
                        return new Datastore(
                            $config['google_project_id']
                        );
                    case 'cloudsql':
                        // Add Unix Socket for CloudSQL 2nd Gen when applicable
                        $socket = GCECredentials::onGce()
                            ? ';unix_socket=/cloudsql/' . $config['cloudsql_connection_name']
                            : '';
                        return new CloudSql(
                            $config['mysql_dsn'],
                            $config['mysql_user'],
                            $config['mysql_password']
                        );
                    default:
                        throw new \DomainException("Invalid \"reports_backend\" given: $config[reports_backend]. "
                            . "Possible values are cloudsql, mongodb, or datastore.");
                }
            };

            /** @var DataModelInterface $model */
            $model = $app['report.model'];

            $reports = $model->listReports(9999); // Limit set to 9999, intended to request ALL          

            // Filter the reports by queue_status, 
 
            /*  EXAMPLE OF $reports
                "reports": [
                {
                    "id": "5919d06c7b441f0023152762",
                    "reporter_email": "willem%2B3%40sumedia.nl",
                    "reporter_firstname": "Willem",
                    "reporter_lastname": "van+Heemstra",
                    "report_type_id": "4",
                    "body": "Geen+opmerkingen",
                    "created_at": "2017-05-15 15:59:40",
                    "queue_status": "queued",
                    "latitude": "52.141964099999996",
                    "longitude": "6.202609100000018",
                    "vri_place": "Zutphen%2C+Nederland",
                    "vri_street_1": "A",
                    "vri_street_2": "B",
                    "date": "2017-05-13",
                    "time": "14%3A22",
                    "vehicle": "scooter",
                    "keep_me_informed": "1"
                },
                {
                    "id": "5919d0c3440b0c002419b9b2",
                    "reporter_email": "willem+3@sumedia.nl",
                    "reporter_firstname": "Willem",
                    "reporter_lastname": "van Heemstra",
                    "report_type_id": "2",
                    "body": "Niets nieuws.",
                    "created_at": "2017-05-15 16:01:07",
                    "queue_status": "queued",
                    "latitude": "52.3670267",
                    "longitude": "6.668491899999935",
                    "vri_place": "Almelo, Buurstede, 7603 BS Almelo, Nederland",
                    "vri_street_1": "C",
                    "vri_street_2": "D",
                    "date": "2017-05-15",
                    "time": "17:58",
                    "vehicle": "motorcycle",
                    "keep_me_informed": "1"
                },
                {
                    "id": "591aa686440b0c002335b6f3",
                    "reporter_email": "willem%2B3%40sumedia.nl",
                    "reporter_firstname": "Willem",
                    "reporter_lastname": "van+Heemstra",
                    "report_type_id": "4",
                    "body": "Geen+opmerkingen",
                    "created_at": "2017-05-16 07:13:10",
                    "queue_status": "queued",
                    "latitude": "52.141964099999996",
                    "longitude": "6.202609100000018",
                    "vri_place": "Zutphen%2C+Nederland",
                    "vri_street_1": "A",
                    "vri_street_2": "B",
                    "date": "",
                    "time": "14%3A22",
                    "vehicle": "scooter",
                    "keep_me_informed": "1"
                },
                {
                    "id": "591ab8a87b441f0023152763",
                    "reporter_email": "patrick%2B3%40robotkittens.nl",
                    "reporter_firstname": "Robot",
                    "reporter_lastname": "Kittens",
                    "report_type_id": "4",
                    "body": "Dit+is+een+test+van+Robot+Kittens",
                    "created_at": "2017-05-16 08:30:32",
                    "queue_status": "queued",
                    "latitude": "52.141964099999996",
                    "longitude": "6.202609100000018",
                    "vri_place": "Zutphen%2C+Nederland",
                    "vri_street_1": "A",
                    "vri_street_2": "B",
                    "date": "2017-05-13",
                    "time": "14%3A22",
                    "vehicle": "scooter",
                    "keep_me_informed": "1"
                },
                {
                    "id": "591ada60d1389b00240c1df2",
                    "reporter_email": "daan@sumedia.nl",
                    "reporter_firstname": "Daan",
                    "reporter_lastname": "Tutert",
                    "report_type_id": "2",
                    "body": "Duurde lang.",
                    "created_at": "2017-05-16 10:54:24",
                    "queue_status": "queued",
                    "latitude": "52.37228115010074",
                    "longitude": "5.974127156250006",
                    "vri_place": "Epe, Nederland",
                    "vri_street_1": "de a",
                    "vri_street_2": "straat b",
                    "date": "2017-05-16",
                    "time": "12:52",
                    "vehicle": "car",
                    "keep_me_informed": "1"
                }
                ],
                "cursor": null
            */

            // FILTER REPORTS WHERE "queue_status" == "queued"
            $filtered_reports = array(
                "reports" => array_filter($reports["reports"], function($element) {
                    return $element["queue_status"] == "queued"; 
                })
            );

            // Connect to cake / mySQL DB
            $modelCloudSql = new CloudSql(
                    //"mysql:unix_socket=/cloudsql/verkeerslicht-165607:europe-west1:verkeerslicht;dbname=verkeerslicht",
                    $app['config']['mysql_dsn'],
                    //"root",
                    $app['config']['mysql_user'],
                    //"secret"
                    $app['config']['mysql_password']                  
                );

            $reportsFromCloudSql = $modelCloudSql->listReports(9999); // WORKS!!

            $cursor = $filtered_reports["reports"]; // A list of reports

            foreach ($cursor as $id => $object) {
                $entryCount++;  // WORKS !!!

                /*
                EXAMPLE OF $object
                    "id": "591ada60d1389b00240c1df2",
                    "reporter_email": "daan@sumedia.nl",
                    "reporter_firstname": "Daan",
                    "reporter_lastname": "Tutert",
                    "report_type_id": "2",
                    "body": "Duurde lang.",
                    "created_at": "2017-05-16 10:54:24",
                    "queue_status": "queued",
                    "latitude": "52.37228115010074",
                    "longitude": "5.974127156250006",
                    "vri_place": "Epe, Nederland",
                    "vri_street_1": "de a",
                    "vri_street_2": "straat b",
                    "date": "2017-05-16",
                    "time": "12:52",
                    "vehicle": "car",
                    "keep_me_informed": "1"
                */

                // Get Mongo vars
                $mongo_id = $object["id"];  // WAS _id
                $created_at = $object["created_at"];

                $reporter_email = $object["reporter_email"];
                $reporter_firstname = $object["reporter_firstname"];
                $reporter_lastname = $object["reporter_lastname"];
                $keep_me_informed = $object["keep_me_informed"];

                $report_type_id = $object["report_type_id"];
                $body = $object["body"];
                $latitude = $object["latitude"];
                $longitude = $object["longitude"];
                $vri_place = $object["vri_place"];
                $vri_street_1 = $object["vri_street_1"];
                $vri_street_2 = $object["vri_street_2"];

                $date = $object["date"];
                if($date !== '') {
                    $date = substr($date,0,4)."-".substr($date,5,2)."-".substr($date,8,2); // Manipulate to date format to YYYY-MM-DD (leave time out)
                }

                $time = $object["time"];
                if($time !== '') {                
                    $time = substr($time,0,2).":".substr($time,-2); // Manipulate to time format to HH:MM (leave seconds out!), this handles encoded strings successfully
                }

                $vehicle = $object["vehicle"];

                // determine if the report is complete or not
                if ( $vehicle == null OR $vehicle == '' ){
                    $report_is_completed = false;
                    //$this->out( "Report is not complete." );
                } else {
                    $report_is_completed = true;
                    //$this->out( "Report is complete." );
                }

                // check if user already exists, if not: create new user
                $exists = $this->Reporters->exists(['Reporters.email' => $reporter_email]); // WORKS!!

                if ($exists) {
                    // add report to existing user

                    //$this->out("User " . $reporter_email . " already exists.");

                    $reporter = $this->Reporters->find()
                        ->where(['Reporters.email' => $reporter_email])
                        ->first();
                    $reporter_id = $reporter["id"];

                    /* EXAMPLE OF $reporter
                        "id": 44,
                        "initials": null,
                        "firstname": "",
                        "middlename": null,
                        "lastname": null,
                        "username": null,
                        "phone": null,
                        "email": "daan@sumedia.nl",
                        "settings": null,
                        "picture": null,
                        "is_active": true,
                        "created_by": null,
                        "modified_by": null,
                        "created_at": "2017-02-13T13:32:39+00:00",
                        "modified_at": "2017-02-13T13:32:39+00:00",
                        "deleted": null,
                        "password_token": "hv8LPOewkZEh",
                        "remember_me": null,
                        "modifiedBy": null,
                        "createdBy": null,
                        "fullname": ""
                    */
                } 

                else {
                    // create new user, send them a registration email, add report to this new user

                    //$this->out("User " . $reporter_email . " does not exist.");

                    $reporter = $this->Reporters->newEntity(); // WORKS !!
                    $reporter["email"] = $reporter_email;
                    $reporter["firstname"] = $reporter_firstname;
                    $reporter["lastname"] = $reporter_lastname;

                    $passwordToken = $reporter->createNewPasswordToken();
                    $reporter["password_token"] = $passwordToken;

                    if ($this->Reporters->save($reporter)) {
                        $reporter->sendConfirmRegistrationMail($passwordToken); // DOES IT WORK??
                        //$this->out("<success>Password token has been sent to: " . $reporter_email . "</success>");
                        $reporter_id = $reporter["id"]; // WORKS!!
                    } else {
                        //$this->out("<error>Error: Could not create Reporter.</error>");
                    }
                }

                // Set vars to Cake entity
                $report = array(); // Placeholder, an associative array

                $reportStatusName = 'Nieuwe melding';
                $reportStatusEntity = $this->ReportStatuses->find()
                    ->where(['ReportStatuses.name' => $reportStatusName])
                    ->first();

                $reportStatusId = $reportStatusEntity["id"];

                $report["report_status_id"] = $reportStatusId;
                $report["reporter_id"] = $reporter_id;
                $report["report_type_id"] = $report_type_id;
                $report["body"] = $body;
                $report["created_at"] = $created_at;
                $report["latitude"] = $latitude;
                $report["longitude"] = $longitude;
                $report["vri_place"] = $vri_place;
                $report["vri_street_1"] = $vri_street_1;
                $report["vri_street_2"] = $vri_street_2;
                $report["date"] = $date;
                $report["time"] = $time;
                $report["vehicle"] = $vehicle;
                $report["keep_me_informed"] = $keep_me_informed;

                /*
                    EXAMPLE OF $report
                    "report_status_id": 3,
                    "reporter_id": 44,
                    "report_type_id": "2",
                    "body": "Duurde lang.",
                    "created_at": "2017-05-16 10:54:24",
                    "latitude": "52.37228115010074",
                    "longitude": "5.974127156250006",
                    "vri_place": "Epe, Nederland",
                    "vri_street_1": "de a",
                    "vri_street_2": "straat b",
                    "date": "2017-05-16",
                    "time": "12:52",
                    "vehicle": "car",
                    "keep_me_informed": "1"
                */

                // Save report in CloudSQL, returns id of saved report
                $created_report_id = $modelCloudSql->create($report); // Store the new report in CloudSQL, WORKS!!!

                if(is_numeric($created_report_id)) {
                    // Update queue status of entry
                    $queue_status = "processed";
                    $object["queue_status"] = $queue_status;
                    $modifiedCount = $model->update($object);

                    $processedCount++;
                } else {
                    //$this->out( "<error>Error, saving entity.</error>" );
                };

                // remind the reporter to finish their report, if it was not complete yet.
                if ( $report_is_completed === false && is_numeric($created_report_id) ){

                     $response = debug($created_report_id); // TESTING WITH created_report_id

// TEMP COMMENTED OUT                    $reporter->sendCompleteReportMail( $report["id"] ); // CAUSES AN ERROR, MOST LIKELY BECAUSE $report["id"] is null, USE $created_report_id INSTEAD
// TEMP COMMENTED OUT                    $reporter->sendCompleteReportMail( $created_report_id ); // TRY THIS, IT FAILS TOO ... WHY?

                    //$this->out("<success>A reminder has been sent to " . $reporter_email . "</success>");
                }
            }

            // Output the filtered array
            // $response = debug($filtered_reports); // TESTING WITH filtered_reports
            // $response = debug($reportsFromCloudSql); // TESTING WITH reportsFromCloudSql
            // $response = debug($created_report_id); // TESTING WITH created_report_id
            // $response = debug($report); // TESTING WITH report 
            // $response = debug($processedCount); // TESTING WITH processedCount
            // $response = debug($report_is_completed); // TESTING WITH report_is_completed
            // $response = debug($report); // TESTING WITH report
            if($response != '') {
                $response = 'success';
            }

        } else {
            $response = 'error';
        }

        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

}
