<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * BlogCategories Controller
 *
 * @property \App\Model\Table\BlogCategoriesTable $BlogCategories
 */
class BlogCategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentBlogCategories', 'CreatedBy', 'ModifiedBy']
        ];
        $blogCategories = $this->paginate($this->BlogCategories);

        $this->set(compact('blogCategories'));
        $this->set('_serialize', ['blogCategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Blog Category id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $blogCategory = $this->BlogCategories->get($id, [
            'contain' => ['ParentBlogCategories', 'CreatedBy', 'ModifiedBy', 'BlogArticles', 'ChildBlogCategories']
        ]);

        $blogArticles = TableRegistry::get('blogArticles');
        $blogArticles = $blogArticles->find();

        $this->set(compact('blogCategory', 'blogArticles'));
        $this->set('_serialize', ['blogCategory']);
    }

}
