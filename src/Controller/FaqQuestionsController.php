<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Search\Manager;

/**
 * FaqQuestions Controller
 *
 * @property \App\Model\Table\FaqQuestionsTable $FaqQuestions
 */
class FaqQuestionsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            // This is default config. You can modify "actions" as needed to make
            // the PRG component work only for specified methods.
            'actions' => ['index']
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
//        $faqCategories = TableRegistry::get('faqCategories');
//
//        $faqCategories = $faqCategories->find('all', [
//            'contain' => ['CreatedBy', 'ModifiedBy'],
//            'order' => ['faqCategories.modified_at' => 'DESC']
//        ])->toList();

        $faqCategories = $this->FaqQuestions->FaqCategories->find('list', ['limit' => 200]);


        // Search by friendsofcake
        $query = $this->FaqQuestions
            ->find('search', ['search' => $this->request->query])
            ->contain(['FaqCategories'])
            ->where(['title IS NOT' => null, 'highlighted' => true]);

        $faqQuestions = $this->paginate($query);

        $this->set('isSearch', $this->FaqQuestions->isSearch());
        $this->set(compact('faqQuestions', 'faqCategories'));
        $this->set('_serialize', ['faqQuestions', 'faqCategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Faq Question id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faqQuestion = $this->Faqquestions->get($id, [
            'contain' => ['FaqCategories']
        ]);

        $this->set('faqQuestion', $faqQuestion);
        $this->set('_serialize', ['faqQuestion']);
    }

}
