<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Settings\Core\Setting;

/**
 * Contact Controller
 *
 * @property \App\Model\Table\ContactTable $Contact
 */
class ContactController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function index()
    {
        $contact = $this->Contact->newEntity();
        $this->set(compact('contact'));
        $this->set('_serialize', ['contact']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contact = $this->Contact->newEntity();
        if ($this->request->is('post')) {

            $data = $this->request->data;
            $contact = $this->Contact->patchEntity($contact, $this->request->data);

            if ($this->Contact->save($contact)) {
                // Send an email.
                $email = new Email();
                $email->emailFormat( 'html' )
                    ->from( $data['email'] )
                    ->to( Setting::Read( 'App.Email.DefaultSenderEmail' ) )
                    ->subject( $data['subject'] );
                $email->template( 'default', 'contact' );
                $email->viewVars( [
                    'firstname'  => $data['firstname'],
                    'lastname'  => $data['lastname'],
                    'email'  => $data['email'],
                    'subject'  => $data['subject'],
                    'body'     => $data['message']
                ] );
                $email->send();

                $this->Flash->success(__('Het bericht is verzonden.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Het bericht kon niet worden opgeslagen. Probeer het a.u.b. nog eens.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('contact'));
        $this->set('_serialize', ['contact']);
    }

}
