<?php

/*
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\DataModel;

/**
 * The common model implemented by Google Datastore, mysql, etc.
 */
interface DataModelInterface
{
    /**
     * Lists all the reports in the data model.
     * Cannot simply be called 'list' due to PHP keyword collision.
     *
     * @param int  $limit  How many reports will we fetch at most?
     * @param null $cursor Returned by an earlier call to listReports().
     *
     * @return array ['reports' => array of associative arrays mapping column
     *               name to column value,
     *               'cursor' => pass to next call to listReports() to fetch
     *               more reports]
     */
    public function listReports($limit = 10, $cursor = null);

    /**
     * Creates a new report in the data model.
     *
     * @param $report array  An associative array.
     * @param null $id integer  The id, if known.
     *
     * @return mixed The id of the new report.
     */
    public function create($report, $id = null);

    /**
     * Reads a report from the data model.
     *
     * @param $id  The id of the report to read.
     *
     * @return mixed An associative array representing the report if found.
     *               Otherwise, a false value.
     */
    public function read($id);

    /**
     * Updates a report in the data model.
     *
     * @param $report array  An associative array representing the report.
     * @param null $id The old id of the report.
     *
     * @return int The number of reports updated.
     */
    public function update($report);

    /**
     * Deletes a report from the data model.
     *
     * @param $id  The report id.
     *
     * @return int The number of reports deleted.
     */
    public function delete($id);
}
