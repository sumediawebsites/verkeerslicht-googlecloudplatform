<?php

/*
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\DataModel;

use Google\Cloud\Datastore\DatastoreClient;
use Google\Cloud\Datastore\Entity;
use App\DataModel\DataModelInterface;

/**
 * Class Datastore implements the DataModel with a Google Data Store.
 */
class Datastore implements DataModelInterface
{
    private $datasetId;
    private $datastore;
    protected $columns = [
        'id'                    => 'integer',
        'reporter_email'        => 'string',
        'reporter_firstname'    => 'string',
        'reporter_lastname'     => 'string',
        'report_type_id'        => 'string',
        'body'                  => 'string',
        'created_at'            => 'timestamp',
        'queue_status'          => 'string',
        'latitude'              => 'string',
        'longitude'             => 'string',
        'vri_place'             => 'string',
        'vri_street_1'          => 'string',
        'vri_street_2'          => 'string',
        'date'                  => 'string',
        'time'                  => 'string',
        'vehicle'               => 'string',
        'keep_me_informed'      => 'string',
    ];

    public function __construct($projectId)
    {
        $this->datasetId = $projectId;
        $this->datastore = new DatastoreClient([
            'projectId' => $projectId,
        ]);
    }

    public function listReports($limit = 10, $cursor = null)
    {
        $query = $this->datastore->query()
            ->kind('Report')
            ->order('created_at')
            ->limit($limit)
            ->start($cursor);

        $results = $this->datastore->runQuery($query);

        $reports = [];
        $nextPageCursor = null;
        foreach ($results as $entity) {
            $report = $entity->get();
            $report['id'] = $entity->key()->pathEndIdentifier();
            $reports[] = $report;
            $nextPageCursor = $entity->cursor();
        }

        return [
            'reports' => $reports,
            'cursor' => $nextPageCursor,
        ];
    }

    public function create($report, $key = null)
    {
        $this->verifyReport($report);

        $key = $this->datastore->key('Report');
        $entity = $this->datastore->entity($key, $report);

        $this->datastore->insert($entity);

        // return the ID of the created datastore entity
        return $entity->key()->pathEndIdentifier();
    }

    public function read($id)
    {
        $key = $this->datastore->key('Report', $id);
        $entity = $this->datastore->lookup($key);

        if ($entity) {
            $report = $entity->get();
            $report['id'] = $id;
            return $report;
        }

        return false;
    }

    public function update($report)
    {
        $this->verifyReport($report);

        if (!isset($report['id'])) {
            throw new \InvalidArgumentException('Report must have an "id" attribute');
        }

        $transaction = $this->datastore->transaction();
        $key = $this->datastore->key('Report', $report['id']);
        $task = $transaction->lookup($key);
        unset($report['id']);
        $entity = $this->datastore->entity($key, $report);
        $transaction->upsert($entity);
        $transaction->commit();

        // return the number of updated rows
        return 1;
    }

    public function delete($id)
    {
        $key = $this->datastore->key('Report', $id);
        return $this->datastore->delete($key);
    }

    private function verifyReport($report)
    {
        if ($invalid = array_diff_key($report, $this->columns)) {
            throw new \InvalidArgumentException(sprintf(
                'unsupported report properties: "%s"',
                implode(', ', $invalid)
            ));
        }
    }
}
