<?php
/*
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\DataModel;

use PDO;
use App\DataModel\DataModelInterface;

/**
 * Class CloudSql implements the DataModelInterface with a mysql database.
 *
 */
class CloudSql implements DataModelInterface
{
    private $dsn;
    private $user;
    private $password;

    /**
     * Creates the SQL reports table if it doesn't already exist.
     */
    public function __construct($dsn, $user, $password)
    {
        $this->dsn = $dsn;
        $this->user = $user;
        $this->password = $password;

        $columns = array(
            'id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ',
            'device_id INT(11)',
            'report_status_id INT(11)',
            'reporter_id INT(11)',
            // 'reporter_email VARCHAR(255)',
            // 'reporter_firstname VARCHAR(255)',
            // 'reporter_lastname VARCHAR(255)',
            'report_type_id INT(11)',
            'title VARCHAR(255)',  
            'body VARCHAR(255)',
            'created_by INT(11)',
            'modified_by INT(11)',     
            'created_at DATE',
            'modified_at DATE',
            'deleted DATE',
            // 'queue_status VARCHAR(255)',
            'longitude VARCHAR(255)',
            'latitude VARCHAR(255)',
            'vri_place VARCHAR(255)',
            'vri_street_1 VARCHAR(255)',
            'vri_street_2 VARCHAR(255)',
            'vehicle VARCHAR(255)',
            'date DATE',
            'time TIME',
            'fromstreet VARCHAR(255)',
            'tostreet VARCHAR(255)',
            'keep_me_informed INT(1)',
        );

        $this->columnNames = array_map(function ($columnDefinition) {
            return explode(' ', $columnDefinition)[0];
        }, $columns);
        $columnText = implode(', ', $columns);
        $pdo = $this->newConnection();
        $pdo->query("CREATE TABLE IF NOT EXISTS reports ($columnText)");
    }

    /**
     * Creates a new PDO instance and sets error mode to exception.
     *
     * @return PDO
     */
    private function newConnection()
    {
        $pdo = new PDO($this->dsn, $this->user, $this->password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }

    /**
     * Throws an exception if $report contains an invalid key.
     *
     * @param $report array
     *
     * @throws \Exception
     */
    private function verifyReport($report)
    {
        if ($invalid = array_diff_key($report, array_flip($this->columnNames))) {
            throw new \Exception(sprintf(
                'unsupported report properties: "%s"',
                implode(', ', $invalid)
            ));
        }
    }

    public function listReports($limit = 10, $cursor = null)
    {
        $pdo = $this->newConnection();
        if ($cursor) {
            $query = 'SELECT * FROM reports WHERE id > :cursor ORDER BY id' .
                ' LIMIT :limit';
            $statement = $pdo->prepare($query);
            $statement->bindValue(':cursor', $cursor, PDO::PARAM_INT);
        } else {
            $query = 'SELECT * FROM reports ORDER BY id LIMIT :limit';
            $statement = $pdo->prepare($query);
        }
        $statement->bindValue(':limit', $limit + 1, PDO::PARAM_INT);
        $statement->execute();
        $rows = array();
        $last_row = null;
        $new_cursor = null;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if (count($rows) == $limit) {
                $new_cursor = $last_row['id'];
                break;
            }
            array_push($rows, $row);
            $last_row = $row;
        }

        return array(
            'reports' => $rows,
            'cursor' => $new_cursor,
        );
    }

    public function create($report, $id = null)
    {
        $this->verifyReport($report);
        if ($id) {
            $report['id'] = $id;
        }
        $pdo = $this->newConnection();
        $names = array_keys($report);
        $placeHolders = array_map(function ($key) {
            return ":$key";
        }, $names);
        $sql = sprintf(
            'INSERT INTO reports (%s) VALUES (%s)',
            implode(', ', $names),
            implode(', ', $placeHolders)
        );
        $statement = $pdo->prepare($sql);
        $statement->execute($report);

        return $pdo->lastInsertId();
    }

    public function read($id)
    {
        $pdo = $this->newConnection();
        $statement = $pdo->prepare('SELECT * FROM reports WHERE id = :id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function update($report)
    {
        $this->verifyReport($report);
        $pdo = $this->newConnection();
        $assignments = array_map(
            function ($column) {
                return "$column=:$column";
            },
            $this->columnNames
        );
        $assignmentString = implode(',', $assignments);
        $sql = "UPDATE reports SET $assignmentString WHERE id = :id";
        $statement = $pdo->prepare($sql);
        $values = array_merge(
            array_fill_keys($this->columnNames, null),
            $report
        );
        return $statement->execute($values);
    }

    public function delete($id)
    {
        $pdo = $this->newConnection();
        $statement = $pdo->prepare('DELETE FROM reports WHERE id = :id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();

        return $statement->rowCount();
    }
}
