<?php

/*
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace App\DataModel;

use \MongoDB\BSON\ObjectId;
use App\DataModel\DataModelInterface;

/**
 * Class MongoDb implements the DataModel with MongoDB.
 *
 * To use this class, set two environment variables; MONGO_URL and
 * MONGO_NAMESPACE.
 */
class MongoDb implements DataModelInterface
{
    /**
     * Properties of the reports.
     *
     * @var array
     */
    protected $columns = array(
        'id',
        'reporter_email',
        'reporter_firstname',
        'reporter_lastname',
        'report_type_id',
        'body',
        'created_at',
        'queue_status',
        'latitude',
        'longitude',
        'vri_place',
        'vri_street_1',
        'vri_street_2',
        'date',
        'time',
        'vehicle',
        'keep_me_informed',
    );

    /**
     * MongoDB collection.
     *
     * @var \MongoDB\Collection
     */
    private $db;

    /**
     * Connects to the MongoDB server.
     */
    public function __construct($dbUrl, $database, $collection)
    {
        $manager = new \MongoDB\Driver\Manager($dbUrl);
        $this->db = new \MongoDB\Collection($manager, $database, $collection);
    }

    /**
     * @see DataModelInterface::listReports
     */
    public function listReports($limit = 10, $cursor = null)
    {
        if ($cursor) {
            $q = $this->db->find(
                array('_id' => array('$gt' => new ObjectId($cursor))),
                array('sort' => array('_id' => 1))
            );
        } else {
            $q = $this->db->find(
                array(),
                array('sort' => array('_id' => 1))
            );
        }
        $rows = array();
        $last_row = null;
        $new_cursor = null;
        foreach ($q as $row) {
            if (count($rows) == $limit) {
                $new_cursor = (string) ($last_row->_id);
                break;
            }
            array_push($rows, $this->reportToArray($row));
            $last_row = $row;
        }
        return array(
            'reports' => $rows,
            'cursor' => $new_cursor,
        );
    }

    /**
     * @see DataModelInterface::create
     */
    public function create($report, $id = null)
    {
        $this->verifyReport($report);
        if ($id) {
            $report['_id'] = $id;
        }
        $result = $this->db->insertOne($report);
        return $result->getInsertedId();
    }

    /**
     * @see DataModelInterface::read
     */
    public function read($id)
    {
        $result = $this->db->findOne(
            array('_id' => new ObjectId($id)));
        if ($result) {
            return $this->reportToArray($result);
        }
        return false;
    }

    /**
     * @see DataModelInterface::update
     */
    public function update($report)
    {
        $this->verifyReport($report);
        $result = $this->db->replaceOne(
            array('_id' => new ObjectId($report['id'])),
            $this->arrayToReport($report));
        return $result->getModifiedCount();
    }

    /**
     * @see DataModelInterface::delete
     */
    public function delete($id)
    {
        $result = $this->db->deleteOne(
            array('_id' => new ObjectId($id)));
        return $result->getDeletedCount();
    }

    /**
     * Throws an exception if $report contains an invalid key.
     *
     * @param $report array
     *
     * @throws \InvalidArgumentException
     */
    private function verifyReport($report)
    {
        if ($invalid = array_diff_key($report, array_flip($this->columns))) {
            throw new \InvalidArgumentException(sprintf(
                'unsupported report properties: "%s"',
                implode(', ', $invalid)
            ));
        }
    }

    /**
     * Converts an array to a \stdClass object representing a report.
     *
     * @param $array array
     *
     * @return \stdClass
     */
    private function arrayToReport($array)
    {
        $report = new \stdClass();
        foreach ($this->columns as $column) {
            if ($column == 'id') {
                $report->_id = new ObjectId($array['id']);
            } elseif (isset($array[$column])) {
                $report->{$column} = $array[$column];
            } else {
                $report->{$column} = null;
            }
        }
        return $report;
    }

    /**
     * Converts a \stdClass object to an array representing a report.
     *
     * @param $report \stdClass
     *
     * @return array
     */
    private function reportToArray($report)
    {
        $ret = array();
        foreach ($this->columns as $column) {
            if ($column == 'id') {
                $ret['id'] = (string) $report->_id;
            } elseif (isset($report->{$column})) {
                $ret[$column] = $report->{$column};
            } else {
                $ret[$column] = null;
            }
        }
        return $ret;
    }
}
