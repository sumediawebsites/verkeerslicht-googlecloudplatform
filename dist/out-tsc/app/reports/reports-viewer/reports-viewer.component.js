var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Reports } from '../../services/reports.service';
import { Devices } from '../../services/devices.service';
var ReportsViewerComponent = (function () {
    function ReportsViewerComponent(router, route, reportsData, devicesData) {
        this.router = router;
        this.route = route;
        this.reportsData = reportsData;
        this.devicesData = devicesData;
        this.count = 0;
        this.offset = 0;
        this.limit = 25;
        this.loading = false;
        this.failed = false;
        this.isNewReportCount = 0;
        this.isOnHoldReportCount = 0;
        this.isPendingReportCount = 0;
        this.isProcessedPositiveReportCount = 0;
        this.isProcessedNeutralReportCount = 0;
        this.searchFilter = '';
        this.deviceMarkers = [];
        this.reportMarkers = [];
        this.reportMarkersToBeDeleted = [];
        this.deviceMarkersToBeDeleted = [];
        this.zoom = 7;
        this.center = new google.maps.LatLng(52.258107, 5.600592);
        this.scrollWheel = false;
    }
    ReportsViewerComponent.prototype.ngOnInit = function () {
        var pagination = true;
        this.retrieveReports(pagination);
        this.retrieveDevices();
    };
    ReportsViewerComponent.prototype.retrieveReports = function (pagination) {
        var _this = this;
        var usePagination = pagination;
        if (usePagination) {
            var observable = this.route.params
                .map(function (params) { return params['nr']; })
                .map(function (pageNr) { return (pageNr - 1) * _this.limit; });
            observable.subscribe(function (offset) { return _this.offset = offset; });
            observable.share().subscribe(function (offset) { return _this.findAll(offset, _this.limit); });
        }
        else {
            this.reportsData.getAll()
                .subscribe(function (reports) { return _this.reports = reports; }, function (err) { return console.log(err); }, function () { return _this.addReportMarkers(_this.reports); });
        }
    };
    ReportsViewerComponent.prototype.retrieveDevices = function () {
        var _this = this;
        this.devicesData.getAll()
            .subscribe(function (devices) { return _this.devices = devices; }, function (err) { return console.log(err); }, function () { return _this.addDeviceMarkers(_this.devices); });
    };
    ReportsViewerComponent.prototype.findAll = function (offset, limit) {
        var _this = this;
        this.reports = [];
        this.loading = true;
        this.failed = false;
        this.reportsData.findAll(offset, limit).subscribe(function (result) {
            _this.reports = result['reports'];
            _this.count = result['count'];
            _this.loading = false;
            _this.deleteAllReportMarkers();
            _this.deleteAllDeviceMarkers();
            if (_this.searchFilter.length !== 0) {
                for (var i = 0; i < _this.reports.length; i++) {
                    if (_this.reports[i]['vri_place'].toLowerCase().indexOf(_this.searchFilter.toLowerCase()) !== -1) {
                    }
                    else {
                        _this.reports.splice(i, 1);
                        i = i - 1;
                    }
                }
            }
            _this.addReportMarkers(_this.reports);
            _this.addDeviceMarkers(_this.devices);
        }, function () {
            _this.loading = false;
            _this.failed = true;
        });
    };
    ReportsViewerComponent.prototype.onPageChange = function (offset) {
        this.offset = offset;
        this.router.navigate(['/pagina', (offset / this.limit) + 1]);
    };
    ReportsViewerComponent.prototype.viewReport = function (reportId) {
        this.router.navigate(['report', reportId]);
    };
    ReportsViewerComponent.prototype.editReport = function (reportId) {
        this.router.navigate(['report', reportId, 'edit']);
    };
    ReportsViewerComponent.prototype.searchInputValue = function (searchInput) {
        this.searchInput = searchInput;
    };
    ReportsViewerComponent.prototype.showOnMap = function (reportId) {
        this.currentReport = this.reports.find(function (x) { return x.id === reportId; });
        this.showReport(this.currentReport);
    };
    ReportsViewerComponent.prototype.setPaginationLimit = function (limit) {
        this.limit = limit;
        this.offset = 0;
        this.onPageChange(this.offset);
        this.deleteAllReportMarkers();
        this.searchFilter = this.searchInput;
        var pagination = true;
        this.retrieveReports(pagination);
        this.retrieveDevices();
    };
    ReportsViewerComponent.prototype.showReport = function (report) {
        this.currentReport = report;
        var position = new google.maps.LatLng(parseFloat(this.currentReport['latitude']), parseFloat(this.currentReport['longitude']));
        this.setCenter(position);
        this.setZoom(17);
    };
    ReportsViewerComponent.prototype.setCenter = function (position) {
        this.center = position;
    };
    ReportsViewerComponent.prototype.setZoom = function (level) {
        this.zoom = level;
    };
    ReportsViewerComponent.prototype.deleteAllReportMarkers = function () {
        this.reportMarkersToBeDeleted = this.reportMarkers;
        this.reportMarkers = [];
    };
    ReportsViewerComponent.prototype.deleteAllDeviceMarkers = function () {
        this.deviceMarkersToBeDeleted = this.deviceMarkers;
        this.deviceMarkers = [];
    };
    ReportsViewerComponent.prototype.addReportMarkers = function (reports) {
        this.isNewReportCount = 0;
        this.isOnHoldReportCount = 0;
        this.isPendingReportCount = 0;
        this.isProcessedNeutralReportCount = 0;
        this.isProcessedPositiveReportCount = 0;
        for (var i = 0; i < reports.length; i++) {
            var draggable = false;
            var title = reports[i]['vri_place'];
            var label = [];
            var label_text = reports[i].id.toString();
            var label_color = 'white';
            label['text'] = label_text;
            label['color'] = label_color;
            var position = new google.maps.LatLng(reports[i]['latitude'], reports[i]['longitude']);
            var fillColor = void 0;
            var icon = {
                path: 'M20, 10c0-5.5-4.5-10-10-10S0, 4.5, 0, 10c0, 1.4, 0.3, 2.7, 0.8, 3.9l0, 0c0, 0, 2.1, 5.2, 9.2, 16.1 c7.2-10.9, 9.2-16.1, 9.2-16.1l0, 0C19.7, 12.7, 20, 11.4, 20, 10z',
                fillOpacity: 1,
                strokeWeight: 0,
                size: [20, 30],
                labelOrigin: new google.maps.Point(10, 10),
                anchor: new google.maps.Point(10, 30)
            };
            switch (reports[i]['report_status'].class) {
                case 'is-new':
                    icon['fillColor'] = '#242e42';
                    this.isNewReportCount++;
                    break;
                case 'is-on-hold':
                    icon['fillColor'] = '#f90';
                    this.isOnHoldReportCount++;
                    break;
                case 'is-pending':
                    icon['fillColor'] = '#f7412d';
                    this.isPendingReportCount++;
                    break;
                case 'is-processed-positive':
                    icon['fillColor'] = '#79c410';
                    this.isProcessedPositiveReportCount++;
                    break;
                case 'is-processed-neutral':
                    icon['fillColor'] = '#ffcf00';
                    label['color'] = '#242e42';
                    this.isProcessedNeutralReportCount++;
                    break;
                default:
                    break;
            }
            var animation = google.maps.Animation.DROP;
            this.addReportMarker(draggable, title, label, position, icon, animation);
        }
    };
    ReportsViewerComponent.prototype.addReportMarker = function (draggable, title, label, position, icon, animation) {
        this.reportMarkers = this.reportMarkers.concat([{
                draggable: draggable,
                position: position,
                icon: icon,
                animation: animation,
                label: label,
                title: title
            }]);
    };
    ReportsViewerComponent.prototype.addDeviceMarkers = function (devices) {
        if (typeof (devices) != 'undefined' && devices != null) {
            for (var i = 0; i < devices.length; i++) {
                var draggable = false;
                var title = devices[i]['name'];
                var label = [];
                var label_text = devices[i].id.toString();
                var label_color = 'black';
                label['text'] = label_text;
                label['color'] = label_color;
                var position = new google.maps.LatLng(devices[i]['location']['latitude'], devices[i]['location']['longitude']);
                var fillColor = '#242e42';
                var icon = {
                    path: 'M4.2,0C1.9,0,0,1.9,0,4.2l0,17.9C0,24.4,4.2,30,4.2,30s4.2-5.6,4.2-7.9l0-17.9C8.3,1.9,6.5,0,4.2,0z M5.8,24.4 c-1.5,0.9-3.6,0.4-4.5-1.2c-0.9-1.5-0.4-3.6,1.2-4.5s3.6-0.4,4.5,1.2C8,21.5,7.4,23.5,5.8,24.4z M5.9,15.7c-1.5,0.9-3.6,0.4-4.5-1.2 S1.1,11,2.6,10.1S6.1,9.7,7,11.3S7.5,14.8,5.9,15.7z M5.9,7.3C4.4,8.2,2.4,7.7,1.5,6.1S1.1,2.6,2.6,1.7s3.6-0.4,4.5,1.2 S7.5,6.4,5.9,7.3z',
                    fillOpacity: 1,
                    fillColor: fillColor,
                    strokeWeight: 0,
                    size: [8, 30],
                    labelOrigin: new google.maps.Point(10, 10),
                    anchor: new google.maps.Point(4, 30)
                };
                var animation = google.maps.Animation.DROP;
                this.addDeviceMarker(draggable, title, label, position, icon, animation);
            }
        }
    };
    ReportsViewerComponent.prototype.addDeviceMarker = function (draggable, title, label, position, icon, animation) {
        this.deviceMarkers = this.deviceMarkers.concat([{
                draggable: draggable,
                position: position,
                icon: icon,
                animation: animation,
                label: label,
                title: title
            }]);
    };
    return ReportsViewerComponent;
}());
ReportsViewerComponent = __decorate([
    Component({
        selector: 'reports-viewer',
        templateUrl: './reports-viewer.component.html',
        styleUrls: ['./reports-viewer.component.scss'],
        providers: [Reports, Devices]
    }),
    __metadata("design:paramtypes", [Router,
        ActivatedRoute,
        Reports,
        Devices])
], ReportsViewerComponent);
export { ReportsViewerComponent };
//# sourceMappingURL=../../../../../src/app/reports/reports-viewer/reports-viewer.component.js.map