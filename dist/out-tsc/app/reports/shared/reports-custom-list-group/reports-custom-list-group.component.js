var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter, ViewContainerRef, NgZone } from '@angular/core';
import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/bootstrap';
var ReportsCustomListGroupComponent = (function () {
    function ReportsCustomListGroupComponent(overlay, vcRef, modal, _ngZone) {
        this.modal = modal;
        this._ngZone = _ngZone;
        this.filterReports = '';
        this.paginationLimit = 9999;
        this.searchInputValue = '';
        this.viewReport = new EventEmitter();
        this.editReport = new EventEmitter();
        this.showOnMap = new EventEmitter();
        this.setSearchInput = new EventEmitter();
        this.setPaginationLimit = new EventEmitter();
        overlay.defaultViewContainer = vcRef;
    }
    ReportsCustomListGroupComponent.prototype.viewReportButton = function (reportId) {
        this.viewReport.emit(reportId);
    };
    ReportsCustomListGroupComponent.prototype.editReportButton = function (reportId) {
        this.editReport.emit(reportId);
    };
    ReportsCustomListGroupComponent.prototype.setPaginationLimitOnInput = function () {
        this.setSearchInput.emit(this.searchInputValue);
        if (this.searchInputValue.length >= 1) {
            this.setPaginationLimit.emit(this.paginationLimit);
        }
        else if (this.searchInputValue.length == 0) {
            this.setPaginationLimit.emit(this.defaultPaginationlimit);
        }
    };
    ReportsCustomListGroupComponent.prototype.modalReportButton = function (reportId) {
        var _this = this;
        this.currentReport = this.reports.find(function (x) { return x.id === reportId; });
        var locationId = this.currentReport.device['location_id'];
        var sameLocationReports = [];
        for (var i = 0; i < this.reports.length; i++) {
            if (this.reports[i].device['location_id'] == locationId) {
                sameLocationReports.push(this.reports[i]);
            }
        }
        var event = new Event('showOnMap');
        this._ngZone.run(function () {
            return _this.showOnMap.emit(reportId);
        });
        var bodyHTML = this.createHTMLList(sameLocationReports);
        this.modal.alert()
            .size('lg')
            .isBlocking(false)
            .showClose(true)
            .title('[' + this.currentReport['id'] + '] ' + this.currentReport['vri_place'])
            .body(bodyHTML)
            .headerClass('modal-ng__header')
            .bodyClass('modal-ng__body')
            .footerClass('modal-ng__footer')
            .okBtn('')
            .open();
        var reportsModalDialogClass = "modal-dialog";
        document.getElementsByClassName(reportsModalDialogClass)[0].addEventListener("click", clickReportDirectButtonHandler, false);
        function clickReportDirectButtonHandler(event) {
            if (typeof event.srcElement.className === 'undefined') {
                return;
            }
            else if (event.srcElement.className.indexOf('report-direct-button') !== -1) {
                goToReportDirect();
            }
            else if (event.srcElement.className.indexOf('report-direct-button-icon') !== -1) {
                goToReportDirect();
            }
            else if (typeof event.srcElement.firstChild.className === 'undefined') {
                return;
            }
            else if (event.srcElement.firstChild.className.indexOf('report-direct-button') !== -1) {
                goToReportDirect();
            }
            else if (event.srcElement.firstChild.className.indexOf('report-direct-button-icon') !== -1) {
                goToReportDirect();
            }
        }
        function goToReportDirect() {
            var evt = new MouseEvent("click", {
                view: window,
                bubbles: true,
                cancelable: true,
                clientX: 20
            });
            var reportsModalNGHeaderClass = "modal-ng__header";
            document.getElementsByClassName(reportsModalNGHeaderClass)[0].firstElementChild.dispatchEvent(evt);
            setTimeout(function () {
                var remodalReportDirectButton = document.querySelectorAll('[data-remodal-target="modal-report"]');
                remodalReportDirectButton[0].dispatchEvent(evt);
            }, 1000);
        }
        ;
    };
    ReportsCustomListGroupComponent.prototype.createHTMLList = function (reportsList) {
        var outerDiv = document.createElement('div');
        var p = document.createElement('p');
        p.innerHTML = reportsList[0]['device']['disclaimer'];
        outerDiv.appendChild(p);
        var span = document.createElement('span');
        var countLabel = reportsList.length == 1 ? "melding" : "meldingen";
        span.innerHTML = reportsList.length.toString() + ' ' + countLabel;
        outerDiv.appendChild(span);
        var ul = document.createElement('ul');
        for (var i = 0; i < reportsList.length; i++) {
            var li = document.createElement('li');
            var innerSpanCreatedAt = document.createElement('span');
            var date = new Date(reportsList[i]['date']);
            var dayNumber = date.getDate();
            var dayString = ("0" + dayNumber).slice(-2);
            var monthNumber = date.getMonth() + 1;
            var monthString = ("0" + monthNumber).slice(-2);
            var yearNumber = date.getFullYear();
            var yearString = ("0" + yearNumber).slice(-2);
            var time = new Date(reportsList[i]['time']);
            var hoursNumber = time.getHours();
            var hoursString = ("0" + hoursNumber).slice(-2);
            var minutesNumber = time.getMinutes();
            var minutesString = ("0" + minutesNumber).slice(-2);
            innerSpanCreatedAt.innerHTML = '[' + reportsList[i]['id'] + '] ' + reportsList[i]['vri_place'] + '<span>' + dayString + '-' + monthString + '-' + yearString + ' | ' + hoursString + ":" + minutesString + '</span>';
            li.appendChild(innerSpanCreatedAt);
            var innerSpanReportType = document.createElement('span');
            innerSpanReportType.innerHTML = reportsList[i]['report_type']['description'];
            li.appendChild(innerSpanReportType);
            var innerSpanFromStreet = document.createElement('span');
            innerSpanFromStreet.innerHTML = 'van: ' + reportsList[i]['vri_street_1'];
            li.appendChild(innerSpanFromStreet);
            var innerSpanToStreet = document.createElement('span');
            innerSpanToStreet.innerHTML = 'naar: ' + reportsList[i]['vri_street_2'];
            li.appendChild(innerSpanToStreet);
            ul.appendChild(li);
        }
        outerDiv.appendChild(ul);
        var button = document.createElement('div');
        button.innerHTML = '<a class="report-direct-button">Ook een melding plaatsen? <i class="icon-arrow-right report-direct-button-icon"></i></a>';
        outerDiv.appendChild(button);
        return outerDiv.outerHTML;
    };
    return ReportsCustomListGroupComponent;
}());
__decorate([
    Input(),
    __metadata("design:type", Array)
], ReportsCustomListGroupComponent.prototype, "reports", void 0);
__decorate([
    Input(),
    __metadata("design:type", Number)
], ReportsCustomListGroupComponent.prototype, "defaultPaginationlimit", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], ReportsCustomListGroupComponent.prototype, "viewReport", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], ReportsCustomListGroupComponent.prototype, "editReport", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], ReportsCustomListGroupComponent.prototype, "showOnMap", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], ReportsCustomListGroupComponent.prototype, "setSearchInput", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], ReportsCustomListGroupComponent.prototype, "setPaginationLimit", void 0);
ReportsCustomListGroupComponent = __decorate([
    Component({
        selector: 'reports-custom-list-group',
        templateUrl: './reports-custom-list-group.component.html',
        styles: ['./reports-custom-list-group.component.scss'],
    }),
    __metadata("design:paramtypes", [Overlay,
        ViewContainerRef,
        Modal,
        NgZone])
], ReportsCustomListGroupComponent);
export { ReportsCustomListGroupComponent };
//# sourceMappingURL=../../../../../../src/app/reports/shared/reports-custom-list-group/reports-custom-list-group.component.js.map