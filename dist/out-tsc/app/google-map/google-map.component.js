var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { ChangeDetectorRef, Component, Input, NgZone, Renderer } from '@angular/core';
import { Report } from '../models/report.model';
var GoogleMapComponent = (function () {
    function GoogleMapComponent(_renderer, _ref, _ngZone) {
        this._renderer = _renderer;
        this._ref = _ref;
        this._ngZone = _ngZone;
        this._zoom = 15;
        this._center = new google.maps.LatLng(0, 0);
        this._mapTypeControl = false;
    }
    Object.defineProperty(GoogleMapComponent.prototype, "center", {
        set: function (value) {
            console.log('Inside GoogleMapComponent set center()');
            this._center = value;
            if (this.map) {
                this.map.setCenter(this._center);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent.prototype, "zoom", {
        set: function (value) {
            this._zoom = value;
            if (this.map) {
                this.map.setZoom(this._zoom);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent.prototype, "scrollWheel", {
        set: function (value) {
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent.prototype, "markers", {
        set: function (value) {
            var _this = this;
            if (this._markers) {
                this._markers.forEach(function (marker) { return marker.setMap(null); });
            }
            this._markers = [];
            value.forEach(function (option) {
                var marker = new google.maps.Marker(option);
                marker.setMap(_this.map);
                _this._markers.push(marker);
            });
            this._ref.detectChanges();
            if (typeof this.map !== 'undefined') {
                this.markerClustererOptions = {
                    styles: [
                        {
                            url: "assets/icons/marker-clusterer/m1.png",
                            width: 52,
                            height: 52,
                            textColor: 'white'
                        },
                        {
                            url: "assets/icons/marker-clusterer/m2.png",
                            width: 56,
                            height: 56,
                            textColor: 'white'
                        },
                        {
                            url: "assets/icons/marker-clusterer/m3.png",
                            width: 66,
                            height: 66,
                            textColor: 'white'
                        },
                        {
                            url: "assets/icons/marker-clusterer/m4.png",
                            width: 78,
                            height: 78,
                            textColor: 'white'
                        },
                        {
                            url: "assets/icons/marker-clusterer/m5.png",
                            width: 90,
                            height: 90,
                            textColor: 'white'
                        }
                    ]
                };
                this.markerClusterer = new MarkerClusterer(this.map, this._markers, this.markerClustererOptions);
            }
        },
        enumerable: true,
        configurable: true
    });
    GoogleMapComponent.prototype.ngOnInit = function () {
        var targetElement = this._renderer.selectRootElement(".map");
        var me = this;
        this.map = new google.maps.Map(targetElement, {
            center: this._center,
            zoom: this._zoom,
            mapTypeControl: this._mapTypeControl,
            fullscreenControl: false,
            styles: [
                {
                    "featureType": "administrative.country",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#FFBB00"
                        },
                        {
                            "saturation": 43.400000000000006
                        },
                        {
                            "lightness": 37.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": -40
                        },
                        {
                            "lightness": 36
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": -77
                        },
                        {
                            "lightness": 28
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#00FF6A"
                        },
                        {
                            "saturation": -1.0989010989011234
                        },
                        {
                            "lightness": 11.200000000000017
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": -24
                        },
                        {
                            "lightness": 61
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#ffc200"
                        },
                        {
                            "saturation": -61.8
                        },
                        {
                            "lightness": 45.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#FF0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 51.19999999999999
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#ff0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 52
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#0078FF"
                        },
                        {
                            "saturation": -13.200000000000003
                        },
                        {
                            "lightness": 2.4000000000000057
                        },
                        {
                            "gamma": 1
                        }
                    ]
                }
            ]
        });
    };
    return GoogleMapComponent;
}());
__decorate([
    Input(),
    __metadata("design:type", Report)
], GoogleMapComponent.prototype, "report", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], GoogleMapComponent.prototype, "center", null);
__decorate([
    Input(),
    __metadata("design:type", Number),
    __metadata("design:paramtypes", [Number])
], GoogleMapComponent.prototype, "zoom", null);
__decorate([
    Input(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], GoogleMapComponent.prototype, "scrollWheel", null);
__decorate([
    Input(),
    __metadata("design:type", Array),
    __metadata("design:paramtypes", [Array])
], GoogleMapComponent.prototype, "markers", null);
GoogleMapComponent = __decorate([
    Component({
        selector: 'app-google-map',
        templateUrl: './google-map.component.html',
        styleUrls: ['./google-map.component.scss'],
        providers: []
    }),
    __metadata("design:paramtypes", [Renderer,
        ChangeDetectorRef,
        NgZone])
], GoogleMapComponent);
export { GoogleMapComponent };
//# sourceMappingURL=../../../../src/app/google-map/google-map.component.js.map