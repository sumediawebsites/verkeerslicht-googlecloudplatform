var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { AppComponent } from './app.component';
import { GoogleMapComponent } from './google-map/google-map.component';
import { PageHeaderComponent } from './shared/page-header/page-header.component';
import { PageFooterComponent } from './shared/page-footer/page-footer.component';
import { ReportsViewerComponent } from './reports/reports-viewer/reports-viewer.component';
import { ReportViewerComponent } from './reports/report-viewer/report-viewer.component';
import { ReportEditorComponent } from './reports/report-editor/report-editor.component';
import { ReportsTableComponent } from './reports/shared/reports-table/reports-table.component';
import { ReportsCustomListGroupComponent } from './reports/shared/reports-custom-list-group/reports-custom-list-group.component';
import { ReportDetailsComponent } from './reports/shared/report-details/report-details.component';
import { ReportFormComponent } from './reports/shared/report-form/report-form.component';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { ActivePipe } from './pipes/active.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { AppRoutingModule } from './app.routes';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    NgModule({
        declarations: [
            AppComponent,
            GoogleMapComponent,
            PageHeaderComponent,
            PageFooterComponent,
            ReportsViewerComponent,
            ReportViewerComponent,
            ReportEditorComponent,
            ReportsTableComponent,
            ReportsCustomListGroupComponent,
            ReportDetailsComponent,
            ReportFormComponent,
            CapitalizePipe,
            ActivePipe,
            FilterPipe
        ],
        imports: [
            BrowserModule,
            InfiniteScrollModule,
            FormsModule,
            HttpModule,
            JsonpModule,
            ModalModule.forRoot(),
            BootstrapModalModule,
            AppRoutingModule
        ],
        providers: [],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=../../../src/app/app.module.js.map