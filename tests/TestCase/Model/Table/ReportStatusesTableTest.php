<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportStatusesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportStatusesTable Test Case
 */
class ReportStatusesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportStatusesTable
     */
    public $ReportStatuses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.report_statuses',
        'app.reports',
        'app.devices',
        'app.locations',
        'app.suppliers',
        'app.created_by',
        'app.roles',
        'app.users',
        'app.filters',
        'app.notifications',
        'app.rights',
        'app.rights_roles',
        'app.modified_by',
        'app.managers',
        'app.reporters',
        'app.report_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReportStatuses') ? [] : ['className' => 'App\Model\Table\ReportStatusesTable'];
        $this->ReportStatuses = TableRegistry::get('ReportStatuses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportStatuses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
