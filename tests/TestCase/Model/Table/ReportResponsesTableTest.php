<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportResponsesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportResponsesTable Test Case
 */
class ReportResponsesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportResponsesTable
     */
    public $ReportResponses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.report_responses',
        'app.reporters',
        'app.reports',
        'app.devices',
        'app.locations',
        'app.councils',
        'app.provinces',
        'app.created_by',
        'app.roles',
        'app.users',
        'app.filters',
        'app.notifications',
        'app.notification_templates',
        'app.users_notification_templates',
        'app.rights',
        'app.rights_roles',
        'app.modified_by',
        'app.places',
        'app.suppliers',
        'app.report_statuses',
        'app.report_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReportResponses') ? [] : ['className' => 'App\Model\Table\ReportResponsesTable'];
        $this->ReportResponses = TableRegistry::get('ReportResponses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportResponses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
