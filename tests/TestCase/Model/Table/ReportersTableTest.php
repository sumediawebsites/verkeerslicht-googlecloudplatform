<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportersTable Test Case
 */
class ReportersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportersTable
     */
    public $Reporters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.reporters',
        'app.reports',
        'app.devices',
        'app.locations',
        'app.suppliers',
        'app.created_by',
        'app.roles',
        'app.users',
        'app.filters',
        'app.notifications',
        'app.rights',
        'app.rights_roles',
        'app.modified_by',
        'app.managers',
        'app.report_statuses',
        'app.report_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Reporters') ? [] : ['className' => 'App\Model\Table\ReportersTable'];
        $this->Reporters = TableRegistry::get('Reporters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Reporters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
