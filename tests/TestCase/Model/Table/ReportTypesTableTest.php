<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportTypesTable Test Case
 */
class ReportTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportTypesTable
     */
    public $ReportTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.report_types',
        'app.reports',
        'app.devices',
        'app.locations',
        'app.suppliers',
        'app.created_by',
        'app.roles',
        'app.users',
        'app.filters',
        'app.notifications',
        'app.rights',
        'app.rights_roles',
        'app.modified_by',
        'app.managers',
        'app.report_statuses',
        'app.reporters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReportTypes') ? [] : ['className' => 'App\Model\Table\ReportTypesTable'];
        $this->ReportTypes = TableRegistry::get('ReportTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
