<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CouncilsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CouncilsTable Test Case
 */
class CouncilsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CouncilsTable
     */
    public $Councils;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.councils',
        'app.provinces',
        'app.locations',
        'app.places'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Councils') ? [] : ['className' => 'App\Model\Table\CouncilsTable'];
        $this->Councils = TableRegistry::get('Councils', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Councils);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
