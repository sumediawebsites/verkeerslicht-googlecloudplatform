<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RightsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RightsTable Test Case
 */
class RightsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RightsTable
     */
    public $Rights;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rights',
        'app.roles',
        'app.users',
        'app.filters',
        'app.notifications',
        'app.rights_roles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Rights') ? [] : ['className' => 'App\Model\Table\RightsTable'];
        $this->Rights = TableRegistry::get('Rights', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Rights);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
