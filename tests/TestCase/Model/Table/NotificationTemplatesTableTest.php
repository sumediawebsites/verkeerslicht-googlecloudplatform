<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NotificationTemplatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NotificationTemplatesTable Test Case
 */
class NotificationTemplatesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NotificationTemplatesTable
     */
    public $NotificationTemplates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.notification_templates',
        'app.users',
        'app.roles',
        'app.rights',
        'app.rights_roles',
        'app.filters',
        'app.notifications',
        'app.devices',
        'app.locations',
        'app.councils',
        'app.provinces',
        'app.created_by',
        'app.users_notification_templates',
        'app.modified_by',
        'app.places',
        'app.suppliers',
        'app.reports',
        'app.report_statuses',
        'app.reporters',
        'app.report_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NotificationTemplates') ? [] : ['className' => 'App\Model\Table\NotificationTemplatesTable'];
        $this->NotificationTemplates = TableRegistry::get('NotificationTemplates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NotificationTemplates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
