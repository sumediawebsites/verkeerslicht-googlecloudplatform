<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportResponsePresetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportResponsePresetsTable Test Case
 */
class ReportResponsePresetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportResponsePresetsTable
     */
    public $ReportResponsePresets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.report_response_presets'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReportResponsePresets') ? [] : ['className' => 'App\Model\Table\ReportResponsePresetsTable'];
        $this->ReportResponsePresets = TableRegistry::get('ReportResponsePresets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportResponsePresets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
