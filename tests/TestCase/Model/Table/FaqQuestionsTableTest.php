<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FaqQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FaqQuestionsTable Test Case
 */
class FaqQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FaqQuestionsTable
     */
    public $FaqQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.faq_questions',
        'app.faq_categories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FaqQuestions') ? [] : ['className' => 'App\Model\Table\FaqQuestionsTable'];
        $this->FaqQuestions = TableRegistry::get('FaqQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FaqQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
