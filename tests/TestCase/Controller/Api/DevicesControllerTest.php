<?php
namespace App\Test\TestCase\Controller\Api;

use App\Controller\Api\DevicesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Api\DevicesController Test Case
 */
class DevicesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.devices',
        'app.locations',
        'app.councils',
        'app.provinces',
        'app.created_by',
        'app.roles',
        'app.users',
        'app.filters',
        'app.notifications',
        'app.notification_templates',
        'app.users_notification_templates',
        'app.rights',
        'app.rights_roles',
        'app.modified_by',
        'app.places',
        'app.suppliers',
        'app.reports',
        'app.report_statuses',
        'app.reporters',
        'app.report_types'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
