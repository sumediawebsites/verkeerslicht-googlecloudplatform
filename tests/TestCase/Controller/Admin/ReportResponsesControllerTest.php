<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\ReportResponsesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\ReportResponsesController Test Case
 */
class ReportResponsesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.report_responses',
        'app.reporters',
        'app.reports',
        'app.devices',
        'app.locations',
        'app.councils',
        'app.provinces',
        'app.created_by',
        'app.roles',
        'app.users',
        'app.filters',
        'app.notifications',
        'app.notification_templates',
        'app.users_notification_templates',
        'app.rights',
        'app.rights_roles',
        'app.modified_by',
        'app.places',
        'app.suppliers',
        'app.report_statuses',
        'app.report_types'
    ];

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
