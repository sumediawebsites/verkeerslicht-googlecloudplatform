<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\WidgetsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\WidgetsController Test Case
 */
class WidgetsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.widgets',
        'app.roles',
        'app.users',
        'app.filters',
        'app.notifications',
        'app.notification_templates',
        'app.users_notification_templates',
        'app.rights',
        'app.rights_roles',
        'app.widgets_roles'
    ];

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
