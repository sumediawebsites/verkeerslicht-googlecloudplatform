<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ContactController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ContactController Test Case
 */
class ContactControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contact',
        'app.created_by',
        'app.roles',
        'app.users',
        'app.filters',
        'app.notifications',
        'app.devices',
        'app.locations',
        'app.councils',
        'app.provinces',
        'app.modified_by',
        'app.notification_templates',
        'app.users_notification_templates',
        'app.places',
        'app.suppliers',
        'app.reports',
        'app.report_statuses',
        'app.reporters',
        'app.report_types',
        'app.rights',
        'app.rights_roles'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
