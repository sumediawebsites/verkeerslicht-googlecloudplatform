$.fn.extend({
    trackChanges: function () {
        $(":input", this).change(function () {
            $(this.form).data("changed", true);
        });
    }
    ,
    isChanged: function () {
        return this.data("changed");
    }
});

$(document).ready(function () {
    $.datetimepicker.setLocale('nl');

    $('input.datetimepicker').datetimepicker({
        format: 'd-m-Y H:i',
        // mask: true,
        step: 15,
        weeks: true,
        dayOfWeekStart: 1,
        scrollInput: false,
        // minDate: 'today',
        // minTime: '08:00',
        // maxTime: '22:00',
    });

    $('input.datepicker').datetimepicker({
        format: 'd-m-Y',
        // mask: true,
        step: 15,
        weeks: true,
        dayOfWeekStart: 1,
        scrollInput: false,
        // minDate: 'today',
        timepicker: false
    });

    $('input.timepicker').datetimepicker({
        format: 'H:i',
        step: 15,
        scrollInput: false,
        datepicker: false
    });

    $('.filter.btn-group').on('click', function () {
        if ($('#builder-holder').is(':visible')) {
            $('#builder-holder').slideUp(300);
        } else {
            $('#builder-holder').slideDown(300);
        }
    });

    $('button[type="submit"]').addClass('btn btn-success');

    $("form").each(function () {
        $(this).validate();
        $(this).trackChanges();
    });

    $("a").on('click', function (e) {
        var hasChanges = false;
        $('form').each(function () {
            if ($(this).isChanged()) {
                hasChanges = true;
            }
        });
        if (hasChanges) {
            if (!confirm('U heeft niet-opgeslagen wijzigingen. Weet u zeker dat u dit scherm wilt verlaten?')) {
                e.preventDefault();
                return false;
            }
        }
    });

    $('form').bind("keypress", function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    $('form').on('focus', 'input', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        });
    });

    $('.panel-toggle').on('click', function () {
        $(this).closest('.panel').find('ul').toggle('slow');
        $(this).parent().toggleClass('open');
    });

    $(document).on('click', '#toggleSidebar', function () {
        switch ($(this).data('state')) {
            case 'open':
                $('.container.main .left-nav .left-nav-item').hide('slow');
                $('.container.main .left-nav').removeClass('col-lg-2').addClass('col-lg-1');
                $('.container.main .right-content').removeClass('col-lg-10').addClass('col-lg-11');
                $(this).data('state', 'closed');
                break;
            default:
                $('.container.main .left-nav').removeClass('col-lg-1').addClass('col-lg-2');
                $('.container.main .right-content').removeClass('col-lg-11').addClass('col-lg-10');
                $('.container.main .left-nav .left-nav-item').show('slow');
                $(this).data('state', 'open');
                break;
        }

        return false;
    });
});

var requiredFormFields = new Array();

function toggleFormValidation(formElement, enableFormValidation) {
    var i = 0;

    fillRequiredFormFieldsArray(formElement);

    formElement.find(':input').each(function () {

        if (enableFormValidation && inArray($(this).attr('name'), requiredFormFields)) {
            $(this).addClass('required');
            $(this).prop("required", "required");
        }
        else {
            $(this).removeClass('required');
            $(this).prop("required", "");
        }

    });
}

function fillRequiredFormFieldsArray(formElement) {
    if (requiredFormFields.length == 0) {
        formElement.find(':input').each(function () {
            if ($(this).hasClass('required')) {
                requiredFormFields[requiredFormFields.length] = $(this).attr('name');
            }
        });
    }
}

function inArray(needle, haystack) {
    for (i = 0; i < haystack.length; i++) {
        if (needle == haystack[i]) {
            return true;
        }
    }
    return false;
}

function parseSql(sql) {
    if (sql.indexOf('SELECT') !== -1) {
        sql = sql.replace(/'/g, "");
    }
    return sql;
}