webpackJsonp([0,3],{

/***/ 1053:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 1055:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(466);


/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return App; });
var App = (function () {
    function App(name, creator, copyrightYear) {
        this.name = name;
        this.creator = creator;
        this.copyrightYear = copyrightYear;
    }
    return App;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/app.model.js.map

/***/ }),

/***/ 250:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Report; });
var Report = (function () {
    function Report(id, device_id, report_status_id, reporter_id, report_type_id, title, body, device, created_by, modified_by, created_at, modified_at, deleted) {
        this.id = id;
        this.device_id = device_id;
        this.report_status_id = report_status_id;
        this.reporter_id = reporter_id;
        this.report_type_id = report_type_id;
        this.title = title;
        this.body = body;
        this.device = device;
        this.created_by = created_by;
        this.modified_by = modified_by;
        this.created_at = created_at;
        this.modified_at = modified_at;
        this.deleted = deleted;
    }
    return Report;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/report.model.js.map

/***/ }),

/***/ 251:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Reports; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Reports = (function () {
    function Reports(http) {
        this.http = http;
        this.baseUrl = 'https://verkeerslicht-165607.appspot.com/api'; // TEMP, RENAME TO FINAL URL!!
        this.requestOptions = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Headers */]({ 'Content-Type': 'application/json' })
        });
    }
    // Used without pagination
    Reports.prototype.getAll = function () {
        return this.http.get(this.baseUrl + "/reports").map(this.extractData).catch(this.handleError);
    };
    // Used with pagination
    Reports.prototype.findAll = function (offset, limit) {
        var _this = this;
        if (offset === void 0) { offset = 0; }
        if (limit === void 0) { limit = 2; }
        return this.http
            .get(this.baseUrl + "/reports/index/" + offset + "/" + limit) // cakePHP expects index, offset and limit as /index/offset/limit
            .map(function (response) { return response.json(); })
            .map(function (results) { return _this.getList(results); });
    };
    Reports.prototype.get = function (reportId) {
        return this.http.get('https://verkeerslicht.dev/api/reports/' + encodeURIComponent(reportId.toString())).map(this.extractData).catch(this.handleError);
    };
    Reports.prototype.insert = function (report) {
        return this.http.post('https://verkeerslicht.dev/api/reports/', JSON.stringify(report), this.requestOptions).map(function (res) { return res.json(); }).catch(this.handleError);
    };
    Reports.prototype.update = function (report) {
        return this.http.put('https://verkeerslicht.dev/api/reports/' + encodeURIComponent(report.id.toString()), JSON.stringify(report), this.requestOptions).map(function (res) { return res.json(); }).catch(this.handleError);
    };
    Reports.prototype.delete = function (reportId) {
        return this.http.delete('https://verkeerslicht.dev/api/reports/' + encodeURIComponent(reportId.toString())).map(function (res) { return res.json(); }).catch(this.handleError);
    };
    Reports.prototype.getList = function (data) {
        // room for additional filtering
        return data;
    };
    /**
     * Pick the array that belongs to the key 'reports'
     *
     * e.g. { reports:[our data is in here] }
     */
    Reports.prototype.extractData = function (res) {
        var body = res.json();
        return body.reports || {};
    };
    /**
     * Handle error
     */
    Reports.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(errMsg);
    };
    Reports = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Http */]) === 'function' && _a) || Object])
    ], Reports);
    return Reports;
    var _a;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/reports.service.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_reports_service__ = __webpack_require__(251);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportEditorComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportEditorComponent = (function () {
    function ReportEditorComponent(reportsData, router, route) {
        this.reportsData = reportsData;
        this.router = router;
        this.route = route;
        this.report = {};
    }
    ReportEditorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            return _this.reportsData.get(parseInt(params['id'], 10)).subscribe(function (report) {
                return _this.report = report;
            });
        });
    };
    ReportEditorComponent.prototype.saveReport = function (report) {
        var _this = this;
        (report.id
            ? this.reportsData.update(report)
            : this.reportsData.insert(report))
            .subscribe(function (report) {
            _this.router.navigate(["/"]);
        });
    };
    ReportEditorComponent.prototype.deleteReport = function (report) {
        var _this = this;
        if (confirm('Weet je zeker dat je het volgende wil verwijderen?: this report?')) {
            this.reportsData.delete(report.id).subscribe(function (report) {
                return _this.router.navigate(["/"]);
            });
        }
    };
    ReportEditorComponent.prototype.cancelReport = function (report) {
        this.router.navigate(["/"]);
    };
    ReportEditorComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'report-editor',
            template: __webpack_require__(777),
            styles: [__webpack_require__(768)],
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_reports_service__["a" /* Reports */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_reports_service__["a" /* Reports */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_reports_service__["a" /* Reports */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === 'function' && _c) || Object])
    ], ReportEditorComponent);
    return ReportEditorComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/report-editor.component.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_reports_service__ = __webpack_require__(251);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportViewerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportViewerComponent = (function () {
    function ReportViewerComponent(route, router, reportsData) {
        this.route = route;
        this.router = router;
        this.reportsData = reportsData;
    }
    ReportViewerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.reportsData.get(parseInt(params['id'], 10)).subscribe(function (report) { return _this.report = report; });
        });
    };
    ReportViewerComponent.prototype.editReport = function (reportId) {
        this.router.navigate(["/report", reportId, "/edit"]);
    };
    ReportViewerComponent.prototype.returnToList = function () {
        this.router.navigate(["/"]);
    };
    ReportViewerComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            template: __webpack_require__(778),
            styles: ['./report-viewer.component.scss'],
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_reports_service__["a" /* Reports */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_reports_service__["a" /* Reports */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_reports_service__["a" /* Reports */]) === 'function' && _c) || Object])
    ], ReportViewerComponent);
    return ReportViewerComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/report-viewer.component.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_reports_service__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_devices_service__ = __webpack_require__(594);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportsViewerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReportsViewerComponent = (function () {
    function ReportsViewerComponent(router, route, reportsData, devicesData) {
        this.router = router;
        this.route = route;
        this.reportsData = reportsData;
        this.devicesData = devicesData;
        // Used by pagination
        this.count = 0;
        this.offset = 0;
        this.limit = 25; // choose an appropriate number e.g. 25
        this.loading = false;
        this.failed = false;
        this.isNewReportCount = 0;
        this.isOnHoldReportCount = 0;
        this.isPendingReportCount = 0;
        this.isProcessedPositiveReportCount = 0;
        this.isProcessedNeutralReportCount = 0;
        // Start out with all report filters set to true (i.e. checked), 
        // so all reports are shown
        this.reportFilters = {
            isNewReport: true,
            isOnHoldReport: true,
            isPendingReport: true,
            isProcessedPositiveReport: true,
            isProcessedNeutralReport: true
        };
        this.reportMarkersStatusArray = [];
        this.searchFilter = '';
        this.deviceMarkersInfoWindowContentStrings = [];
        this.deviceMarkers = []; // <-- USE THIS
        this.reportMarkers = []; // <-- USE THIS
        //markers: google.maps.Marker[] = []; // <-- DON'T USE THIS
        this.reportMarkersVisibilityUpdated = [];
        this.reportMarkersToBeDeleted = [];
        this.deviceMarkersToBeDeleted = [];
        this.zoom = 7; // 20 is max, is most zoomed in
        this.center = new google.maps.LatLng(52.258107, 5.600592); // center of Holland
        this.scrollWheel = false; // disable zooming on the map with mouse scroll wheel
        this.onClickFilterReportMarkers = function (event) {
            var elementID = event['srcElement']['id'];
            var elementChecked = event['srcElement']['checked'];
            switch (elementID) {
                case 'filterIsNewReport':
                    this.reportFilters['isNewReport'] = elementChecked;
                    break;
                case 'filterIsOnHoldReport':
                    this.reportFilters['isOnHoldReport'] = elementChecked;
                    break;
                case 'filterIsPendingReport':
                    this.reportFilters['isPendingReport'] = elementChecked;
                    break;
                case 'filterIsProcessedPositiveReport':
                    this.reportFilters['isProcessedPositiveReport'] = elementChecked;
                    break;
                case 'filterIsProcessedNeutralReport':
                    this.reportFilters['isProcessedNeutralReport'] = elementChecked;
                    break;
                default:
                    break;
            }
            this.filterReportMarkers();
        };
        // Get a subset of the report filters that are set to true
        this.getTrueReportFiltersOptions = function () {
            var trueReportFiltersOptions = [];
            for (var option in this.reportFilters) {
                if (this.reportFilters[option]) {
                    trueReportFiltersOptions.push(option);
                }
            }
            return trueReportFiltersOptions;
        };
        this.filterReportMarkers = function () {
            var setReportMarkerFilters = this.getTrueReportFiltersOptions();
            var trueFilterStatusStrings = [];
            var keyStatus = '';
            for (var key in this.reportFilters) {
                // Use only the filters that have a value of true
                if (this.reportFilters[key]) {
                    if (key == 'isNewReport') {
                        keyStatus = 'is-new';
                    }
                    if (key == 'isOnHoldReport') {
                        keyStatus = 'is-on-hold';
                    }
                    if (key == 'isPendingReport') {
                        keyStatus = 'is-pending';
                    }
                    if (key == 'isProcessedPositiveReport') {
                        keyStatus = 'is-processed-positive';
                    }
                    if (key == 'isProcessedNeutralReport') {
                        keyStatus = 'is-processed-neutral';
                    }
                    trueFilterStatusStrings.push(keyStatus);
                }
            }
            this.reportMarkersVisibilityUpdated = [];
            var reportMarkerStatus;
            // for each marker, check to see if all required options are set
            for (var i = 0; i < this.reportMarkersStatusArray.length; i++) {
                reportMarkerStatus = this.reportMarkersStatusArray[i];
                var marker = [];
                // Set the marker with this index to visibile: false by default
                var options = {
                    draggable: this.reportMarkers[reportMarkerStatus['markerIndex']].draggable,
                    position: this.reportMarkers[reportMarkerStatus['markerIndex']].position,
                    icon: this.reportMarkers[reportMarkerStatus['markerIndex']].icon,
                    animation: this.reportMarkers[reportMarkerStatus['markerIndex']].animation,
                    label: this.reportMarkers[reportMarkerStatus['markerIndex']].label,
                    title: this.reportMarkers[reportMarkerStatus['markerIndex']].title,
                    visible: false
                };
                for (var n = 0; n < trueFilterStatusStrings.length; n++) {
                    if (trueFilterStatusStrings[n] == reportMarkerStatus['status']) {
                        options.visible = true;
                    }
                }
                marker = [options];
                this.reportMarkersVisibilityUpdated = this.reportMarkersVisibilityUpdated.concat(marker);
            }
            // This will update the map
            this.reportMarkers = this.reportMarkersVisibilityUpdated;
        };
    }
    ReportsViewerComponent.prototype.ngOnInit = function () {
        // Retrieve reports	
        var pagination = true;
        this.retrieveReports(pagination);
        this.retrieveDevices();
    };
    ReportsViewerComponent.prototype.retrieveReports = function (pagination) {
        var _this = this;
        // Retrieve reports	
        var usePagination = pagination;
        if (usePagination) {
            // With pagination
            var observable = this.route.params
                .map(function (params) { return params['nr']; })
                .map(function (pageNr) { return (pageNr - 1) * _this.limit; });
            observable.subscribe(function (offset) { return _this.offset = offset; });
            observable.share().subscribe(function (offset) { return _this.findAll(offset, _this.limit); });
        }
        else {
            // Without pagination
            this.reportsData.getAll()
                .subscribe(function (reports) { return _this.reports = reports; }, function (err) { return console.log(err); }, function () { return _this.addReportMarkers(_this.reports); });
        }
    };
    ReportsViewerComponent.prototype.retrieveDevices = function () {
        var _this = this;
        // Retrieve devices, no pagination
        this.devicesData.getAll()
            .subscribe(function (devices) { return _this.devices = devices; }, function (err) { return console.log(err); }, function () { return _this.addDeviceMarkers(_this.devices); });
    };
    ReportsViewerComponent.prototype.findAll = function (offset, limit) {
        var _this = this;
        this.reports = [];
        this.loading = true;
        this.failed = false;
        this.reportsData.findAll(offset, limit).subscribe(function (result) {
            _this.reports = result['reports'];
            _this.count = result['count'];
            _this.loading = false;
            _this.deleteAllReportMarkers(); // clean the map of report markers
            _this.deleteAllDeviceMarkers(); // clean the map of device markers
            if (_this.searchFilter.length !== 0) {
                // Remove reports which do not match the searchFilter
                for (var i = 0; i < _this.reports.length; i++) {
                    if (_this.reports[i]['vri_place'].toLowerCase().indexOf(_this.searchFilter.toLowerCase()) !== -1) {
                    }
                    else {
                        // remove the current report from this.reports
                        _this.reports.splice(i, 1);
                        // reduce the index by 1
                        i = i - 1;
                    }
                }
            }
            // We add only those reports that fulfill the searchFilter (if applicable)
            _this.addReportMarkers(_this.reports);
            _this.addDeviceMarkers(_this.devices);
        }, function () {
            _this.loading = false;
            _this.failed = true;
        });
    };
    ReportsViewerComponent.prototype.onPageChange = function (offset) {
        this.offset = offset;
        this.router.navigate(['/pagina', (offset / this.limit) + 1]);
    };
    ReportsViewerComponent.prototype.viewReport = function (reportId) {
        this.router.navigate(['report', reportId]);
    };
    ReportsViewerComponent.prototype.editReport = function (reportId) {
        this.router.navigate(['report', reportId, 'edit']);
    };
    ReportsViewerComponent.prototype.searchInputValue = function (searchInput) {
        this.searchInput = searchInput;
    };
    ReportsViewerComponent.prototype.showOnMap = function (reportId) {
        this.currentReport = this.reports.find(function (x) { return x.id === reportId; });
        this.showReport(this.currentReport);
    };
    ReportsViewerComponent.prototype.setPaginationLimit = function (limit) {
        // set the limit for pagination
        this.limit = limit;
        // reset the offset to 0
        this.offset = 0;
        // navigate to the first page
        this.onPageChange(this.offset);
        // remove all markers from the map
        this.deleteAllReportMarkers();
        this.searchFilter = this.searchInput;
        var pagination = true;
        this.retrieveReports(pagination);
        this.retrieveDevices();
    };
    ReportsViewerComponent.prototype.showReport = function (report) {
        this.currentReport = report;
        var position = new google.maps.LatLng(parseFloat(this.currentReport['latitude']), parseFloat(this.currentReport['longitude']));
        this.setCenter(position);
        this.setZoom(17); // zoomed into street view
    };
    ReportsViewerComponent.prototype.setCenter = function (position) {
        this.center = position;
    };
    ReportsViewerComponent.prototype.setZoom = function (level) {
        this.zoom = level;
    };
    ReportsViewerComponent.prototype.deleteAllReportMarkers = function () {
        this.reportMarkersToBeDeleted = this.reportMarkers;
        this.reportMarkers = [];
    };
    ReportsViewerComponent.prototype.deleteAllDeviceMarkers = function () {
        this.deviceMarkersToBeDeleted = this.deviceMarkers;
        this.deviceMarkers = [];
    };
    ReportsViewerComponent.prototype.addReportMarkers = function (reports) {
        this.isNewReportCount = 0;
        this.isOnHoldReportCount = 0;
        this.isPendingReportCount = 0;
        this.isProcessedNeutralReportCount = 0;
        this.isProcessedPositiveReportCount = 0;
        this.reportMarkersStatusArray = [];
        var markerIndex;
        for (var i = 0; i < reports.length; i++) {
            markerIndex = i;
            var draggable = false;
            var title = reports[i]['vri_place'];
            var label = [];
            var label_text = reports[i].id.toString();
            var label_color = 'white';
            label['text'] = label_text;
            label['color'] = label_color;
            var position = new google.maps.LatLng(reports[i]['latitude'], reports[i]['longitude']);
            var fillColor = void 0;
            var icon = {
                path: 'M20, 10c0-5.5-4.5-10-10-10S0, 4.5, 0, 10c0, 1.4, 0.3, 2.7, 0.8, 3.9l0, 0c0, 0, 2.1, 5.2, 9.2, 16.1 c7.2-10.9, 9.2-16.1, 9.2-16.1l0, 0C19.7, 12.7, 20, 11.4, 20, 10z',
                fillOpacity: 1,
                strokeWeight: 0,
                size: [20, 30],
                labelOrigin: new google.maps.Point(10, 10),
                anchor: new google.maps.Point(10, 30)
            };
            switch (reports[i]['report_status'].class) {
                case 'is-new':
                    icon['fillColor'] = '#242e42';
                    status = 'is-new';
                    this.isNewReportCount++;
                    break;
                case 'is-on-hold':
                    icon['fillColor'] = '#f90';
                    status = 'is-on-hold';
                    this.isOnHoldReportCount++;
                    break;
                case 'is-pending':
                    icon['fillColor'] = '#f7412d';
                    status = 'is-pending';
                    this.isPendingReportCount++;
                    break;
                case 'is-processed-positive':
                    icon['fillColor'] = '#79c410';
                    status = 'is-processed-positive';
                    this.isProcessedPositiveReportCount++;
                    break;
                case 'is-processed-neutral':
                    icon['fillColor'] = '#ffcf00';
                    label['color'] = '#242e42';
                    status = 'is-processed-neutral';
                    this.isProcessedNeutralReportCount++;
                    break;
                default:
                    break;
            }
            var animation = google.maps.Animation.DROP;
            this.addReportMarker(status, draggable, title, label, position, icon, animation, markerIndex);
        }
    };
    ReportsViewerComponent.prototype.addReportMarker = function (status, draggable, title, label, position, icon, animation, markerIndex) {
        var marker = [{
                draggable: draggable,
                position: position,
                icon: icon,
                animation: animation,
                label: label,
                title: title,
                visible: true
            }];
        // Append to the lookup table [status, markerIndex]
        this.reportMarkersStatusArray = this.reportMarkersStatusArray.concat([{ status: status, markerIndex: markerIndex }]);
        this.reportMarkers = this.reportMarkers.concat(marker);
    };
    ReportsViewerComponent.prototype.addDeviceMarkers = function (devices) {
        if (typeof (devices) != 'undefined' && devices != null) {
            for (var i = 0; i < devices.length; i++) {
                var draggable = false;
                var title = devices[i]['name'];
                var label = [];
                var label_text = devices[i].id.toString();
                var label_color = 'black';
                label['text'] = label_text;
                label['color'] = label_color;
                var position = new google.maps.LatLng(devices[i]['location']['latitude'], devices[i]['location']['longitude']);
                // let fillColor: string = 'lightgrey';
                // let icon: any = {
                // 	path: 'M20, 10c0-5.5-4.5-10-10-10S0, 4.5, 0, 10c0, 1.4, 0.3, 2.7, 0.8, 3.9l0, 0c0, 0, 2.1, 5.2, 9.2, 16.1 c7.2-10.9, 9.2-16.1, 9.2-16.1l0, 0C19.7, 12.7, 20, 11.4, 20, 10z',
                // 	fillOpacity: 1,
                // 	fillColor: fillColor,
                // 	strokeWeight: 0,
                // 	size: [20, 30],
                // 	labelOrigin: new google.maps.Point(10, 10),
                // 	anchor: new google.maps.Point(10, 30)
                // };
                var fillColor = '#242e42';
                var icon = {
                    path: 'M4.2,0C1.9,0,0,1.9,0,4.2l0,17.9C0,24.4,4.2,30,4.2,30s4.2-5.6,4.2-7.9l0-17.9C8.3,1.9,6.5,0,4.2,0z M5.8,24.4 c-1.5,0.9-3.6,0.4-4.5-1.2c-0.9-1.5-0.4-3.6,1.2-4.5s3.6-0.4,4.5,1.2C8,21.5,7.4,23.5,5.8,24.4z M5.9,15.7c-1.5,0.9-3.6,0.4-4.5-1.2 S1.1,11,2.6,10.1S6.1,9.7,7,11.3S7.5,14.8,5.9,15.7z M5.9,7.3C4.4,8.2,2.4,7.7,1.5,6.1S1.1,2.6,2.6,1.7s3.6-0.4,4.5,1.2 S7.5,6.4,5.9,7.3z',
                    fillOpacity: 1,
                    fillColor: fillColor,
                    strokeWeight: 0,
                    size: [8, 12],
                    labelOrigin: new google.maps.Point(18, 10),
                    anchor: new google.maps.Point(4, 12)
                };
                var animation = google.maps.Animation.DROP;
                var contentString = devices[i].disclaimer.toString();
                this.addDeviceMarkerInfoWindowContentString(contentString);
                this.addDeviceMarker(draggable, title, label, position, icon, animation);
            }
        }
    };
    ReportsViewerComponent.prototype.addDeviceMarkerInfoWindowContentString = function (contentString) {
        this.deviceMarkersInfoWindowContentStrings = this.deviceMarkersInfoWindowContentStrings.concat(contentString);
    };
    ReportsViewerComponent.prototype.addDeviceMarker = function (draggable, title, label, position, icon, animation) {
        this.deviceMarkers = this.deviceMarkers.concat([{
                draggable: draggable,
                position: position,
                icon: icon,
                animation: animation,
                label: label,
                title: title
            }]);
    };
    ReportsViewerComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'reports-viewer',
            template: __webpack_require__(779),
            styles: [__webpack_require__(769)],
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_reports_service__["a" /* Reports */], __WEBPACK_IMPORTED_MODULE_3__services_devices_service__["a" /* Devices */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_reports_service__["a" /* Reports */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_reports_service__["a" /* Reports */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_devices_service__["a" /* Devices */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__services_devices_service__["a" /* Devices */]) === 'function' && _d) || Object])
    ], ReportsViewerComponent);
    return ReportsViewerComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/reports-viewer.component.js.map

/***/ }),

/***/ 465:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 465;


/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts__ = __webpack_require__(600);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(553);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(599);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_module__ = __webpack_require__(584);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_4__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/main.js.map

/***/ }),

/***/ 583:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_app_model__ = __webpack_require__(249);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.app = new __WEBPACK_IMPORTED_MODULE_2__models_app_model__["a" /* App */]('Reports Manager', 'Our Company, Inc.', (new Date()).getFullYear());
    };
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(775),
            styles: [__webpack_require__(765)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === 'function' && _a) || Object])
    ], AppComponent);
    return AppComponent;
    var _a;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/app.component.js.map

/***/ }),

/***/ 584:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(544);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(583);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__google_map_google_map_component__ = __webpack_require__(586);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_page_header_page_header_component__ = __webpack_require__(597);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_page_footer_page_footer_component__ = __webpack_require__(596);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__reports_reports_viewer_reports_viewer_component__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__reports_report_viewer_report_viewer_component__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__reports_report_editor_report_editor_component__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__reports_shared_reports_table_reports_table_component__ = __webpack_require__(593);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__reports_shared_reports_custom_list_group_reports_custom_list_group_component__ = __webpack_require__(592);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__reports_shared_report_details_report_details_component__ = __webpack_require__(590);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__reports_shared_report_form_report_form_component__ = __webpack_require__(591);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__shared_loader_loader_component__ = __webpack_require__(595);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pipes_capitalize_pipe__ = __webpack_require__(588);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pipes_active_pipe__ = __webpack_require__(587);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pipes_filter_pipe__ = __webpack_require__(589);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_angular2_modal__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_angular2_modal_plugins_bootstrap__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__app_routes__ = __webpack_require__(585);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__shared_pagination_pagination_component__ = __webpack_require__(598);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_intl__ = __webpack_require__(771);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_23_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_intl_locale_data_jsonp_en__ = __webpack_require__(773);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_intl_locale_data_jsonp_en___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_24_intl_locale_data_jsonp_en__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__google_map_google_map_component__["a" /* GoogleMapComponent */],
                __WEBPACK_IMPORTED_MODULE_6__shared_page_header_page_header_component__["a" /* PageHeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_7__shared_page_footer_page_footer_component__["a" /* PageFooterComponent */],
                __WEBPACK_IMPORTED_MODULE_8__reports_reports_viewer_reports_viewer_component__["a" /* ReportsViewerComponent */],
                __WEBPACK_IMPORTED_MODULE_9__reports_report_viewer_report_viewer_component__["a" /* ReportViewerComponent */],
                __WEBPACK_IMPORTED_MODULE_10__reports_report_editor_report_editor_component__["a" /* ReportEditorComponent */],
                __WEBPACK_IMPORTED_MODULE_11__reports_shared_reports_table_reports_table_component__["a" /* ReportsTableComponent */],
                __WEBPACK_IMPORTED_MODULE_12__reports_shared_reports_custom_list_group_reports_custom_list_group_component__["a" /* ReportsCustomListGroupComponent */],
                __WEBPACK_IMPORTED_MODULE_13__reports_shared_report_details_report_details_component__["a" /* ReportDetailsComponent */],
                __WEBPACK_IMPORTED_MODULE_14__reports_shared_report_form_report_form_component__["a" /* ReportFormComponent */],
                __WEBPACK_IMPORTED_MODULE_16__pipes_capitalize_pipe__["a" /* CapitalizePipe */],
                __WEBPACK_IMPORTED_MODULE_17__pipes_active_pipe__["a" /* ActivePipe */],
                __WEBPACK_IMPORTED_MODULE_18__pipes_filter_pipe__["a" /* FilterPipe */],
                __WEBPACK_IMPORTED_MODULE_22__shared_pagination_pagination_component__["a" /* PaginationComponent */],
                __WEBPACK_IMPORTED_MODULE_15__shared_loader_loader_component__["a" /* LoaderComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* JsonpModule */],
                __WEBPACK_IMPORTED_MODULE_19_angular2_modal__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_20_angular2_modal_plugins_bootstrap__["a" /* BootstrapModalModule */],
                __WEBPACK_IMPORTED_MODULE_21__app_routes__["a" /* AppRoutingModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/app.module.js.map

/***/ }),

/***/ 585:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__reports_reports_viewer_reports_viewer_component__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__reports_report_viewer_report_viewer_component__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__reports_report_editor_report_editor_component__ = __webpack_require__(382);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });




var appRoutes = [
    { path: '', redirectTo: '/pagina/1', pathMatch: 'full' },
    { path: 'pagina/:nr', component: __WEBPACK_IMPORTED_MODULE_1__reports_reports_viewer_reports_viewer_component__["a" /* ReportsViewerComponent */] },
    { path: 'report/:id', component: __WEBPACK_IMPORTED_MODULE_2__reports_report_viewer_report_viewer_component__["a" /* ReportViewerComponent */] },
    { path: 'report/:id/edit', component: __WEBPACK_IMPORTED_MODULE_3__reports_report_editor_report_editor_component__["a" /* ReportEditorComponent */] }
];
var AppRoutingModule = __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* RouterModule */].forRoot(appRoutes, { useHash: true });
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/app.routes.js.map

/***/ }),

/***/ 586:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_report_model__ = __webpack_require__(250);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleMapComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * A component wrapper of Google Maps.
 */
var GoogleMapComponent = (function () {
    function GoogleMapComponent(_renderer, _ref, _ngZone) {
        this._renderer = _renderer;
        this._ref = _ref;
        this._ngZone = _ngZone;
        this._deviceMarkersInfoWindowContentStrings = [];
        this._zoom = 15;
        this._center = new google.maps.LatLng(0, 0);
        //private _scrollWheel: boolean = true; // true, to enable zooming on mouse wheel scrolling.
        this._mapTypeControl = false; // the initial enabled/disabled state of the Map type control.
        // Map styles
        this.styles = [
            {
                "featureType": "administrative.country",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#FFBB00"
                    },
                    {
                        "saturation": 43.400000000000006
                    },
                    {
                        "lightness": 37.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "saturation": -40
                    },
                    {
                        "lightness": 36
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "saturation": -77
                    },
                    {
                        "lightness": 28
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#00FF6A"
                    },
                    {
                        "saturation": -1.0989010989011234
                    },
                    {
                        "lightness": 11.200000000000017
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "saturation": -24
                    },
                    {
                        "lightness": 61
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#ffc200"
                    },
                    {
                        "saturation": -61.8
                    },
                    {
                        "lightness": 45.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.highway.controlled_access",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#FF0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 51.19999999999999
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#ff0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 52
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#0078FF"
                    },
                    {
                        "saturation": -13.200000000000003
                    },
                    {
                        "lightness": 2.4000000000000057
                    },
                    {
                        "gamma": 1
                    }
                ]
            }
        ];
    }
    Object.defineProperty(GoogleMapComponent.prototype, "deleteAllReportMarkers", {
        set: function (value) {
            // Delete only report markers, not device markers
            if (this._reportMarkers) {
                this._reportMarkers.forEach(function (marker) { return marker.setMap(null); });
                this._reportMarkers = [];
                if (typeof this.reportMarkerClusterer !== 'undefined') {
                    this.reportMarkerClusterer.clearMarkers();
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent.prototype, "center", {
        set: function (value) {
            this._center = value;
            if (this.map) {
                this.map.setCenter(this._center);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent.prototype, "zoom", {
        set: function (value) {
            this._zoom = value;
            if (this.map) {
                this.map.setZoom(this._zoom);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent.prototype, "scrollWheel", {
        set: function (value) {
            // this._scrollWheel = value;
            // if (this.map) {
            //     this.map.setOptions({ scrollwheel: this._scrollWheel })
            // }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent.prototype, "reportMarkers", {
        set: function (value) {
            var _this = this;
            // Remove report markers
            if (this._reportMarkers) {
                this._reportMarkers.forEach(function (marker) { return marker.setMap(null); });
            }
            this._reportMarkers = [];
            var newMapNeeded = false;
            // Add report markers
            value.forEach(function (option) {
                var marker = new google.maps.Marker(option);
                marker.setMap(_this.map);
                if (option.visible == false) {
                    // Map does not hide markers correctly with visibile: false
                    // hence the need for a new map
                    newMapNeeded = true;
                }
                _this._reportMarkers = _this._reportMarkers.concat(marker);
            });
            if (newMapNeeded) {
                var targetElement = this._renderer.selectRootElement(".map");
                this.map = new google.maps.Map(targetElement, {
                    center: this.map.getCenter(),
                    zoom: this.map.getZoom(),
                    //scrollwheel: this._scrollWheel,
                    mapTypeControl: this._mapTypeControl,
                    fullscreenControl: false,
                    styles: this.styles
                });
                // Re-add the deviceMarkers
                if (this._deviceMarkers) {
                    this._deviceMarkers.forEach(function (marker) { return marker.setMap(_this.map); });
                }
            }
            this._ref.detectChanges();
            // If map is undefined here we cannot use this.reportMarkerClusterer
            if (typeof this.map !== 'undefined') {
                this.reportMarkerClustererOptions = {
                    styles: [
                        {
                            url: "assets/icons/marker-clusterer/m1.png",
                            width: 52,
                            height: 52,
                            textColor: 'white'
                        },
                        {
                            url: "assets/icons/marker-clusterer/m2.png",
                            width: 56,
                            height: 56,
                            textColor: 'white'
                        },
                        {
                            url: "assets/icons/marker-clusterer/m3.png",
                            width: 66,
                            height: 66,
                            textColor: 'white'
                        },
                        {
                            url: "assets/icons/marker-clusterer/m4.png",
                            width: 78,
                            height: 78,
                            textColor: 'white'
                        },
                        {
                            url: "assets/icons/marker-clusterer/m5.png",
                            width: 90,
                            height: 90,
                            textColor: 'white'
                        }
                    ]
                };
                this.reportMarkerClusterer = new MarkerClusterer(this.map, this._reportMarkers, this.reportMarkerClustererOptions);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent.prototype, "deviceMarkers", {
        set: function (value) {
            var _this = this;
            // Remove device markers
            if (this._deviceMarkers) {
                this._deviceMarkers.forEach(function (marker) { return marker.setMap(null); });
            }
            this._deviceMarkers = [];
            // Add device markers
            var index = 0;
            value.forEach(function (option) {
                var marker = new google.maps.Marker(option);
                var contentString = _this._deviceMarkersInfoWindowContentStrings[index];
                var infoWindow = new google.maps.InfoWindow({ content: contentString });
                marker.addListener('click', function () {
                    infoWindow.open(this.map, marker);
                });
                marker.setMap(_this.map);
                _this._deviceMarkers.push(marker);
                index++;
            });
            this._ref.detectChanges();
            // NO MARKER CLUSTERER FOR DEVICES
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GoogleMapComponent.prototype, "deviceMarkersInfoWindowContentStrings", {
        set: function (value) {
            this._deviceMarkersInfoWindowContentStrings = value;
        },
        enumerable: true,
        configurable: true
    });
    GoogleMapComponent.prototype.ngOnInit = function () {
        var targetElement = this._renderer.selectRootElement(".map");
        var me = this;
        this.map = new google.maps.Map(targetElement, {
            center: this._center,
            zoom: this._zoom,
            //scrollwheel: this._scrollWheel,
            mapTypeControl: this._mapTypeControl,
            fullscreenControl: false,
            styles: this.styles
        });
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_report_model__["a" /* Report */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__models_report_model__["a" /* Report */]) === 'function' && _a) || Object)
    ], GoogleMapComponent.prototype, "report", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Array), 
        __metadata('design:paramtypes', [Array])
    ], GoogleMapComponent.prototype, "deleteAllReportMarkers", null);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Object), 
        __metadata('design:paramtypes', [Object])
    ], GoogleMapComponent.prototype, "center", null);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Number), 
        __metadata('design:paramtypes', [Number])
    ], GoogleMapComponent.prototype, "zoom", null);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Boolean), 
        __metadata('design:paramtypes', [Boolean])
    ], GoogleMapComponent.prototype, "scrollWheel", null);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Array), 
        __metadata('design:paramtypes', [Array])
    ], GoogleMapComponent.prototype, "reportMarkers", null);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Array), 
        __metadata('design:paramtypes', [Array])
    ], GoogleMapComponent.prototype, "deviceMarkers", null);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Array), 
        __metadata('design:paramtypes', [Array])
    ], GoogleMapComponent.prototype, "deviceMarkersInfoWindowContentStrings", null);
    GoogleMapComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-google-map',
            template: __webpack_require__(776),
            styles: [__webpack_require__(767)],
            providers: []
        }), 
        __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Renderer */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Renderer */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* ChangeDetectorRef */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* ChangeDetectorRef */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* NgZone */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* NgZone */]) === 'function' && _d) || Object])
    ], GoogleMapComponent);
    return GoogleMapComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/google-map.component.js.map

/***/ }),

/***/ 587:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivePipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ActivePipe = (function () {
    function ActivePipe() {
    }
    ActivePipe.prototype.transform = function (value) {
        return value ? 'Active' : 'Inactive';
    };
    ActivePipe = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Pipe */])({ name: 'active' }), 
        __metadata('design:paramtypes', [])
    ], ActivePipe);
    return ActivePipe;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/active.pipe.js.map

/***/ }),

/***/ 588:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CapitalizePipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CapitalizePipe = (function () {
    function CapitalizePipe() {
    }
    CapitalizePipe.prototype.transform = function (value) {
        if (!value)
            return "";
        return value.charAt(0).toUpperCase() + value.slice(1);
    };
    CapitalizePipe = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Pipe */])({ name: 'capitalize' }), 
        __metadata('design:paramtypes', [])
    ], CapitalizePipe);
    return CapitalizePipe;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/capitalize.pipe.js.map

/***/ }),

/***/ 589:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FilterPipe = (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (value, args) {
        if (value) {
            var searchFilter_1 = "";
            // Make the incoming search arguments lower case
            if (typeof args[0] === 'undefined') {
                return value;
            }
            else {
                for (var i = 0; i < args.length; i++) {
                    searchFilter_1 = searchFilter_1 + args[i].toLowerCase();
                }
            }
            if (searchFilter_1 && Array.isArray(value)) {
                return value.filter(function (el) {
                    return el.vri_place.toLowerCase().indexOf(searchFilter_1) !== -1;
                });
            }
            return value;
        }
        else {
            return;
        }
    };
    FilterPipe = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Pipe */])({
            name: 'filter'
        }), 
        __metadata('design:paramtypes', [])
    ], FilterPipe);
    return FilterPipe;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/filter.pipe.js.map

/***/ }),

/***/ 590:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_report_model__ = __webpack_require__(250);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportDetailsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReportDetailsComponent = (function () {
    function ReportDetailsComponent() {
    }
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_report_model__["a" /* Report */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__models_report_model__["a" /* Report */]) === 'function' && _a) || Object)
    ], ReportDetailsComponent.prototype, "report", void 0);
    ReportDetailsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'report-details',
            template: __webpack_require__(780),
            styles: ['./report-details.component.scss']
        }), 
        __metadata('design:paramtypes', [])
    ], ReportDetailsComponent);
    return ReportDetailsComponent;
    var _a;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/report-details.component.js.map

/***/ }),

/***/ 591:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_report_model__ = __webpack_require__(250);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// interface SelectItem { 
// 	value: string;
// 	label: string;
// }
var ReportFormComponent = (function () {
    function ReportFormComponent() {
        this.report = {};
        this.saveReport = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
        this.deleteReport = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
        this.cancelReport = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
    }
    // colors: SelectItem[] = [
    // 	{ value: 'red', label: 'Red' },
    // 	{ value: 'blue', label: 'Blue' },
    // 	{ value: 'green', label: 'Green' },
    // 	{ value: 'orange', label: 'Orange' },
    // 	{ value: 'yellow', label: 'Yellow' },
    // 	{ value: 'purple', label: 'Purple' }
    // ] 
    // sizes: SelectItem[] = [
    // 	{ value: 'tiny', label: 'Tiny' },
    // 	{ value: 'small', label: 'Small' },
    // 	{ value: 'medium', label: 'Medium' },
    // 	{ value: 'large', label: 'Large' },
    // 	{ value: 'huge', label: 'Huge' }
    // ] 
    ReportFormComponent.prototype.saveReportButton = function (report) {
        this.saveReport.emit(report);
    };
    ReportFormComponent.prototype.deleteReportButton = function (report) {
        this.deleteReport.emit(report);
    };
    ReportFormComponent.prototype.cancelReportButton = function (report) {
        this.cancelReport.emit(report);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_report_model__["a" /* Report */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__models_report_model__["a" /* Report */]) === 'function' && _a) || Object)
    ], ReportFormComponent.prototype, "report", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _b) || Object)
    ], ReportFormComponent.prototype, "saveReport", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _c) || Object)
    ], ReportFormComponent.prototype, "deleteReport", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _d) || Object)
    ], ReportFormComponent.prototype, "cancelReport", void 0);
    ReportFormComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'report-form',
            template: __webpack_require__(781),
            styles: ['./report-form.component.scss']
        }), 
        __metadata('design:paramtypes', [])
    ], ReportFormComponent);
    return ReportFormComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/report-form.component.js.map

/***/ }),

/***/ 592:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_modal__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_modal_plugins_bootstrap__ = __webpack_require__(391);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportsCustomListGroupComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportsCustomListGroupComponent = (function () {
    function ReportsCustomListGroupComponent(overlay, vcRef, modal, _ngZone) {
        this.modal = modal;
        this._ngZone = _ngZone;
        this.filterReports = '';
        this.paginationLimit = 9999; // Max pagination limit when searching for reports
        this.searchInputValue = '';
        this.viewReport = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
        this.editReport = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
        this.showOnMap = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
        this.setSearchInput = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
        this.setPaginationLimit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
        overlay.defaultViewContainer = vcRef;
    }
    ReportsCustomListGroupComponent.prototype.viewReportButton = function (reportId) {
        this.viewReport.emit(reportId);
    };
    ReportsCustomListGroupComponent.prototype.editReportButton = function (reportId) {
        this.editReport.emit(reportId);
    };
    ReportsCustomListGroupComponent.prototype.setPaginationLimitOnInput = function () {
        this.setSearchInput.emit(this.searchInputValue);
        // If a single character or more has been typed in the input field
        // we should reset the limit.
        if (this.searchInputValue.length >= 1) {
            this.setPaginationLimit.emit(this.paginationLimit);
        }
        else if (this.searchInputValue.length == 0) {
            this.setPaginationLimit.emit(this.defaultPaginationlimit); // default value of 25
        }
    };
    ReportsCustomListGroupComponent.prototype.modalReportButton = function (reportId) {
        var _this = this;
        this.currentReport = this.reports.find(function (x) { return x.id === reportId; });
        var locationId = this.currentReport.device['location_id'];
        var sameLocationReports = [];
        // Find all reports that contain the same location id
        for (var i = 0; i < this.reports.length; i++) {
            // Check if the current report has the same location id
            if (this.reports[i].device['location_id'] == locationId) {
                sameLocationReports.push(this.reports[i]);
            }
        }
        // TEMP center and zoom on Map for the current report
        var event = new Event('showOnMap');
        this._ngZone.run(function () {
            return _this.showOnMap.emit(reportId);
        }); // Emits the event raised showing the report's marker on the map to the output port called 'showOnMap'.
        var bodyHTML = this.createHTMLList(sameLocationReports);
        this.modal.alert()
            .size('lg')
            .isBlocking(false)
            .showClose(true)
            .title('[' + this.currentReport['id'] + '] ' + this.currentReport['vri_place'])
            .body(bodyHTML)
            .headerClass('modal-ng__header')
            .bodyClass('modal-ng__body')
            .footerClass('modal-ng__footer')
            .okBtn('')
            .open();
        // Capture click event on modal button 'Ook een melding plaatsen?'
        var reportsModalDialogClass = "modal-dialog";
        document.getElementsByClassName(reportsModalDialogClass)[0].addEventListener("click", clickReportDirectButtonHandler, false);
        function clickReportDirectButtonHandler(event) {
            if (typeof event.srcElement.className === 'undefined') {
                return;
            }
            else if (event.srcElement.className.indexOf('report-direct-button') !== -1) {
                goToReportDirect();
            }
            else if (event.srcElement.className.indexOf('report-direct-button-icon') !== -1) {
                goToReportDirect();
            }
            else if (typeof event.srcElement.firstChild.className === 'undefined') {
                return;
            }
            else if (event.srcElement.firstChild.className.indexOf('report-direct-button') !== -1) {
                goToReportDirect();
            }
            else if (event.srcElement.firstChild.className.indexOf('report-direct-button-icon') !== -1) {
                goToReportDirect();
            }
        }
        function goToReportDirect() {
            // Close the current reports modal
            var evt = new MouseEvent("click", {
                view: window,
                bubbles: true,
                cancelable: true,
                clientX: 20
            });
            var reportsModalNGHeaderClass = "modal-ng__header";
            document.getElementsByClassName(reportsModalNGHeaderClass)[0].firstElementChild.dispatchEvent(evt);
            // Allow time for the modal to Close
            setTimeout(function () {
                // Open the 'Report Direct' modal
                var remodalReportDirectButton = document.querySelectorAll('[data-remodal-target="modal-report"]');
                remodalReportDirectButton[0].dispatchEvent(evt);
            }, 1000); // 1 second, adjust to fit
        }
        ;
    };
    ReportsCustomListGroupComponent.prototype.createHTMLList = function (reportsList) {
        var outerDiv = document.createElement('div');
        var p = document.createElement('p');
        p.innerHTML = reportsList[0]['device']['disclaimer'];
        outerDiv.appendChild(p);
        var span = document.createElement('span');
        var countLabel = reportsList.length == 1 ? "melding" : "meldingen";
        span.innerHTML = reportsList.length.toString() + ' ' + countLabel;
        outerDiv.appendChild(span);
        var ul = document.createElement('ul');
        for (var i = 0; i < reportsList.length; i++) {
            // create a list here of all list items
            var li = document.createElement('li');
            var innerSpanCreatedAt = document.createElement('span');
            var date = new Date(reportsList[i]['date']);
            var dayNumber = date.getDate();
            var dayString = ("0" + dayNumber).slice(-2);
            var monthNumber = date.getMonth() + 1;
            var monthString = ("0" + monthNumber).slice(-2);
            var yearNumber = date.getFullYear();
            var yearString = ("0" + yearNumber).slice(-2);
            var time = new Date(reportsList[i]['time']);
            var hoursNumber = time.getHours();
            var hoursString = ("0" + hoursNumber).slice(-2);
            var minutesNumber = time.getMinutes();
            var minutesString = ("0" + minutesNumber).slice(-2);
            innerSpanCreatedAt.innerHTML = '[' + reportsList[i]['id'] + '] ' + reportsList[i]['vri_place'] + '<span>' + dayString + '-' + monthString + '-' + yearString + ' | ' + hoursString + ":" + minutesString + '</span>';
            li.appendChild(innerSpanCreatedAt);
            var innerSpanReportType = document.createElement('span');
            innerSpanReportType.innerHTML = reportsList[i]['report_type']['description'];
            li.appendChild(innerSpanReportType);
            var innerSpanFromStreet = document.createElement('span');
            innerSpanFromStreet.innerHTML = 'van: ' + reportsList[i]['vri_street_1'];
            li.appendChild(innerSpanFromStreet);
            var innerSpanToStreet = document.createElement('span');
            innerSpanToStreet.innerHTML = 'naar: ' + reportsList[i]['vri_street_2'];
            li.appendChild(innerSpanToStreet);
            ul.appendChild(li);
        }
        outerDiv.appendChild(ul);
        // Create a button to report directly
        var button = document.createElement('div');
        button.innerHTML = '<a class="report-direct-button">Ook een melding plaatsen? <i class="icon-arrow-right report-direct-button-icon"></i></a>';
        outerDiv.appendChild(button);
        // Return HTML Elements as a string
        return outerDiv.outerHTML;
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Array)
    ], ReportsCustomListGroupComponent.prototype, "reports", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Number)
    ], ReportsCustomListGroupComponent.prototype, "defaultPaginationlimit", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _a) || Object)
    ], ReportsCustomListGroupComponent.prototype, "viewReport", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _b) || Object)
    ], ReportsCustomListGroupComponent.prototype, "editReport", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _c) || Object)
    ], ReportsCustomListGroupComponent.prototype, "showOnMap", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _d) || Object)
    ], ReportsCustomListGroupComponent.prototype, "setSearchInput", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _e) || Object)
    ], ReportsCustomListGroupComponent.prototype, "setPaginationLimit", void 0);
    ReportsCustomListGroupComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'reports-custom-list-group',
            template: __webpack_require__(782),
            styles: ['./reports-custom-list-group.component.scss'],
        }), 
        __metadata('design:paramtypes', [(typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_angular2_modal__["g" /* Overlay */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_angular2_modal__["g" /* Overlay */]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* ViewContainerRef */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* ViewContainerRef */]) === 'function' && _g) || Object, (typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_2_angular2_modal_plugins_bootstrap__["b" /* Modal */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2_angular2_modal_plugins_bootstrap__["b" /* Modal */]) === 'function' && _h) || Object, (typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* NgZone */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* NgZone */]) === 'function' && _j) || Object])
    ], ReportsCustomListGroupComponent);
    return ReportsCustomListGroupComponent;
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/reports-custom-list-group.component.js.map

/***/ }),

/***/ 593:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportsTableComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ReportsTableComponent = (function () {
    function ReportsTableComponent() {
        this.viewReport = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
        this.editReport = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
    }
    ReportsTableComponent.prototype.viewReportButton = function (reportId) {
        this.viewReport.emit(reportId);
    };
    ReportsTableComponent.prototype.editReportButton = function (reportId) {
        this.editReport.emit(reportId);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Array)
    ], ReportsTableComponent.prototype, "reports", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _a) || Object)
    ], ReportsTableComponent.prototype, "viewReport", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _b) || Object)
    ], ReportsTableComponent.prototype, "editReport", void 0);
    ReportsTableComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'reports-table',
            template: __webpack_require__(783),
            styles: ['./reports-table.component.scss']
        }), 
        __metadata('design:paramtypes', [])
    ], ReportsTableComponent);
    return ReportsTableComponent;
    var _a, _b;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/reports-table.component.js.map

/***/ }),

/***/ 594:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Devices; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Devices = (function () {
    function Devices(http) {
        this.http = http;
        this.baseUrl = 'https://verkeerslicht-165607.appspot.com/api'; // TEMP, RENAME TO FINAL URL!!
        this.requestOptions = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Headers */]({ 'Content-Type': 'application/json' })
        });
    }
    // Used without pagination
    Devices.prototype.getAll = function () {
        return this.http.get(this.baseUrl + "/devices").map(this.extractData).catch(this.handleError);
    };
    // Used with pagination
    Devices.prototype.findAll = function (offset, limit) {
        var _this = this;
        if (offset === void 0) { offset = 0; }
        if (limit === void 0) { limit = 2; }
        return this.http
            .get(this.baseUrl + "/devices/index/" + offset + "/" + limit) // cakePHP expects index, offset and limit as /index/offset/limit
            .map(function (response) { return response.json(); })
            .map(function (results) { return _this.getList(results); });
    };
    Devices.prototype.get = function (deviceId) {
        return this.http.get('https://verkeerslicht-165607.appspot.com/api/device/' + encodeURIComponent(deviceId.toString())).map(this.extractData).catch(this.handleError);
    };
    Devices.prototype.insert = function (device) {
        return this.http.post('https://verkeerslicht-165607.appspot.com/api/devices/', JSON.stringify(device), this.requestOptions).map(function (res) { return res.json(); }).catch(this.handleError); // WORKS!!
    };
    Devices.prototype.update = function (device) {
        return this.http.put('https://verkeerslicht-165607.appspot.com/api/devices/' + encodeURIComponent(device.id.toString()), JSON.stringify(device), this.requestOptions).map(function (res) { return res.json(); }).catch(this.handleError);
    };
    Devices.prototype.delete = function (deviceId) {
        return this.http.delete('https://verkeerslicht-165607.appspot.com/api/devices/' + encodeURIComponent(deviceId.toString())).map(function (res) { return res.json(); }).catch(this.handleError); // WORKS!!
    };
    Devices.prototype.getList = function (data) {
        // room for additional filtering
        return data;
    };
    /**
     * Pick the array that belongs to the key 'devices'
     *
     * e.g. { devices:[our data is in here] }
     */
    Devices.prototype.extractData = function (res) {
        var body = res.json();
        //console.log(body.devices);
        return body.devices || {};
    };
    /**
     * Handle error
     */
    Devices.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(errMsg);
    };
    Devices = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Http */]) === 'function' && _a) || Object])
    ], Devices);
    return Devices;
    var _a;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/devices.service.js.map

/***/ }),

/***/ 595:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoaderComponent = (function () {
    function LoaderComponent() {
        this.loading = false;
        this.failed = false;
    }
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Boolean)
    ], LoaderComponent.prototype, "loading", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Boolean)
    ], LoaderComponent.prototype, "failed", void 0);
    LoaderComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-loader',
            template: __webpack_require__(784)
        }), 
        __metadata('design:paramtypes', [])
    ], LoaderComponent);
    return LoaderComponent;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/loader.component.js.map

/***/ }),

/***/ 596:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_app_model__ = __webpack_require__(249);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageFooterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PageFooterComponent = (function () {
    function PageFooterComponent() {
    }
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_app_model__["a" /* App */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__models_app_model__["a" /* App */]) === 'function' && _a) || Object)
    ], PageFooterComponent.prototype, "app", void 0);
    PageFooterComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'page-footer',
            template: __webpack_require__(785),
            styles: ['./page-footer.component.scss']
        }), 
        __metadata('design:paramtypes', [])
    ], PageFooterComponent);
    return PageFooterComponent;
    var _a;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/page-footer.component.js.map

/***/ }),

/***/ 597:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_app_model__ = __webpack_require__(249);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageHeaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PageHeaderComponent = (function () {
    function PageHeaderComponent() {
    }
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_app_model__["a" /* App */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__models_app_model__["a" /* App */]) === 'function' && _a) || Object)
    ], PageHeaderComponent.prototype, "app", void 0);
    PageHeaderComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'page-header',
            template: __webpack_require__(786),
            styles: ['./page-header.component.scss']
        }), 
        __metadata('design:paramtypes', [])
    ], PageHeaderComponent);
    return PageHeaderComponent;
    var _a;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/page-header.component.js.map

/***/ }),

/***/ 598:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaginationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/* Based on 'https://g00glen00b.be/pagination-component-angular-2/' */
var PaginationComponent = (function () {
    function PaginationComponent() {
        this.offset = 0;
        this.limit = 1;
        this.size = 1;
        this.range = 3;
        this.pageChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]();
    }
    PaginationComponent.prototype.ngOnInit = function () {
        this.getPages(this.offset, this.limit, this.size);
    };
    PaginationComponent.prototype.ngOnChanges = function () {
        this.getPages(this.offset, this.limit, this.size);
    };
    PaginationComponent.prototype.getPages = function (offset, limit, size) {
        var _this = this;
        this.currentPage = this.getCurrentPage(offset, limit);
        this.totalPages = this.getTotalPages(limit, size);
        this.pages = __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].range(-this.range, this.range * 2 + 1)
            .map(function (offset) { return _this.currentPage + offset; })
            .filter(function (page) { return _this.isValidPageNumber(page, _this.totalPages); })
            .toArray();
    };
    PaginationComponent.prototype.isValidPageNumber = function (page, totalPages) {
        return page > 0 && page <= totalPages;
    };
    PaginationComponent.prototype.getCurrentPage = function (offset, limit) {
        // For example, if the offset is 20 and the limit is 10, then the page number would be 3 
        // (because offset 0 = page 1, offset 10 = page 2 and offset 20 = page 3).
        return Math.floor(offset / limit) + 1;
    };
    PaginationComponent.prototype.getTotalPages = function (limit, size) {
        // To get the total amount of pages we divide the size by the limit. 
        // If there are 810 items and the page size is 20, then there are 41 pages, 
        // of which there are 40 whole pages and 1 half page. 
        // To be sure that we don’t have a limit of 0 and we’re dividing by zero, 
        // I’m using Math.max() to make sure that both the size and the limit are always at least 1.
        return Math.ceil(Math.max(size, 1) / Math.max(limit, 1));
    };
    PaginationComponent.prototype.selectPage = function (page, event) {
        this.cancelEvent(event);
        if (this.isValidPageNumber(page, this.totalPages)) {
            this.pageChange.emit((page - 1) * this.limit);
        }
    };
    PaginationComponent.prototype.cancelEvent = function (event) {
        event.preventDefault();
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Number)
    ], PaginationComponent.prototype, "offset", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Number)
    ], PaginationComponent.prototype, "limit", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Number)
    ], PaginationComponent.prototype, "size", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* Input */])(), 
        __metadata('design:type', Number)
    ], PaginationComponent.prototype, "range", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* EventEmitter */]) === 'function' && _a) || Object)
    ], PaginationComponent.prototype, "pageChange", void 0);
    PaginationComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-pagination',
            template: __webpack_require__(787),
            styles: [__webpack_require__(766)]
        }), 
        __metadata('design:paramtypes', [])
    ], PaginationComponent);
    return PaginationComponent;
    var _a;
}());
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/pagination.component.js.map

/***/ }),

/***/ 599:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/environment.js.map

/***/ }),

/***/ 600:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__ = __webpack_require__(626);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__ = __webpack_require__(619);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__ = __webpack_require__(615);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__ = __webpack_require__(621);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__ = __webpack_require__(620);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__ = __webpack_require__(618);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__ = __webpack_require__(617);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__ = __webpack_require__(625);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__ = __webpack_require__(614);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__ = __webpack_require__(613);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__ = __webpack_require__(623);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__ = __webpack_require__(616);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__ = __webpack_require__(624);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__ = __webpack_require__(622);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__ = __webpack_require__(627);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__ = __webpack_require__(1052);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__);
















//# sourceMappingURL=/Applications/MAMP/htdocs/verkeerslicht-googlecloudplatform/src/polyfills.js.map

/***/ }),

/***/ 765:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(69)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 766:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(69)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 767:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(69)(false);
// imports


// module
exports.push([module.i, "/* Set height of map */\n.map {\n  height: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 768:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(69)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 769:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(69)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 775:
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ 776:
/***/ (function(module, exports) {

module.exports = "<div class=\"map\"></div>"

/***/ }),

/***/ 777:
/***/ (function(module, exports) {

module.exports = "<report-form\n\t[report]=\"report\"\n\t(saveReport)=\"saveReport($event)\"\n\t(deleteReport)=\"deleteReport($event)\"\n\t(cancelReport)=\"cancelReport($event)\">\n</report-form>"

/***/ }),

/***/ 778:
/***/ (function(module, exports) {

module.exports = "<report-details [report]=\"report\"></report-details>\n<button type=\"button\" (click)=\"editReport(report.id)\">Edit</button>\n<button type=\"button\" (click)=\"returnToList()\">Return to List</button>"

/***/ }),

/***/ 779:
/***/ (function(module, exports) {

module.exports = "<div class=\"container reports-container\">\n\t<a href=\"/\" class=\"btn btn--back has-icon-left hidden-md-up\"><i class=\"icon-arrow-left\"></i>Terug</a>\n\n    <section class=\"page__section section--white section--first section--first--reports hidden-md-up\">\n        <h1 class=\"h1\">Meldingen</h1>\n\n        <h4 class=\"h4\">Bekijk hier alle reeds geplaatste meldingen door heel Nederland!</h4>\n    </section>\n\n    <aside>\n\t   <div class=\"center\">\n          <app-loader [loading]=\"loading\" [failed]=\"failed\"></app-loader>\n       </div>\n\t\t<reports-custom-list-group\n\t\t\t[reports]=\"reports\"\n\t\t\t[defaultPaginationlimit]=\"limit\"\n\t\t\t(viewReport)=\"viewReport($event)\"\n\t\t\t(editReport)=\"editReport($event)\"\n\t\t\t(showOnMap)=\"showOnMap($event)\"\n\t\t\t(setSearchInput)=\"searchInputValue($event)\"\n\t\t\t(setPaginationLimit)=\"setPaginationLimit($event)\">\n\t\t</reports-custom-list-group>\n\t\t<app-pagination [offset]=\"offset\" [limit]=\"limit\" [size]=\"count\" (pageChange)=\"onPageChange($event)\"></app-pagination>\n\t</aside>\n\n\t<div class=\"reports-map hidden-sm-down\">\n\t\t<app-google-map [report]='currentReport' [center]='center' [zoom]='zoom' [scrollWheel]='scrollWheel' [deviceMarkers]='deviceMarkers' [reportMarkers]='reportMarkers' [deviceMarkersInfoWindowContentStrings]='deviceMarkersInfoWindowContentStrings' [deleteAllReportMarkers]='reportMarkersToBeDeleted'></app-google-map>\n\n\t\t<div class=\"reports-filter hidden-sm-down\">\n\t\t\t<label for='filterIsNewReport'>\n\t\t\t\t<input type=\"checkbox\" name=\"filterIsNewReport\" id=\"filterIsNewReport\" (click)=\"onClickFilterReportMarkers($event)\" checked>\n\t\t\t\t\n\t\t\t\t<span class=\"btn btn--filter is-new\">Nieuw ({{isNewReportCount}})</span>\n\t\t\t</label>\n\n\t\t\t<label for='filterIsOnHoldReport'>\n\t\t\t\t<input type=\"checkbox\" name=\"filterIsOnHoldReport\" id=\"filterIsOnHoldReport\" (click)=\"onClickFilterReportMarkers($event)\" checked>\n\t\t\t\t\n\t\t\t\t<span class=\"btn btn--filter is-on-hold\">In de wacht ({{isOnHoldReportCount}})</span>\n\t\t\t</label>\n\n\t\t\t<label for='filterIsPendingReport'>\n\t\t\t\t<input type=\"checkbox\" name=\"filterIsPendingReport\" id=\"filterIsPendingReport\" (click)=\"onClickFilterReportMarkers($event)\" checked>\n\t\t\t\t\n\t\t\t\t<span class=\"btn btn--filter is-pending\">Openstaand ({{isPendingReportCount}})</span>\n\t\t\t</label>\n\n\t\t\t<label for='filterIsProcessedPositiveReport'>\n\t\t\t\t<input type=\"checkbox\" name=\"filterIsProcessedPositiveReport\" id=\"filterIsProcessedPositiveReport\" (click)=\"onClickFilterReportMarkers($event)\" checked>\n\t\t\t\t\n\t\t\t\t<span class=\"btn btn--filter is-processed-positive\">Behandeld, positief ({{isProcessedPositiveReportCount}})</span>\n\t\t\t</label>\n\n\t\t\t<label for='filterIsProcessedNeutralReport'>\n\t\t\t\t<input type=\"checkbox\" name=\"filterIsProcessedNeutralReport\" id=\"filterIsProcessedNeutralReport\" (click)=\"onClickFilterReportMarkers($event)\" checked>\n\t\t\t\t\n\t\t\t\t<span class=\"btn btn--filter is-processed-neutral\">Behandeld, neutraal ({{isProcessedNeutralReportCount}})</span>\n\t\t\t</label>\n\n\t\t</div>\n\t</div>\n</div>\n"

/***/ }),

/***/ 780:
/***/ (function(module, exports) {

module.exports = "<div>\n\t<div>\n\t\tID: {{report?.id}}\n\t</div>\n\t<div>\n\t\tDevice ID: {{report?.device_id}}\n\t</div>\t\n\t<div>\n\t\tReport Status ID: {{report?.report_status_id}}\n\t</div>\n\t<div>\n\t\tReporter ID: {{report?.reporter_id}}\n\t</div>\n\t<div>\n\t\tReport Type ID: {{report?.report_type_id}}\n\t</div>\n\t<div>\n\t\tTitle: {{report?.title  | capitalize}}\n\t</div>\n\t<div>\n\t\tBody: {{report?.body}}\n\t</div>\n\t<div>\n\t\tCreated By: {{report?.created_by}}\n\t</div>\n\t<div>\n\t\tModified By: {{report?.modified_by}}\n\t</div>\n\t<div>\n\t\tCreated At: {{report?.created_at | date:'short'}}\n\t</div>\n\t<div>\n\t\tModified At: {{report?.modified_at | date:'short'}}\n\t</div>\n\t<div>\n\t\tDeleted: {{report?.deleted | date:'short'}}\n\t</div>\n\n</div>"

/***/ }),

/***/ 781:
/***/ (function(module, exports) {

module.exports = "<!--<form novalidate>\n\n\t<div>\n\t\t<label>\n\t\t\tTitle:\n\t\t\t<input type=\"text\" [(ngModel)]=\"report.title\" name=\"reportTitle\" required #reportTitle=\"ngModel\">\n\t\t</label>\n\t\t<span *ngIf=\"!reportTitle.valid && (reportTitle.touched || !!reportTitle.id)\">\n\t\t\tPlease enter a report title.\n\t\t</span>\n\t</div>\n\n\t<div>\n\t\t<label>Body: <textarea [(ngModel)]=\"report.body\" name=\"reportBody\"></textarea></label>\n\t</div>\n\n\t<button type=\"button\" (click)=\"saveReportButton(report)\">Save</button>\n\t<button type=\"button\" (click)=\"cancelReportButton(report)\">Cancel</button>\n\t<button type=\"button\" (click)=\"deleteReportButton(report)\" *ngIf=\"!!report.id\">Delete</button>\n\n</form>-->"

/***/ }),

/***/ 782:
/***/ (function(module, exports) {

module.exports = "<div class=\"search-reports\">\n    <input type=\"search\" class=\"form-control\" placeholder=\"Zoek een melding....\" #listFilter (keyup)=\"0\" [(ngModel)]=\"searchInputValue\"  (input)=\"setPaginationLimitOnInput()\"/>\n\n    <i class=\"icon-search\"></i>\n</div>\n\n<section class=\"page__section section--white section--reports latest-reports\">\n    <ul class=\"reports-list\">\n        <li *ngFor='let report of reports | filter:listFilter.value' class=\"report {{report.report_status.class}}\" (click)=\"modalReportButton(report.id)\">\n            <div class=\"report__content\">\n                <span data-field=\"report-vri_place\">  \n                    <span class=\"report__marker\">     \n                        <span class=\"report__id\">{{report.id}}</span>\n                    </span>\n                    {{report.vri_place}}\n                </span>\n                <span data-field=\"report-report_type-description\" class=\"report__description\">{{report.report_type.description}}</span>\n            </div>\n            <span class=\"report__timestamp\"><span>{{report.date | date:\"dd-MM-yy\"}}</span>&nbsp;|&nbsp;<span>{{report.time | date:\"HH:mm\"}}</span></span>\n        </li>\n        <!-- ngFor -->\n        <div *ngIf=\"reports.length==0\">\n            <div class=\"spinner\">\n                <div class=\"bounce1\"></div>\n                <div class=\"bounce2\"></div>\n                <div class=\"bounce3\"></div>\n            </div>\n        </div>\n    </ul>\n</section>"

/***/ }),

/***/ 783:
/***/ (function(module, exports) {

module.exports = "<table class=\"table table-striped\">\n\n\t<thead>\n\t\t<tr>\n\t\t\t<th>ID</th>\n\t\t\t<th>Devide ID</th>\n\t\t\t<th>Report Status ID</th>\n\t\t\t<th>Reporter ID</th>\n\t\t\t<th>Report Type ID</th>\n\t\t\t<th>Title</th>\n\t\t\t<th>Body</th>\n\t\t\t<th>Created By</th>\n\t\t\t<th>Modified By</th>\n\t\t\t<th>Created At</th>\n\t\t\t<th>Modified At</th>\n\t\t\t<th>Deleted</th>\n\t\t</tr>\n\t</thead>\n\n\t<tbody>\n\t\t<tr *ngFor='let report of reports'>\n\t\t\t<td>{{report.id}}</td>\n\t\t\t<td>{{report.device_id}}</td>\n\t\t\t<td>{{report.report_status_id}}</td>\n\t\t\t<td>{{report.reporter_id}}</td>\n\t\t\t<td>{{report.report_type_id}}</td>\n\t\t\t<td>{{report.title | capitalize}}</td>\n\t\t\t<td>{{report.body}}</td>\n\t\t\t<td>{{report.created_by}}</td>\n\t\t\t<td>{{report.modified_by}}</td>\n\t\t\t<td>{{report.created_at}}</td>\n\t\t\t<td>{{report.modified_at}}</td>\n\t\t\t<td>{{report.deleted}}</td>\n\t\t\t<td>\n\t\t\t\t<button type=\"button\" (click)=\"viewReportButton(report.id)\">View</button>\n\t\t\t\t<button type=\"button\" (click)=\"editReportButton(report.id)\">Edit</button>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n\n</table>"

/***/ }),

/***/ 784:
/***/ (function(module, exports) {

module.exports = "<div class=\"preloader-wrapper big active\" *ngIf=\"loading\">\n  <div class=\"spinner-layer spinner-red-only\">\n    <div class=\"circle-clipper left\">\n      <div class=\"circle\"></div>\n    </div><div class=\"gap-patch\">\n    <div class=\"circle\"></div>\n  </div><div class=\"circle-clipper right\">\n    <div class=\"circle\"></div>\n  </div>\n  </div>\n</div>\n<div class=\"center grey-text text-lighten-1\" *ngIf=\"failed\">\n  <i class=\"icon-alert\"></i>\n  <span class=\"text-alert\">Geen resultaat gevonden bij laden.</span>\n</div>"

/***/ }),

/***/ 785:
/***/ (function(module, exports) {

module.exports = "<footer>\n\t<small>&copy; {{app.copyrightYear}}, {{app.creator}}</small> \n</footer>\n"

/***/ }),

/***/ 786:
/***/ (function(module, exports) {

module.exports = "<header>\n\t<h1 class=\"display-4\">{{app.name}} - 003</h1>\n</header>\n\n\n<nav class=\"navbar navbar-toggleable-md navbar-light bg-faded\">\n  <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n  <a class=\"navbar-brand\" href=\"#\">Navbar</a>\n  <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\n    <ul class=\"navbar-nav\">\n      <li class=\"nav-item active\">\n        <a class=\"nav-link\" href=\"#\">Home <span class=\"sr-only\">(current)</span></a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Features</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" href=\"#\">Pricing</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link disabled\" href=\"#\">Disabled</a>\n      </li>\n    </ul>\n  </div>\n</nav>"

/***/ }),

/***/ 787:
/***/ (function(module, exports) {

module.exports = "<ul class=\"pagination\">\n    <li (click)=\"selectPage(1, $event)\" [class.disabled]=\"currentPage == 1\">\n        <a href=\"\" title=\"Ga naar de eerste pagina\">&laquo;</a>\n    </li>\n    <li (click)=\"selectPage(currentPage - 1, $event)\" [class.disabled]=\"currentPage == 1\">\n        <a href=\"\" title=\"Ga naar de vorige pagina\">&lsaquo;</a>\n    </li>\n    <li class=\"disabled\" (click)=\"cancelEvent($event)\" *ngIf=\"(currentPage - range) > 1\">\n        <a href=\"\">...</a>\n    </li>\n    <li *ngFor=\"let page of pages | async\" [class.active]=\"page == currentPage\">\n        <a href=\"\" (click)=\"selectPage(page, $event)\">\n      {{page}}\n    </a>\n    </li>\n    <li class=\"disabled\" (click)=\"cancelEvent($event)\" *ngIf=\"(currentPage + range) < totalPages\">\n        <a href=\"\">...</a>\n    </li>\n    <li (click)=\"selectPage(currentPage + 1, $event)\" [class.disabled]=\"currentPage == totalPages\">\n        <a href=\"\" title=\"Ga naar de volgende pagina\">&rsaquo;</a>\n    </li>\n    <li (click)=\"selectPage(totalPages, $event)\" [class.disabled]=\"currentPage == totalPages\">\n        <a href=\"\" title=\"Ga naar de laatste pagina\">&raquo;</a>\n    </li>\n</ul>\n"

/***/ })

},[1055]);
//# sourceMappingURL=main.bundle.js.map