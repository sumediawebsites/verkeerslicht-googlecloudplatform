$(document).ready(function() {
    initSlick();
    closeAlert();
    desktopSubNavOverlay();
    googleMapRemodalToggle();
    //infinitescrollReports();
    mobilePageTransition();
    mobileToggleNav();
    mobileToggleNavSub();
    smoothScroll();
});
