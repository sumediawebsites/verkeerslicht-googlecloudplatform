function googleMapRemodalToggle() {
    $('.toggle--google-map--use-location').click(function() {
        $('.gllpLatlonPicker--use-location').toggleClass('is-visible');
    });

    $('.toggle--google-map--pin-on-map').click(function() {
        $('.gllpLatlonPicker--pin-on-map').toggleClass('is-visible');
    });
}
