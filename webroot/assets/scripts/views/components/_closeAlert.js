function closeAlert() {
    $('div.alert .close').on('click', function() {
        $("div.alert").addClass('is-fading-out');
    });
}
