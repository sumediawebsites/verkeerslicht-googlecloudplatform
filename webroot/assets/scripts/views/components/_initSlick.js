function initSlick() {
    $('.slick-slider').slick({
        arrows: false,
        dots: true,
        infinite: true,
        speed: 500,
        cssEase: 'linear',
        zIndex: 1,
        autoplay: true,
        autoplaySpeed: 5000
    });
}
