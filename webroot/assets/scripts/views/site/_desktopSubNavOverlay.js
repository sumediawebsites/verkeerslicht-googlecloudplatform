function desktopSubNavOverlay() {
    $('.nav-desktop .has-sub').on({
        'mouseenter' : function() {
            $('main').addClass('overlay-visible');
        },
        'mouseleave':function(){
            $('main').removeClass('overlay-visible');
        }
    });
}
