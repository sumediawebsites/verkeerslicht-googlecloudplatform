function mobileToggleNav() {
    $('.toggle-nav').click(function() {
        $(this).toggleClass('can-close');

        $('.nav-mobile').toggleClass('is-open');

        $('main').toggleClass('overlay-visible');

        $('.remodal-wrapper').toggleClass('make-way-for-nav');
    });

    $(document).on('opening', '.remodal', function () {
        $('.nav-mobile').addClass('big-z-index');

        $('.header').addClass('bg-non-transparent');
    });

    $(document).on('closing', '.remodal', function () {
        $('.nav-mobile').removeClass('big-z-index');

        $('.header').removeClass('bg-non-transparent');
    });
}
