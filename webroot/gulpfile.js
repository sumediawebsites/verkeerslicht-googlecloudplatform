// This Gulp 4 gulpfile was made by Tobias Boekwijt @ Sumedia

'use strict';

// Alphabetized requirements
var autoprefixer    = require('gulp-autoprefixer'),
    browserSync     = require('browser-sync').create(),
    bulkSass        = require('gulp-sass-bulk-import'),
    concat          = require('gulp-concat'),
    cssnano         = require('gulp-cssnano'),
    del             = require('del'),
    eslint          = require('gulp-eslint'),
    flatten         = require('gulp-flatten'),
    gulp            = require('gulp'),
    imagemin        = require('gulp-imagemin'),
    mjml            = require('gulp-mjml'),
    rename          = require('gulp-rename'),
    sass            = require('gulp-sass'),
    sassLint        = require('gulp-sass-lint'),
    sourcemaps      = require('gulp-sourcemaps'),
    tslint          = require('gulp-tslint'),
    typescript      = require('gulp-tsc'),
    uglify          = require('gulp-uglify');

// Are you using TypeScript in your project?
var useTypeScript = false;

// Paths
var CLIENT      = './assets',
    BUILD       = './dist',
    STYLES      = '/styles',
    SCRIPTS     = '/scripts',
    FONTS       = '/fonts',
    IMAGES      = '/images',
    EMAILS      = '/emails';

if (useTypeScript) {
    var TSORJS  = '.ts';
} else {
    var TSORJS  = '.js';
}

var paths = {
    clean:      BUILD   + '/*',
    styles:     [CLIENT + STYLES    + '/**/*.scss', '!' + CLIENT + STYLES + '/vendors/**/*.scss'],
    scripts:    [CLIENT + SCRIPTS   + '/**/*' + TSORJS, '!' + CLIENT + SCRIPTS + '/vendors/**/*' + TSORJS],
    fonts:      CLIENT  + FONTS     + '/**/*',
    images:     CLIENT  + IMAGES    + '/**/*',
    emails:     CLIENT  + EMAILS    + '/**/*.mjml'
};

// Register tasks to expose to the cli
gulp.task('build', gulp.series(
    clean,
    gulp.parallel(
        gulp.series(stylesLint, styles, stylesLms),
        gulp.series(scriptsLint, scripts),
        nonBowerVendorScripts,
        fonts
    )
));
gulp.task(clean);
gulp.task(watch);
gulp.task(watchdev);

// The default task (called when you run 'gulp' from cli)
gulp.task('default', gulp.series('build'));

// Clean the dist folder
function clean() {
    return del(paths.clean);
}

// Rerun the task when a file changes
// Without sourcemaps and no-console set to true
function watch() {
    browserSync.init({
        files: ['../src/Template/**/*.ctp', '*.ctp'],
        proxy: 'verkeerslicht.dev',
        index: "index.php"
    });
    gulp.watch(paths.styles, gulp.series(stylesLint, styles, stylesLms));
    gulp.watch(paths.scripts, gulp.series(scriptsLint, scripts));
    gulp.watch(paths.fonts, fonts);
}

// Rerun the task when a file changes
// With sourcemaps and no-console set to false
function watchdev() {
    browserSync.init({
        files: ['../src/Template/**/*.ctp', '*.ctp'],
        proxy: 'verkeerslicht.dev',
        index: "index.php"
    });
    gulp.watch(paths.styles, gulp.series(stylesLint, stylesDev, stylesLmsDev));
    gulp.watch(paths.scripts, gulp.series(scriptsLint, scriptsDev));
    gulp.watch(paths.fonts, fonts);
}

// Lint all non-vendor Sass
function stylesLint() {
    return gulp.src(paths.styles)
        .pipe(sassLint({
            configFile: 'config-sasslint.yml'
        }))
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
}

// Minify and copy all non-vendor Sass
// Without sourcemaps
function styles() {
    return gulp.src(CLIENT + STYLES + '/main.scss')
        .pipe(bulkSass())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', '> 2%'],
            cascade: false
        }))
        .pipe(cssnano({
            zindex: false
        }))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest(BUILD + STYLES))
        .pipe(browserSync.stream());
}

// Minify and copy all non-vendor Sass
// With sourcemaps
function stylesDev() {
    return gulp.src(CLIENT + STYLES + '/main.scss')
        .pipe(sourcemaps.init())
        .pipe(bulkSass())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', '> 2%'],
            cascade: false
        }))
        .pipe(cssnano({
            zindex: false
        }))
        .pipe(rename('main.min.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(BUILD + STYLES))
        .pipe(browserSync.stream());
}

// Minify and copy all non-vendor LMS Sass
// Without sourcemaps
function stylesLms() {
    return gulp.src(CLIENT + STYLES + '/lms.scss')
        .pipe(bulkSass())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', '> 2%'],
            cascade: false
        }))
        .pipe(cssnano({
            zindex: false
        }))
        .pipe(rename('lms.min.css'))
        .pipe(gulp.dest(BUILD + STYLES))
        .pipe(browserSync.stream());
}

// Minify and copy all non-vendor LMS Sass
// With sourcemaps
function stylesLmsDev() {
    return gulp.src(CLIENT + STYLES + '/lms.scss')
    .pipe(sourcemaps.init())
    .pipe(bulkSass())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['last 2 versions', '> 2%'],
        cascade: false
    }))
    .pipe(cssnano({
        zindex: false
    }))
    .pipe(rename('lms.min.css'))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest(BUILD + STYLES))
    .pipe(browserSync.stream());
}

// Emails
function emails() {
    return gulp.src(paths.emails)
        .pipe(mjml())
        .pipe(gulp.dest(BUILD + EMAILS))
        .pipe(browserSync.stream());
}

// Lint all scripts
// With no-console set to true
function scriptsLint() {
    if (useTypeScript) {
        gulp.src(paths.scripts)
            .pipe(tslint({
                formatter: 'stylish',
                configuration: 'config-tslint.json'
            }))
            .pipe(tslint.report());
    } else {
        return gulp.src(paths.scripts)
            .pipe(eslint({
                configFile: 'config.eslintrc'
            }))
            .pipe(eslint.format())
            .pipe(eslint.failAfterError());
    }
}

// Lint all scripts
// With no-console set to false
function scriptsLintDev() {
    if (useTypeScript) {
        gulp.src(paths.scripts)
            .pipe(tslint({
                formatter: 'stylish',
                configuration: 'config-dev-tslint.json'
            }))
            .pipe(tslint.report());
    } else {
        return gulp.src(paths.scripts)
            .pipe(eslint({
                configFile: 'config-dev.eslintrc'
            }))
            .pipe(eslint.format())
            .pipe(eslint.failAfterError());
    }
}

// TSconfig var
var tsConfig = function(bool) {
    return {
        module: 'amd',
        target: 'ES5',
        sourceMap: bool,
        out: 'main.min.js'
    }
}

// Minify and copy all scripts
// Without sourcemaps
function scripts() {
    if (useTypeScript) {
        return gulp.src(paths.scripts)
            .pipe(typescript(tsConfig(false)))
            .pipe(uglify())
            .pipe(gulp.dest(BUILD + SCRIPTS));
    } else {
        return gulp.src(paths.scripts)
            .pipe(uglify())
            .pipe(concat('main.min.js'))
            .pipe(gulp.dest(BUILD + SCRIPTS))
            .pipe(browserSync.stream());
    }
}

// Minify and copy all scripts
// With sourcemaps all the way down
function scriptsDev() {
    if (useTypeScript) {
        return gulp.src(paths.scripts)
            .pipe(typescript(tsConfig(true)))
            .pipe(gulp.dest(BUILD + SCRIPTS))
            .pipe(browserSync.stream());
    } else {
        return gulp.src(paths.scripts)
            .pipe(sourcemaps.init())
            .pipe(concat('main.min.js'))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(BUILD + SCRIPTS))
            .pipe(browserSync.stream());
    }
}

// Minify and copy all non-bower vendor scripts
function nonBowerVendorScripts() {
    if (useTypeScript) {
        return gulp.src(CLIENT + SCRIPTS + '/vendors/**/*.js')
            .pipe(typescript(tsConfig(true)))
            .pipe(gulp.dest(BUILD + SCRIPTS))
            .pipe(browserSync.stream());
    } else {
        return gulp.src(CLIENT + SCRIPTS + '/vendors/**/*.js')
            .pipe(uglify())
            .pipe(concat('vendors-non-bower.min.js'))
            .pipe(gulp.dest(BUILD + SCRIPTS))
            .pipe(browserSync.stream());
    }
}

// Flatten and copy all fonts
function fonts() {
    return gulp.src(paths.fonts)
        .pipe(flatten())
        .pipe(gulp.dest(BUILD + FONTS))
        .pipe(browserSync.stream());
}

// Copy all static images
function images() {
    return gulp.src(paths.images)
        .pipe(imagemin({
            optimizationLevel: 5
        }))
        .pipe(gulp.dest(BUILD + IMAGES))
        .pipe(browserSync.stream());
}
